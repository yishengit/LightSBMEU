package test.light.utils;

import java.util.Arrays;
import java.util.TreeSet;

import org.junit.Test;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.ValidateInfo;
import org.light.exception.ValidateException;
import org.light.utils.DeniableUtils;
import org.light.verb.Delete;

public class DeniablesTest {
	//@Test
	public void testGenMax() {
		System.out.println("==========testGenMax=========");
		try {
			TreeSet<String> results = DeniableUtils.parseVerbDenyString("User","max");
			for (String str : results) {
				System.out.println(str +",");
			}
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGenToggle() {
		System.out.println("==========TestGenToggle=========");
		try {
			TreeSet<String> results = DeniableUtils.parseVerbDenyString("User","Toggle");
			for (String str : results) {
				System.out.println(str +",");
			}
		}catch (ValidateException ve)
		{	
			ValidateInfo info = ve.getValidateInfo();
			for (String str : info.getCompileErrors()) {
				System.out.println(str +",");
			}			
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGenToggleAndToggleOne() {
		System.out.println("==========testGenToggleAndToggleOne=========");
		try {
			TreeSet<String> results = DeniableUtils.parseVerbDenyString("User","Toggle,ToggleOne");
			for (String str : results) {
				System.out.println(str +",");
			}
		}catch (ValidateException ve)
		{	
			ValidateInfo info = ve.getValidateInfo();
			for (String str : info.getCompileErrors()) {
				System.out.println(str +",");
			}			
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGenNone() {
		System.out.println("==========testGenNone=========");
		try {
			TreeSet<String> results = DeniableUtils.parseVerbDenyString("User","none");
			for (String str : results) {
				System.out.println(str +",");
			}
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGenAdd() {
		System.out.println("==========testGenAdd=========");
		try {
			TreeSet<String> results = DeniableUtils.parseVerbDenyString("User","Add");
			for (String str : results) {
				System.out.println(str +",");
			}
		}catch (ValidateException ve)
		{	
			ValidateInfo info = ve.getValidateInfo();
			for (String str : info.getCompileErrors()) {
				System.out.println(str +",");
			}			
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGenDenyFindById() {
		System.out.println("==========testGenDenyFindById=========");
		try {
			TreeSet<String> results = DeniableUtils.parseVerbDenyString("User","FindById");
			for (String str : results) {
				System.out.println(str +",");
			}
		}catch (ValidateException ve)
		{	
			ValidateInfo info = ve.getValidateInfo();
			for (String str : info.getCompileErrors()) {
				System.out.println(str +",");
			}			
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGenDenyMaxDenyIdSuccess() {
		System.out.println("==========testGenDenyMaxDenyIdSuccess=========");
		try {
			TreeSet<String> allverbs = new TreeSet<String>();
			allverbs.addAll(Arrays.asList(DeniableUtils.getOneDomainVerbs()));
			TreeSet<String> results = DeniableUtils.parseVerbFieldDenyString("User","max","domianid");
			for (String str : results) {
				System.out.println(str +",");
			}
		}catch (ValidateException ve)
		{	
			ValidateInfo info = ve.getValidateInfo();
			for (String str : info.getCompileErrors()) {
				System.out.println(str +",");
			}			
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGenDenyMaxAdd() {
		System.out.println("==========testGenDenyMaxAdd=========");
		try {
			TreeSet<String> results = DeniableUtils.parseVerbDenyString("User","max+,add");
			for (String str : results) {
				System.out.println(str +",");
			}
		}catch (ValidateException ve)
		{	
			ValidateInfo info = ve.getValidateInfo();
			for (String str : info.getCompileErrors()) {
				System.out.println(str +",");
			}			
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGenDenyMaxAdd2() {
		System.out.println("==========testGenDenyMaxAdd2=========");
		try {
			TreeSet<String> results = DeniableUtils.parseVerbDenyString("User","max+,add,Clone,CloneAll");
			for (String str : results) {
				System.out.println(str +",");
			}
		}catch (ValidateException ve)
		{	
			ValidateInfo info = ve.getValidateInfo();
			for (String str : info.getCompileErrors()) {
				System.out.println(str +",");
			}			
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGenDenyNoneDenyNoneFieldIdReadOnlySuccess() {
		System.out.println("==========testGenDenyNoneDenyNoneFieldIdReadOnlySuccess=========");
		try {
			TreeSet<String> results = DeniableUtils.parseVerbFieldDenyFieldReadonlyString("User","none","","domainid");
			for (String str : results) {
				System.out.println(str +",");
			}
		}catch (ValidateException ve)
		{	
			ValidateInfo info = ve.getValidateInfo();
			for (String str : info.getCompileErrors()) {
				System.out.println(str +",");
			}			
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGenDenyFieldsAndVerbs() throws Exception{
		Domain d = new Domain();
		d.setStandardName("LeaveType");
		Field f1 = new Field();
		//d.setDomainId(f1);
		d.setDomainName(f1);
		d.setActive(f1);
		d.setVerbDeniesStr("max+,Delete,DeleteAll");
		System.out.println(d.getDeniedVerbs());
		Delete dl = new Delete(d);
		System.out.println(dl.isDenied());
	}
	
	//@Test
	public void testGenDenyFieldsAndVerbs2() throws Exception{
		Domain d = new Domain();
		d.setStandardName("LeaveType");
		Field f1 = new Field();
		d.setDomainId(f1);
		//d.setDomainName(f1);
		d.setActive(f1);
		d.setVerbDeniesStr("Delete,DeleteAll");
		System.out.println(d.getDeniedVerbs());
		Delete dl = new Delete(d);
		System.out.println(dl.isDenied());
	}
	
	//@Test
	public void testGenDenyFieldsAndVerbs3() throws Exception{
		Domain d = new Domain();
		d.setStandardName("LeaveType");
		Field f1 = new Field();
		d.setDomainId(f1);
		//d.setDomainName(f1);
		//d.setActive(f1);
		d.setVerbDeniesStr("max+,Delete,DeleteAll");
		System.out.println(d.getDeniedVerbs());
		Delete dl = new Delete(d);
		System.out.println(dl.isDenied());
	}
	
	//@Test
	public void testGenMaxPlus() {
		System.out.println("==========testGenMaxPlus=========");
		try {
			TreeSet<String> results = DeniableUtils.parseVerbDenyString("User","max+,Add,Update");
			for (String str : results) {
				System.out.println(str +",");
			}
		} catch (Exception e)
		{	
			e.printStackTrace();
		}
	}
}