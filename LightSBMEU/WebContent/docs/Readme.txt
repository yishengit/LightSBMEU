本文档是第三代动词算子式代码生成器：光SBMEU版所有版本的Readme合集

=========第三代动词算子式代码生成器：光的介绍目标和景愿===========
光是无垠式代码生成器的第二代，轻量级定制版的代码生成器。
项目地址：https://gitee.com/jerryshensjf/LightSBMEU
他有如下的特点:
1)轻量
2)一致性好
3)只支持Excel模板代码生成
4)彻底重构

========第三代动词算子式代码生成器：光SBMEU版1.0.0版 (燃烧Burning BNG)============
1)支持SpringBoot
2)支持Maven
3)迁徙至SBMEU技术栈

========PeaceWingSMEU 4.0 (宝船)============
1)新增激活和批激活两个动词
2)新增两个域对象之间的多重多对多关系，以多对多别名支持此特性
3)新增高分辨率UI模式，以支持新的高分辨率的计算机
4)标题,副标题,页脚可以设置
5)Spring和SpringMVC升级至4.2
6)支持跨域
7)附带Vue ElementUI前后端分离样例,配合GenerateSample示例使用
8)新增DualLangBBS示c例
9)更新Excel代码生成对LiberOffice的支持,现在的xls格式的源文件支持MS Excel,WPS和LiberOffice
10)代码生成器自身的JDK编译兼容性升级至8.0

========PeaceWingSMEU 3.2(乌篷船)============
1)多对多初始化数据导入
2)组件扫描机制
3)在线文档：代码生成器技术乱弹
4)Excel模板代码生成错误修正
5)界面错误修正
6)其它错误修正
7)文档更新

========PeaceWingSMEU 3.1(乌篷船)============
1)SGS语法加亮代码编辑器
2)SGS支持初始化数据导入(DataDomain)
2)错误修正

========PeaceWingSMEU 3.0(乌篷船)============
1)研发代号(Black Awning Boat 乌篷船）
2)支持使用Excel模板生成代码
3)支持Excel模板的初始化数据
4)新增一系列Excel模板样例
5)SGS模板的除错更新和完善

========PeaceWingSMEU 2.0(红头船)============
1)支持Oracle数据库，开发环境为Oracle 11g
2)支持dbtype关键字有mysql和oracle两种选项
3)文档更新
4)例程更新
5)研发代号(Chinese Sailboat 红头船）

==============PeaceWingSMEU 1.6=============
1)支持下拉列表(外键)

==============PeaceWingSMEU 1.5=============
1)支持多对多：功能,界面和数据库支持。
2)新增四个双域动词Assign,Revoke,ListMyActive和ListMyAvailableActive
3)更新了文档，新增理论文档
4)特性修改与功能增强
5)校验增强
6)例程有修改增强，老用户请使用生成的数据库脚本重建数据库

==============PeaceWingSMEU 1.2=============
1)全面支持UTF-8编码
2)修复了主查询对Boolean类型支持的Bug
3)GenerateSample示例全面支持中文字段标签
4)所有考勤类例程有少量字段重命名，如老用户需注意需要使用生成的数据库脚本重建数据库。

==============PeaceWingSMEU 1.1=============
1)全面支持字段中文标签，请参加附带的One示例
2)全面更新了生成器所附的理论文档
3)修复和DomainId格式主键有关的Bug
4)修复SoftDeleteAll动词的Bug

==============PeaceWingSMEU 1.0=============
1)技术栈改为SMEU：SpringMVC 4, Spring 4, MyBatis 3,测试界面使用
JQuery Easy UI界面
2)新增三个动词：Toggle, ToggleOne和SearchByFieldsByPage
3)为Domain,Verb和Field新增Label属性以支持中文
4)页面主查询改为SearchByFieldsByPage
5)系统采用类类型，不再使用原始类型，以支持空值查询
6)更新了文档
7)新增新的神秘礼物

==============PeaceWingSM 0.7.12=============
1)将生成器改进到无垠式代码生成器0.8,即和平之翼SHC 0.7.11的水平
2)增加ActiveField名为deleted或delete自动反义功能
3)增加对DomainID为int类型的支持
4)修改了数据库生成器
5)修复Boolean转换字符串“false”被视为true的Bug

==============PeaceWingSM 0.7.9==============
1)完善了文档

==============PeaceWingSM 0.7.8==============
1)Bug修复

==============PeaceWingSM 0.7.7==============
1)Bug修复

==============PeaceWingSM 0.7.6==============
1)完善了文档

==============PeaceWingSM 0.7.5==============
1)完成了SM技术栈(SpringMVC 4, Spring 4, Mybatis 3)

===============PeaceWing 0.7=================
1)完成了输出和Eclipse JEE版相兼容的完整源码包
2)修复测试见面的一些缺陷

===============PeaceWing 0.6=================
1)完成PeaceWing的骨架。
2)完成了S2SM技术栈(Struts 2, Spring 4, Mybatis 3)
3)完成了SGS源码在生成物中的保存
4)完成了SGS1.2,新增了call magic语句
5)完成了HTML5的测试界面






