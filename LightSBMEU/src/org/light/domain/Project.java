package org.light.domain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;
import org.light.core.ApplicationContextXml;
import org.light.core.DotClassPathXml;
import org.light.core.DotProjectXml;
import org.light.core.LayoutComb;
import org.light.core.Log4jProperties;
import org.light.core.POMXml;
import org.light.core.ReportComb;
import org.light.core.SettingEclipseJdtPrefs;
import org.light.core.SettingEclipsePrefs;
import org.light.core.SettingEclipseSuperTypeContainer;
import org.light.core.SettingEclipseSuperTypeName;
import org.light.core.SettingEclipseWstFacetXml;
import org.light.core.SettingJsdtScopeXml;
import org.light.core.SettingWstComponentXml;
import org.light.core.SpringMVCController;
import org.light.core.SpringMVCXml;
import org.light.core.StrutsConfigXml;
import org.light.core.WebXml;
import org.light.easyuilayouts.widgets.Nav;
import org.light.exception.ValidateException;
import org.light.generator.ApplicationDevPropertiesGenerator;
import org.light.generator.ApplicationJavaGenerator;
import org.light.generator.ApplicationProdPropertiesGenerator;
import org.light.generator.ApplicationPropertiesGenerator;
import org.light.generator.DBDefinitionGenerator;
import org.light.generator.IndexControllerJavaGenerator;
import org.light.generator.MSExcelUtilGenerator;
import org.light.generator.PDFUtilGenerator;
import org.light.generator.POIExcelUtilGenerator;
import org.light.generator.StringUtilGenerator;
import org.light.generator.TwoDomainsDBDefinitionGenerator;
import org.light.generator.WordUtilGenerator;
import org.light.include.Footer;
import org.light.include.Header;
import org.light.include.JsonUserNav;
import org.light.include.JumpHomePage;
import org.light.include.Updates;
import org.light.layouts.EasyUIHomePagePI;
import org.light.layouts.EasyUIMtmPI;
import org.light.oracle.core.OracleManyToMany;
import org.light.oracle.core.OraclePrism;
import org.light.oracle.generator.Oracle11gSqlReflector;
import org.light.oracle.generator.OracleTwoDomainsDBDefinitionGenerator;
import org.light.shiroauth.ShiroAuthModule;
import org.light.shiroauth.SpringShiroXml;
import org.light.utils.PgsqlReflector;
import org.light.utils.SqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.ZipCompressor;
import org.light.wizard.ExcelWizard;

import net.sf.json.JSONObject;

public class Project implements Serializable {
	private static final long serialVersionUID = 5684368331555485772L;
	protected static Logger logger = Logger.getLogger(Project.class);
	protected long projectId;
	protected String standardName;
	protected String packageToken;
	protected String technicalstack;
	protected long namingId;
	protected Naming naming;
	protected String folderPath = "D:/JerryWork/Infinity/testFiles/";
	protected String sourceFolderPath = "D:/JerryLunaWorkspace/InfinityGPGenerator/WebContent/templates/";
	protected List<Prism> prisms = new ArrayList<Prism>();
	protected List<Util> utils = new ArrayList<Util>();
	protected Set<Independent> independents = new TreeSet<Independent>();
	protected List<Include> includes = new ArrayList<Include>();
	protected List<Include> jsonIncludes = new ArrayList<Include>();
	protected List<DBDefinitionGenerator> dbDefinitionGenerators = new ArrayList<DBDefinitionGenerator>();
	protected String dbName;
	protected TestSuite projectTestSuite = new TestSuite();
	protected List<ConfigFile> configFiles = new ArrayList<ConfigFile>();
	protected Set<IndependentConfig> independentConfigs = new TreeSet<IndependentConfig>();
	protected List<Domain> domains = new ArrayList<Domain>();
	protected List<String> domainNames = new ArrayList<>();
	protected String dbPrefix = "";
	protected String dbUsername = "root";
	protected String dbPassword = "";
	protected String dbType = "mariadb";
	protected boolean emptypassword = false;
	protected EasyUIHomePagePI homepage;
	protected JumpHomePage jumphomepage = new JumpHomePage();
	protected String sgsSource;
	protected String sqlSource;
	protected String label;
	protected List<TwoDomainsDBDefinitionGenerator> myTwoDBGenerators = new ArrayList<TwoDomainsDBDefinitionGenerator>();
	protected List<List<Domain>> dataDomains = new ArrayList<>();
	protected String excelTemplateName = "";
	protected String excelTemplateFolder = "";
	protected String title = "";
	protected String subTitle = "";
	protected String footer = "";
	protected String crossOrigin = "";
	protected String resolution = "high";
	protected String domainSuffix = "domain";
	protected String daoSuffix = "dao";
	protected String daoimplSuffix = "daoimpl";
	protected String serviceSuffix = "service";
	protected String serviceimplSuffix = "serviceimpl";
	protected String controllerSuffix = "controller";
	protected String domainNamingSuffix = "";
	protected String controllerNamingSuffix = "Controller";
	protected String language = "chinese";
	protected String schema = "normal";
	protected String frontBaseApi = "";
	protected Set<org.light.core.Module> modules = new TreeSet<>();
	protected Set<LayoutComb> layoutCombs = new TreeSet<>();
	protected Set<ReportComb> reportCombs = new TreeSet<>();
	protected Nav nav;

	public String getResolutmoion() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getSgsSource() {
		return sgsSource;
	}

	public void setSgsSource(String sgsSource) {
		this.sgsSource = sgsSource;
	}

	public String getSqlSource() {
		return sqlSource;
	}

	public void setSqlSource(String sqlSource) {
		this.sqlSource = sqlSource;
	}

	public boolean isEmptypassword() {
		return emptypassword;
	}

	public void setEmptypassword(boolean emptypassword) {
		this.emptypassword = emptypassword;
	}

	public Project() {
		super();
	}

	public List<Include> getJsonIncludes() {
		return jsonIncludes;
	}

	public void setJsonIncludes(List<Include> jsonIncludes) {
		this.jsonIncludes = jsonIncludes;
	}

	public void decorateConfigFiles() throws ValidateException {
		if (this.technicalstack == null || this.technicalstack.equals("") || this.technicalstack.equalsIgnoreCase("sbmeu")) {
			if (dbType.equalsIgnoreCase("mysql") || dbType.equalsIgnoreCase("mariadb")
					|| dbType.equalsIgnoreCase("postgresql") || dbType.equalsIgnoreCase("pgsql")) {
				WebXml webxml = new WebXml();
				webxml.setPackageToken(packageToken);
				webxml.setProjectName(this.getStandardName());
				webxml.setStandardName("web.xml");
				webxml.setModuleNames(this.getModuleNames());
				addConfigFile(webxml);

				POMXml pomxml = new POMXml(dbType);
				pomxml.setProjectName(this.getStandardName());
				pomxml.setFolder("");
				pomxml.setModuleNames(this.getModuleNames());
				addIndependentConfig(pomxml);

				ApplicationJavaGenerator appj = new ApplicationJavaGenerator();
				appj.setPackageToken(packageToken);
				addIndependent(appj);

				IndexControllerJavaGenerator idc = new IndexControllerJavaGenerator();
				idc.setPackageToken(packageToken + "." + this.getControllerSuffix());
				addIndependent(idc);

				ApplicationProdPropertiesGenerator apppg = new ApplicationProdPropertiesGenerator();
				apppg.setProjectName(this.getStandardName());
				addIndependentConfig(apppg);

				ApplicationDevPropertiesGenerator appdg = new ApplicationDevPropertiesGenerator();
				appdg.setProjectName(this.getStandardName());
				addIndependentConfig(appdg);

				ApplicationPropertiesGenerator appg = new ApplicationPropertiesGenerator(this.domainSuffix);
				appg.setDbType(dbType);
				appg.setDbName(this.getDbName());
				appg.setDbUserName(this.dbUsername);
				appg.setDbUserPassword(this.dbPassword);
				appg.setPackageToken(packageToken);
				addIndependentConfig(appg);

				List<SpringMVCController> myControllers = new ArrayList<SpringMVCController>();
				for (Prism p : this.prisms) {
					myControllers.add(p.getController());
				}

				ApplicationContextXml axml = new ApplicationContextXml(packageToken, this.dbName, dbUsername,
						dbPassword);
				if (this.isEmptypassword())	axml.setDbPassword("");
				axml.setDomainList(this.domains);
				axml.setTemplateDomain(this.domains.get(0));
				TreeSet<SpringMVCController> myFSet = new TreeSet<SpringMVCController>();
				myFSet.addAll(myControllers);
				axml.setControllers(myFSet);
				List<String> packageToScanList = new ArrayList<String>();
				packageToScanList.add(this.packageToken + ".domain");
				axml.setPackagesToScanList(packageToScanList);
				axml.setPutInsideSrcAndClasses(true);
				replaceConfigFile(axml);
				
				if (this.getModuleNames().contains("ShiroAuthModule"))
				{					
					for (org.light.core.Module m:modules) {
						if ("ShiroAuthModule".equalsIgnoreCase(m.getStandardName())) {
							SpringShiroXml ssxml = new SpringShiroXml();	
							ssxml.setUserDomain(((ShiroAuthModule)m).getUserDomain());
							ssxml.setPrivilegeDomain(((ShiroAuthModule)m).getPrivilegeDomain());
							ssxml.setRoleDomain(((ShiroAuthModule)m).getRoleDomain());
							ssxml.setNav(this.nav);
							replaceConfigFile(ssxml);
						}
					}
				}
				
				SpringMVCXml smxml = new SpringMVCXml();
				smxml.setPackageToken(packageToken);
				addConfigFile(smxml);

				Log4jProperties log4j = new Log4jProperties();
				log4j.setPutInsideSrcAndClasses(true);
				addConfigFile(log4j);

				DotProjectXml dpxml = new DotProjectXml();
				dpxml.setProjectName(this.getStandardName());
				addConfigFile(dpxml);

				DotClassPathXml dcpxml = new DotClassPathXml();
				addConfigFile(dcpxml);

				SettingJsdtScopeXml sscopexml = new SettingJsdtScopeXml();
				addConfigFile(sscopexml);

				SettingEclipseJdtPrefs sejPrefs = new SettingEclipseJdtPrefs();
				addConfigFile(sejPrefs);

				SettingEclipsePrefs sePrefs = new SettingEclipsePrefs();
				addConfigFile(sePrefs);

				SettingEclipseSuperTypeContainer sContainer = new SettingEclipseSuperTypeContainer();
				addConfigFile(sContainer);

				SettingEclipseSuperTypeName sSuperTypeName = new SettingEclipseSuperTypeName();
				addConfigFile(sSuperTypeName);

				SettingEclipseWstFacetXml sFacetXml = new SettingEclipseWstFacetXml();
				addConfigFile(sFacetXml);

				SettingWstComponentXml sComponentXml = new SettingWstComponentXml();
				sComponentXml.setProjectName(this.getStandardName());
				addConfigFile(sComponentXml);

				addJsonInclude(new Header());
				addJsonInclude(new Footer());
				addJsonInclude(new Updates());
				addJsonInclude(new JsonUserNav(this.domains));

				addUtil(new BooleanUtil(packageToken));
				addUtil(new StringUtilGenerator(packageToken));
				addUtil(new POIExcelUtilGenerator(packageToken));
				addUtil(new PDFUtilGenerator(packageToken));
				addUtil(new WordUtilGenerator(packageToken));
				addUtil(new MSExcelUtilGenerator(packageToken));
			} else if (dbType.equalsIgnoreCase("oracle")) {
				WebXml webxml = new WebXml();
				webxml.setPackageToken(packageToken);
				webxml.setProjectName(this.getStandardName());
				webxml.setStandardName("web.xml");
				webxml.setModuleNames(this.getModuleNames());
				addConfigFile(webxml);

				POMXml pomxml = new POMXml(dbType);
				pomxml.setProjectName(this.getStandardName());
				pomxml.setFolder("");
				pomxml.setModuleNames(this.getModuleNames());
				addIndependentConfig(pomxml);

				ApplicationJavaGenerator appj = new ApplicationJavaGenerator();
				appj.setPackageToken(packageToken);
				addIndependent(appj);

				IndexControllerJavaGenerator idc = new IndexControllerJavaGenerator();
				idc.setPackageToken(packageToken + "." + this.getControllerSuffix());
				addIndependent(idc);

				ApplicationProdPropertiesGenerator apppg = new ApplicationProdPropertiesGenerator();
				apppg.setProjectName(this.getStandardName());
				addIndependentConfig(apppg);

				ApplicationDevPropertiesGenerator appdg = new ApplicationDevPropertiesGenerator();
				appdg.setProjectName(this.getStandardName());
				addIndependentConfig(appdg);

				ApplicationPropertiesGenerator appg = new ApplicationPropertiesGenerator(this.domainSuffix);
				appg.setDbType(dbType);
				appg.setDbName(this.getDbName());
				appg.setDbUserName(this.dbUsername);
				appg.setDbUserPassword(this.dbPassword);
				appg.setPackageToken(packageToken);
				addIndependentConfig(appg);

				List<SpringMVCController> myControllers = new ArrayList<SpringMVCController>();
				for (Prism p : this.prisms) {
					myControllers.add(p.getController());
				}

				ApplicationContextXml axml = new ApplicationContextXml(packageToken, this.dbName, dbUsername,
						dbPassword, dbType);
				if (this.isEmptypassword())
					axml.setDbPassword("");
				axml.setDomainList(this.domains);
				TreeSet<SpringMVCController> myFSet = new TreeSet<SpringMVCController>();
				myFSet.addAll(myControllers);
				axml.setControllers(myFSet);
				List<String> packageToScanList = new ArrayList<String>();
				packageToScanList.add(this.packageToken + ".entity");
				axml.setPackagesToScanList(packageToScanList);
				axml.setPutInsideSrcAndClasses(true);
				replaceConfigFile(axml);
				
				if (this.getModuleNames().contains("ShiroAuthModule"))
				{					
					for (org.light.core.Module m:modules) {
						if ("ShiroAuthModule".equalsIgnoreCase(m.getStandardName())) {
							SpringShiroXml ssxml = new SpringShiroXml();	
							ssxml.setUserDomain(((ShiroAuthModule)m).getUserDomain());
							ssxml.setPrivilegeDomain(((ShiroAuthModule)m).getPrivilegeDomain());
							ssxml.setRoleDomain(((ShiroAuthModule)m).getRoleDomain());
							ssxml.setNav(this.nav);
							replaceConfigFile(ssxml);
						}
					}
				}

				SpringMVCXml smxml = new SpringMVCXml();
				smxml.setPackageToken(packageToken);
				addConfigFile(smxml);

				Log4jProperties log4j = new Log4jProperties();
				log4j.setPutInsideSrcAndClasses(true);
				addConfigFile(log4j);

				DotProjectXml dpxml = new DotProjectXml();
				dpxml.setProjectName(this.getStandardName());
				addConfigFile(dpxml);

				DotClassPathXml dcpxml = new DotClassPathXml();
				addConfigFile(dcpxml);

				SettingJsdtScopeXml sscopexml = new SettingJsdtScopeXml();
				addConfigFile(sscopexml);

				SettingEclipseJdtPrefs sejPrefs = new SettingEclipseJdtPrefs();
				addConfigFile(sejPrefs);

				SettingEclipsePrefs sePrefs = new SettingEclipsePrefs();
				addConfigFile(sePrefs);

				SettingEclipseSuperTypeContainer sContainer = new SettingEclipseSuperTypeContainer();
				addConfigFile(sContainer);

				SettingEclipseSuperTypeName sSuperTypeName = new SettingEclipseSuperTypeName();
				addConfigFile(sSuperTypeName);

				SettingEclipseWstFacetXml sFacetXml = new SettingEclipseWstFacetXml();
				addConfigFile(sFacetXml);

				SettingWstComponentXml sComponentXml = new SettingWstComponentXml();
				sComponentXml.setProjectName(this.getStandardName());
				addConfigFile(sComponentXml);

				addJsonInclude(new Header());
				addJsonInclude(new Footer());
				addJsonInclude(new Updates());
				addJsonInclude(new JsonUserNav(this.domains));

				addUtil(new BooleanUtil(packageToken));
				addUtil(new StringUtilGenerator(packageToken));
				addUtil(new POIExcelUtilGenerator(packageToken));
				addUtil(new PDFUtilGenerator(packageToken));
				addUtil(new WordUtilGenerator(packageToken));
				addUtil(new MSExcelUtilGenerator(packageToken));
			} else {
				throw new ValidateException("项目所用的数据库未被支持。");
			}
		} else if (this.technicalstack.equalsIgnoreCase("msmeu") || this.technicalstack.equalsIgnoreCase("smeu")) {
			if (dbType.equalsIgnoreCase("mysql") || dbType.equalsIgnoreCase("mariadb")
					|| dbType.equalsIgnoreCase("postgresql") || dbType.equalsIgnoreCase("pgsql")) {
				WebXml webxml = new WebXml();
				webxml.setPackageToken(packageToken);
				webxml.setControllerPackage(this.getControllerSuffix());
				webxml.setProjectName(this.getStandardName());
				webxml.setStandardName("web.xml");
				webxml.setModuleNames(this.getModuleNames());
				addConfigFile(webxml);

				if ("msmeu".equalsIgnoreCase(this.getTechnicalstack())) {
					org.light.msmeu.POMXml pomxml = new org.light.msmeu.POMXml(dbType);
					pomxml.setProjectName(this.standardName);
					pomxml.setFolder("");
					pomxml.setModuleNames(this.getModuleNames());
					addIndependentConfig(pomxml);
				}

				List<SpringMVCController> myFacades = new ArrayList<SpringMVCController>();
				for (Prism p : this.prisms) {
					myFacades.add(p.getController());
				}

				ApplicationContextXml axml = new ApplicationContextXml(packageToken, this.dbName, dbUsername,
						dbPassword);
				if (this.isEmptypassword())	axml.setDbPassword("");
				axml.setDomainList(this.domains);
				TreeSet<SpringMVCController> myFSet = new TreeSet<SpringMVCController>();
				myFSet.addAll(myFacades);
				axml.setControllers(myFSet);
				List<String> packageToScanList = new ArrayList<String>();
				packageToScanList.add(this.packageToken + "." + domainSuffix);
				axml.setPackagesToScanList(packageToScanList);
				axml.setPutInsideSrcAndClasses(true);
				replaceConfigFile(axml);
				
				if (this.getModuleNames().contains("ShiroAuthModule"))
				{					
					for (org.light.core.Module m:modules) {
						if ("ShiroAuthModule".equalsIgnoreCase(m.getStandardName())) {
							SpringShiroXml ssxml = new SpringShiroXml();	
							ssxml.setUserDomain(((ShiroAuthModule)m).getUserDomain());
							ssxml.setPrivilegeDomain(((ShiroAuthModule)m).getPrivilegeDomain());
							ssxml.setRoleDomain(((ShiroAuthModule)m).getRoleDomain());
							ssxml.setNav(this.nav);
							replaceConfigFile(ssxml);
						}
					}
				}

				SpringMVCXml smxml = new SpringMVCXml();
				smxml.setPackageToken(packageToken);
				smxml.setControllerSuffix(controllerSuffix);
				addConfigFile(smxml);

				Log4jProperties log4j = new Log4jProperties();
				log4j.setPutInsideSrcAndClasses(true);
				addConfigFile(log4j);

				if (!"msmeu".equalsIgnoreCase(this.getTechnicalstack())) {
					DotProjectXml dpxml = new DotProjectXml();
					dpxml.setProjectName(this.standardName);
					addConfigFile(dpxml);

					DotClassPathXml dcpxml = new DotClassPathXml();
					addConfigFile(dcpxml);
				} else {
					org.light.msmeu.DotProjectXml dpxml = new org.light.msmeu.DotProjectXml();
					dpxml.setProjectName(this.standardName);
					addConfigFile(dpxml);

					org.light.msmeu.DotClassPathXml dcpxml = new org.light.msmeu.DotClassPathXml();
					addConfigFile(dcpxml);
				}

				SettingJsdtScopeXml sscopexml = new SettingJsdtScopeXml();
				addConfigFile(sscopexml);

				SettingEclipseJdtPrefs sejPrefs = new SettingEclipseJdtPrefs();
				addConfigFile(sejPrefs);

				SettingEclipsePrefs sePrefs = new SettingEclipsePrefs();
				addConfigFile(sePrefs);

				SettingEclipseSuperTypeContainer sContainer = new SettingEclipseSuperTypeContainer();
				addConfigFile(sContainer);

				SettingEclipseSuperTypeName sSuperTypeName = new SettingEclipseSuperTypeName();
				addConfigFile(sSuperTypeName);

				SettingEclipseWstFacetXml sFacetXml = new SettingEclipseWstFacetXml();
				addConfigFile(sFacetXml);

				SettingWstComponentXml sComponentXml = new SettingWstComponentXml();
				sComponentXml.setProjectName(this.standardName);
				addConfigFile(sComponentXml);

				addJsonInclude(new Header());
				addJsonInclude(new Footer());
				addJsonInclude(new Updates());
				addJsonInclude(new JsonUserNav(this.domains));

				addUtil(new BooleanUtil(packageToken));
				addUtil(new StringUtilGenerator(packageToken));
				addUtil(new POIExcelUtilGenerator(packageToken));
				addUtil(new PDFUtilGenerator(packageToken));
				addUtil(new WordUtilGenerator(packageToken));
				addUtil(new MSExcelUtilGenerator(packageToken));
			} else if (dbType.equalsIgnoreCase("oracle")) {
				WebXml webxml = new WebXml();
				webxml.setPackageToken(packageToken);
				webxml.setControllerPackage(this.getControllerSuffix());
				webxml.setProjectName(this.getStandardName());
				webxml.setStandardName("web.xml");
				webxml.setModuleNames(this.getModuleNames());
				addConfigFile(webxml);

				if ("msmeu".equalsIgnoreCase(this.getTechnicalstack())) {
					org.light.msmeu.POMXml pomxml = new org.light.msmeu.POMXml(dbType);
					pomxml.setProjectName(this.standardName);
					pomxml.setFolder("");
					pomxml.setModuleNames(this.getModuleNames());
					addIndependentConfig(pomxml);
				}

				List<SpringMVCController> myFacades = new ArrayList<SpringMVCController>();
				for (Prism p : this.prisms) {
					myFacades.add(p.getController());
				}

				ApplicationContextXml axml = new ApplicationContextXml(packageToken, this.dbName, dbUsername,
						dbPassword, dbType);
				if (this.isEmptypassword())	axml.setDbPassword("");
				axml.setDomainList(this.domains);
				TreeSet<SpringMVCController> myFSet = new TreeSet<SpringMVCController>();
				myFSet.addAll(myFacades);
				axml.setControllers(myFSet);
				List<String> packageToScanList = new ArrayList<String>();
				packageToScanList.add(this.packageToken + "." + domainSuffix);
				axml.setPackagesToScanList(packageToScanList);
				axml.setPutInsideSrcAndClasses(true);
				replaceConfigFile(axml);
				
				if (this.getModuleNames().contains("ShiroAuthModule"))
				{					
					for (org.light.core.Module m:modules) {
						if ("ShiroAuthModule".equalsIgnoreCase(m.getStandardName())) {
							SpringShiroXml ssxml = new SpringShiroXml();	
							ssxml.setUserDomain(((ShiroAuthModule)m).getUserDomain());
							ssxml.setPrivilegeDomain(((ShiroAuthModule)m).getPrivilegeDomain());
							ssxml.setRoleDomain(((ShiroAuthModule)m).getRoleDomain());
							ssxml.setNav(this.nav);
							replaceConfigFile(ssxml);
						}
					}
				}

				SpringMVCXml smxml = new SpringMVCXml();
				smxml.setPackageToken(packageToken);
				addConfigFile(smxml);

				Log4jProperties log4j = new Log4jProperties();
				log4j.setPutInsideSrcAndClasses(true);
				addConfigFile(log4j);

				if (!"msmeu".equalsIgnoreCase(this.getTechnicalstack())) {
					DotProjectXml dpxml = new DotProjectXml();
					dpxml.setProjectName(this.standardName);
					addConfigFile(dpxml);

					DotClassPathXml dcpxml = new DotClassPathXml();
					addConfigFile(dcpxml);
				} else {
					org.light.msmeu.DotProjectXml dpxml = new org.light.msmeu.DotProjectXml();
					dpxml.setProjectName(this.standardName);
					addConfigFile(dpxml);

					org.light.msmeu.DotClassPathXml dcpxml = new org.light.msmeu.DotClassPathXml();
					addConfigFile(dcpxml);
				}

				SettingJsdtScopeXml sscopexml = new SettingJsdtScopeXml();
				addConfigFile(sscopexml);

				SettingEclipseJdtPrefs sejPrefs = new SettingEclipseJdtPrefs();
				addConfigFile(sejPrefs);

				SettingEclipsePrefs sePrefs = new SettingEclipsePrefs();
				addConfigFile(sePrefs);

				SettingEclipseSuperTypeContainer sContainer = new SettingEclipseSuperTypeContainer();
				addConfigFile(sContainer);

				SettingEclipseSuperTypeName sSuperTypeName = new SettingEclipseSuperTypeName();
				addConfigFile(sSuperTypeName);

				SettingEclipseWstFacetXml sFacetXml = new SettingEclipseWstFacetXml();
				addConfigFile(sFacetXml);

				SettingWstComponentXml sComponentXml = new SettingWstComponentXml();
				sComponentXml.setProjectName(this.standardName);
				addConfigFile(sComponentXml);

				addJsonInclude(new Header());
				addJsonInclude(new Footer());
				addJsonInclude(new Updates());
				addJsonInclude(new JsonUserNav(this.domains));

				addUtil(new BooleanUtil(packageToken));
				addUtil(new StringUtilGenerator(packageToken));
				addUtil(new POIExcelUtilGenerator(packageToken));
				addUtil(new PDFUtilGenerator(packageToken));
				addUtil(new WordUtilGenerator(packageToken));
				addUtil(new MSExcelUtilGenerator(packageToken));
			} else {
				throw new ValidateException("项目所用的数据库未被支持。");
			}
		}
	}

	private void addIndependent(Independent idt) {
		this.independents.add(idt);
	}

	private void addIndependentConfig(IndependentConfig idc) {
		this.independentConfigs.add(idc);
	}

	public void decorateMentuItems() throws ValidateException{
		Set<ManyToMany> manyToManies = new TreeSet<ManyToMany>();
		for (Prism p : this.prisms) {
			manyToManies.addAll(p.getManyToManies());
		}
		Set<MenuItem> menuItems = new TreeSet<MenuItem>();
		if (this.nav!=null) {
			menuItems=nav.getNavMenuItems();
		}else {
			for (Prism p : this.prisms) {
				for (ManyToMany mtm : manyToManies) {
					MenuItem mi = new MenuItem("../pages/" + mtm.getStandardName().toLowerCase() + ".html",
							mtm.getStandardName(), mtm.getText());
					menuItems.add(mi);
				}
			}
		}
		for (ManyToMany mtm : manyToManies) {
			this.myTwoDBGenerators.add(mtm.toTwoDBGenerator());
		}
	}

	public ConfigFile findConfigFile(String standardName) {
		if (this.configFiles != null && this.configFiles.size() > 0) {
			for (ConfigFile c : this.configFiles) {
				if (c.getStandardName().equals(standardName))
					return c;
			}
		}
		return null;
	}

	public List<Util> getUtils() {
		return utils;
	}

	public void setUtils(List<Util> utils) {
		this.utils = utils;
	}

	public void addUtil(Util util) {
		this.utils.add(util);
	}

	public void addInclude(Include include) {
		this.includes.add(include);
	}

	public void addJsonInclude(Include include) {
		this.jsonIncludes.add(include);
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
		// this.homepage.setPageTitile("Welcome to the " +
		// StringUtil.capFirst(standardName)+ " System.");
		WebXml webxml = (WebXml) findConfigFile("web.xml");
		if (webxml != null)
			webxml.setProjectName(standardName);
	}

	public long getNamingId() {
		return namingId;
	}

	public void setNamingId(long namingId) {
		this.namingId = namingId;
	}

	public Naming getNaming() {
		return naming;
	}

	public void setNaming(Naming naming) {
		this.naming = naming;
	}

	public List<Prism> getPrisms() {
		return prisms;
	}

	public void setPrisms(List<Prism> prisms) {
		if (this.technicalstack == null || "".equals(this.technicalstack)
				|| "sbmeu".equalsIgnoreCase(this.technicalstack) || "smeu".equalsIgnoreCase(this.technicalstack)
				|| "msmeu".equalsIgnoreCase(this.technicalstack)) {
			this.prisms = prisms;
			if (this.technicalstack == null || this.technicalstack.equals("")
					|| this.technicalstack.equalsIgnoreCase("sbmeu")) {
				this.prisms = prisms;
			} else if (this.technicalstack.equalsIgnoreCase("smeu") || this.technicalstack.equalsIgnoreCase("msmeu")) {
				this.prisms = prisms;
				StrutsConfigXml sxml = (StrutsConfigXml) this.findConfigFile("struts.xml");
				ApplicationContextXml axml = (ApplicationContextXml) this.findConfigFile("applicationContext.xml");
				axml.setTemplateDomain(this.domains.get(0));
				for (Prism prism : prisms) {
					if (sxml != null && prism != null) {
						// sxml.addAction(prism.getAction());
						sxml.addFacade(prism.getController());
					}

					if (axml != null && prism != null) {
						// axml.addAction(prism.getAction());
						axml.addFacade(prism.getController());
					}

					for (Include in : this.jsonIncludes) {
						in.allDomainList.add(prism.getDomain());
					}
				}
			}
		}
	}

	public void addPrism(Prism prism) {
		if (this.packageToken != null)
			prism.setPackageToken(this.packageToken);
		this.prisms.add(prism);
	}

	public String getPackageToken() {
		return packageToken;
	}

	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
		for (Util u : this.utils) {
			u.setPackageToken(packageToken);
		}
		WebXml webxml = (WebXml) findConfigFile("web.xml");
		if (webxml != null)
			webxml.setPackageToken(packageToken);
	}

	public void generatePgProjectFiles(Boolean ignoreWarning, Boolean genFormatted, Boolean genUi,
			Boolean genController, Boolean genService, Boolean genServiceImpl, Boolean genDao, Boolean genDaoImpl)
			throws Exception {
		if ("postgresql".equalsIgnoreCase(this.dbType) || "pgsql".equalsIgnoreCase(this.dbType)) {
			ValidateInfo info = this.validate(ignoreWarning);
			if (info.success(ignoreWarning) == false) {
				throw new ValidateException(info);
			}
			try {
				decorateMentuItems();
				String srcfolderPath = this.getProjectFolderPath();
				if ("normal".equalsIgnoreCase(this.getSchema())) {
					for (Prism ps : this.prisms) {
						ps.setFolderPath(this.getProjectFolderPath());
						ps.generatePrismFiles(ignoreWarning, genUi, genController, genService, genServiceImpl, genDao,
								genDaoImpl);
						for (ManyToMany mtm:ps.getManyToManies()) {
							EasyUIMtmPI mpage = mtm.getEuPI();
							mpage.setTechnicalStack(this.technicalstack);
							mpage.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
							mpage.setNav(this.nav);
							mpage.generatePIFiles(srcfolderPath);
						}
					}
					
					for (Util u : utils) {
						String utilPath = this.getProjectFolderPath() + "src/main/java/"
								+ packagetokenToFolder(u.getPackageToken()) + "utils/";
						writeToFile(utilPath + u.getFileName(), u.generateUtilString());
					}

					for (Independent idt : this.independents) {
						String idtPath = this.getProjectFolderPath() + "src/main/java/"
								+ packagetokenToFolder(idt.getPackageToken());
						writeToFile(idtPath + idt.getFileName(), idt.generateImplString());
					}
					
					for (org.light.core.Module m : this.modules) {
						m.generateModuleFiles(this.getProjectFolderPath());
					}
					
					for (LayoutComb lcb : this.layoutCombs) {
						lcb.generateCombFiles(this.getProjectFolderPath());
					}
					
					for (ReportComb rcb : this.reportCombs) {
						rcb.generateCombFiles(this.getProjectFolderPath());
					}

					String homePath = this.getProjectFolderPath() + "src/main/resources/static/";
					if (genUi) {
						if (this.containsLoginModule()) {
							this.jumphomepage.setJumpFolder("login");
						}
						writeToFile(homePath + "index.html", this.jumphomepage.generateIncludeString());
						String templateIndexFolder = this.getProjectFolderPath() + "src/main/resources/templates/";
						writeToFile(templateIndexFolder + "index.html", this.jumphomepage.generateIncludeString());
					}
					
					if (genUi) {
						this.homepage.generatePIFiles(this.getProjectFolderPath());
					}
					
					FileCopyer copy = new FileCopyer();

					if (genUi) {
						File cssfrom = new File(this.getSourceFolderPath() + "css/");
						File cssto = new File(this.getProjectFolderPath() + "src/main/resources/static/css/");

						copy.dirFrom = cssfrom;
						copy.dirTo = cssto;
						copy.listFileInDir(cssfrom);

						File jsfrom = new File(this.getSourceFolderPath() + "js/");
						File jsto = new File(this.getProjectFolderPath() + "src/main/resources/static/js/");

						copy.dirFrom = jsfrom;
						copy.dirTo = jsto;
						copy.listFileInDir(jsfrom);

						File easyuifrom = new File(this.getSourceFolderPath() + "easyui/");
						File easyuito = new File(this.getProjectFolderPath() + "src/main/resources/static/easyui/");

						copy.dirFrom = easyuifrom;
						copy.dirTo = easyuito;
						copy.listFileInDir(easyuifrom);

						File uploadjsfrom = new File(this.getSourceFolderPath() + "uploadjs/");
						File uploadjsto = new File(this.getProjectFolderPath() + "src/main/resources/static/uploadjs/");

						copy.dirFrom = uploadjsfrom;
						copy.dirTo = uploadjsto;
						copy.listFileInDir(uploadjsfrom);
						
						File imagefrom = new File(this.getSourceFolderPath() + "images/");
						File imageto = new File(this.getProjectFolderPath() + "src/main/resources/static/images/");

						copy.dirFrom = imagefrom;
						copy.dirTo = imageto;
						copy.listFileInDir(imagefrom);
						
						File echartsfrom = new File(this.getSourceFolderPath() + "echarts/");
						File echartsto = new File(this.getProjectFolderPath() + "src/main/resources/static/echarts/");

						copy.dirFrom = echartsfrom;
						copy.dirTo = echartsto;
						copy.listFileInDir(echartsfrom);
					}

					if (!StringUtil.isBlank(this.getExcelTemplateName())) {
						File mF1 = new File(
								(this.getExcelTemplateFolder() + this.getExcelTemplateName()).replace("\\", "/"));
						File mF2 = new File(
								(this.getProjectFolderPath() + "exceltemplate/" + this.getExcelTemplateName())
										.replace("\\", "/"));
						if (mF1.exists()) {
							if (!mF2.getParentFile().exists()) {
								mF2.getParentFile().mkdirs();
							}
							if (!mF2.exists()) {
								mF2.createNewFile();
							}
							copy.copy(mF1.getPath(), mF2.getPath());
						}
						if (genFormatted) {
							String genExcelFile = "Gen_formatted";
							String[] genExcelFiles = this.getExcelTemplateName().split("\\.");
							if (genExcelFiles != null && genExcelFiles.length > 0)
								genExcelFile = genExcelFiles[0] + "_formatted";
							ExcelWizard.outputExcelWorkBook(this,
									(this.getProjectFolderPath() + "exceltemplate/").replace("\\", "/"),
									genExcelFile + ".xls");
						}
					}

					for (IndependentConfig idc : this.independentConfigs) {
						writeToFile(this.getProjectFolderPath() + idc.getFolder() + idc.getFileName(),
								idc.generateImplString());
					}

					StringBuilder sql = new StringBuilder();
					boolean createNew = true;
					for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
						sql.append(dbd.generateDBSql(createNew)).append("\n");
						if (createNew)
							createNew = false;
					}
					for (TwoDomainsDBDefinitionGenerator mtg : this.myTwoDBGenerators) {
						sql.append(mtg.generateDBSql(this.getDbType()));
					}

					sql.append("\n");
					for (Domain d : getDomains()) {
						sql.append(PgsqlReflector.generateSetSerialVal10000(d)).append("\n");
					}
					sql.append("\n");
					for (List<Domain> dataDomains : this.getDataDomains()) {
						sql.append("\n");
						for (Domain d : dataDomains) {
							sql.append(PgsqlReflector.generateInsertSqlWithValue(d)).append("\n");
						}

						for (Domain d : dataDomains) {
							for (ManyToMany mtm : d.getManyToManies()) {
								sql.append(PgsqlReflector.generateMtmInsertSqlWithValues(mtm)).append("\n");
							}
						}
					}

					writeToFile(this.getProjectFolderPath() + "sql/" + this.getStandardName() + ".sql", sql.toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
	}

	private boolean containsLoginModule() {
		Set<String> moduleNames = this.getModuleNames();
		if (moduleNames.contains("ShiroAuthModule")) return true;
		else return false;
	}

	public void generateMariaDBToolsFiles(boolean genFormatted) throws Exception {
		decorateMentuItems();
		StringBuilder sql = new StringBuilder();
		boolean createNew = true;
		for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
			sql.append(dbd.generateDBSql(createNew)).append("\n");
			if (createNew)
				createNew = false;
		}
		for (TwoDomainsDBDefinitionGenerator mtg : this.getMyTwoDBGenerators()) {
			sql.append(mtg.generateDBSql(this.getDbType()));
		}

		for (List<Domain> dataDomains : this.getDataDomains()) {
			sql.append("\n");
			for (Domain d : dataDomains) {
				sql.append(SqlReflector.generateInsertSqlWithValue(d)).append("\n");
			}

			for (Domain d : dataDomains) {
				for (ManyToMany mtm : d.getManyToManies()) {
					sql.append(SqlReflector.generateMtmInsertSqlWithValues(mtm)).append("\n");
				}
			}
		}

		for (List<Domain> dataDomains : this.getDataDomains()) {
			sql.append("\n");
			for (Domain d : dataDomains) {
				if (d.hasDomainId())
					sql.append(SqlReflector.generateUpdateSqlWithValue(d)).append("\n");
			}
		}

		for (List<Domain> dataDomains : this.getDataDomains()) {
			sql.append("\n");
			for (Domain d : dataDomains) {
				if (d.hasDomainId())
					sql.append(SqlReflector.generateDeleteSqlWithValue(d)).append("\n");
			}

			for (Domain d : dataDomains) {
				for (ManyToMany mtm : d.getManyToManies()) {
					sql.append(SqlReflector.generateMtmDeleteSqlWithValues(mtm)).append("\n");
				}
			}
		}

		sql.append("\n");
		for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
			sql.append(dbd.generateDropTableSqls(false)).append("\n");
		}

		for (TwoDomainsDBDefinitionGenerator mtg : this.getMyTwoDBGenerators()) {
			sql.append(mtg.generateDropLinkTableSql());
		}

		writeToFile(this.getProjectFolderPath() + "sql/" + this.getStandardName() + ".sql", sql.toString());

		FileCopyer copy = new FileCopyer();
		if (!StringUtil.isBlank(this.getExcelTemplateName())) {
			File mF1 = new File((this.getExcelTemplateFolder() + this.getExcelTemplateName()).replace("\\", "/"));
			File mF2 = new File(
					(this.getProjectFolderPath() + "exceltemplate/" + this.getExcelTemplateName()).replace("\\", "/"));
			if (mF1.exists()) {
				if (!mF2.getParentFile().exists()) {
					mF2.getParentFile().mkdirs();
				}
				if (!mF2.exists()) {
					mF2.createNewFile();
				}
				copy.copy(mF1.getPath(), mF2.getPath());
			}
			if (genFormatted) {
				String genExcelFile = "Gen_formatted";
				String[] genExcelFiles = this.getExcelTemplateName().split("\\.");
				if (genExcelFiles != null && genExcelFiles.length > 0)
					genExcelFile = genExcelFiles[0] + "_formatted";
				ExcelWizard.outputExcelWorkBook(this,
						(this.getProjectFolderPath() + "exceltemplate/").replace("\\", "/"), genExcelFile + ".xls");
			}
		}
	}

	public void generateOracleDBToolsFiles(boolean genFormatted) throws Exception {
		decorateMentuItems();
		StringBuilder sql = new StringBuilder();
		boolean createNew = false;
		for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
			sql.append(dbd.generateDropTableSqls(createNew));
		}
		
		for (TwoDomainsDBDefinitionGenerator mtg : this.myTwoDBGenerators) {
			OracleTwoDomainsDBDefinitionGenerator otg = OracleTwoDomainsDBDefinitionGenerator.toOracleTwoDomainsDBDefinitionGenerator(mtg);
			sql.append(otg.generateDropLinkTableSql());
		}

		sql.append("\n\n");

		sql.append("drop sequence COMMONSEQUENCE;").append("\n\n");
		sql.append("-- Create sequence").append("\n").append("create sequence COMMONSEQUENCE").append("\n")
				.append("minvalue 10000").append("\n").append("maxvalue 9999999999999999999999999999").append("\n")
				.append("start with 10000").append("\n").append("increment by 1").append("\n").append("cache 20;")
				.append("\n\n");

		for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
			sql.append(dbd.generateDBSql(createNew)).append("\n");
		}
		for (TwoDomainsDBDefinitionGenerator mtg : this.myTwoDBGenerators) {
			sql.append(mtg.generateDBSql(this.getDbType()));
		}

		// load initial data
		for (List<Domain> dataDomains : this.getDataDomains()) {
			sql.append("\n");
			for (Domain d : dataDomains) {
				sql.append(Oracle11gSqlReflector.generateInsertSqlWithValue(d)).append("\n");
			}

			for (Domain d : dataDomains) {
				for (ManyToMany mtm : d.getManyToManies()) {
					sql.append(Oracle11gSqlReflector.generateMtmInsertSqlWithValues(mtm)).append("\n");
				}
			}
		}

		for (List<Domain> dataDomains : this.getDataDomains()) {
			sql.append("\n");
			for (Domain d : dataDomains) {
				sql.append(Oracle11gSqlReflector.generateUpdateSqlWithValue(d)).append("\n");
			}
		}

		for (List<Domain> dataDomains : this.getDataDomains()) {
			sql.append("\n");
			for (Domain d : dataDomains) {
				sql.append(Oracle11gSqlReflector.generateDeleteSqlWithValue(d)).append("\n");
			}

			for (Domain d : dataDomains) {
				for (ManyToMany mtm : d.getManyToManies()) {
					sql.append(SqlReflector.generateMtmDeleteSqlWithValues(mtm)).append("\n");
				}
			}
		}

		sql.append("commit;\n");
		writeToFile(this.getProjectFolderPath() + "sql/" + this.standardName + ".sql", sql.toString());

		FileCopyer copy = new FileCopyer();
		if (!StringUtil.isBlank(this.getExcelTemplateName())) {
			File mF1 = new File((this.getExcelTemplateFolder() + this.getExcelTemplateName()).replace("\\", "/"));
			File mF2 = new File(
					(this.getProjectFolderPath() + "exceltemplate/" + this.getExcelTemplateName()).replace("\\", "/"));
			if (mF1.exists()) {
				if (!mF2.getParentFile().exists()) {
					mF2.getParentFile().mkdirs();
				}
				if (!mF2.exists()) {
					mF2.createNewFile();
				}
				copy.copy(mF1.getPath(), mF2.getPath());
			}
			if (genFormatted) {
				String genExcelFile = "Gen_formatted";
				String[] genExcelFiles = this.getExcelTemplateName().split("\\.");
				if (genExcelFiles != null && genExcelFiles.length > 0)
					genExcelFile = genExcelFiles[0] + "_formatted";
				ExcelWizard.outputExcelWorkBook(this,
						(this.getProjectFolderPath() + "exceltemplate/").replace("\\", "/"), genExcelFile + ".xls");
			}
		}
	}

	public void generatePgDBToolsFiles(boolean genFormatted) throws Exception {
		decorateMentuItems();
		StringBuilder sql = new StringBuilder();
		boolean createNew = true;
		for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
			sql.append(dbd.generateDBSql(createNew)).append("\n");
			if (createNew)
				createNew = false;
		}
		for (TwoDomainsDBDefinitionGenerator mtg : this.getMyTwoDBGenerators()) {
			sql.append(mtg.generateDBSql(this.getDbType()));
		}

		sql.append("\n");
		for (Domain d : getDomains()) {
			sql.append(PgsqlReflector.generateSetSerialVal10000(d)).append("\n");
		}
		sql.append("\n");

		for (List<Domain> dataDomains : this.getDataDomains()) {
			sql.append("\n");
			for (Domain d : dataDomains) {
				sql.append(SqlReflector.generateInsertSqlWithValue(d)).append("\n");
			}

			for (Domain d : dataDomains) {
				for (ManyToMany mtm : d.getManyToManies()) {
					sql.append(SqlReflector.generateMtmInsertSqlWithValues(mtm)).append("\n");
				}
			}
		}

		for (List<Domain> dataDomains : this.getDataDomains()) {
			sql.append("\n");
			for (Domain d : dataDomains) {
				if (d.hasDomainId())
					sql.append(SqlReflector.generateUpdateSqlWithValue(d)).append("\n");
			}
		}

		for (List<Domain> dataDomains : this.getDataDomains()) {
			sql.append("\n");
			for (Domain d : dataDomains) {
				if (d.hasDomainId())
					sql.append(SqlReflector.generateDeleteSqlWithValue(d)).append("\n");
			}

			for (Domain d : dataDomains) {
				for (ManyToMany mtm : d.getManyToManies()) {
					sql.append(SqlReflector.generateMtmDeleteSqlWithValues(mtm)).append("\n");
				}
			}
		}

		sql.append("\n");
		for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
			sql.append(dbd.generateDropTableSqls(false)).append("\n");
		}

		for (TwoDomainsDBDefinitionGenerator mtg : this.getMyTwoDBGenerators()) {
			sql.append(mtg.generateDropLinkTableSql());
		}

		writeToFile(this.getProjectFolderPath() + "sql/" + this.getStandardName() + ".sql", sql.toString());

		FileCopyer copy = new FileCopyer();
		if (!StringUtil.isBlank(this.getExcelTemplateName())) {
			File mF1 = new File((this.getExcelTemplateFolder() + this.getExcelTemplateName()).replace("\\", "/"));
			File mF2 = new File(
					(this.getProjectFolderPath() + "exceltemplate/" + this.getExcelTemplateName()).replace("\\", "/"));
			if (mF1.exists()) {
				if (!mF2.getParentFile().exists()) {
					mF2.getParentFile().mkdirs();
				}
				if (!mF2.exists()) {
					mF2.createNewFile();
				}
				copy.copy(mF1.getPath(), mF2.getPath());
			}
			if (genFormatted) {
				String genExcelFile = "Gen_formatted";
				String[] genExcelFiles = this.getExcelTemplateName().split("\\.");
				if (genExcelFiles != null && genExcelFiles.length > 0)
					genExcelFile = genExcelFiles[0] + "_formatted";
				ExcelWizard.outputExcelWorkBook(this,
						(this.getProjectFolderPath() + "exceltemplate/").replace("\\", "/"), genExcelFile + ".xls");
			}
		}
	}

	public void generateProjectFiles(Boolean ignoreWarning, Boolean genFormatted, Boolean genUi, Boolean genController,
			Boolean genService, Boolean genServiceImpl, Boolean genDao, Boolean genDaoImpl) throws Exception {
		if (this.dbType == null || "".equals(this.dbType) || "mysql".equalsIgnoreCase(this.dbType)
				|| "mariadb".equalsIgnoreCase(this.dbType)) {
			ValidateInfo info = this.validate(ignoreWarning);
			if (info.success(ignoreWarning) == false) {
				throw new ValidateException(info);
			}
			try {
				decorateMentuItems();
				String srcfolderPath = this.getProjectFolderPath();
				if ("normal".equalsIgnoreCase(this.getSchema())) {
					for (Prism ps : this.prisms) {
						ps.setFolderPath(this.getProjectFolderPath());
						ps.generatePrismFiles(ignoreWarning, genUi, genController, genService, genServiceImpl, genDao,
								genDaoImpl);
						for (ManyToMany mtm:ps.getManyToManies()) {
							EasyUIMtmPI mpage = mtm.getEuPI();
							mpage.setTechnicalStack(this.technicalstack);
							mpage.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
							mpage.setNav(this.nav);
							mpage.generatePIFiles(srcfolderPath);
						}
					}

					for (Util u : utils) {
						String utilPath = this.getProjectFolderPath() + "src/main/java/"
								+ packagetokenToFolder(u.getPackageToken()) + "utils/";
						writeToFile(utilPath + u.getFileName(), u.generateUtilString());
					}

					for (Independent idt : this.independents) {
						String idtPath = this.getProjectFolderPath() + "src/main/java/"
								+ packagetokenToFolder(idt.getPackageToken());
						writeToFile(idtPath + idt.getFileName(), idt.generateImplString());
					}
					
					for (org.light.core.Module m : this.modules) {
						m.generateModuleFiles(this.getProjectFolderPath());
					}
					
					for (LayoutComb lcb : this.layoutCombs) {
						lcb.generateCombFiles(this.getProjectFolderPath());
					}
					
					for (ReportComb rcb : this.reportCombs) {
						rcb.generateCombFiles(this.getProjectFolderPath());
					}

					String homePath = this.getProjectFolderPath() + "src/main/resources/static/";
					if (genUi) {
						if (this.containsLoginModule()) {
							this.jumphomepage.setJumpFolder("login");
						}
						writeToFile(homePath + "index.html", this.jumphomepage.generateIncludeString());
						String templateIndexFolder = this.getProjectFolderPath() + "src/main/resources/templates/";
						writeToFile(templateIndexFolder + "index.html", this.jumphomepage.generateIncludeString());
					}
	
					if (genUi) {
						this.homepage.generatePIFiles(this.getProjectFolderPath());
					}
					
					FileCopyer copy = new FileCopyer();

					if (genUi) {
						File cssfrom = new File(this.getSourceFolderPath() + "css/");
						File cssto = new File(this.getProjectFolderPath() + "src/main/resources/static/css/");

						copy.dirFrom = cssfrom;
						copy.dirTo = cssto;
						copy.listFileInDir(cssfrom);

						File jsfrom = new File(this.getSourceFolderPath() + "js/");
						File jsto = new File(this.getProjectFolderPath() + "src/main/resources/static/js/");

						copy.dirFrom = jsfrom;
						copy.dirTo = jsto;
						copy.listFileInDir(jsfrom);

						File easyuifrom = new File(this.getSourceFolderPath() + "easyui/");
						File easyuito = new File(this.getProjectFolderPath() + "src/main/resources/static/easyui/");

						copy.dirFrom = easyuifrom;
						copy.dirTo = easyuito;
						copy.listFileInDir(easyuifrom);

						File uploadjsfrom = new File(this.getSourceFolderPath() + "uploadjs/");
						File uploadjsto = new File(this.getProjectFolderPath() + "src/main/resources/static/uploadjs/");

						copy.dirFrom = uploadjsfrom;
						copy.dirTo = uploadjsto;
						copy.listFileInDir(uploadjsfrom);
						
						File imagefrom = new File(this.getSourceFolderPath() + "images/");
						File imageto = new File(this.getProjectFolderPath() + "src/main/resources/static/images/");

						copy.dirFrom = imagefrom;
						copy.dirTo = imageto;
						copy.listFileInDir(imagefrom);
						
						File echartsfrom = new File(this.getSourceFolderPath() + "echarts/");
						File echartsto = new File(this.getProjectFolderPath() + "src/main/resources/static/echarts/");

						copy.dirFrom = echartsfrom;
						copy.dirTo = echartsto;
						copy.listFileInDir(echartsfrom);
					}

					if (!StringUtil.isBlank(this.getExcelTemplateName())) {
						File mF1 = new File(
								(this.getExcelTemplateFolder() + this.getExcelTemplateName()).replace("\\", "/"));
						File mF2 = new File(
								(this.getProjectFolderPath() + "exceltemplate/" + this.getExcelTemplateName())
										.replace("\\", "/"));
						if (mF1.exists()) {
							if (!mF2.getParentFile().exists()) {
								mF2.getParentFile().mkdirs();
							}
							if (!mF2.exists()) {
								mF2.createNewFile();
							}
							copy.copy(mF1.getPath(), mF2.getPath());
						}
						if (genFormatted) {
							String genExcelFile = "Gen_formatted";
							String[] genExcelFiles = this.getExcelTemplateName().split("\\.");
							if (genExcelFiles != null && genExcelFiles.length > 0)
								genExcelFile = genExcelFiles[0] + "_formatted";
							ExcelWizard.outputExcelWorkBook(this,
									(this.getProjectFolderPath() + "exceltemplate/").replace("\\", "/"),
									genExcelFile + ".xls");
						}
					}

					for (IndependentConfig idc : this.independentConfigs) {
						writeToFile(this.getProjectFolderPath() + idc.getFolder() + idc.getFileName(),
								idc.generateImplString());
					}

					StringBuilder sql = new StringBuilder();
					boolean createNew = true;
					for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
						sql.append(dbd.generateDBSql(createNew)).append("\n");
						if (createNew)
							createNew = false;
					}
					for (TwoDomainsDBDefinitionGenerator mtg : this.myTwoDBGenerators) {
						sql.append(mtg.generateDBSql(this.getDbType()));
					}

					for (List<Domain> dataDomains : this.getDataDomains()) {
						sql.append("\n");
						for (Domain d : dataDomains) {
							sql.append(SqlReflector.generateInsertSqlWithValue(d)).append("\n");
						}

						for (Domain d : dataDomains) {
							for (ManyToMany mtm : d.getManyToManies()) {
								sql.append(SqlReflector.generateMtmInsertSqlWithValues(mtm)).append("\n");
							}
						}
					}

					writeToFile(this.getProjectFolderPath() + "sql/" + this.getStandardName() + ".sql", sql.toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		} else if ("oracle".equalsIgnoreCase(this.dbType)) {
			ValidateInfo info = this.validate(ignoreWarning);
			if (info.success(ignoreWarning) == false) {
				for (String s : info.getCompileErrors()) {
					logger.debug(s);
				}
				return;
			}
			try {
				decorateMentuItems();
				String projectFolderPath = this.getProjectFolderPath().replace('\\', '/');
				String templateFolderPath = this.getSourceFolderPath().replace('\\', '/');
				if ("normal".equalsIgnoreCase(this.getSchema())) {
					if (this.getSgsSource() != null && !this.getSgsSource().equals("")) {
						writeToFile(projectFolderPath + "sgs/" + StringUtil.capFirst(this.getStandardName())
								+ "_original.sgs", this.getSgsSource());
					}
					for (Prism ps : this.prisms) {
						OraclePrism ops = (OraclePrism) ps;
						ops.setFolderPath(projectFolderPath);
						ops.generatePrismFiles(ignoreWarning, genUi, genController, genService, genServiceImpl, genDao,
								genDaoImpl);
						for (ManyToMany mtm:ops.getManyToManies()) {
							OracleManyToMany omtm = (OracleManyToMany) mtm;
							EasyUIMtmPI mpage = omtm.getEuPI();
							mpage.setTechnicalStack(this.technicalstack);
							mpage.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
							mpage.setNav(this.nav);
							mpage.generatePIFiles(projectFolderPath);
						}
					}
					
					for (Util u : utils) {
						String utilPath = projectFolderPath + "src/main/java/"
								+ packagetokenToFolder(u.getPackageToken()) + "utils/";
						writeToFile(utilPath + u.getFileName(), u.generateUtilString());
					}
					for (Independent idt : this.independents) {
						String idtPath = this.getProjectFolderPath() + "src/main/java/"
								+ packagetokenToFolder(idt.getPackageToken());
						writeToFile(idtPath + idt.getFileName(), idt.generateImplString());
					}
					
					for (org.light.core.Module m : this.modules) {
						m.generateModuleFiles(this.getProjectFolderPath());
					}
					
					for (LayoutComb lcb : this.layoutCombs) {
						lcb.generateCombFiles(this.getProjectFolderPath());
					}
					
					for (ReportComb rcb : this.reportCombs) {
						rcb.generateCombFiles(this.getProjectFolderPath());
					}

					String homePath = projectFolderPath + "src/main/resources/static/";
					if (genUi) {
						if (this.containsLoginModule()) {
							this.jumphomepage.setJumpFolder("login");
						}
						writeToFile(homePath + "index.html", this.jumphomepage.generateIncludeString());
						String templateIndexFolder = projectFolderPath+ "src/main/resources/templates/";
						writeToFile(templateIndexFolder + "index.html", this.jumphomepage.generateIncludeString());
					}

					if (genUi) {
						this.homepage.generatePIFiles(projectFolderPath);
					}
					
					FileCopyer copy = new FileCopyer();

//					File libto = new File(this.getProjectFolderPath() + "src/main/webapp/WEB-INF/lib/");
//					File libfrom = new File(this.getSourceFolderPath() + "lib/");
//
//					copy.dirFrom = libfrom;
//					copy.dirTo = libto;
//					copy.listFileInDir(libfrom);

					if (genUi) {
						File cssfrom = new File(templateFolderPath + "css/");
						File cssto = new File(projectFolderPath + "src/main/resources/static/css/");

						copy.dirFrom = cssfrom;
						copy.dirTo = cssto;
						copy.listFileInDir(cssfrom);

						File jsfrom = new File(templateFolderPath + "js/");
						File jsto = new File(projectFolderPath + "src/main/resources/static/js/");

						copy.dirFrom = jsfrom;
						copy.dirTo = jsto;
						copy.listFileInDir(jsfrom);

						File easyuifrom = new File(templateFolderPath + "easyui/");
						File easyuito = new File(projectFolderPath + "src/main/resources/static/easyui/");

						copy.dirFrom = easyuifrom;
						copy.dirTo = easyuito;
						copy.listFileInDir(easyuifrom);

						File uploadjsfrom = new File(this.getSourceFolderPath() + "uploadjs/");
						File uploadjsto = new File(this.getProjectFolderPath() + "src/main/resources/static/uploadjs/");

						copy.dirFrom = uploadjsfrom;
						copy.dirTo = uploadjsto;
						copy.listFileInDir(uploadjsfrom);
						
						File imagefrom = new File(this.getSourceFolderPath() + "images/");
						File imageto = new File(this.getProjectFolderPath() + "src/main/resources/static/images/");

						copy.dirFrom = imagefrom;
						copy.dirTo = imageto;
						copy.listFileInDir(imagefrom);
						
						File echartsfrom = new File(this.getSourceFolderPath() + "echarts/");
						File echartsto = new File(this.getProjectFolderPath() + "src/main/resources/static/echarts/");

						copy.dirFrom = echartsfrom;
						copy.dirTo = echartsto;
						copy.listFileInDir(echartsfrom);
					}

					if (!StringUtil.isBlank(this.getExcelTemplateName())) {
						File mF1 = new File(
								(this.getExcelTemplateFolder() + this.getExcelTemplateName()).replace("\\", "/"));
						File mF2 = new File((projectFolderPath + "exceltemplate/" + this.getExcelTemplateName())
								.replace("\\", "/"));
						if (mF1.exists()) {
							if (!mF2.getParentFile().exists()) {
								mF2.getParentFile().mkdirs();
							}
							if (!mF2.exists()) {
								mF2.createNewFile();
							}
							copy.copy(mF1.getPath(), mF2.getPath());
						}
						if (genFormatted) {
							String genExcelFile = "Gen_formatted";
							String[] genExcelFiles = this.getExcelTemplateName().split("\\.");
							if (genExcelFiles != null && genExcelFiles.length > 0)
								genExcelFile = genExcelFiles[0] + "_formatted";
							ExcelWizard.outputExcelWorkBook(this,
									(this.getProjectFolderPath() + "exceltemplate/").replace("\\", "/"),
									genExcelFile + ".xls");
						}
					}

					for (IndependentConfig idc : this.independentConfigs) {
						writeToFile(this.getProjectFolderPath() + idc.getFolder() + idc.getFileName(),
								idc.generateImplString());
					}

					StringBuilder sql = new StringBuilder();
					boolean createNew = false;
					for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
						sql.append(dbd.generateDropTableSqls(createNew));
					}

					for (TwoDomainsDBDefinitionGenerator mtg : this.myTwoDBGenerators) {
						OracleTwoDomainsDBDefinitionGenerator otg = OracleTwoDomainsDBDefinitionGenerator.toOracleTwoDomainsDBDefinitionGenerator(mtg);
						sql.append(otg.generateOracleDropLinkTableSql());
					}

					sql.append("\n\n");

					sql.append("drop sequence COMMONSEQUENCE;").append("\n\n");
					sql.append("-- Create sequence").append("\n").append("create sequence COMMONSEQUENCE").append("\n")
							.append("minvalue 10000").append("\n").append("maxvalue 9999999999999999999999999999")
							.append("\n").append("start with 10000").append("\n").append("increment by 1").append("\n")
							.append("cache 20;").append("\n\n");

					for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
						sql.append(dbd.generateDBSql(createNew)).append("\n");
					}
					for (TwoDomainsDBDefinitionGenerator mtg : this.myTwoDBGenerators) {
						sql.append(mtg.generateDBSql(this.getDbType()));
					}

					// load initial data
					for (List<Domain> dataDomains : this.getDataDomains()) {
						sql.append("\n");
						for (Domain d : dataDomains) {
							sql.append(Oracle11gSqlReflector.generateInsertSqlWithValue(d)).append("\n");
						}

						for (Domain d : dataDomains) {
							for (ManyToMany mtm : d.getManyToManies()) {
								sql.append(Oracle11gSqlReflector.generateMtmInsertSqlWithValues(mtm)).append("\n");
							}
						}
					}
					sql.append("commit;\n");
					writeToFile(projectFolderPath + "sql/" + this.standardName + ".sql", sql.toString());

				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		} else if ("postgresql".equalsIgnoreCase(this.dbType) || "pgsql".equalsIgnoreCase(this.dbType)) {
			generatePgProjectFiles(ignoreWarning, genFormatted, genUi, genController, genService, genServiceImpl,
					genDao, genDaoImpl);
		} else {
			ValidateInfo info = new ValidateInfo();
			info.addCompileError("未支持项目所用数据库。");
			throw new ValidateException(info);
		}
	}

	public String getSourceFolderPath() {
		return sourceFolderPath;
	}

	public void setSourceFolderPath(String sourceFolderPath) {
		this.sourceFolderPath = sourceFolderPath;
	}

	public void generateProjectZip(Boolean ignoreWarning, Boolean genFormatted, Boolean genUi, Boolean genController,
			Boolean genService, Boolean genServiceImpl, Boolean genDao, Boolean genDaoImpl) throws Exception {
		delAllFile(this.folderPath + this.standardName + ".zip");
		delFolder(this.getProjectFolderPath());
		File f = new File(this.getProjectFolderPath());
		if (!f.getParentFile().exists()) {
			f.getParentFile().mkdirs();
		}
		f.mkdirs();
		if ("DBTools".equalsIgnoreCase(this.getSchema())) {
			if ("mariadb".equalsIgnoreCase(this.getDbType())||"mysql".equalsIgnoreCase(this.getDbType())) {
				generateMariaDBToolsFiles(genFormatted);
			}else if ("oracle".equalsIgnoreCase(this.getDbType())) {
				generateOracleDBToolsFiles(genFormatted);
			} else if ("postgresql".equalsIgnoreCase(this.getDbType())||"pgsql".equalsIgnoreCase(this.getDbType())) {
				generatePgDBToolsFiles(genFormatted);
			}			
		} else if (this.getTechnicalstack().equalsIgnoreCase("sbmeu")) {
			generateProjectFiles(ignoreWarning, genFormatted, genUi, genController, genService, genServiceImpl, genDao,
					genDaoImpl);
		} else if (this.getTechnicalstack().equalsIgnoreCase("msmeu")
				|| this.getTechnicalstack().equalsIgnoreCase("smeu")) {
			generateMSMEUProjectFiles(ignoreWarning, genFormatted, genUi, genController, genService, genServiceImpl,
					genDao, genDaoImpl);
		}
		ZipCompressor compressor = new ZipCompressor(this.folderPath + this.standardName + ".zip");
		compressor.compressExe(this.getProjectFolderPath());
		delFolder(this.getProjectFolderPath());
	}

	public String getFolderPath() {
		return this.folderPath;
	}

	public String getProjectFolderPath() {
		if (this.getStandardName() != null && !"".equals(this.getStandardName())) {
			return folderPath + this.getStandardName() + "/";
		} else
			return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public static String packagetokenToFolder(String packageToken) {
		String folder = "";
		if (packageToken != null)
			folder = packageToken.replace('.', '/');
		folder += "/";
		return folder;
	}

	public static String folderToPackageToken(String folder) {
		String packagetoken = folder.replace('/', '.');
		if (packagetoken.charAt(packagetoken.length() - 1) == '.')
			packagetoken = packagetoken.substring(0, packagetoken.length() - 1);
		return packagetoken;
	}

	public List<DBDefinitionGenerator> getDbDefinitionGenerators() {
		return dbDefinitionGenerators;
	}

	public void setDbDefinitionGenerators(List<DBDefinitionGenerator> dbDefinitionGenerators) {
		this.dbDefinitionGenerators = dbDefinitionGenerators;
	}

	public void addDBDefinitionGenerator(DBDefinitionGenerator generator) {
		this.dbDefinitionGenerators.add(generator);
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public TestSuite getProjectTestSuite() {
		return projectTestSuite;
	}

	public void setProjectTestSuite(TestSuite projectTestSuite) {
		this.projectTestSuite = projectTestSuite;
	}

	public void addTestSuite(TestSuite testSuite) {
		this.projectTestSuite.testSuites.add(testSuite);
	}

	public void addTestCase(TestCase testCase) {
		this.projectTestSuite.testCases.add(testCase);
	}

	public void addTestCases(List<TestCase> testCases) {
		this.projectTestSuite.testCases.addAll(testCases);
	}

	public List<ConfigFile> getConfigFiles() {
		return configFiles;
	}

	public void setConfigFiles(List<ConfigFile> configFiles) {
		this.configFiles = configFiles;
	}

	public void addConfigFile(ConfigFile configFile) {
		this.configFiles.add(configFile);
	}

	public void addConfigFiles(List<ConfigFile> configFiles) {
		this.configFiles.addAll(configFiles);
	}

	public static void zipFile(File inFile, ZipOutputStream zos, String dir) throws IOException {
		if (inFile.isDirectory()) {
			File[] files = inFile.listFiles();
			for (File file : files)
				zipFile(file, zos, dir + "/" + inFile.getName());
		} else {
			String entryName = null;
			if (!"".equals(dir))
				entryName = dir + "/" + inFile.getName();
			else
				entryName = inFile.getName();
			ZipEntry entry = new ZipEntry(entryName);
			zos.putNextEntry(entry);
			InputStream is = new FileInputStream(inFile);
			int len = 0;
			while ((len = is.read()) != -1)
				zos.write(len);
			is.close();
		}
	}

	public static void delFolder(String folderPath) {
		try {
			delAllFile(folderPath); // 删除完里面所有内容
			String filePath = folderPath;
			filePath = filePath.toString();
			java.io.File myFilePath = new java.io.File(filePath);
			myFilePath.delete(); // 删除空文件夹
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean delAllFile(String path) {
		boolean flag = false;
		File file = new File(path);
		if (!file.exists()) {
			return flag;
		}
		if (!file.isDirectory()) {
			return flag;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);// 先删除文件夹里面的文件
				delFolder(path + "/" + tempList[i]);// 再删除空文件夹
				flag = true;
			}
		}
		return flag;
	}

	public ValidateInfo validate(Boolean ignoreWarning) {
		ValidateInfo info = new ValidateInfo();
		for (Domain d : this.domains) {
			ValidateInfo v = d.validate(this.getDbType());
			if (v.success(ignoreWarning) == false) {
				info.setSuccess(false);
				info.addAllCompileErrors(v.getCompileErrors());
				info.addAllCompileWarnings(v.getCompileWarnings());
			}
		}
		for (Prism ps : this.prisms) {
			ValidateInfo v = ps.validate(ignoreWarning);
			info.addAllCompileErrors(v.getCompileErrors());
			info.addAllCompileWarnings(v.getCompileWarnings());
		}
		return info;
	}

	public List<Domain> getDomains() {
		return domains;
	}

	public boolean updateDomain(int pos, Domain domain) {
		if (this.domains != null && this.domains.size() == pos - 1) {
			this.domains.add(domain);
			return true;
		} else if (this.domains != null && this.domains.size() > pos) {
			this.domains.set(pos, domain);
			return true;
		}
		return false;
	}

	public void setDomains(List<Domain> domains) throws Exception {
		for (Domain d : domains) {
			this.addDomain(d);
			for (Include include : this.includes) {
				include.addDomain(d);
			}
		}
	}

	public void addDomain(Domain domain) throws Exception {
		if (this.getDbPrefix() != null && !this.getDbPrefix().equals("")) {
			domain.setDbPrefix(this.getDbPrefix());
		}
		this.domains.add(domain);
		for (Include include : this.includes) {
			include.addDomain(domain);
		}
	}

	public String getDbPrefix() {
		return dbPrefix;
	}

	public void setDbPrefix(String dbPrefix) {
		this.dbPrefix = dbPrefix;
		for (Domain d : this.getDomains()) {
			d.setDbPrefix(dbPrefix);
		}
		for (Prism p : this.getPrisms()) {
			p.getDomain().setDbPrefix(dbPrefix);
		}

	}

	public String getTechnicalstack() {
		return technicalstack;
	}

	public void setTechnicalstack(String technicalstack) {
		this.technicalstack = technicalstack;
	}

	public List<Include> getIncludes() {
		return includes;
	}

	public void setIncludes(List<Include> includes) {
		this.includes = includes;
	}

	public JumpHomePage getJumphomepage() {
		return jumphomepage;
	}

	public void setJumphomepage(JumpHomePage jumphomepage) {
		this.jumphomepage = jumphomepage;
	}

	public String getDbUsername() {
		return dbUsername;
	}

	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public void replaceConfigFile(ConfigFile cf) {
		if (cf != null && this.configFiles != null) {
			if (this.configFiles.size() == 0 || !this.configFiles.contains(cf.getStandardName())) {
				this.configFiles.add(cf);
				return;
			}
			for (int i = 0; i < this.configFiles.size(); i++) {
				if (this.configFiles.get(i).getStandardName().equals(cf.getStandardName())) {
					this.configFiles.remove(i);
					this.configFiles.add(cf);
				}
			}
		}
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getText() {
		if (this.label != null && !this.label.equals(""))
			return this.label;
		else
			return this.standardName;
	}

	public void writeToFile(String filePath, String content) throws Exception {
		File f = new File(filePath);
		if (!f.getParentFile().exists()) {
			f.getParentFile().mkdirs();
		}
		f.createNewFile();
		try (Writer fw = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(f.getAbsolutePath()), "UTF-8"))) {
			fw.write(content, 0, content.length());
		}
	}

	public List<TwoDomainsDBDefinitionGenerator> getMyTwoDBGenerators() {
		return myTwoDBGenerators;
	}

	public void setMyTwoDBGenerators(List<TwoDomainsDBDefinitionGenerator> myTwoDBGenerators) {
		this.myTwoDBGenerators = myTwoDBGenerators;
	}

	public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	public String toString() {
		return JSONObject.fromObject(this).toString();
	}

	public static File createPathFile(String path) {
		try {
			File f = new File(path);
			if (!f.getParentFile().exists()) {
				f.getParentFile().mkdirs();
			}
			f.createNewFile();
			return f;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<List<Domain>> getDataDomains() {
		return dataDomains;
	}

	public void setDataDomains(List<List<Domain>> dataDomains) {
		this.dataDomains = dataDomains;
	}

	public void addDataDomains(List<Domain> datas) {
		this.dataDomains.add(datas);
	}

	public String getExcelTemplateName() {
		return excelTemplateName;
	}

	public void setExcelTemplateName(String excelTemplateName) {
		this.excelTemplateName = excelTemplateName;
	}

	public String getExcelTemplateFolder() {
		return excelTemplateFolder;
	}

	public void setExcelTemplateFolder(String excelTemplateFolder) {
		this.excelTemplateFolder = excelTemplateFolder;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public String getCrossOrigin() {
		return crossOrigin;
	}

	public void setCrossOrigin(String crossOrigin) {
		this.crossOrigin = crossOrigin;
	}

	public Set<Independent> getIndependents() {
		return independents;
	}

	public void setIndependents(Set<Independent> independents) {
		this.independents = independents;
	}

	public Set<IndependentConfig> getIndependentConfigs() {
		return independentConfigs;
	}

	public void setIndependentConfigs(Set<IndependentConfig> independentConfigs) {
		this.independentConfigs = independentConfigs;
	}

	public String getDaoSuffix() {
		return daoSuffix;
	}

	public void setDaoSuffix(String daoSuffix) {
		this.daoSuffix = daoSuffix;
	}

	public String getServiceSuffix() {
		return serviceSuffix;
	}

	public void setServiceSuffix(String serviceSuffix) {
		this.serviceSuffix = serviceSuffix;
	}

	public String getServiceimplSuffix() {
		return serviceimplSuffix;
	}

	public void setServiceimplSuffix(String serviceimplSuffix) {
		this.serviceimplSuffix = serviceimplSuffix;
	}

	public String getControllerSuffix() {
		return controllerSuffix;
	}

	public void setControllerSuffix(String controllerSuffix) {
		this.controllerSuffix = controllerSuffix;
	}

	public String getDomainNamingSuffix() {
		return domainNamingSuffix;
	}

	public void setDomainNamingSuffix(String domainNamingSuffix) {
		this.domainNamingSuffix = domainNamingSuffix;
	}

	public String getControllerNamingSuffix() {
		return controllerNamingSuffix;
	}

	public void setControllerNamingSuffix(String controllerNamingSuffix) {
		this.controllerNamingSuffix = controllerNamingSuffix;
	}

	public String getDaoimplSuffix() {
		return daoimplSuffix;
	}

	public void setDaoimplSuffix(String daoimplSuffix) {
		this.daoimplSuffix = daoimplSuffix;
	}

	public String getDomainSuffix() {
		return domainSuffix;
	}

	public void setDomainSuffix(String domainSuffix) {
		this.domainSuffix = domainSuffix;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public void generateMSMEUProjectFiles(Boolean ignoreWarning, Boolean genFormatted, Boolean genUi,
			Boolean genController, Boolean genService, Boolean genServiceImpl, Boolean genDao, Boolean genDaoImpl)
			throws Exception {
		if (this.dbType == null || "".equals(this.dbType) || "mysql".equalsIgnoreCase(this.dbType)
				|| "mariadb".equalsIgnoreCase(this.dbType) || "postgresql".equalsIgnoreCase(this.dbType)
				|| "pgsql".equalsIgnoreCase(this.dbType)) {
			ValidateInfo info = this.validate(ignoreWarning);
			if (info.success(ignoreWarning) == false) {
				throw new ValidateException(info);
			}
			try {
				decorateMentuItems();
				String projectFolderPath = this.getProjectFolderPath().replace('\\', '/');
				String templateFolderPath = this.getSourceFolderPath().replace('\\', '/');

				for (Prism ps : this.prisms) {
					ps.setFolderPath(projectFolderPath);
					ps.generateSMEUPrismFiles(ignoreWarning, genUi, genController, genService, genServiceImpl, genDao,
							genDaoImpl);
					
					for (ManyToMany mtm:ps.getManyToManies()) {
						EasyUIMtmPI mpage = mtm.getEuPI();
						mpage.setTechnicalStack(this.technicalstack);
						mpage.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
						mpage.setNav(this.nav);
						mpage.generatePIFiles(projectFolderPath);
					}
				}
		
				for (Util u : utils) {
					String utilPath = projectFolderPath + "src/" + packagetokenToFolder(u.getPackageToken()) + "utils/";
					writeToFile(utilPath + u.getFileName(), u.generateUtilString());
				}
				
				for (org.light.core.Module m : this.modules) {
					m.generateModuleFiles(this.getProjectFolderPath());
				}
				
				for (LayoutComb lcb : this.layoutCombs) {
					lcb.generateCombFiles(this.getProjectFolderPath());
				}
				
				for (ReportComb rcb : this.reportCombs) {
					rcb.generateCombFiles(this.getProjectFolderPath());
				}
				
				if (genUi) {
					String homePath = projectFolderPath + "WebContent/";
					if (this.containsLoginModule()) {
						this.jumphomepage.setJumpFolder("login");
					}
					writeToFile(homePath + "index.html", this.jumphomepage.generateIncludeString());
					this.homepage.generatePIFiles(projectFolderPath);
				}

				FileCopyer copy = new FileCopyer();


				if ("smeu".equalsIgnoreCase(this.getTechnicalstack())) {
					File smeulibto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
					File smeulibfrom = new File(templateFolderPath + "smeulib/");

					// 设置来源去向
					copy.dirFrom = smeulibfrom;
					copy.dirTo = smeulibto;
					copy.listFileInDir(smeulibfrom);
					
					File shirolibto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
					File shirolibfrom = new File(templateFolderPath + "shirolib/");

					// 设置来源去向
					copy.dirFrom = shirolibfrom;
					copy.dirTo = shirolibto;
					copy.listFileInDir(shirolibfrom);
					
					if (StringUtil.isBlank(this.getDbType())||"mariadb".equalsIgnoreCase(this.getDbType())) {
						File libto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
						File libfrom = new File(templateFolderPath + "mariadblib/");
						// 设置来源去向
						copy.dirFrom = libfrom;
						copy.dirTo = libto;
						copy.listFileInDir(libfrom);
					}else if ("mysql".equalsIgnoreCase(this.getDbType())) {
						File libto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
						File libfrom = new File(templateFolderPath + "mysql8lib");
						// 设置来源去向
						copy.dirFrom = libfrom;
						copy.dirTo = libto;
						copy.listFileInDir(libfrom);
					}else if ("postgresql".equalsIgnoreCase(this.getDbType())||"pgsql".equalsIgnoreCase(this.getDbType())) {
						File libto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
						File libfrom = new File(templateFolderPath + "pgsqllib/");
						// 设置来源去向
						copy.dirFrom = libfrom;
						copy.dirTo = libto;
						copy.listFileInDir(libfrom);
					}							
				}

				if (genUi) {
					File cssfrom = new File(templateFolderPath + "css/");
					File cssto = new File(projectFolderPath + "WebContent/css/");

					copy.dirFrom = cssfrom;
					copy.dirTo = cssto;
					copy.listFileInDir(cssfrom);

					File jsfrom = new File(templateFolderPath + "js/");
					File jsto = new File(projectFolderPath + "WebContent/js/");

					copy.dirFrom = jsfrom;
					copy.dirTo = jsto;
					copy.listFileInDir(jsfrom);

					File easyuifrom = new File(templateFolderPath + "easyui/");
					File easyuito = new File(projectFolderPath + "WebContent/easyui/");

					copy.dirFrom = easyuifrom;
					copy.dirTo = easyuito;
					copy.listFileInDir(easyuifrom);

					File uploadjsfrom = new File(templateFolderPath + "uploadjs/");
					File uploadjsto = new File(this.getProjectFolderPath() + "WebContent/uploadjs/");

					copy.dirFrom = uploadjsfrom;
					copy.dirTo = uploadjsto;
					copy.listFileInDir(uploadjsfrom);
					
					File imagefrom = new File(templateFolderPath + "images/");
					File imageto = new File(this.getProjectFolderPath() + "WebContent/images/");

					copy.dirFrom = imagefrom;
					copy.dirTo = imageto;
					copy.listFileInDir(imagefrom);
					
					File echartsfrom = new File(templateFolderPath + "echarts/");
					File echartsto = new File(this.getProjectFolderPath() + "WebContent/echarts/");

					copy.dirFrom = echartsfrom;
					copy.dirTo = echartsto;
					copy.listFileInDir(echartsfrom);
				}

				if (!StringUtil.isBlank(this.getExcelTemplateName())) {
					File mF1 = new File(
							(this.getExcelTemplateFolder() + this.getExcelTemplateName()).replace("\\", "/"));
					File mF2 = new File(
							(projectFolderPath + "exceltemplate/" + this.getExcelTemplateName()).replace("\\", "/"));
					if (mF1.exists()) {
						if (!mF2.getParentFile().exists()) {
							mF2.getParentFile().mkdirs();
						}
						if (!mF2.exists()) {
							mF2.createNewFile();
						}
						copy.copy(mF1.getPath(), mF2.getPath());
					}
					if (genFormatted) {
						String genExcelFile = "Gen_formatted";
						String[] genExcelFiles = this.getExcelTemplateName().split("\\.");
						if (genExcelFiles != null && genExcelFiles.length > 0)
							genExcelFile = genExcelFiles[0] + "_formatted";
						ExcelWizard.outputExcelWorkBook(this,
								(this.getProjectFolderPath() + "exceltemplate/").replace("\\", "/"),
								genExcelFile + ".xls");
					}
				}

				for (IndependentConfig idc : this.independentConfigs) {
					writeToFile(this.getProjectFolderPath() + idc.getFolder() + idc.getFileName(),
							idc.generateImplString());
				}

				StringBuilder sql = new StringBuilder();
				boolean createNew = true;
				for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
					sql.append(dbd.generateDBSql(createNew)).append("\n");
					if (createNew)
						createNew = false;
				}
				for (TwoDomainsDBDefinitionGenerator mtg : this.myTwoDBGenerators) {
					sql.append(mtg.generateDBSql(this.getDbType()));
				}

				if ("postgresql".equalsIgnoreCase(this.dbType) || "pgsql".equalsIgnoreCase(this.dbType)) {
					sql.append("\n");
					for (Domain d : getDomains()) {
						sql.append(PgsqlReflector.generateSetSerialVal10000(d)).append("\n");
					}
					sql.append("\n");
				}

				for (List<Domain> dataDomains : this.getDataDomains()) {
					sql.append("\n");
					for (Domain d : dataDomains) {
						sql.append(SqlReflector.generateInsertSqlWithValue(d)).append("\n");
					}

					for (Domain d : dataDomains) {
						for (ManyToMany mtm : d.getManyToManies()) {
							sql.append(SqlReflector.generateMtmInsertSqlWithValues(mtm)).append("\n");
						}
					}
				}

				writeToFile(projectFolderPath + "sql/" + this.getStandardName() + ".sql", sql.toString());

				for (ConfigFile cf : this.configFiles) {
					if (cf.isTopLevel()) {
						writeToFile(projectFolderPath + cf.getStandardName(), cf.generateConfigFileString());
					} else if (cf.isSettingParamFile()) {
						writeToFile(projectFolderPath + ".settings/" + cf.getStandardName(),
								cf.generateConfigFileString());
					} else if (cf.isPutInsideSrcAndClasses == false) {
						writeToFile(projectFolderPath + "WebContent/WEB-INF/" + cf.getStandardName(),
								cf.generateConfigFileString());
					} else {
						writeToFile(projectFolderPath + "src/" + cf.getStandardName(), cf.generateConfigFileString());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		} else if ("oracle".equalsIgnoreCase(this.dbType)) {
			ValidateInfo info = this.validate(ignoreWarning);
			if (info.success(ignoreWarning) == false) {
				for (String s : info.getCompileErrors()) {
					System.out.println(s);
				}
				return;
			}
			try {
				decorateMentuItems();
				String projectFolderPath = this.getProjectFolderPath().replace('\\', '/');
				String templateFolderPath = this.getSourceFolderPath().replace('\\', '/');

				for (Prism ps : this.prisms) {
					OraclePrism ops = (OraclePrism) ps;
					ops.setFolderPath(projectFolderPath);
					ops.generateSMEUPrismFiles(ignoreWarning, genUi, genController, genService, genServiceImpl, genDao,
							genDaoImpl);
					
					for (ManyToMany mtm:ops.getManyToManies()) {
						OracleManyToMany omtm = (OracleManyToMany) mtm;
						EasyUIMtmPI mpage = omtm.getEuPI();
						mpage.setTechnicalStack(this.technicalstack);
						mpage.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
						mpage.setNav(this.nav);
						mpage.generatePIFiles(projectFolderPath);
					}
				}
				
				for (Util u : utils) {
					String utilPath = projectFolderPath + "src/" + packagetokenToFolder(u.getPackageToken()) + "utils/";
					writeToFile(utilPath + u.getFileName(), u.generateUtilString());
				}
				for (org.light.core.Module m : this.modules) {
					m.generateModuleFiles(this.getProjectFolderPath());
				}
				
				for (LayoutComb lcb : this.layoutCombs) {
					lcb.generateCombFiles(this.getProjectFolderPath());
				}
				
				for (ReportComb rcb : this.reportCombs) {
					rcb.generateCombFiles(this.getProjectFolderPath());
				}
				
				if (genUi) {
					String homePath = projectFolderPath + "WebContent/";

					if (this.containsLoginModule()) {
						this.jumphomepage.setJumpFolder("login");
					}
					writeToFile(homePath + "index.html", this.jumphomepage.generateIncludeString());
					this.homepage.generatePIFiles(projectFolderPath);
				}
				
				FileCopyer copy = new FileCopyer();

				if ("smeu".equalsIgnoreCase(this.getTechnicalstack())) {
					File libto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
					File libfrom = new File(templateFolderPath + "lib/");
					// 设置来源去向
					copy.dirFrom = libfrom;
					copy.dirTo = libto;
					copy.listFileInDir(libfrom);
										
					File shirolibto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
					File shirolibfrom = new File(templateFolderPath + "shirolib/");

					// 设置来源去向
					copy.dirFrom = shirolibfrom;
					copy.dirTo = shirolibto;
					copy.listFileInDir(shirolibfrom);
					
					File smeulibto = new File(projectFolderPath + "WebContent/WEB-INF/lib/");
					File smeulibfrom = new File(templateFolderPath + "smeulib/");

					// 设置来源去向
					copy.dirFrom = smeulibfrom;
					copy.dirTo = smeulibto;
					copy.listFileInDir(smeulibfrom);
				}

				if (genUi) {
					File cssfrom = new File(templateFolderPath + "css/");
					File cssto = new File(projectFolderPath + "WebContent/css/");

					copy.dirFrom = cssfrom;
					copy.dirTo = cssto;
					copy.listFileInDir(cssfrom);

					File jsfrom = new File(templateFolderPath + "js/");
					File jsto = new File(projectFolderPath + "WebContent/js/");

					copy.dirFrom = jsfrom;
					copy.dirTo = jsto;
					copy.listFileInDir(jsfrom);

					File easyuifrom = new File(templateFolderPath + "easyui/");
					File easyuito = new File(projectFolderPath + "WebContent/easyui/");

					copy.dirFrom = easyuifrom;
					copy.dirTo = easyuito;
					copy.listFileInDir(easyuifrom);

					File uploadjsfrom = new File(templateFolderPath+ "uploadjs/");
					File uploadjsto = new File(projectFolderPath + "WebContent/uploadjs/");

					copy.dirFrom = uploadjsfrom;
					copy.dirTo = uploadjsto;
					copy.listFileInDir(uploadjsfrom);
					
					File imagefrom = new File(templateFolderPath + "images/");
					File imageto = new File(this.getProjectFolderPath() + "WebContent/images/");

					copy.dirFrom = imagefrom;
					copy.dirTo = imageto;
					copy.listFileInDir(imagefrom);
					
					File echartsfrom = new File(this.getSourceFolderPath() + "echarts/");
					File echartsto = new File(this.getProjectFolderPath() + "WebContent/echarts/");

					copy.dirFrom = echartsfrom;
					copy.dirTo = echartsto;
					copy.listFileInDir(echartsfrom);
				}

				if (!StringUtil.isBlank(this.getExcelTemplateName())) {
					File mF1 = new File(
							(this.getExcelTemplateFolder() + this.getExcelTemplateName()).replace("\\", "/"));
					File mF2 = new File(
							(projectFolderPath + "exceltemplate/" + this.getExcelTemplateName()).replace("\\", "/"));
					if (mF1.exists()) {
						if (!mF2.getParentFile().exists()) {
							mF2.getParentFile().mkdirs();
						}
						if (!mF2.exists()) {
							mF2.createNewFile();
						}
						copy.copy(mF1.getPath(), mF2.getPath());
					}
					if (genFormatted) {
						String genExcelFile = "Gen_formatted";
						String[] genExcelFiles = this.getExcelTemplateName().split("\\.");
						if (genExcelFiles != null && genExcelFiles.length > 0)
							genExcelFile = genExcelFiles[0] + "_formatted";
						ExcelWizard.outputExcelWorkBook(this,
								(this.getProjectFolderPath() + "exceltemplate/").replace("\\", "/"),
								genExcelFile + ".xls");
					}
				}

				for (IndependentConfig idc : this.independentConfigs) {
					writeToFile(this.getProjectFolderPath() + idc.getFolder() + idc.getFileName(),
							idc.generateImplString());
				}

				StringBuilder sql = new StringBuilder();
				boolean createNew = true;
				for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
					sql.append(dbd.generateDropTableSqls(createNew));
				}

				for (TwoDomainsDBDefinitionGenerator mtg : this.myTwoDBGenerators) {
					OracleTwoDomainsDBDefinitionGenerator otg = OracleTwoDomainsDBDefinitionGenerator.toOracleTwoDomainsDBDefinitionGenerator(mtg);
					sql.append(otg.generateDropLinkTableSql());
				}

				sql.append("\n\n");

				sql.append("drop sequence COMMONSEQUENCE;").append("\n\n");
				sql.append("-- Create sequence").append("\n").append("create sequence COMMONSEQUENCE").append("\n")
						.append("minvalue 10000").append("\n").append("maxvalue 9999999999999999999999999999")
						.append("\n").append("start with 10000").append("\n").append("increment by 1").append("\n")
						.append("cache 20;").append("\n\n");

				for (DBDefinitionGenerator dbd : dbDefinitionGenerators) {
					sql.append(dbd.generateDBSql(createNew)).append("\n");
				}
				for (TwoDomainsDBDefinitionGenerator mtg : this.myTwoDBGenerators) {
					sql.append(mtg.generateDBSql(this.getDbType()));
				}

				// load initial data
				for (List<Domain> dataDomains : this.getDataDomains()) {
					sql.append("\n");
					for (Domain d : dataDomains) {
						sql.append(Oracle11gSqlReflector.generateInsertSqlWithValue(d)).append("\n");
					}

					for (Domain d : dataDomains) {
						for (ManyToMany mtm : d.getManyToManies()) {
							sql.append(Oracle11gSqlReflector.generateMtmInsertSqlWithValues(mtm)).append("\n");
						}
					}
				}

				writeToFile(projectFolderPath + "sql/" + this.standardName + ".sql", sql.toString());

				for (ConfigFile cf : this.configFiles) {
					if (cf.isTopLevel()) {
						writeToFile(projectFolderPath + cf.getStandardName(), cf.generateConfigFileString());
					} else if (cf.isSettingParamFile()) {
						writeToFile(projectFolderPath + ".settings/" + cf.getStandardName(),
								cf.generateConfigFileString());
					} else if (cf.isPutInsideSrcAndClasses == false) {
						writeToFile(projectFolderPath + "WebContent/WEB-INF/" + cf.getStandardName(),
								cf.generateConfigFileString());
					} else {
						writeToFile(projectFolderPath + "src/" + cf.getStandardName(), cf.generateConfigFileString());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		} else {
			ValidateInfo info = new ValidateInfo();
			info.addCompileError("未支持项目所用数据库。");
			throw new ValidateException(info);
		}
	}

	public String getFrontBaseApi() {
		return frontBaseApi;
	}

	public void setFrontBaseApi(String frontBaseApi) {
		this.frontBaseApi = frontBaseApi;
	}

	public void addModule(org.light.core.Module m) {
		this.modules.add(m);
	}
	
	public Set<String> getModuleNames() {
		Set<String> moduleNames = new TreeSet<>();
		for(org.light.core.Module m: this.modules) {
			moduleNames.add(m.getStandardName());
		}
		return moduleNames;
	}

	public String getResolution() {
		return resolution;
	}

	public Set<org.light.core.Module> getModules() {
		return modules;
	}

	public void setModules(Set<org.light.core.Module> modules) {
		this.modules = modules;
	}

	public Set<LayoutComb> getLayoutCombs() {
		return layoutCombs;
	}

	public void setLayoutCombs(Set<LayoutComb> layoutCombs) {
		this.layoutCombs = layoutCombs;
	}
	
	public void addLayoutComb(LayoutComb layoutComb) {
		this.layoutCombs.add(layoutComb);
	}

	public Set<ReportComb> getReportCombs() {
		return reportCombs;
	}

	public void setReportCombs(Set<ReportComb> reportCombs) {
		this.reportCombs = reportCombs;
	}
	
	public void addReportComb(ReportComb reportComb) {
		this.reportCombs.add(reportComb);
	}

	public Nav getNav() {
		return nav;
	}

	public void setNav(Nav nav) {
		this.nav = nav;
	}

	public EasyUIHomePagePI getHomepage() {
		return homepage;
	}

	public void setHomepage(EasyUIHomePagePI homepage) {
		this.homepage = homepage;
	}

	public List<String> getDomainNames() {
		return domainNames;
	}

	public void setDomainNames(List<String> domainNames) {
		this.domainNames = domainNames;
	}
	
	public void addDomainName(String domainName) {
		this.domainNames.add(domainName);
	}
}
