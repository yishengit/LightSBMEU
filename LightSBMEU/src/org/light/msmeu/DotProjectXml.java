package org.light.msmeu;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.ConfigFile;
import org.light.domain.Statement;
import org.light.utils.WriteableUtil;

public class DotProjectXml extends ConfigFile{
	protected String projectName = "PeaceWingGenerated";	
	
	public DotProjectXml(){
		super();
		this.topLevel = true;
		this.standardName = ".project";
	}
	
	@Override 
	public void setStandardName(String standardName){
	}
	
	@Override
	public String generateConfigFileString() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
		sList.add(new Statement(2000L,0,"<projectDescription>"));
		sList.add(new Statement(3000L,1,"<name>"+this.getProjectName()+"</name>"));
		sList.add(new Statement(4000L,1,"<comment></comment>"));
		sList.add(new Statement(5000L,1,"<projects>"));
		sList.add(new Statement(6000L,1,"</projects>"));
		sList.add(new Statement(7000L,1,"<buildSpec>"));
		sList.add(new Statement(8000L,2,"<buildCommand>"));
		sList.add(new Statement(9000L,3,"<name>org.eclipse.wst.jsdt.core.javascriptValidator</name>"));
		sList.add(new Statement(10000L,3,"<arguments>"));
		sList.add(new Statement(11000L,3,"</arguments>"));
		sList.add(new Statement(12000L,2,"</buildCommand>"));
		sList.add(new Statement(13000L,2,"<buildCommand>"));
		sList.add(new Statement(14000L,3,"<name>org.eclipse.jdt.core.javabuilder</name>"));
		sList.add(new Statement(15000L,3,"<arguments>"));
		sList.add(new Statement(16000L,3,"</arguments>"));
		sList.add(new Statement(17000L,2,"</buildCommand>"));
		sList.add(new Statement(18000L,2,"<buildCommand>"));
		sList.add(new Statement(19000L,3,"<name>org.eclipse.wst.common.project.facet.core.builder</name>"));
		sList.add(new Statement(20000L,3,"<arguments>"));
		sList.add(new Statement(21000L,3,"</arguments>"));
		sList.add(new Statement(22000L,2,"</buildCommand>"));
		sList.add(new Statement(23000L,2,"<buildCommand>"));
		sList.add(new Statement(24000L,3,"<name>org.eclipse.wst.validation.validationbuilder</name>"));
		sList.add(new Statement(25000L,3,"<arguments>"));
		sList.add(new Statement(26000L,3,"</arguments>"));
		sList.add(new Statement(27000L,2,"</buildCommand>"));
		sList.add(new Statement(28000L,2,"<buildCommand>"));
		sList.add(new Statement(29000L,3,"<name>org.eclipse.m2e.core.maven2Builder</name>"));
		sList.add(new Statement(30000L,3,"<arguments>"));
		sList.add(new Statement(31000L,3,"</arguments>"));
		sList.add(new Statement(32000L,2,"</buildCommand>"));
		sList.add(new Statement(33000L,1,"</buildSpec>"));
		sList.add(new Statement(34000L,1,"<natures>"));
		sList.add(new Statement(35000L,2,"<nature>org.eclipse.m2e.core.maven2Nature</nature>"));
		sList.add(new Statement(36000L,2,"<nature>org.eclipse.jem.workbench.JavaEMFNature</nature>"));
		sList.add(new Statement(37000L,2,"<nature>org.eclipse.wst.common.modulecore.ModuleCoreNature</nature>"));
		sList.add(new Statement(38000L,2,"<nature>org.eclipse.wst.common.project.facet.core.nature</nature>"));
		sList.add(new Statement(39000L,2,"<nature>org.eclipse.jdt.core.javanature</nature>"));
		sList.add(new Statement(40000L,2,"<nature>org.eclipse.wst.jsdt.core.jsNature</nature>"));
		sList.add(new Statement(41000L,1,"</natures>"));
		sList.add(new Statement(42000L,0,"</projectDescription>"));
		return WriteableUtil.merge(sList).getContent();
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
