package org.light.msmeu;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.javaforever.gatescore.utils.StringUtil;
import org.light.core.Writeable;
import org.light.domain.IndependentConfig;
import org.light.domain.Statement;
import org.light.utils.WriteableUtil;

public class POMXml extends IndependentConfig{	
	private static final long serialVersionUID = -4790994605747142428L;
	protected String projectName;
	protected String dbType = "mariadb";
	protected Set<String> moduleNames = new TreeSet<>();
	
	public POMXml(){
		super();		
		this.standardName = "pomXml";
		this.fileName = "pom.xml";
	}
	
	public POMXml(String dbType){
		this();
		this.dbType = dbType;
	}
	
	@Override
	public String generateImplString() {
    	List<Writeable> sList = new ArrayList<Writeable>();
    	sList.add(new Statement(1000L,0,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
    	sList.add(new Statement(2000L,0,"<project xmlns=\"http://maven.apache.org/POM/4.0.0\""));
    	sList.add(new Statement(3000L,1,"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""));
    	sList.add(new Statement(4000L,1,"xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">"));
    	sList.add(new Statement(5000L,1,"<!-- <packaging>jar</packaging> -->"));
    	sList.add(new Statement(6000L,1,"<modelVersion>4.0.0</modelVersion>"));
    	sList.add(new Statement(7000L,0,""));
    	sList.add(new Statement(8000L,1,"<groupId>org.javaforever</groupId>"));
    	sList.add(new Statement(9000L,1,"<artifactId>"+this.projectName+"</artifactId>"));
    	sList.add(new Statement(10000L,1,"<version>1.0-SNAPSHOT</version>"));
    	sList.add(new Statement(11000L,0,""));
    	sList.add(new Statement(12000L,1,"<dependencies>"));
    	sList.add(new Statement(13000L,2,"<dependency>"));
    	sList.add(new Statement(14000L,3,"<groupId>org.springframework</groupId>"));
    	sList.add(new Statement(15000L,3,"<artifactId>spring-core</artifactId>"));
    	sList.add(new Statement(16000L,3,"<version>4.3.7.RELEASE</version>"));
    	sList.add(new Statement(17000L,2,"</dependency>"));
    	sList.add(new Statement(18000L,2,"<dependency>"));
    	sList.add(new Statement(19000L,3,"<groupId>org.springframework</groupId>"));
    	sList.add(new Statement(20000L,3,"<artifactId>spring-beans</artifactId>"));
    	sList.add(new Statement(21000L,3,"<version>4.3.7.RELEASE</version>"));
    	sList.add(new Statement(22000L,2,"</dependency>"));
    	sList.add(new Statement(23000L,2,"<dependency>"));
    	sList.add(new Statement(24000L,3,"<groupId>org.springframework</groupId>"));
    	sList.add(new Statement(25000L,3,"<artifactId>spring-context</artifactId>"));
    	sList.add(new Statement(26000L,3,"<version>4.3.7.RELEASE</version>"));
    	sList.add(new Statement(27000L,2,"</dependency>"));
    	sList.add(new Statement(28000L,2,"<dependency>"));
    	sList.add(new Statement(29000L,3,"<groupId>org.springframework</groupId>"));
    	sList.add(new Statement(30000L,3,"<artifactId>spring-context-support</artifactId>"));
    	sList.add(new Statement(31000L,3,"<version>4.3.7.RELEASE</version>"));
    	sList.add(new Statement(32000L,2,"</dependency>"));
    	sList.add(new Statement(33000L,2,"<dependency>"));
    	sList.add(new Statement(34000L,3,"<groupId>org.springframework</groupId>"));
    	sList.add(new Statement(35000L,3,"<artifactId>spring-jdbc</artifactId>"));
    	sList.add(new Statement(36000L,3,"<version>4.3.7.RELEASE</version>"));
    	sList.add(new Statement(37000L,2,"</dependency>"));
    	sList.add(new Statement(38000L,2,"<dependency>"));
    	sList.add(new Statement(39000L,3,"<groupId>org.springframework</groupId>"));
    	sList.add(new Statement(40000L,3,"<artifactId>spring-tx</artifactId>"));
    	sList.add(new Statement(41000L,3,"<version>4.3.7.RELEASE</version>"));
    	sList.add(new Statement(42000L,2,"</dependency>"));
    	sList.add(new Statement(43000L,2,"<dependency>"));
    	sList.add(new Statement(44000L,3,"<groupId>org.springframework</groupId>"));
    	sList.add(new Statement(45000L,3,"<artifactId>spring-orm</artifactId>"));
    	sList.add(new Statement(46000L,3,"<version>4.3.7.RELEASE</version>"));
    	sList.add(new Statement(47000L,2,"</dependency>"));
    	sList.add(new Statement(48000L,2,"<dependency>"));
    	sList.add(new Statement(49000L,3,"<groupId>org.springframework</groupId>"));
    	sList.add(new Statement(50000L,3,"<artifactId>spring-web</artifactId>"));
    	sList.add(new Statement(51000L,3,"<version>4.3.7.RELEASE</version>"));
    	sList.add(new Statement(52000L,2,"</dependency>"));
    	sList.add(new Statement(53000L,2,"<dependency>"));
    	sList.add(new Statement(54000L,3,"<groupId>org.springframework</groupId>"));
    	sList.add(new Statement(55000L,3,"<artifactId>spring-webmvc</artifactId>"));
    	sList.add(new Statement(56000L,3,"<version>4.3.7.RELEASE</version>"));
    	sList.add(new Statement(57000L,2,"</dependency>"));
    	sList.add(new Statement(58000L,2,"<dependency>"));
    	sList.add(new Statement(59000L,3,"<groupId>org.mybatis</groupId>"));
    	sList.add(new Statement(60000L,3,"<artifactId>mybatis</artifactId>"));
    	sList.add(new Statement(61000L,3,"<version>3.4.5</version>"));
    	sList.add(new Statement(62000L,2,"</dependency>"));
    	sList.add(new Statement(63000L,2,"<dependency>"));
    	sList.add(new Statement(64000L,3,"<groupId>org.mybatis</groupId>"));
    	sList.add(new Statement(65000L,3,"<artifactId>mybatis-spring</artifactId>"));
    	sList.add(new Statement(66000L,3,"<version>1.3.0</version>"));
    	sList.add(new Statement(67000L,2,"</dependency>"));
    	sList.add(new Statement(68000L,2,"<dependency>"));
    	sList.add(new Statement(69000L,3,"<groupId>com.fasterxml.jackson.core</groupId>"));
    	sList.add(new Statement(70000L,3,"<artifactId>jackson-databind</artifactId>"));
    	sList.add(new Statement(71000L,3,"<version>2.11.1</version>"));
    	sList.add(new Statement(72000L,2,"</dependency>"));
    	sList.add(new Statement(73000L,2,"<dependency>"));
    	sList.add(new Statement(74000L,3,"<groupId>com.fasterxml.jackson.core</groupId>"));
    	sList.add(new Statement(75000L,3,"<artifactId>jackson-core</artifactId>"));
    	sList.add(new Statement(76000L,3,"<version>2.11.1</version>"));
    	sList.add(new Statement(77000L,2,"</dependency>"));
    	sList.add(new Statement(78000L,2,"<dependency>"));
    	sList.add(new Statement(79000L,3,"<groupId>com.fasterxml.jackson.core</groupId>"));
    	sList.add(new Statement(80000L,3,"<artifactId>jackson-annotations</artifactId>"));
    	sList.add(new Statement(81000L,3,"<version>2.11.1</version>"));
    	sList.add(new Statement(82000L,2,"</dependency>"));
    	sList.add(new Statement(83000L,0,""));
    	sList.add(new Statement(84000L,2,"<dependency>"));
    	sList.add(new Statement(85000L,3,"<groupId>org.apache.commons</groupId>"));
    	sList.add(new Statement(86000L,3,"<artifactId>commons-lang3</artifactId>"));
    	sList.add(new Statement(87000L,3,"<version>3.0</version>"));
    	sList.add(new Statement(88000L,2,"</dependency>"));
    	sList.add(new Statement(89000L,2,"<dependency>"));
    	sList.add(new Statement(90000L,3,"<groupId>commons-io</groupId>"));
    	sList.add(new Statement(91000L,3,"<artifactId>commons-io</artifactId>"));
    	sList.add(new Statement(92000L,3,"<version>2.2</version>"));
    	sList.add(new Statement(93000L,2,"</dependency>"));
    	sList.add(new Statement(94000L,2,"<dependency>"));
    	sList.add(new Statement(95000L,3,"<groupId>commons-fileupload</groupId>"));
    	sList.add(new Statement(96000L,3,"<artifactId>commons-fileupload</artifactId>"));
    	sList.add(new Statement(97000L,3,"<version>1.3</version>"));
    	sList.add(new Statement(98000L,2,"</dependency>"));
    	
    	if (this.moduleNames.contains("ShiroAuthModule")) {
    		sList.add(new Statement(98100L,2,"<!-- https://mvnrepository.com/artifact/org.apache.shiro/shiro-spring -->"));
    		sList.add(new Statement(98200L,2,"<dependency>"));
    		sList.add(new Statement(98300L,3,"<groupId>org.apache.shiro</groupId>"));
    		sList.add(new Statement(98400L,3,"<artifactId>shiro-spring</artifactId>"));
    		sList.add(new Statement(98500L,3,"<version>1.4.0</version>"));
    		sList.add(new Statement(98600L,2,"</dependency>"));
    		sList.add(new Statement(98700L,2,"<!-- https://mvnrepository.com/artifact/com.github.theborakompanioni/thymeleaf-extras-shiro -->"));
    		sList.add(new Statement(98800L,2,"<dependency>"));
    		sList.add(new Statement(98900L,3,"<groupId>com.github.theborakompanioni</groupId>"));
    		sList.add(new Statement(99000L,3,"<artifactId>thymeleaf-extras-shiro</artifactId>"));
    		sList.add(new Statement(99110L,3,"<version>2.0.0</version>"));
    		sList.add(new Statement(99120L,2,"</dependency>"));
    		sList.add(new Statement(99130L,2,""));
    		sList.add(new Statement(99150L,2,"<dependency>"));
    		sList.add(new Statement(99160L,3,"<groupId>org.springframework.boot</groupId>"));
    		sList.add(new Statement(99170L,3,"<artifactId>spring-boot-starter-thymeleaf</artifactId>"));
    		sList.add(new Statement(99180L,3,"<version>2.1.1.RELEASE</version>"));
    		sList.add(new Statement(99190L,2,"</dependency>"));
		}

    	if (StringUtil.isBlank(this.dbType)||"mariadb".equalsIgnoreCase(this.dbType)) {
	    	sList.add(new Statement(99500L,2,"<dependency>"));
	    	sList.add(new Statement(100000L,3,"<groupId>mysql</groupId>"));
	    	sList.add(new Statement(101000L,3,"<artifactId>mysql-connector-java</artifactId> "));
	    	sList.add(new Statement(102000L,3,"<version>5.1.8</version>"));
	    	sList.add(new Statement(103000L,2,"</dependency>"));
    	} else if ("mysql".equalsIgnoreCase(this.dbType)) {
	    	sList.add(new Statement(99500L,2,"<dependency>"));
	    	sList.add(new Statement(100000L,3,"<groupId>mysql</groupId>"));
	    	sList.add(new Statement(101000L,3,"<artifactId>mysql-connector-java</artifactId> "));
	    	sList.add(new Statement(102000L,3,"<version>8.0.17</version>"));
	    	sList.add(new Statement(103000L,2,"</dependency>"));
    	} else if ("oracle".equalsIgnoreCase(this.dbType)){
//	    	sList.add(new Statement(99500L,2,"<dependency>"));
//	    	sList.add(new Statement(100000L,3,"<groupId>com.oracle.jdbc</groupId>"));
//	    	sList.add(new Statement(101000L,3,"<artifactId>ojdbc6</artifactId>"));
//	    	sList.add(new Statement(101500L,3,"<version>11.2.0.4</version>"));
//	    	sList.add(new Statement(102000L,3,"<scope>system</scope>"));
//	    	sList.add(new Statement(103000L,3,"<systemPath>${basedir}/WebContent/WEB-INF/lib/ojdbc6.jar</systemPath>"));
//	    	sList.add(new Statement(103500L,2,"</dependency>"));
    		sList.add(new Statement(99500L,2,"<dependency>"));
    		sList.add(new Statement(100000L,3,"<groupId>com.oracle.database.jdbc</groupId>"));
    		sList.add(new Statement(101000L,3,"<artifactId>ojdbc8</artifactId>"));
    		sList.add(new Statement(101300L,3,"<version>19.6.0.0</version>"));
    		sList.add(new Statement(101400L,2,"</dependency>"));
    		sList.add(new Statement(101500L,0,""));
    		sList.add(new Statement(101600L,2,"<!-- https://mvnrepository.com/artifact/cn.easyproject/orai18n -->"));
    		sList.add(new Statement(101700L,2,"<dependency>"));
    		sList.add(new Statement(101800L,3,"<groupId>cn.easyproject</groupId>"));
    		sList.add(new Statement(101900L,3,"<artifactId>orai18n</artifactId>"));
    		sList.add(new Statement(102000L,3,"<version>12.1.0.2.0</version>"));
    		sList.add(new Statement(102100L,2,"</dependency>"));
    	}
    	else if ("postgresql".equalsIgnoreCase(this.dbType)||"pgsql".equalsIgnoreCase(this.dbType)){
	    	sList.add(new Statement(99500L,2,"<dependency>"));
	    	sList.add(new Statement(100000L,3,"<groupId>org.postgresql</groupId>"));
	    	sList.add(new Statement(101000L,3,"<artifactId>postgresql</artifactId>"));
	    	sList.add(new Statement(102000L,3,"<version>42.2.5</version>"));
	    	sList.add(new Statement(103000L,2,"</dependency>"));
    	}
    	sList.add(new Statement(113000L,2,"<dependency>"));
    	sList.add(new Statement(113100L,3,"<groupId>org.apache.poi</groupId>"));
    	sList.add(new Statement(113200L,3,"<artifactId>poi</artifactId>"));
    	sList.add(new Statement(113300L,3,"<version>4.0.1</version>"));
    	sList.add(new Statement(113400L,2,"</dependency>"));
    	sList.add(new Statement(114000L,2,"<dependency>"));
    	sList.add(new Statement(115000L,3,"<groupId>com.itextpdf</groupId>"));
    	sList.add(new Statement(116000L,3,"<artifactId>itext-asian</artifactId>"));
    	sList.add(new Statement(117000L,3,"<version>5.2.0</version>"));
    	sList.add(new Statement(118000L,2,"</dependency>"));
    	sList.add(new Statement(119000L,2,"<dependency>"));
    	sList.add(new Statement(120000L,3,"<groupId>com.itextpdf</groupId>"));
    	sList.add(new Statement(121000L,3,"<artifactId>itextpdf</artifactId>"));
    	sList.add(new Statement(122000L,3,"<version>5.4.3</version>"));
    	sList.add(new Statement(123000L,2,"</dependency>"));
    	sList.add(new Statement(124000L,2,"<dependency>"));
    	sList.add(new Statement(125000L,3,"<groupId>com.lowagie</groupId>"));
    	sList.add(new Statement(126000L,3,"<artifactId>itext</artifactId>"));
    	sList.add(new Statement(127000L,3,"<version>2.1.7</version>"));
    	sList.add(new Statement(128000L,2,"</dependency>"));
    	sList.add(new Statement(129000L,2,"<dependency>"));
    	sList.add(new Statement(130000L,3,"<groupId>com.lowagie</groupId>"));
    	sList.add(new Statement(131000L,3,"<artifactId>itext-rtf</artifactId>"));
    	sList.add(new Statement(132000L,3,"<version>2.1.7</version>"));
    	sList.add(new Statement(133000L,2,"</dependency>"));
    	sList.add(new Statement(134000L,1,"</dependencies>"));
    	sList.add(new Statement(135000L,0,"</project>"));

    	return WriteableUtil.merge(sList).getContent();
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	public Set<String> getModuleNames() {
		return moduleNames;
	}

	public void setModuleNames(Set<String> moduleNames) {
		this.moduleNames = moduleNames;
	}

}
