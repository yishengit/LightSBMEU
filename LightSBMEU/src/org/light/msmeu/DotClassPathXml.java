package org.light.msmeu;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.ConfigFile;
import org.light.domain.Statement;
import org.light.utils.WriteableUtil;

public class DotClassPathXml extends ConfigFile{
	public DotClassPathXml(){
		super();
		this.topLevel= true;
		this.standardName = ".classpath";
	}

	@Override
	public String generateConfigFileString() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
		sList.add(new Statement(2000L,0,"<classpath>"));
		sList.add(new Statement(3000L,1,"<classpathentry kind=\"src\" path=\"src\"/>"));
		sList.add(new Statement(4000L,1,"<classpathentry kind=\"con\" path=\"org.eclipse.jst.j2ee.internal.web.container\"/>"));
		sList.add(new Statement(5000L,1,"<classpathentry kind=\"con\" path=\"org.eclipse.jst.j2ee.internal.module.container\"/>"));
		sList.add(new Statement(6000L,1,"<classpathentry kind=\"con\" path=\"org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/JavaSE-1.8\">"));
		sList.add(new Statement(7000L,2,"<attributes>"));
		sList.add(new Statement(8000L,3,"<attribute name=\"maven.pomderived\" value=\"true\"/>"));
		sList.add(new Statement(9000L,2,"</attributes>"));
		sList.add(new Statement(10000L,1,"</classpathentry>"));
		sList.add(new Statement(11000L,1,"<classpathentry kind=\"con\" path=\"org.eclipse.jst.server.core.container/org.eclipse.jst.server.tomcat.runtimeTarget/Apache Tomcat v8.5\"/>"));
		sList.add(new Statement(12000L,1,"<classpathentry kind=\"con\" path=\"org.eclipse.m2e.MAVEN2_CLASSPATH_CONTAINER\">"));
		sList.add(new Statement(13000L,2,"<attributes>"));
		sList.add(new Statement(14000L,3,"<attribute name=\"maven.pomderived\" value=\"true\"/>"));
		sList.add(new Statement(15000L,3,"<attribute name=\"org.eclipse.jst.component.dependency\" value=\"/WEB-INF/lib\"/>"));
		sList.add(new Statement(16000L,2,"</attributes>"));
		sList.add(new Statement(17000L,1,"</classpathentry>"));
		sList.add(new Statement(18000L,1,"<classpathentry kind=\"output\" path=\"target/classes\"/>"));
		sList.add(new Statement(19000L,0,"</classpath>"));
		return WriteableUtil.merge(sList).getContent();
	}
}
