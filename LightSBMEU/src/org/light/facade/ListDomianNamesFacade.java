package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2017/11/25
 *
 */
public class ListDomianNamesFacade extends HttpServlet {
	private static final long serialVersionUID = 8350069920208925062L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListDomianNamesFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null&&ExcelWizardFacade.getInstance().getDomains().size()>0) {
			List<Domain> domains = ExcelWizardFacade.getInstance().getDomains();
			List<String> domainNames = new ArrayList<>();
			for (Domain d:domains) {
				domainNames.add(d.getStandardName());
			}
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			result.put("data", domainNames);
			out.print(JSONObject.fromObject(result));
		}else {
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", false);
			result.put("data", null);
			out.print(JSONObject.fromObject(result));
		}
	}
}
