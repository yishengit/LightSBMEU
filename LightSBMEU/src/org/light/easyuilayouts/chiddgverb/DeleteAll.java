package org.light.easyuilayouts.chiddgverb;

import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.exception.ValidateException;

public class DeleteAll  extends org.light.verb.DeleteAll{
	protected String detailPrefix = "detail";
	
	public DeleteAll(Domain d) throws ValidateException{
		super(d);
	}
	
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied)
			return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("delete" + domain.getCapFirstDomainName());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'DeleteAll',"));
			}else {
				sl.add(new Statement(2000, 1, "text:'批删除',"));
			}
			sl.add(new Statement(3000, 1, "iconCls:'icon-remove',"));
			sl.add(new Statement(4000, 1, "handler:function(){"));
			sl.add(new Statement(5000, 2, "var rows = $(\"#"+this.detailPrefix+"dg\").datagrid(\"getChecked\");"));
			sl.add(new Statement(6000, 2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(7000,3, "$.messager.alert(\"Alert\",\"Please choose record!\",\"warning\");"));
			}else {
				sl.add(new Statement(7000, 3, "$.messager.alert(\"警告\",\"请选定记录！\",\"warning\");"));
			}
			sl.add(new Statement(8000, 3, "return;"));
			sl.add(new Statement(9000, 2, "}"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(10000, 2, "if ($.messager.confirm(\"Alert\",\"Do you confirm to delete record?\", function(data){"));
			}else {
				sl.add(new Statement(10000, 2, "if ($.messager.confirm(\"警告\",\"确认要删除选定记录吗？\",function(data){"));
			}
			sl.add(new Statement(11000, 3, "if (data){"));
			sl.add(new Statement(12000, 4, "var ids = \"\";"));
			sl.add(new Statement(13000, 4, "for(var i=0;i<rows.length;i++){"));
			sl.add(new Statement(14000, 5,
					"ids += rows[i][\"" + domain.getDomainId().getLowerFirstFieldName() + "\"];"));
			sl.add(new Statement(15000, 5, "if (i < rows.length-1) ids += \",\";"));
			sl.add(new Statement(16000, 4, "}"));
			sl.add(new Statement(17000, 4, "deleteAll" + domain.getCapFirstPlural() + "(ids);"));
			sl.add(new Statement(18000, 3, "}"));
			sl.add(new Statement(19000, 2, "}));"));
			sl.add(new Statement(20000, 1, "}"));
			sl.add(new Statement(21000, 0, "}"));
			block.setMethodStatementList(sl);
			return block;
		}
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSButtonBlock().generateBlockContentString();
		}
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
		}
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Domain domain = this.domain;
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("deleteAll" + domain.getCapFirstPlural());
			Signature s1 = new Signature();
			s1.setName("ids");
			s1.setPosition(1);
			s1.setType(new Type("var"));
			method.addSignature(s1);

			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 1, "$.ajax({"));
			sl.add(new Statement(2000, 2, "type: \"post\","));
			sl.add(new Statement(3000, 2, "url: \"../"+this.domain.getControllerPackagePrefix() + domain.getLowerFirstDomainName()
					+ domain.getControllerNamingSuffix() + "/deleteAll" + domain.getCapFirstPlural() + "\","));
			sl.add(new Statement(4000, 2, "data: {"));
			sl.add(new Statement(5000, 3, "ids:ids"));
			sl.add(new Statement(6000, 2, "},"));
			sl.add(new Statement(7000, 2, "dataType: 'json',"));
			sl.add(new Statement(8000, 2, "success: function(data, textStatus) {"));
			sl.add(new Statement(9000, 3, "$(\"#"+this.detailPrefix+"dg\").datagrid(\"load\");"));
			sl.add(new Statement(10000, 2, "},"));
			sl.add(new Statement(11000, 2, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(12000, 2, "},"));
			sl.add(new Statement(13000, 2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(14000, 3, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(15000, 3, "alert(errorThrown.toString());"));
			sl.add(new Statement(16000, 2, "}"));
			sl.add(new Statement(17000, 1, "});"));

			method.setMethodStatementList(sl);
			return method;
		}
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSActionMethod().generateMethodString();
		}
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSActionMethod().generateMethodStringWithSerial();
		}
	}
	public String getDetailPrefix() {
		return detailPrefix;
	}

	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}
}
