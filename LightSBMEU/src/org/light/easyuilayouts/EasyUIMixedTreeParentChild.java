package org.light.easyuilayouts;

import org.light.domain.Domain;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.AddDialog;
import org.light.easyuilayouts.widgets.ChildDatagrid;
import org.light.easyuilayouts.widgets.MainDatagrid;
import org.light.easyuilayouts.widgets.SearchPanel;
import org.light.easyuilayouts.widgets.TreePanel;
import org.light.easyuilayouts.widgets.UpdateDialog;
import org.light.easyuilayouts.widgets.ViewDialog;

public class EasyUIMixedTreeParentChild extends EasyUILayout{
	protected Domain treeDomain;
	protected Domain parentDomain;
	protected Domain childDomain;
	protected TreePanel tree;
	protected SearchPanel searchPanel;	

	protected MainDatagrid mainDatagrid;
	protected AddDialog addDialog;
	protected UpdateDialog updateDialog;
	protected ViewDialog viewDialog;
	
	protected ChildDatagrid childDatagrid;
	protected AddDialog addChildDialog;
	protected UpdateDialog updateChildDialog;
	protected ViewDialog viewChildDialog;

	@Override
	public StatementList generateLayoutStatements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StatementList generateLayoutScriptStatements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean parse() {
		// TODO Auto-generated method stub
		return false;
	}

	public Domain getTreeDomain() {
		return treeDomain;
	}

	public void setTreeDomain(Domain treeDomain) {
		this.treeDomain = treeDomain;
	}

	public Domain getParentDomain() {
		return parentDomain;
	}

	public void setParentDomain(Domain parentDomain) {
		this.parentDomain = parentDomain;
	}

	public Domain getChildDomain() {
		return childDomain;
	}

	public void setChildDomain(Domain childDomain) {
		this.childDomain = childDomain;
	}

	public TreePanel getTree() {
		return tree;
	}

	public void setTree(TreePanel tree) {
		this.tree = tree;
	}

	public SearchPanel getSearchPanel() {
		return searchPanel;
	}

	public void setSearchPanel(SearchPanel searchPanel) {
		this.searchPanel = searchPanel;
	}

	public MainDatagrid getMainDatagrid() {
		return mainDatagrid;
	}

	public void setMainDatagrid(MainDatagrid mainDatagrid) {
		this.mainDatagrid = mainDatagrid;
	}

	public AddDialog getAddDialog() {
		return addDialog;
	}

	public void setAddDialog(AddDialog addDialog) {
		this.addDialog = addDialog;
	}

	public UpdateDialog getUpdateDialog() {
		return updateDialog;
	}

	public void setUpdateDialog(UpdateDialog updateDialog) {
		this.updateDialog = updateDialog;
	}

	public ViewDialog getViewDialog() {
		return viewDialog;
	}

	public void setViewDialog(ViewDialog viewDialog) {
		this.viewDialog = viewDialog;
	}

	public ChildDatagrid getChildDatagrid() {
		return childDatagrid;
	}

	public void setChildDatagrid(ChildDatagrid childDatagrid) {
		this.childDatagrid = childDatagrid;
	}

	public AddDialog getAddChildDialog() {
		return addChildDialog;
	}

	public void setAddChildDialog(AddDialog addChildDialog) {
		this.addChildDialog = addChildDialog;
	}

	public UpdateDialog getUpdateChildDialog() {
		return updateChildDialog;
	}

	public void setUpdateChildDialog(UpdateDialog updateChildDialog) {
		this.updateChildDialog = updateChildDialog;
	}

	public ViewDialog getViewChildDialog() {
		return viewChildDialog;
	}

	public void setViewChildDialog(ViewDialog viewChildDialog) {
		this.viewChildDialog = viewChildDialog;
	}

}
