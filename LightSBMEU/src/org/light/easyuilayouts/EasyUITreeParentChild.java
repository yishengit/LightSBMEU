package org.light.easyuilayouts;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.AddDialog;
import org.light.easyuilayouts.widgets.ChildDatagrid;
import org.light.easyuilayouts.widgets.MainDatagrid;
import org.light.easyuilayouts.widgets.SearchPanel;
import org.light.easyuilayouts.widgets.TreePanel;
import org.light.easyuilayouts.widgets.UpdateDialog;
import org.light.easyuilayouts.widgets.ViewDialog;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;

public class EasyUITreeParentChild extends EasyUILayout{
	protected Domain treeDomain;
	protected Domain parentDomain;
	protected Domain childDomain;
	protected Field innerTreeParentId;
	protected Field treeParentId;
	protected Field parentId;
	protected TreePanel tree;
	protected SearchPanel searchPanel;	

	protected MainDatagrid mainDatagrid;
	protected AddDialog addDialog;
	protected UpdateDialog updateDialog;
	protected ViewDialog viewDialog;
	
	protected ChildDatagrid childDatagrid;
	protected AddDialog addChildDialog;
	protected UpdateDialog updateChildDialog;
	protected ViewDialog viewChildDialog;

	@Override
	public StatementList generateLayoutStatements() {
		List<Writeable> sList = new ArrayList<>();
		sList.add(new Statement(1000L,0,"<div style=\"width:15%;float:left\">"));
		StatementList sl = tree.generateWidgetStatements();
		sl.setSerial(2000L);
		sl.setIndent(1);
		sList.add(sl);
		sList.add(new Statement(3000L,0,"</div>"));
		sList.add(new Statement(4000L,0,"<div style=\"width:85%;float:right;\">"));
		StatementList sl1 = searchPanel.generateWidgetStatements();
		sl1.setSerial(4000L);
		StatementList sl2 = mainDatagrid.generateWidgetStatements();
		sl2.setSerial(5000L);
		StatementList sl20 = childDatagrid.generateWidgetStatements();
		sl20.setSerial(5500L);
		StatementList sl3 = addDialog.generateWidgetStatements();
		sl3.setSerial(6000L);
		StatementList sl4 = updateDialog.generateWidgetStatements();
		sl4.setSerial(7000L);
		StatementList sl5 = viewDialog.generateWidgetStatements();
		sl5.setSerial(8000L);
		sList.add(sl1);
		sList.add(sl2);
		sList.add(sl20);
		sList.add(sl3);
		sList.add(sl4);
		sList.add(sl5);	
		
		StatementList sl30 = addChildDialog.generateWidgetStatements();
		sl30.setSerial(10000L);
		StatementList sl40 = updateChildDialog.generateWidgetStatements();
		sl40.setSerial(11000L);
		StatementList sl50 = viewChildDialog.generateWidgetStatements();
		sl50.setSerial(12000L);

		sList.add(sl30);
		sList.add(sl40);
		sList.add(sl50);
		sList.add(new Statement(9000L,0,"</div>"));
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		StatementList sl = tree.generateWidgetScriptStatements();
		sl.setSerial(1000L);
		sl.setIndent(1);
		sList.add(sl);
		StatementList sl1 = mainDatagrid.generateWidgetScriptStatements();
		sl1.setSerial(2000L);
		sl1.setIndent(1);
		sList.add(sl1);
		StatementList sl2 = childDatagrid.generateWidgetScriptStatements();
		sl2.setSerial(3000L);
		sl2.setIndent(1);
		sList.add(sl2);
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public boolean parse() {
		if (this.treeDomain!=null&&this.parentDomain!=null&&this.childDomain!=null) {
			this.tree = new TreePanel();
			this.searchPanel = new SearchPanel();
			this.mainDatagrid = new MainDatagrid();
			this.addDialog = new AddDialog();
			this.updateDialog = new UpdateDialog();
			this.viewDialog = new ViewDialog();
			
			this.childDatagrid = new ChildDatagrid();
			this.childDatagrid.setParentDomain(this.parentDomain);
			this.childDatagrid.setChildDomain(this.childDomain);
			this.childDatagrid.setParentId(this.parentId);
			this.addChildDialog = new AddDialog();
			this.updateChildDialog = new UpdateDialog();
			this.viewChildDialog = new ViewDialog();
			this.addChildDialog.setDetailPrefix("detail");
			this.updateChildDialog.setDetailPrefix("detail");
			this.viewChildDialog.setDetailPrefix("detail");
			
			this.tree.setParentTreeDomain(this.treeDomain);
			this.tree.setChildDomain(this.parentDomain);
			this.tree.setInnerTreeParentId(this.innerTreeParentId);
			this.tree.setTreeParentId(this.treeParentId);
			this.searchPanel.setDomain(this.parentDomain);
			this.mainDatagrid.setDomain(this.parentDomain);
			this.addDialog.setDomain(this.parentDomain);
			this.updateDialog.setDomain(this.parentDomain);
			this.viewDialog.setDomain(this.parentDomain);
			
			this.childDatagrid.setDomain(this.childDomain);
			this.addChildDialog.setDomain(this.childDomain);
			this.updateChildDialog.setDomain(this.childDomain);
			this.viewChildDialog.setDomain(this.childDomain);
			return true;
		}
		return false;
	}

	public Domain getTreeDomain() {
		return treeDomain;
	}

	public void setTreeDomain(Domain treeDomain) {
		this.treeDomain = treeDomain;
	}

	public Domain getParentDomain() {
		return parentDomain;
	}

	public void setParentDomain(Domain parentDomain) {
		this.parentDomain = parentDomain;
	}

	public Domain getChildDomain() {
		return childDomain;
	}

	public void setChildDomain(Domain childDomain) {
		this.childDomain = childDomain;
	}

	public TreePanel getTree() {
		return tree;
	}

	public void setTree(TreePanel tree) {
		this.tree = tree;
	}

	public SearchPanel getSearchPanel() {
		return searchPanel;
	}

	public void setSearchPanel(SearchPanel searchPanel) {
		this.searchPanel = searchPanel;
	}

	public MainDatagrid getMainDatagrid() {
		return mainDatagrid;
	}

	public void setMainDatagrid(MainDatagrid mainDatagrid) {
		this.mainDatagrid = mainDatagrid;
	}

	public AddDialog getAddDialog() {
		return addDialog;
	}

	public void setAddDialog(AddDialog addDialog) {
		this.addDialog = addDialog;
	}

	public UpdateDialog getUpdateDialog() {
		return updateDialog;
	}

	public void setUpdateDialog(UpdateDialog updateDialog) {
		this.updateDialog = updateDialog;
	}

	public ViewDialog getViewDialog() {
		return viewDialog;
	}

	public void setViewDialog(ViewDialog viewDialog) {
		this.viewDialog = viewDialog;
	}

	public ChildDatagrid getChildDatagrid() {
		return childDatagrid;
	}

	public void setChildDatagrid(ChildDatagrid childDatagrid) {
		this.childDatagrid = childDatagrid;
	}

	public AddDialog getAddChildDialog() {
		return addChildDialog;
	}

	public void setAddChildDialog(AddDialog addChildDialog) {
		this.addChildDialog = addChildDialog;
	}

	public UpdateDialog getUpdateChildDialog() {
		return updateChildDialog;
	}

	public void setUpdateChildDialog(UpdateDialog updateChildDialog) {
		this.updateChildDialog = updateChildDialog;
	}

	public ViewDialog getViewChildDialog() {
		return viewChildDialog;
	}

	public void setViewChildDialog(ViewDialog viewChildDialog) {
		this.viewChildDialog = viewChildDialog;
	}

	public Field getTreeParentId() {
		return treeParentId;
	}

	public void setTreeParentId(Field treeParentId) {
		this.treeParentId = treeParentId;
	}

	public Field getParentId() {
		return parentId;
	}

	public void setParentId(Field parentId) {
		this.parentId = parentId;
	}

	public Field getInnerTreeParentId() {
		return innerTreeParentId;
	}

	public void setInnerTreeParentId(Field innerTreeParentId) {
		this.innerTreeParentId = innerTreeParentId;
	}

}
