package org.light.easyuilayouts;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.AddDialog;
import org.light.easyuilayouts.widgets.ChildDatagrid;
import org.light.easyuilayouts.widgets.MainDatagrid;
import org.light.easyuilayouts.widgets.OracleMainDatagrid;
import org.light.easyuilayouts.widgets.SearchPanel;
import org.light.easyuilayouts.widgets.UpdateDialog;
import org.light.easyuilayouts.widgets.ViewDialog;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;

public class OracleEasyUIParentChild extends EasyUIParentChild{
	protected OracleMainDatagrid mainDatagrid;

	@Override
	public StatementList generateLayoutStatements() {
		List<Writeable> sList = new ArrayList<>();
		StatementList sl1 = searchPanel.generateWidgetStatements();
		sl1.setSerial(4000L);
		StatementList sl2 = mainDatagrid.generateWidgetStatements();
		sl2.setSerial(5000L);
		StatementList sl20 = childDatagrid.generateWidgetStatements();
		sl20.setSerial(5500L);
		StatementList sl3 = addDialog.generateWidgetStatements();
		sl3.setSerial(6000L);
		StatementList sl4 = updateDialog.generateWidgetStatements();
		sl4.setSerial(7000L);
		StatementList sl5 = viewDialog.generateWidgetStatements();
		sl5.setSerial(8000L);
		sList.add(sl1);
		sList.add(sl2);
		sList.add(sl20);
		sList.add(sl3);
		sList.add(sl4);
		sList.add(sl5);		
		
		StatementList sl30 = addChildDialog.generateWidgetStatements();
		sl30.setSerial(10000L);
		StatementList sl40 = updateChildDialog.generateWidgetStatements();
		sl40.setSerial(11000L);
		StatementList sl50 = viewChildDialog.generateWidgetStatements();
		sl50.setSerial(12000L);

		sList.add(sl30);
		sList.add(sl40);
		sList.add(sl50);
		
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		StatementList sl1 = mainDatagrid.generateWidgetScriptStatements();
		sl1.setSerial(1000L);
		sl1.setIndent(1);
		sList.add(sl1);
		
		StatementList sl2 = childDatagrid.generateWidgetScriptStatements();
		sl2.setSerial(2000L);
		sl2.setIndent(1);
		sList.add(sl2);
		
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public boolean parse() {
		if (this.parentDomain!=null&&this.childDomain!=null) {
			this.searchPanel = new SearchPanel();
			this.mainDatagrid = new OracleMainDatagrid();
			this.addDialog = new AddDialog();
			this.updateDialog = new UpdateDialog();
			this.viewDialog = new ViewDialog();
			
			this.childDatagrid = new ChildDatagrid();
			this.childDatagrid.setParentDomain(this.parentDomain);
			this.childDatagrid.setChildDomain(this.childDomain);
			this.childDatagrid.setParentId(this.parentId);
			this.childDatagrid.setDbType("oracle");
			this.addChildDialog = new AddDialog();
			this.updateChildDialog = new UpdateDialog();
			this.viewChildDialog = new ViewDialog();
			this.addChildDialog.setDetailPrefix("detail");
			this.updateChildDialog.setDetailPrefix("detail");
			this.viewChildDialog.setDetailPrefix("detail");

			this.searchPanel.setDomain(this.parentDomain);
			this.mainDatagrid.setDomain(this.parentDomain);
			this.addDialog.setDomain(this.parentDomain);
			this.updateDialog.setDomain(this.parentDomain);
			this.viewDialog.setDomain(this.parentDomain);
			
			this.childDatagrid.setDomain(this.childDomain);
			this.addChildDialog.setDomain(this.childDomain);
			this.updateChildDialog.setDomain(this.childDomain);
			this.viewChildDialog.setDomain(this.childDomain);
			return true;
		}
		return false;
	}

}
