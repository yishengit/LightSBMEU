package org.light.easyuilayouts.widgets;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.core.Ｗidget;
import org.light.domain.Domain;
import org.light.domain.DragonHideStatement;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.WriteableUtil;
import org.light.verb.FilterExcel;
import org.light.verb.FilterPDF;
import org.light.verb.FilterWord;

public class SearchPanel extends Ｗidget{
	protected Domain domain;
	@Override
	public StatementList generateWidgetStatements() {
		try {
			List<Writeable> sList = new ArrayList<>();
			if (this.domain.getLanguage().equalsIgnoreCase("english")){
				sList.add(new Statement(1000L,0,"<div title=\"Search Panel\" class=\"easyui-panel\" style=\"width:1600px;height:200px\">"));	
			}else {
				sList.add(new Statement(1000L,0,"<div title=\"搜索面板\" class=\"easyui-panel\" style=\"width:1600px;height:200px\">"));	
			}
			sList.add(new Statement(2000L,0,"<form id=\"ffsearch\" method=\"post\">"));
			sList.add(new Statement(3000L,0,"<table cellpadding=\"5\">"));
			
			serial = 4000L;
			List<Field> fields = new ArrayList<Field>();
			fields.addAll(this.domain.getSearchFields());
			fields.sort(new FieldSerialComparator());
			Field f1,f2,f3;
			for (int i=0;i<fields.size();i=i+3){
				f1 = fields.get(i); 
				sList.add(new Statement(serial,0,"<tr>"));
				sList.add(new DragonHideStatement(serial+1000L,0,"<td>"+f1.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f1.getLowerFirstFieldName()+"' id='"+f1.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f1 instanceof Dropdown)));
				if (f1 instanceof Dropdown)
					sList.add(new DragonHideStatement(serial+1000L,0,"<td>"+f1.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f1.getLowerFirstFieldName()+"' id='"+f1.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f1).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f1).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f1).getTarget().getLowerFirstDomainName()+((Dropdown)f1).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f1).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td>",f1 instanceof Dropdown));
				if (i < fields.size()-1) {
					f2 = fields.get(i+1);
					sList.add(new DragonHideStatement(serial+2000L,0,"<td>"+f2.getText()+":</td><td><input class='easyui-textbox' type='text' name='"+f2.getLowerFirstFieldName()+"' id='"+f2.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f2 instanceof Dropdown)));					
					if (f2 instanceof Dropdown)
						sList.add(new DragonHideStatement(serial+2000L,0,"<td>"+f2.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f2.getLowerFirstFieldName()+"' id='"+f2.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f2).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f2).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f2).getTarget().getLowerFirstDomainName()+((Dropdown)f2).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f2).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td>",f2 instanceof Dropdown));
				}else{
					sList.add(new Statement(serial+2000L,0,"<td>&nbsp;</td>"));
				}
				if (i < fields.size()-2) {
					f3 = fields.get(i+2);
					sList.add(new DragonHideStatement(serial+3000L,0,"<td>"+f3.getText()+":</td><td><input class='easyui-textbox' type='text' name='"+f3.getLowerFirstFieldName()+"' id='"+f3.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f3 instanceof Dropdown)));					
					if (f3 instanceof Dropdown)
						sList.add(new DragonHideStatement(serial+3000L,0,"<td>"+f3.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f3.getLowerFirstFieldName()+"' id='"+f3.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f3).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f3).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f3).getTarget().getLowerFirstDomainName()+((Dropdown)f3).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f3).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td>",f3 instanceof Dropdown));
				}else{
					sList.add(new Statement(serial+3000L,0,"<td>&nbsp;</td>"));
				}
				if (i==0) {
					sList.add(new Statement(serial+3100L,0,"<td>"));
					
					FilterExcel filterExcelDropdown = new FilterExcel(this.domain);
					FilterPDF filterPdfDropdown = new FilterPDF(this.domain);
					FilterWord filterWordDropdown = new FilterWord(this.domain);
					int dropdownindex = 2;
					if (!filterExcelDropdown.isDenied() || !filterPdfDropdown.isDenied()) {
						if  (this.domain.getLanguage().equalsIgnoreCase("english")){
							sList.add(new Statement(serial+3200L,2,"<select id=\"actionSelect\" class='easyui-select' style=\"	width:90px;height:28px\" onchange=\"toggleBtnShow(this.value)\">"));
							sList.add(new Statement(serial+3300L,3,"<option value=\"1\" selected>Search</option>"));
							if (!filterExcelDropdown.isDenied() ) sList.add(new Statement(serial+3400L,3,"<option value=\""+dropdownindex++ +"\">Filter Excel</option>"));
							if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+3500L,3,"<option value=\""+dropdownindex++ +"\">Filter PDF</option>"));
							if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+3550L,3,"<option value=\""+dropdownindex+"\">Filter Word</option>"));
							sList.add(new Statement(serial+3600L,2,"</select>"));
						}else {
							sList.add(new Statement(serial+3200L,2,"<select id=\"actionSelect\" class='easyui-select' style=\"	width:90px;height:28px\" onchange=\"toggleBtnShow(this.value)\">"));
							sList.add(new Statement(serial+3300L,3,"<option value=\"1\" selected>搜索</option>"));
							if (!filterExcelDropdown.isDenied() ) sList.add(new Statement(serial+3400L,3,"<option value=\""+dropdownindex++ +"\">Excel过滤</option>"));
							if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+3500L,3,"<option value=\""+dropdownindex++ +"\">PDF过滤</option>"));
							if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+3550L,3,"<option value=\""+dropdownindex+"\">Word过滤</option>"));
							sList.add(new Statement(serial+3600L,2,"</select>"));						
						}					
					}
					sList.add(new Statement(serial+3700L,2,"</td><td>"));
					sList.add(new Statement(serial+3800L,2,"<div id=\"button-bar\">"));
					if  (this.domain.getLanguage().equalsIgnoreCase("english")){
						sList.add(new Statement(serial+3900L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:block;width:110px;height:28px\" data-options=\"iconCls:'icon-search'\" onclick=\"search"+this.domain.getCapFirstPlural()+"ByFieldsByPage()\">Search</a>"));
						if (!filterExcelDropdown.isDenied()) sList.add(new Statement(serial+4000L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Excel()\">Filter Excel</a>"));
						if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+4100L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"PDF()\"\">Filter PDF</a>"));
						if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+4150L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Word()\"\">Filter Word</a>"));
					}else {
						sList.add(new Statement(serial+3900L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:block;width:110px;height:28px\" data-options=\"iconCls:'icon-search'\" onclick=\"search"+this.domain.getCapFirstPlural()+"ByFieldsByPage()\">搜索</a>"));
						if (!filterExcelDropdown.isDenied()) sList.add(new Statement(serial+4000L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Excel()\">Excel过滤</a>"));
						if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+4100L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"PDF()\"\">PDF过滤</a>"));
						if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+4150L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Word()\"\">Word过滤</a>"));
					}
					sList.add(new Statement(serial+4200L,2,"</div>"));
					sList.add(new Statement(serial+4300L,2,"</td><td>"));
					if  (this.domain.getLanguage().equalsIgnoreCase("english")){
						sList.add(new Statement(serial+4400L,3,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" style=\"width:80px\" data-options=\"iconCls:'icon-clear'\"  onclick=\"clearForm('ffsearch');toggleBtnShow(1);$('#actionSelect').val(1)\">Clear</a>"));
					}else {
						sList.add(new Statement(serial+4400L,3,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" style=\"width:80px\" data-options=\"iconCls:'icon-clear'\"  onclick=\"clearForm('ffsearch');toggleBtnShow(1);$('#actionSelect').val(1)\">清除</a>"));
					}
					sList.add(new Statement(serial+4500L,0,"</td>"));
				}else {
					sList.add(new Statement(serial+4600L,0,"<td></td><td colspan='3'></td>"));
				}
				sList.add(new Statement(serial+5000L,0,"</tr>"));
				serial += 6000L;
			}
			sList.add(new Statement(serial+1000L,0,"</table>"));
			sList.add(new Statement(serial+2000L,0,"</form>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));
			
			StatementList sl = WriteableUtil.merge(sList);
			sl.setSerial(this.serial);
			return sl;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public StatementList generateWidgetScriptStatements() {
		return null;
	}

	@Override
	public boolean parse() {
		// TODO Auto-generated method stub
		return false;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

}
