package org.light.easyuilayouts.widgets;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.complexverb.DomainFieldVerb;
import org.light.easyuilayouts.chiddgverb.UpdateUploadDomainField;
import org.light.core.Writeable;
import org.light.core.Ｗidget;
import org.light.domain.Domain;
import org.light.domain.DragonHideStatement;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyui.EasyUIPositions;
import org.light.easyuilayouts.chiddgverb.Activate;
import org.light.easyuilayouts.chiddgverb.ActivateAll;
import org.light.easyuilayouts.chiddgverb.Add;
import org.light.easyuilayouts.chiddgverb.AddUploadDomainField;
import org.light.easyuilayouts.chiddgverb.Clone;
import org.light.easyuilayouts.chiddgverb.CloneAll;
import org.light.easyuilayouts.chiddgverb.Delete;
import org.light.easyuilayouts.chiddgverb.DeleteAll;
import org.light.easyuilayouts.chiddgverb.SoftDelete;
import org.light.easyuilayouts.chiddgverb.SoftDeleteAll;
import org.light.easyuilayouts.chiddgverb.Toggle;
import org.light.easyuilayouts.chiddgverb.ToggleOne;
import org.light.easyuilayouts.chiddgverb.Update;
import org.light.easyuilayouts.chiddgverb.View;
import org.light.exception.ValidateException;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.Export;
import org.light.verb.ExportPDF;
import org.light.verb.ExportWord;
import org.light.verb.SearchByFieldsByPage;

public class ChildDatagrid extends Ｗidget{
	protected String detailPrefix = "detail";
	protected Domain parentDomain;
	protected Domain childDomain;
	protected Field parentId;
	@Override
	public StatementList generateWidgetStatements() {
		List<Writeable> sList = new ArrayList<>();
		if ("english".equalsIgnoreCase(this.childDomain.getLanguage())) {
			sList.add(new Statement(1000L,0,"<table id=\""+this.detailPrefix+"dg\" class=\"easyui-datagrid\" title=\""+this.childDomain.getText()+" List\" style=\"width:1600px;height:600px\" data-options=\"singleSelect:false,url:'../"+this.childDomain.getControllerPackagePrefix()+childDomain.getLowerFirstDomainName()+childDomain.getControllerNamingSuffix()+"/search"+childDomain.getCapFirstPlural()+"ByFieldsByPage',queryParams:"+this.detailPrefix+"params,method:'post',pagination:true,toolbar:"+this.detailPrefix+"toolbar\">"));
		}else {
			sList.add(new Statement(1000L,0,"<table id=\""+this.detailPrefix+"dg\" class=\"easyui-datagrid\" title=\""+this.childDomain.getText()+"清单\" style=\"width:1600px;height:600px\" data-options=\"singleSelect:false,url:'../"+this.childDomain.getControllerPackagePrefix()+childDomain.getLowerFirstDomainName()+childDomain.getControllerNamingSuffix()+"/search"+childDomain.getCapFirstPlural()+"ByFieldsByPage',queryParams:"+this.detailPrefix+"params,method:'post',pagination:true,toolbar:"+this.detailPrefix+"toolbar\">"));				
		}
		sList.add(new Statement(2000L,0,"<thead>"));
		sList.add(new Statement(3000L,0,"<tr>"));
		if (this.childDomain!=null&&this.childDomain.hasDomainId()) sList.add(new Statement(4000L,0,"<th data-options=\"field:'"+this.childDomain.getDomainId().getLowerFirstFieldName()+"',checkbox:true\">"+this.childDomain.getDomainId().getText()+"</th>"));
		serial = 5000L;
		List<Field> fields2 = new ArrayList<Field>();
		fields2.addAll(this.childDomain.getFieldsWithoutId());
		//fields2.sort(new FieldSerialComparator());
		for (Field f: fields2){
			if (f.getFieldType().equalsIgnoreCase("image")) {
				sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:140,formatter:show"+this.childDomain.getCapFirstDomainName()+f.getCapFirstFieldName()+"Image\">"+f.getText()+"</th>"));
			}else {
				sList.add(new DragonHideStatement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>",!(f instanceof Dropdown)));
				if (f instanceof Dropdown)
					sList.add(new DragonHideStatement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80,formatter:translate"+((Dropdown)f).getTarget().getCapFirstDomainName()+"\">"+f.getText()+"</th>",f instanceof Dropdown));
			}
			serial+=1000L;
		}
		sList.add(new Statement(serial,0,"</tr>"));
		sList.add(new Statement(serial+1000L,0,"</thead>"));
		sList.add(new Statement(serial+2000L,0,"</table>"));
		StatementList sl = WriteableUtil.merge(sList);
		sl.setSerial(this.serial);
		return sl;
	}

	@Override
	public StatementList generateWidgetScriptStatements() throws ValidateException{
		try {	
			List<Writeable> sList = new ArrayList<>();
			long serial = 0L;
			sList.add(new Statement(serial+10000L,0,"var "+this.detailPrefix+"params = {};"));			
			sList.add(new Statement(serial+11000L,0,"var "+this.detailPrefix+"pagesize = 10;"));
			sList.add(new Statement(serial+12000L,0,"var "+this.detailPrefix+"pagenum = 1;"));
			Set<DomainFieldVerb> domainFieldVerbs = new TreeSet<>();
			for (Field f:this.childDomain.getPlainFields()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					AddUploadDomainField addChildUpload=new AddUploadDomainField(this.childDomain,f);
					addChildUpload.setDetailPrefix("detail");
					UpdateUploadDomainField updateChildUpload=new UpdateUploadDomainField(this.childDomain,f);
					updateChildUpload.setDetailPrefix("detail");
 					domainFieldVerbs.add(addChildUpload);
					domainFieldVerbs.add(updateChildUpload);
				}
			}
			serial += 14000L;
			if (domainFieldVerbs!=null&&domainFieldVerbs.size()>0) {
				sList.add(new Statement(serial,0,"$(function () {"));	
				serial+=1000L;
				for (DomainFieldVerb dfv:domainFieldVerbs) {
					JavascriptBlock uploadBlock = dfv.generateEasyUIJSButtonBlock();
					StatementList  slupload = uploadBlock.getMethodStatementList();
					slupload.setSerial(serial);
					sList.add(slupload);
					serial+= 1000L;
				}
				sList.add(new Statement(serial+2000L,0,"});"));
			}
			sList.add(new Statement(serial+13000L,0,"var "+this.detailPrefix+"toolbar = ["));
			
			JavascriptBlock slViewJb = ((EasyUIPositions)new View(this.childDomain)).generateEasyUIJSButtonBlock();
			if (slViewJb != null) {
				StatementList  slView = slViewJb.getMethodStatementList();			
				slView.setSerial(serial+13010L);
				sList.add(slView);
				sList.add(new Statement(serial+13050L,0,","));
			}
			JavascriptBlock slAddJb = this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new Add(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slAddJb != null) {
				StatementList  slAdd = slAddJb.getMethodStatementList();			
				slAdd.setSerial(serial+13100L);
				sList.add(slAdd);
				sList.add(new Statement(serial+13150L,0,","));
			}
			
			JavascriptBlock slUpdateJb = this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new Update(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slUpdateJb != null) {
				StatementList  slUpdate =slUpdateJb.getMethodStatementList();
				slUpdate.setSerial(serial+13200L);
				sList.add(slUpdate);
				sList.add(new Statement(serial+13250L,0,","));
			}
			
			JavascriptBlock slSoftDeleteJb = this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new SoftDelete(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slSoftDeleteJb != null) {
				StatementList  slSoftDelete =slSoftDeleteJb.getMethodStatementList();
				slSoftDelete.setSerial(serial+13300L);
				sList.add(slSoftDelete);
				sList.add(new Statement(serial+13320L,0,","));
			}
			
			JavascriptBlock slActivateJb = this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new Activate(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slActivateJb != null) {
				StatementList  slActivate =slActivateJb.getMethodStatementList();
				slActivate.setSerial(serial+13340L);
				sList.add(slActivate);
				sList.add(new Statement(serial+13342L,0,","));
			}	
			
			JavascriptBlock slCloneJb = this.childDomain.hasDomainId()?((EasyUIPositions)new Clone(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slCloneJb != null) {
				StatementList  slClone=slCloneJb.getMethodStatementList();
				slClone.setSerial(serial+13345L);
				sList.add(slClone);			
				sList.add(new Statement(serial+13350L,0,","));
			}
			
			JavascriptBlock slDeleteJb = this.childDomain.hasDomainId()?((EasyUIPositions)new Delete(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slDeleteJb != null) {
				StatementList  slDelete =slDeleteJb.getMethodStatementList();
				slDelete.setSerial(serial+13400L);
				sList.add(slDelete);
				sList.add(new Statement(serial+13450L,0,","));
			}
			
			JavascriptBlock slToggleJb = this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new Toggle(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slToggleJb != null) {
				StatementList  slToggle =slToggleJb.getMethodStatementList();
				slToggle.setSerial(serial+13500L);
				sList.add(slToggle);
				sList.add(new Statement(serial+13550L,0,","));
			}
			
			JavascriptBlock slToggleOneJb = this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new ToggleOne(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slToggleOneJb != null) {
				StatementList  slToggleOne =slToggleOneJb.getMethodStatementList();
				slToggleOne.setSerial(serial+13600L);
				sList.add(slToggleOne);
				sList.add(new Statement(serial+13650L,0,",'-',"));
			}
			
			JavascriptBlock slSoftDeleteAllJb = this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new SoftDeleteAll(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slSoftDeleteAllJb != null) {
				StatementList  slSoftDeleteAll =slSoftDeleteAllJb.getMethodStatementList();
				slSoftDeleteAll.setSerial(serial+13700L);
				sList.add(slSoftDeleteAll);
				sList.add(new Statement(serial+13710L,0,","));
			}
			
			JavascriptBlock slActivateAllJb = this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new ActivateAll(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slActivateAllJb != null) {
				StatementList  slActivateAll =slActivateAllJb.getMethodStatementList();
				slActivateAll.setSerial(serial+13720L);
				sList.add(slActivateAll);			
				sList.add(new Statement(serial+13722L,0,","));
			}
			
			JavascriptBlock slCloneAllJb = this.childDomain.hasDomainId()?((EasyUIPositions)new CloneAll(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slCloneAllJb != null) {
				StatementList  slCloneAll =slCloneAllJb.getMethodStatementList();
				slCloneAll.setSerial(serial+13725L);
				sList.add(slCloneAll);			
				sList.add(new Statement(serial+13750L,0,","));
			}
			
			JavascriptBlock slDeleteAllJb = this.childDomain.hasDomainId()?((EasyUIPositions)new DeleteAll(this.childDomain)).generateEasyUIJSButtonBlock():null;
			if (slDeleteAllJb != null) {
				StatementList  slDeleteAll =slDeleteAllJb.getMethodStatementList();
				slDeleteAll.setSerial(serial+13800L);
				sList.add(slDeleteAll);				
				sList.add(new Statement(serial+13850L,0,","));
			}
			
			JavascriptBlock slExportJb = ((EasyUIPositions)new Export(this.childDomain)).generateEasyUIJSButtonBlock();
			if (slExportJb != null) {
				StatementList  slExport =slExportJb.getMethodStatementList();
				slExport.setSerial(serial+13900L);
				sList.add(slExport);			
				sList.add(new Statement(serial+13950L,0,","));
			}
			
			JavascriptBlock slExportPDFJb = ((EasyUIPositions)new ExportPDF(this.childDomain)).generateEasyUIJSButtonBlock();
			if (slExportPDFJb != null) {
				StatementList  slExportPDF =slExportPDFJb.getMethodStatementList();
				slExportPDF.setSerial(serial+13960L);
				sList.add(slExportPDF);
				sList.add(new Statement(serial+13970L,0,","));
			}
			
			JavascriptBlock slExportWordJb = ((EasyUIPositions)new ExportWord(this.childDomain)).generateEasyUIJSButtonBlock();
			if (slExportWordJb != null) {
				StatementList  slExportWord =slExportWordJb.getMethodStatementList();
				slExportWord.setSerial(serial+13980L);
				sList.add(slExportWord);
				sList.add(new Statement(serial+13990L,0,","));
			}
			
			// remove last ','
			if (((Statement) sList.get(sList.size()-1)).getContent().contentEquals(",")) {
				sList.remove(sList.size()-1);
			}
			
			sList.add(new Statement(serial+14000L,0,"];"));
			
//			sList.add(new Statement(serial+15000L,0,"$(document).ready(function(){"));
//			sList.add(new Statement(serial+16000L,0,"$(\"#dg\").datagrid(\"load\");"));
//			sList.add(new Statement(serial+17000L,0,"});"));
//	
//			sList.add(new Statement(serial+18000L,0,"function clearForm(formId){"));
//			sList.add(new Statement(serial+19000L,0,"$('#'+formId).form('clear');"));
//			sList.add(new Statement(serial+20000L,0,"}"));
			
			JavascriptMethod slAddJm = this.childDomain.hasActiveField()? ((EasyUIPositions)new Add(this.childDomain,this.technicalStack,this.dbType)).generateEasyUIJSActionMethod():null;
			if(	slAddJm != null) {
				StatementList slAddAction = slAddJm.generateMethodStatementListIncludingContainer(serial+21000L,0);
				sList.add(slAddAction);
			}
			
			JavascriptMethod slUpdateJm =  this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new Update(this.childDomain,this.technicalStack,this.dbType)).generateEasyUIJSActionMethod():null;
			if(	slUpdateJm != null) {
				StatementList slUpdateAction = slUpdateJm.generateMethodStatementListIncludingContainer(serial+22000L,0);
				sList.add(slUpdateAction);
			}
			
			JavascriptMethod slSoftDeleteJm =  this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new SoftDelete(this.childDomain)).generateEasyUIJSActionMethod():null;
			if(	slSoftDeleteJm != null) {
				StatementList slSoftDeleteAction = slSoftDeleteJm.generateMethodStatementListIncludingContainer(serial+23000L,0);
				sList.add(slSoftDeleteAction);
			}
			
			JavascriptMethod slActivateJm =  this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new Activate(this.childDomain)).generateEasyUIJSActionMethod():null;
			if(	slActivateJm != null) {
				StatementList slActivateAction = slActivateJm.generateMethodStatementListIncludingContainer(serial+24000L,0);
				sList.add(slActivateAction);
			}
			
			JavascriptMethod slCloneJm =  this.childDomain.hasDomainId()?((EasyUIPositions)new Clone(this.childDomain)).generateEasyUIJSActionMethod():null;
			if(	slCloneJm != null) {
				StatementList slCloneAction = slCloneJm.generateMethodStatementListIncludingContainer(serial+25000L,0);
				sList.add(slCloneAction);
			}
			
			JavascriptMethod slDeleteJm = this.childDomain.hasDomainId()? ((EasyUIPositions)new Delete(this.childDomain)).generateEasyUIJSActionMethod():null;
			if(	slDeleteJm != null) {
				StatementList slDeleteAction = slDeleteJm.generateMethodStatementListIncludingContainer(serial+28000L,0);
				sList.add(slDeleteAction);
			}
			
			JavascriptMethod slToggleJm =  this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new Toggle(this.childDomain)).generateEasyUIJSActionMethod():null;
			if(	slToggleJm != null) {
				StatementList slToggleAction = slToggleJm.generateMethodStatementListIncludingContainer(serial+28200L,0);
				sList.add(slToggleAction);
			}
			
			JavascriptMethod slToggleOneJm = this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()? ((EasyUIPositions)new ToggleOne(this.childDomain)).generateEasyUIJSActionMethod():null;
			if(	slToggleOneJm != null) {
				StatementList slToggleOneAction = slToggleOneJm.generateMethodStatementListIncludingContainer(serial+28400L,0);
				sList.add(slToggleOneAction);
			}
			
			JavascriptMethod slSoftDeleteAllJm =  this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new SoftDeleteAll(this.childDomain)).generateEasyUIJSActionMethod():null;
			if(	slSoftDeleteAllJm != null) {
				StatementList slSoftDeleteAllAction = slSoftDeleteAllJm.generateMethodStatementListIncludingContainer(serial+28600L,0);
				sList.add(slSoftDeleteAllAction);
			}
			
			JavascriptMethod slActivateAllJm =  this.childDomain.hasDomainId()&&this.childDomain.hasActiveField()?((EasyUIPositions)new ActivateAll(this.childDomain)).generateEasyUIJSActionMethod():null;
			if(	slActivateAllJm != null) {
				StatementList slActivateAllAction = slActivateAllJm.generateMethodStatementListIncludingContainer(serial+28800L,0);
				sList.add(slActivateAllAction);
			}
			
			JavascriptMethod slCloneAllJm = this.childDomain.hasDomainId()? ((EasyUIPositions)new CloneAll(this.childDomain)).generateEasyUIJSActionMethod():null;
			if(	slCloneAllJm != null) {
				StatementList slCloneAllAction = slCloneAllJm.generateMethodStatementListIncludingContainer(serial+29000L,0);
				sList.add(slCloneAllAction);
			}
			
			JavascriptMethod slDeleteAllJm =  this.childDomain.hasDomainId()? ((EasyUIPositions)new DeleteAll(this.childDomain)).generateEasyUIJSActionMethod():null;
			if(	slDeleteAllJm != null) {
				StatementList slDeleteAllAction = slDeleteAllJm.generateMethodStatementListIncludingContainer(serial+29200L,0);
				sList.add(slDeleteAllAction);
			}
			
			JavascriptMethod slSearchByFieldsByPageJm =  ((EasyUIPositions)new SearchByFieldsByPage(this.childDomain)).generateEasyUIJSActionMethod();
			if(	slSearchByFieldsByPageJm != null) {
				StatementList slSearchByFieldsByPageAction = slSearchByFieldsByPageJm.generateMethodStatementListIncludingContainer(serial+29400L,0);
				sList.add(slSearchByFieldsByPageAction);
			}
			
			serial = serial+33000L;
			List<Field> fields5 = new ArrayList<Field>();
			fields5.addAll(this.childDomain.getFieldsWithoutIdAndActive());
			fields5.sort(new FieldSerialComparator());
			List<Domain> translateDomains = new ArrayList<Domain>();
			for (Field f: fields5){
				if (f instanceof Dropdown){
					Dropdown dp = (Dropdown)f;
					Domain target = dp.getTarget();
					if (!target.isLegacy()&&	!DomainUtil.inDomainList(target, translateDomains)){
						sList.add(dp.generateTranslateMethod().generateMethodStatementList(serial));
						serial+=1000L;
						translateDomains.add(target);
					}
				}
			}
			
			for (Field f: fields5){
				if (f.getFieldType().equalsIgnoreCase("image")) {
					AddUploadDomainField audf = new AddUploadDomainField(this.childDomain,f);
					sList.add(audf.generateEasyUIJSActionMethod().generateMethodStatementList(serial));
					serial+=1000L;
				}
			}
			
			sList.add(new Statement(serial+1000L,0,"$(function(){"));
			sList.add(new Statement(serial+2000L,1,"$(\"#dg\").datagrid({"));
			sList.add(new Statement(serial+3000L,2,"onLoadSuccess:function(){"));
			sList.add(new Statement(serial+4000L,3,"var rows = $(\"#dg\").datagrid(\"getChecked\");"));
			sList.add(new Statement(serial+5000L,3,"if (rows.length==0){"));
			sList.add(new Statement(serial+6000L,4,"$(\"#dg\").datagrid(\"selectRow\",0);"));
			sList.add(new Statement(serial+7000L,3,"}"));
			sList.add(new Statement(serial+8000L,2,"},"));
			sList.add(new Statement(serial+9000L,2,"onCheck:function(rowIndex,rowData){"));
			sList.add(new Statement(serial+10000L,3,"var id = rowData[\""+this.parentDomain.getDomainId().getLowerFirstFieldName()+"\"];"));
			sList.add(new Statement(serial+11000L,3,"params = {"));
			sList.add(new Statement(serial+12000L,4,this.parentId.getLowerFirstFieldName()+":id,"));
			sList.add(new Statement(serial+13000L,3,"};"));
			sList.add(new Statement(serial+14000L,3,"$(\"#"+this.detailPrefix+"dg\").datagrid({queryParams:params});"));
			sList.add(new Statement(serial+15000L,2,"}"));
			sList.add(new Statement(serial+16000L,1,"});"));
			sList.add(new Statement(serial+17000L,0,"})"));

			StatementList sl = WriteableUtil.merge(sList);
			sl.setSerial(this.serial);
			return sl;
		}catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ValidateException) throw (ValidateException)e;
			return null;
		}
	}

	@Override
	public boolean parse() {
		return true;
	}

	public Domain getDomain() {
		return childDomain;
	}

	public void setDomain(Domain domain) {
		this.childDomain = domain;
	}

	public String getDetailPrefix() {
		return detailPrefix;
	}

	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}

	public Field getParentId() {
		return parentId;
	}

	public void setParentId(Field parentId) {
		this.parentId = parentId;
	}

	public Domain getParentDomain() {
		return parentDomain;
	}

	public void setParentDomain(Domain parentDomain) {
		this.parentDomain = parentDomain;
	}

	public Domain getChildDomain() {
		return childDomain;
	}

	public void setChildDomain(Domain childDomain) {
		this.childDomain = childDomain;
	}

}
