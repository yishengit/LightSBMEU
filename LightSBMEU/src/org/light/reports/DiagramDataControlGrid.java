package org.light.reports;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.complexverb.DomainFieldVerb;
import org.light.core.Writeable;
import org.light.core.Ｗidget;
import org.light.domain.Domain;
import org.light.domain.DragonHideStatement;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyui.EasyUIPositions;
import org.light.easyuilayouts.chiddgverb.ActivateAll;
import org.light.easyuilayouts.chiddgverb.AddUploadDomainField;
import org.light.easyuilayouts.chiddgverb.SoftDeleteAll;
import org.light.easyuilayouts.chiddgverb.UpdateUploadDomainField;
import org.light.easyuilayouts.chiddgverb.View;
import org.light.generator.NamedS2SMJavascriptMethodGenerator;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.SearchByFieldsByPage;

public class DiagramDataControlGrid extends Ｗidget{
	protected Domain domain;
	protected String detailPrefix;
	protected Set<Domain> tranlateDomains = new TreeSet<>();
	protected Set<Domain> parentTranlateDomains = new TreeSet<>();
	
	public DiagramDataControlGrid(String detailPrefix) {
		super();
		this.detailPrefix = detailPrefix;
	}
	
	@Override
	public StatementList generateWidgetStatements() {
		List<Writeable> sList = new ArrayList<>();
		if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
			sList.add(new Statement(1000L,0,"<table id=\""+this.detailPrefix+"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+" List\" style=\"width:1600px;height:600px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:"+this.detailPrefix+"params,method:'post',pagination:true,toolbar:"+this.detailPrefix+"toolbar\">"));
		}else{
			sList.add(new Statement(1000L,0,"<table id=\""+this.detailPrefix+"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+"清单\" style=\"width:1600px;height:600px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:"+this.detailPrefix+"params,method:'post',pagination:true,toolbar:"+this.detailPrefix+"toolbar\">"));
		}
		sList.add(new Statement(2000L,0,"<thead>"));
		sList.add(new Statement(3000L,0,"<tr>"));
		if (this.domain!=null&&this.domain.hasDomainId()) sList.add(new Statement(4000L,0,"<th data-options=\"field:'"+this.domain.getDomainId().getLowerFirstFieldName()+"',checkbox:true\">"+this.domain.getDomainId().getText()+"</th>"));
		serial = 5000L;
		List<Field> fields2 = new ArrayList<Field>();
		fields2.addAll(this.domain.getFieldsWithoutId());
		fields2.sort(new FieldSerialComparator());
		for (Field f: fields2){
			if (f.getFieldType().equalsIgnoreCase("image")) {
				sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:140,formatter:show"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"Image\">"+f.getText()+"</th>"));
			}else {
				sList.add(new DragonHideStatement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>",!(f instanceof Dropdown)));
				if (f instanceof Dropdown)
					sList.add(new DragonHideStatement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80,formatter:translate"+((Dropdown)f).getTarget().getCapFirstDomainName()+"\">"+f.getText()+"</th>",f instanceof Dropdown));
			}
			serial+=1000L;
		}
		sList.add(new Statement(serial,0,"</tr>"));
		sList.add(new Statement(serial+1000L,0,"</thead>"));
		sList.add(new Statement(serial+2000L,0,"</table>"));
		StatementList sl = WriteableUtil.merge(sList);
		sl.setSerial(this.serial);
		return sl;
	}

	@Override
	public StatementList generateWidgetScriptStatements() {
		try {
			List<Writeable> sList = new ArrayList<>();
			long serial = 0L;
			sList.add(new Statement(1000L,0,"var "+this.getDetailPrefix()+"params = {};"));			
			sList.add(new Statement(2000L,0,"var "+this.getDetailPrefix()+"pagesize = 10;"));
			sList.add(new Statement(3000L,0,"var "+this.getDetailPrefix()+"pagenum = 1;"));
			Set<DomainFieldVerb> domainFieldVerbs = new TreeSet<>();
			for (Field f:this.domain.getPlainFields()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					domainFieldVerbs.add(new AddUploadDomainField(this.domain,f));
					domainFieldVerbs.add(new UpdateUploadDomainField(this.domain,f));
				}
			}
			serial = 4000L;
			if (domainFieldVerbs!=null&&domainFieldVerbs.size()>0) {
				sList.add(new Statement(serial,0,"$(function () {"));	
				serial+=1000L;
				for (DomainFieldVerb dfv:domainFieldVerbs) {
					JavascriptBlock uploadBlock = dfv.generateEasyUIJSButtonBlock();
					StatementList  slupload = uploadBlock.getMethodStatementList();
					slupload.setSerial(serial);
					sList.add(slupload);
					serial+= 1000L;
				}
				sList.add(new Statement(serial+2000L,0,"});"));
			}
			sList.add(new Statement(serial+13000L,0,"var "+this.detailPrefix+"toolbar = ["));
			JavascriptBlock slViewJb = ((EasyUIPositions)new View(this.domain,this.getDetailPrefix())).generateEasyUIJSButtonBlock();
			if (slViewJb != null) {
				StatementList  slView = slViewJb.getMethodStatementList();			
				slView.setSerial(serial+13010L);
				sList.add(slView);
				sList.add(new Statement(serial+13050L,0,","));
			}
					
			JavascriptBlock slSoftDeleteAllJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDeleteAll(this.domain,this.getDetailPrefix())).generateEasyUIJSButtonBlock():null;
			if (slSoftDeleteAllJb != null) {
				StatementList  slSoftDeleteAll =slSoftDeleteAllJb.getMethodStatementList();
				slSoftDeleteAll.setSerial(serial+13700L);
				sList.add(slSoftDeleteAll);
				sList.add(new Statement(serial+13710L,0,","));
			}
			
			JavascriptBlock slActivateAllJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ActivateAll(this.domain,this.getDetailPrefix())).generateEasyUIJSButtonBlock():null;
			if (slActivateAllJb != null) {
				StatementList  slActivateAll =slActivateAllJb.getMethodStatementList();
				slActivateAll.setSerial(serial+13720L);
				sList.add(slActivateAll);			
				sList.add(new Statement(serial+13722L,0,","));
			}
			
			// remove last ','
			if (((Statement) sList.get(sList.size()-1)).getContent().contentEquals(",")) {
				sList.remove(sList.size()-1);
			}
			
			sList.add(new Statement(serial+14000L,0,"];"));

			JavascriptMethod slSoftDeleteAllJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDeleteAll(this.domain,this.getDetailPrefix())).generateEasyUIJSActionMethod():null;
			if(	slSoftDeleteAllJm != null) {
				StatementList slSoftDeleteAllAction = slSoftDeleteAllJm.generateMethodStatementListIncludingContainer(serial+28600L,0);
				sList.add(slSoftDeleteAllAction);
			}
			
			JavascriptMethod slActivateAllJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ActivateAll(this.domain,this.getDetailPrefix())).generateEasyUIJSActionMethod():null;
			if(	slActivateAllJm != null) {
				StatementList slActivateAllAction = slActivateAllJm.generateMethodStatementListIncludingContainer(serial+28800L,0);
				sList.add(slActivateAllAction);
			}
						
			JavascriptMethod slSearchByFieldsByPageJm =  ((EasyUIPositions)new SearchByFieldsByPage(this.domain)).generateEasyUIJSActionMethod();
			if(	slSearchByFieldsByPageJm != null) {
				StatementList slSearchByFieldsByPageAction = slSearchByFieldsByPageJm.generateMethodStatementListIncludingContainer(serial+29400L,0);
				sList.add(slSearchByFieldsByPageAction);
			}
			
			if (this.getParentTranlateDomains()== null || this.getParentTranlateDomains().size()==0) {
				sList.add(NamedS2SMJavascriptMethodGenerator.generateParseBooleanMethod().generateMethodStatementList(serial+30000L));
//				sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementList(serial+31000L));
				sList.add(NamedS2SMJavascriptMethodGenerator.generateCheckRadioBoxValueMethod().generateMethodStatementList(serial+32000L));
				sList.add(NamedS2SMJavascriptMethodGenerator.generateToggleBtnShowMethod().generateMethodStatementList(serial+32500L));
			}
			
			serial = serial+33000L;
			List<Field> fields5 = new ArrayList<Field>();
			fields5.addAll(this.domain.getFieldsWithoutIdAndActive());
			fields5.sort(new FieldSerialComparator());
			
			List<Domain> translateDomains = new ArrayList<Domain>();
			translateDomains.addAll(this.parentTranlateDomains);
			for (Field f: fields5){
				if (f instanceof Dropdown){
					Dropdown dp = (Dropdown)f;
					Domain target = dp.getTarget();
					if (!target.isLegacy()&&	!DomainUtil.inDomainList(target, translateDomains)){
						sList.add(dp.generateTranslateMethod().generateMethodStatementList(serial));
						serial+=1000L;
						translateDomains.add(target);
					}
				}
			}
			this.tranlateDomains.addAll(translateDomains);
			
			for (Field f: fields5){
				if (f.getFieldType().equalsIgnoreCase("image")) {
					AddUploadDomainField audf = new AddUploadDomainField(this.domain,f);
					sList.add(audf.generateEasyUIJSActionMethod().generateMethodStatementList(serial));
					serial+=1000L;
				}
			}
			StatementList sl = WriteableUtil.merge(sList);
			sl.setSerial(this.serial);
			return sl;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean parse() {
		return true;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public String getDetailPrefix() {
		return detailPrefix;
	}

	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}

	public Set<Domain> getTranlateDomains() {
		return tranlateDomains;
	}

	public void setTranlateDomains(Set<Domain> tranlateDomains) {
		this.tranlateDomains = tranlateDomains;
	}

	public Set<Domain> getParentTranlateDomains() {
		return parentTranlateDomains;
	}

	public void setParentTranlateDomains(Set<Domain> parentTranlateDomains) {
		this.parentTranlateDomains = parentTranlateDomains;
	}

}
