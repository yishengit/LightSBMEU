package org.light.reports.verbs;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.easyui.EasyUIPositions;
import org.light.utils.FieldUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ListActiveOption extends Verb implements EasyUIPositions{	
	protected List<Field> xAxisFields = new ArrayList<>();
	protected Field yName;
	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		return null;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setStandardName("listActive"+StringUtil.capFirst(this.domain.getCapFirstPlural()+"Option"));
		method.addSignature(new Signature(1,"chartstype","var"));
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(78000L,1,"var option = {};"));
		sList.add(new Statement(79000L,1,"option.title={ text: ''};"));
		sList.add(new Statement(80000L,1,"option.tooltip={};"));
		sList.add(new Statement(81000L,1,"option.xAxis={data: ["+FieldUtil.genFieldArrayTextQStr(this.xAxisFields)+"]};"));
		sList.add(new Statement(82000L,1,"option.yAxis={};"));
		sList.add(new Statement(83000L,1,"var planData = [];"));
		sList.add(new Statement(84000L,1,"var legendData = [];"));
		sList.add(new Statement(85000L,1,"$.ajax({"));
		sList.add(new Statement(86000L,2,"type: \"post\","));
		sList.add(new Statement(87000L,2,"url: \"../"+this.domain.getControllerPackagePrefix()+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/listActive"+this.domain.getCapFirstPlural()+"\","));
		sList.add(new Statement(88000L,2,"dataType: 'json',"));
		sList.add(new Statement(89000L,2,"async:false,"));
		sList.add(new Statement(90000L,2,"contentType:\"application/json;charset=UTF-8\","));
		sList.add(new Statement(91000L,2,"success: function(data, textStatus) {"));
		sList.add(new Statement(92000L,3,"if (data.success) {"));
		sList.add(new Statement(93000L,4,"$.each(data.rows, function (i, item) {"));
		sList.add(new Statement(94000L,5,"var itemdata = {};"));
		if (this.yName instanceof Dropdown) {
			sList.add(new Statement(95000L,5,"itemdata.name = translate"+((Dropdown)this.yName).getTarget().getCapFirstDomainName()+"(item."+this.yName.getLowerFirstFieldName()+");"));
		}else {
			sList.add(new Statement(95000L,5,"itemdata.name = item."+this.yName.getLowerFirstFieldName()+";"));
		}
		sList.add(new Statement(96000L,5,""));
		sList.add(new Statement(97000L,5,"legendData.push(itemdata.name);"));
		sList.add(new Statement(98000L,5,"itemdata.type = chartstype;"));
		sList.add(new Statement(99000L,5,"var itemdatadata = ["+FieldUtil.genFieldArrayLowerFirstNameWithPrefixStr(this.xAxisFields, "item.")+"]"));
		sList.add(new Statement(102000L,5,"itemdata.data = itemdatadata;"));
		sList.add(new Statement(103000L,5,"planData.push(itemdata);"));
		sList.add(new Statement(104000L,3,"});"));
		sList.add(new Statement(105000L,3,"}"));
		sList.add(new Statement(106000L,3,"},"));
		sList.add(new Statement(107000L,2,"complete : function(XMLHttpRequest, textStatus) {"));
		sList.add(new Statement(108000L,2,"},"));
		sList.add(new Statement(109000L,2,"error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sList.add(new Statement(110000L,3,"alert(\"Error:\"+textStatus);"));
		sList.add(new Statement(111000L,3,"alert(errorThrown.toString());"));
		sList.add(new Statement(112000L,2,"}"));
		sList.add(new Statement(113000L,1,"});"));
		sList.add(new Statement(114000L,1,""));
		sList.add(new Statement(115000L,1,"option.legend = {},"));
		sList.add(new Statement(116000L,1,"option.legend.data = legendData;"));
		sList.add(new Statement(117000L,1,"option.series = planData;"));
		sList.add(new Statement(118000L,1,"debugger;"));
		sList.add(new Statement(119000L,1,"return option;"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception{
		return generateEasyUIJSActionMethod().generateMethodContentString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodContentString();
	}
	
	public ListActiveOption(Domain domain, List<Field> xAxisFields, Field yName) {
		super();
		this.setVerbName("ListActiveOption");
		this.setLabel("列出活跃记录图表");
		this.domain = domain;
		this.xAxisFields = xAxisFields;
		this.yName = yName;
	}

	public List<Field> getxAxisFields() {
		return xAxisFields;
	}

	public void setxAxisFields(List<Field> xAxisFields) {
		this.xAxisFields = xAxisFields;
	}

	public Field getyName() {
		return yName;
	}

	public void setyName(Field yName) {
		this.yName = yName;
	}

}
