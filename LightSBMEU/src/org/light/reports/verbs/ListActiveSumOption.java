package org.light.reports.verbs;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.easyui.EasyUIPositions;
import org.light.utils.FieldUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ListActiveSumOption extends Verb implements EasyUIPositions{
	protected List<Field> xAxisFields = new ArrayList<>();
	protected Field yName;
	@Override
	public Method generateDaoImplMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setStandardName("listActive"+StringUtil.capFirst(this.domain.getCapFirstPlural()+"SumOption"));
		method.addSignature(new Signature(1,"chartstype","var"));
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"var option = {};"));
		sList.add(new Statement(2000L,1,"option.title={ text: ''};"));
		sList.add(new Statement(3000L,1,"option.tooltip={};"));
		sList.add(new Statement(4000L,1,"option.xAxis={data: ["+FieldUtil.genFieldArrayTextQStr(this.xAxisFields)+"]};"));
		sList.add(new Statement(5000L,1,"option.yAxis={};"));
		sList.add(new Statement(6000L,1,"var planData = [];"));
		sList.add(new Statement(7000L,1,"var legendData = [];"));
		sList.add(new Statement(8000L,1,"$.ajax({"));
		sList.add(new Statement(9000L,2,"type: \"post\","));
		sList.add(new Statement(10000L,2,"url: \"../"+this.domain.getControllerPackagePrefix()+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/listActive"+this.domain.getCapFirstPlural()+"\","));
		sList.add(new Statement(11000L,2,"dataType: 'json',"));
		sList.add(new Statement(12000L,2,"async:false,"));
		sList.add(new Statement(13000L,2,"contentType:\"application/json;charset=UTF-8\","));
		sList.add(new Statement(14000L,2,"success: function(data, textStatus) {"));
		sList.add(new Statement(15000L,3,"if (data.success) {"));
		sList.add(new Statement(16000L,4,"$.each(data.rows, function (i, item) {"));
		sList.add(new Statement(17000L,5,"var itemdata = {};"));
		if (this.yName instanceof Dropdown) {
			sList.add(new Statement(18000L,5,"itemdata.name = translate"+((Dropdown)this.yName).getTarget().getCapFirstDomainName()+"(item."+this.yName.getLowerFirstFieldName()+");"));
		}else {
			sList.add(new Statement(18000L,5,"itemdata.name = item."+this.yName.getLowerFirstFieldName()+";"));
		}sList.add(new Statement(19000L,5,""));
		sList.add(new Statement(20000L,5,"legendData.push(itemdata.name);"));
		sList.add(new Statement(21000L,5,"itemdata.type = chartstype;"));
		sList.add(new Statement(22000L,5,"var itemdatadata = ["+FieldUtil.genFieldArrayLowerFirstNameWithPrefixStr(this.xAxisFields, "item.")+"]"));
		sList.add(new Statement(23000L,5,"var itemdatasumdata = [];"));
		sList.add(new Statement(26000L,5,"for (var i=0;i<itemdatadata.length;i++){"));
		sList.add(new Statement(27000L,6,"var sum = 0;"));
		sList.add(new Statement(28000L,6,"for (var j=0;j<=i;j++){"));
		sList.add(new Statement(29000L,7,"sum += itemdatadata[j]"));
		sList.add(new Statement(30000L,6,"}"));
		sList.add(new Statement(31000L,6,"itemdatasumdata.push(sum);"));
		sList.add(new Statement(32000L,5,"}"));
		sList.add(new Statement(33000L,5,"itemdata.data = itemdatasumdata;"));
		sList.add(new Statement(34000L,5,"planData.push(itemdata);"));
		sList.add(new Statement(35000L,3,"});"));
		sList.add(new Statement(36000L,3,"}"));
		sList.add(new Statement(37000L,3,"},"));
		sList.add(new Statement(38000L,2,"complete : function(XMLHttpRequest, textStatus) {"));
		sList.add(new Statement(39000L,2,"},"));
		sList.add(new Statement(40000L,2,"error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sList.add(new Statement(41000L,3,"alert(\"Error:\"+textStatus);"));
		sList.add(new Statement(42000L,3,"alert(errorThrown.toString());"));
		sList.add(new Statement(43000L,2,"}"));
		sList.add(new Statement(44000L,1,"});"));
		sList.add(new Statement(45000L,1,""));
		sList.add(new Statement(46000L,1,"option.legend = {},"));
		sList.add(new Statement(47000L,1,"option.legend.data = legendData;"));
		sList.add(new Statement(48000L,1,"option.series = planData;"));
		sList.add(new Statement(49000L,1,"debugger;"));
		sList.add(new Statement(50000L,1,"return option;"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public ListActiveSumOption(Domain domain, List<Field> xAxisFields, Field yName) {
		super();
		this.setVerbName("ListActiveOption");
		this.setLabel("列出活跃记录图表");
		this.domain = domain;
		this.xAxisFields = xAxisFields;
		this.yName = yName;
	}

	public List<Field> getxAxisFields() {
		return xAxisFields;
	}

	public void setxAxisFields(List<Field> xAxisFields) {
		this.xAxisFields = xAxisFields;
	}

	public Field getyName() {
		return yName;
	}

	public void setyName(Field yName) {
		this.yName = yName;
	}
}
