package org.light.reports;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Writeable;
import org.light.core.Ｗidget;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.generator.NamedS2SMJavascriptMethodGenerator;
import org.light.reports.verbs.ListActiveOption;
import org.light.reports.verbs.ListActiveSumOption;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;

public class EchartsDiagram extends Ｗidget{
	protected Domain domain;
	protected List<Field> xAxisFields = new ArrayList<>();
	protected Field yName;
	protected String detailPrefix = "";
	protected Set<Domain> tranlateDomains = new TreeSet<>();
	protected Set<Domain> parentTranlateDomains = new TreeSet<>();
	protected JavascriptMethod listOption;
	protected JavascriptMethod listSumOption;
	
	@Override
	public StatementList generateWidgetStatements() {
		List<Writeable> sList = new ArrayList<>();
		sList.add(new Statement(66000L,0,"<div data-options=\"region:'center',title:'计划图表'\">"));
		sList.add(new Statement(67000L,0,"<input class=\"easyui-combobox\" id=\"chartstype\" name=\"chartstype\" value='line' onSelect=\"setup()\" data-options=\"valueField: 'value',textField: 'label', data: ["));
		sList.add(new Statement(68000L,0,"{value: 'line',label: 'line'},"));
		sList.add(new Statement(69000L,0,"{value: 'bar',label: 'bar'},"));
		sList.add(new Statement(70000L,0,"{value: 'pie',label: 'pie'},]\"/>"));
		sList.add(new Statement(71000L,0,"<input class=\"easyui-combobox\" id=\"reporttype\" name=\"reporttype\" value='original' onSelect=\"setup()\" data-options=\"valueField: 'value',textField: 'label', data: ["));
		sList.add(new Statement(72000L,0,"{value: 'original',label: 'original'},"));
		sList.add(new Statement(73000L,0,"{value: 'sum',label: 'sum'}]\"/>"));
		sList.add(new Statement(74000L,0,"<div id=\"main\" style=\"width: 1000px;height:600px;\"></div>"));
		sList.add(new Statement(75000L,0,"</div>"));
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public StatementList generateWidgetScriptStatements() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"var myChart = echarts.init($('#main').get(0));"));
		sList.add(new Statement(2000L,0,""));
		sList.add(new Statement(3000L,0,"function setup(){"));
		sList.add(new Statement(4000L,1,"var chartstype = $(\"#chartstype\").combobox(\"getValue\");"));
		sList.add(new Statement(5000L,1,"var reporttype = $(\"#reporttype\").combobox(\"getValue\");"));
		sList.add(new Statement(6000L,1,"if (reporttype == \"sum\"){"));
		sList.add(new Statement(7000L,2,"myChart.setOption("+this.listSumOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(8000L,1,"}else{"));
		sList.add(new Statement(9000L,2,"myChart.setOption("+this.listOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(10000L,1,"}"));
		sList.add(new Statement(11000L,0,"}"));
		sList.add(new Statement(12000L,0,"$(document).ready(function(){"));
		sList.add(new Statement(13000L,1,"setup();"));
		sList.add(new Statement(14000L,0,"});"));
		sList.add(new Statement(15000L,0,""));
		sList.add(new Statement(16000L,0,"$(function () {"));
		sList.add(new Statement(17000L,1,"$('#chartstype').combobox({"));
		sList.add(new Statement(18000L,2,"onSelect: function(record){"));
		sList.add(new Statement(19000L,3,"var chartstype = record.value;"));
		sList.add(new Statement(20000L,3,"var reporttype = $(\"#reporttype\").combobox(\"getValue\");"));
		sList.add(new Statement(21000L,3,"if (reporttype == \"sum\"){"));
		sList.add(new Statement(22000L,4,"myChart.setOption("+this.listSumOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(23000L,3,"}else{"));
		sList.add(new Statement(24000L,4,"myChart.setOption("+this.listOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(25000L,3,"}"));
		sList.add(new Statement(26000L,2,"}"));
		sList.add(new Statement(27000L,1,"});"));
		sList.add(new Statement(28000L,1,"$('#reporttype').combobox({"));
		sList.add(new Statement(29000L,2,"onSelect: function(record){"));
		sList.add(new Statement(30000L,3,"var chartstype = $(\"#chartstype\").combobox(\"getValue\");"));
		sList.add(new Statement(31000L,3,"var reporttype = record.value;"));
		sList.add(new Statement(32000L,3,"if (reporttype == \"sum\"){"));
		sList.add(new Statement(33000L,4,"myChart.setOption("+this.listSumOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(34000L,3,"}else{"));
		sList.add(new Statement(35000L,4,"myChart.setOption("+this.listOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(36000L,3,"}"));
		sList.add(new Statement(37000L,2,"}"));
		sList.add(new Statement(38000L,1,"});"));
		sList.add(new Statement(39000L,0,"});"));
		sList.add(new Statement(40000L,0,""));
		sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementList(41000L));
		
		ListActiveOption listActiveOption = new ListActiveOption(this.domain,this.xAxisFields,this.yName);
		ListActiveSumOption listActiveSumOption = new ListActiveSumOption(this.domain,this.xAxisFields,this.yName);
		
		sList.add(listActiveOption.generateEasyUIJSActionMethod().generateMethodStatementList(42000));
		sList.add(listActiveSumOption.generateEasyUIJSActionMethod().generateMethodStatementList(43000L));
		
		long serial = 44000L;
		List<Domain> translateDomains = new ArrayList<Domain>();
		if (this.yName instanceof Dropdown){
			Dropdown dp = (Dropdown) this.yName;
			Domain target = dp.getTarget();
			if (!target.isLegacy()&&	!DomainUtil.inDomainList(target, translateDomains)){
				sList.add(dp.generateTranslateMethod().generateMethodStatementList(serial));
				serial+=1000L;
				translateDomains.add(target);
			}
		}
		this.tranlateDomains.addAll(translateDomains);
		return WriteableUtil.merge(sList);
	}

	@Override
	public boolean parse() {
		// TODO Auto-generated method stub
		return false;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public String getDetailPrefix() {
		return detailPrefix;
	}

	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}

	public List<Field> getxAxisFields() {
		return xAxisFields;
	}

	public void setxAxisFields(List<Field> xAxisFields) {
		this.xAxisFields = xAxisFields;
	}

	public Field getyName() {
		return yName;
	}

	public void setyName(Field yName) {
		this.yName = yName;
	}

	public Set<Domain> getTranlateDomains() {
		return tranlateDomains;
	}

	public void setTranlateDomains(Set<Domain> tranlateDomains) {
		this.tranlateDomains = tranlateDomains;
	}

	public Set<Domain> getParentTranlateDomains() {
		return parentTranlateDomains;
	}

	public void setParentTranlateDomains(Set<Domain> parentTranlateDomains) {
		this.parentTranlateDomains = parentTranlateDomains;
	}

	public JavascriptMethod getListOption() {
		return listOption;
	}

	public void setListOption(JavascriptMethod listOption) {
		this.listOption = listOption;
	}

	public JavascriptMethod getListSumOption() {
		return listSumOption;
	}

	public void setListSumOption(JavascriptMethod listSumOption) {
		this.listSumOption = listSumOption;
	}

}
