package org.light.reports;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.StatementList;
import org.light.easyuilayouts.EasyUILayout;
import org.light.easyuilayouts.widgets.MainDatagrid;
import org.light.easyuilayouts.widgets.ViewDialog;
import org.light.reports.verbs.ListActiveCompareOption;
import org.light.reports.verbs.ListActiveCompareSumOption;
import org.light.reports.verbs.ListActiveOption;
import org.light.reports.verbs.ListActiveSumOption;
import org.light.utils.WriteableUtil;

public class EchartsGridReportLayout extends EasyUILayout{
	protected Domain reportDomain;
	protected List<Field> xAxisFields;
	protected Field yName;
	protected EchartsDiagram diagram;
	protected DiagramDataControlGrid mainGrid;
	protected ViewDialog viewDialog;

	@Override
	public StatementList generateLayoutStatements() throws Exception{	
		List<Writeable> sList = new ArrayList<>();
		StatementList sl = diagram.generateWidgetStatements();
		sl.setSerial(1000L);
		StatementList sl2 = mainGrid.generateWidgetStatements();
		sl2.setSerial(2000L);
		
		StatementList sl3 = viewDialog.generateWidgetStatements();
		sl3.setSerial(3000L);
		
		sList.add(sl);
		sList.add(sl2);
		sList.add(sl3);
		StatementList result =  WriteableUtil.merge(sList);
		result.setSerial(this.serial);
		return result;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws Exception{
		List<Writeable> sList = new ArrayList<>();
		StatementList sl =  diagram.generateWidgetScriptStatements();
		sl.setSerial(1000L);
		this.mainGrid.setParentTranlateDomains(this.diagram.getTranlateDomains());
		StatementList sl2 = mainGrid.generateWidgetScriptStatements();
		sl2.setSerial(2000L);
		
//		StatementList sl3 = viewDialog.generateWidgetScriptStatements();
//		sl3.setSerial(3000L);
		
		sList.add(sl);
		sList.add(sl2);
//		sList.add(sl3);
		StatementList result =  WriteableUtil.merge(sList);
		result.setSerial(this.serial);
		return result;
	}

	@Override
	public boolean parse() throws Exception{
		if (this.diagram == null) {
			ListActiveOption lao = new ListActiveOption(this.reportDomain,this.xAxisFields,this.yName);
			ListActiveSumOption laso = new ListActiveSumOption(this.reportDomain,this.xAxisFields,this.yName);
			
			this.diagram = new EchartsDiagram();
			this.diagram.setDomain(this.reportDomain);
			this.diagram.setxAxisFields(this.xAxisFields);
			this.diagram.setyName(this.yName);
			this.diagram.setListOption(lao.generateEasyUIJSActionMethod());
			this.diagram.setListSumOption(laso.generateEasyUIJSActionMethod());
						
			this.mainGrid = new DiagramDataControlGrid("");
			this.mainGrid.setDomain(this.reportDomain);
			
			this.viewDialog = new ViewDialog();
			this.viewDialog.setDomain(this.reportDomain);
		}
		return true;
	}

	public Domain getReportDomain() {
		return reportDomain;
	}

	public void setReportDomain(Domain reportDomain) {
		this.reportDomain = reportDomain;
	}

	public List<Field> getxAxisFields() {
		return xAxisFields;
	}

	public void setxAxisFields(List<Field> xAxisFields) {
		this.xAxisFields = xAxisFields;
	}

	public Field getyName() {
		return yName;
	}

	public void setyName(Field yName) {
		this.yName = yName;
	}

	public EchartsDiagram getDiagram() {
		return diagram;
	}

	public void setDiagram(EchartsDiagram diagram) {
		this.diagram = diagram;
	}

	
}
