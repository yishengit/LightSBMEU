package org.light.tools;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.SpringLayout;
import javax.swing.UIManager;

import org.light.utils.PluralUtil;
import org.light.utils.StringUtil;

public class MoreAdvancedTool  extends JFrame implements MouseListener, ActionListener {
	private static final long serialVersionUID = 1L;
	protected JTextArea source;
	protected JTextArea target;
	protected JTextField first;
	protected JTextField serial;
	protected JTextField domainName;
	protected JTextField domainName2;
	protected JTextField domainName3;
	protected JTextField domainName4;
	protected JTextField domainName5;
	protected JTextField domainName6;
	
	protected JTextField domainNamingSuffix;
	protected JTextField domainNamingSuffix2;
	protected JTextField domainNamingSuffix3;
	protected JTextField domainNamingSuffix4;
	protected JTextField domainNamingSuffix5;
	protected JTextField domainNamingSuffix6;
	
	protected JTextField domainToken;
	protected JTextField domainToken2;
	protected JTextField domainToken3;
	protected JTextField domainToken4;
	protected JTextField domainToken5;
	protected JTextField domainToken6;
	
	protected JTextField domainIdName;
	protected JTextField domainIdName2;
	protected JTextField domainIdName3;
	protected JTextField domainIdName4;
	protected JTextField domainIdName5;
	protected JTextField domainIdName6;
	
	protected JTextField domainNameName;
	protected JTextField domainNameName2;
	protected JTextField domainNameName3;
	protected JTextField domainNameName4;
	protected JTextField domainNameName5;
	protected JTextField domainNameName6;
	
	protected JTextField domainActive;
	protected JTextField domainActive2;
	protected JTextField domainActive3;
	protected JTextField domainActive4;
	protected JTextField domainActive5;
	protected JTextField domainActive6;
	
	protected JTextField fieldName;
	protected JTextField fieldName2;
	protected JTextField fieldName3;
	protected JTextField fieldName4;
	protected JTextField fieldName5;
	protected JTextField fieldName6;
	
	protected JTextField end;
	protected JButton generateBtn;
	protected JScrollPane sourcePane;
	protected JScrollPane targetPane;
	protected JCheckBox completeMethod;
	protected JCheckBox isFront;
	protected JCheckBox isAutoPLural;
	protected JCheckBox isAutoUnderLine;
	
	protected Set<FakeDomain> fakeDomains;
	public MoreAdvancedTool() throws Exception{
		super("无垠式代码生成器模板助手");
		
//		String windows="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
//		UIManager.setLookAndFeel(windows);		
//		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		JPanel jpl = new JPanel();
		Font myFont = new Font("wqy", Font.PLAIN, 20);
		
		first = new JTextField("sList.add(new Statement(",28);
		serial = new JTextField("1000",8);
		//afterSerial = new JTextField("L,0,\"",15);
		end =  new JTextField("\"));",10);
		completeMethod = new JCheckBox("生成完整方法");
		isFront = new JCheckBox("前端格式",true);
		isAutoPLural = new JCheckBox("自动复数",true);
		isAutoUnderLine = new JCheckBox("自动下划线",true);
		
		first.setFont(myFont);
		serial.setFont(myFont);
		//afterSerial.setFont(myFont);
		end.setFont(myFont);
//		jpl.add(domainName);
		jpl.add(first);
		jpl.add(serial);
		//jpl.add(afterSerial);
		jpl.add(end);
		jpl.add(completeMethod);
		jpl.add(isFront);
		jpl.add(isAutoPLural);
		jpl.add(isAutoUnderLine);
		
		generateBtn = new JButton("Generate");
		generateBtn.addActionListener(this);
		jpl.add(generateBtn);
		
		jpl.setSize(1600,60);	
		JPanel jpl2 = new JPanel();
		domainName = new JTextField("domainName",10);
		domainName.setFont(myFont);
		domainNamingSuffix = new JTextField("domainNamingSuffix",10);
		domainNamingSuffix.setFont(myFont);
		domainIdName = new JTextField("domainId",10);
		domainIdName.setFont(myFont);
		domainNameName = new JTextField("domainNameName",10);
		domainNameName.setFont(myFont);
		domainActive = new JTextField("domainActive",10);
		domainActive.setFont(myFont);
		domainToken = new JTextField("domainToken",10);
		domainToken.setFont(myFont);
		fieldName = new JTextField("fieldName",10);
		fieldName.setFont(myFont);
		jpl2.add(domainName);
		jpl2.add(domainNamingSuffix);
		jpl2.add(domainIdName);
		jpl2.add(domainNameName);
		jpl2.add(domainActive);
		jpl2.add(domainToken);
		jpl2.add(fieldName);
		jpl2.setSize(1600,60);	
		
		JPanel jpl3 = new JPanel();
		domainName2 = new JTextField("",10);
		domainName2.setFont(myFont);
		domainNamingSuffix2 = new JTextField("",10);
		domainNamingSuffix2.setFont(myFont);
		domainIdName2 = new JTextField("",10);
		domainIdName2.setFont(myFont);
		domainNameName2 = new JTextField("",10);
		domainNameName2.setFont(myFont);
		domainActive2 = new JTextField("",10);
		domainActive2.setFont(myFont);
		domainToken2 = new JTextField("",10);
		domainToken2.setFont(myFont);
		fieldName2 = new JTextField("",10);
		fieldName2.setFont(myFont);
		jpl3.add(domainName2);
		jpl3.add(domainNamingSuffix2);
		jpl3.add(domainIdName2);
		jpl3.add(domainNameName2);
		jpl3.add(domainActive2);
		jpl3.add(domainToken2);
		jpl3.add(fieldName2);
		jpl3.setSize(1600,60);	
		
		JPanel jpl4 = new JPanel();
		domainName3 = new JTextField("",10);
		domainName3.setFont(myFont);
		domainNamingSuffix3 = new JTextField("",10);
		domainNamingSuffix3.setFont(myFont);
		domainIdName3 = new JTextField("",10);
		domainIdName3.setFont(myFont);
		domainNameName3 = new JTextField("",10);
		domainNameName3.setFont(myFont);
		domainActive3 = new JTextField("",10);
		domainActive3.setFont(myFont);
		domainToken3 = new JTextField("",10);
		domainToken3.setFont(myFont);
		fieldName3 = new JTextField("",10);
		fieldName3.setFont(myFont);
		jpl4.add(domainName3);
		jpl4.add(domainNamingSuffix3);
		jpl4.add(domainIdName3);
		jpl4.add(domainNameName3);
		jpl4.add(domainActive3);
		jpl4.add(domainToken3);
		jpl4.add(fieldName3);
		jpl4.setSize(1600,60);	
		
		JPanel jpl5 = new JPanel();
		domainName4 = new JTextField("",10);
		domainName4.setFont(myFont);
		domainNamingSuffix4 = new JTextField("",10);
		domainNamingSuffix4.setFont(myFont);
		domainIdName4 = new JTextField("",10);
		domainIdName4.setFont(myFont);
		domainNameName4 = new JTextField("",10);
		domainNameName4.setFont(myFont);
		domainActive4= new JTextField("",10);
		domainActive4.setFont(myFont);
		domainToken4 = new JTextField("",10);
		domainToken4.setFont(myFont);
		fieldName4 = new JTextField("",10);
		fieldName4.setFont(myFont);
		jpl5.add(domainName4);
		jpl5.add(domainNamingSuffix4);
		jpl5.add(domainIdName4);
		jpl5.add(domainNameName4);
		jpl5.add(domainActive4);
		jpl5.add(domainToken4);
		jpl5.add(fieldName4);
		jpl5.setSize(1600,60);	
		
		JPanel jpl6 = new JPanel();
		domainName5 = new JTextField("",10);
		domainName5.setFont(myFont);
		domainNamingSuffix5 = new JTextField("",10);
		domainNamingSuffix5.setFont(myFont);
		domainIdName5 = new JTextField("",10);
		domainIdName5.setFont(myFont);
		domainNameName5 = new JTextField("",10);
		domainNameName5.setFont(myFont);
		domainActive5 = new JTextField("",10);
		domainActive5.setFont(myFont);
		domainToken5 = new JTextField("",10);
		domainToken5.setFont(myFont);
		fieldName5 = new JTextField("",10);
		fieldName5.setFont(myFont);
		jpl6.add(domainName5);
		jpl6.add(domainNamingSuffix5);
		jpl6.add(domainIdName5);
		jpl6.add(domainNameName5);
		jpl6.add(domainActive5);
		jpl6.add(domainToken5);
		jpl6.add(fieldName5);
		jpl6.setSize(1600,60);	
		
		JPanel jpl7 = new JPanel();
		domainName6 = new JTextField("",10);
		domainName6.setFont(myFont);
		domainNamingSuffix6 = new JTextField("",10);
		domainNamingSuffix6.setFont(myFont);
		domainIdName6 = new JTextField("",10);
		domainIdName6.setFont(myFont);
		domainNameName6 = new JTextField("",10);
		domainNameName6.setFont(myFont);
		domainActive6 = new JTextField("",10);
		domainActive6.setFont(myFont);
		domainToken6 = new JTextField("",10);
		domainToken6.setFont(myFont);
		fieldName6 = new JTextField("",10);
		fieldName6.setFont(myFont);
		jpl7.add(domainName6);
		jpl7.add(domainNamingSuffix6);
		jpl7.add(domainIdName6);
		jpl7.add(domainNameName6);
		jpl7.add(domainActive6);
		jpl7.add(domainToken6);
		jpl7.add(fieldName6);
		jpl7.setSize(1600,60);	
		
		JPanel contentPane = new JPanel();
		source = new JTextArea(50,60);
		Font sfont=new Font("宋体",Font.PLAIN,14); 
		//source.setFont(sfont);
		sourcePane = new JScrollPane(source,  JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		sourcePane.setSize(700,60);
		sourcePane.setVisible(true);
		target = new JTextArea(50,60);
		//target.setFont(sfont);
		targetPane = new JScrollPane(target,  JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		targetPane.setSize(700,60);
		targetPane.setVisible(true);
		contentPane.add(sourcePane);
		contentPane.add(targetPane);
		contentPane.setSize(1600,400);

		SpringLayout slayout = new SpringLayout();
		JPanel jpls = new JPanel(slayout);
		SpringLayout.Constraints jplCons = slayout.getConstraints(jpl);
		jplCons.setX(Spring.constant(5));
		jplCons.setY(Spring.constant(5));
		
		SpringLayout.Constraints jpl2Cons = slayout.getConstraints(jpl2);
		jpl2Cons.setX(Spring.constant(5));
		jpl2Cons.setY(Spring.constant(45));
		
		SpringLayout.Constraints jpl3Cons = slayout.getConstraints(jpl3);
		jpl3Cons.setX(Spring.constant(5));
		jpl3Cons.setY(Spring.constant(85));
		
		SpringLayout.Constraints jpl4Cons = slayout.getConstraints(jpl4);
		jpl4Cons.setX(Spring.constant(5));
		jpl4Cons.setY(Spring.constant(125));
		
		SpringLayout.Constraints jpl5Cons = slayout.getConstraints(jpl5);
		jpl5Cons.setX(Spring.constant(5));
		jpl5Cons.setY(Spring.constant(165));
		
		SpringLayout.Constraints jpl6Cons = slayout.getConstraints(jpl6);
		jpl6Cons.setX(Spring.constant(5));
		jpl6Cons.setY(Spring.constant(205));
		
		SpringLayout.Constraints jpl7Cons = slayout.getConstraints(jpl7);
		jpl7Cons.setX(Spring.constant(5));
		jpl7Cons.setY(Spring.constant(245));
		
		SpringLayout.Constraints conCons = slayout.getConstraints(contentPane);
		conCons.setX(Spring.constant(5));
		conCons.setY(Spring.constant(285));
		
		jpls.add(jpl);
		jpls.add(jpl2);
		jpls.add(jpl3);
		jpls.add(jpl4);
		jpls.add(jpl5);
		jpls.add(jpl6);
		jpls.add(jpl7);
		jpls.add(contentPane);
		jpls.setSize(1400,1000);	
		
		this.setContentPane(jpls);
		setSize(1600,1400);
		setLocationRelativeTo(null);//窗口在屏幕中间显示
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (e.getSource() == generateBtn) {
				String sourceTxt = source.getText().trim();
				String firstTxt = first.getText().trim();
				String endTxt = end.getText().trim();
				boolean isCompleteMethod = completeMethod.isSelected();
				boolean isFrontMethod = isFront.isSelected();
				
				FakeDomain domain1 = parseDomain(domainName.getText().trim(),domainNamingSuffix.getText().trim(),domainIdName.getText().trim(),domainNameName.getText().trim(),
						domainActive.getText().trim(),domainToken.getText().trim(),fieldName.getText().trim());
				FakeDomain domain2 = parseDomain(domainName2.getText().trim(),domainNamingSuffix2.getText().trim(),domainIdName2.getText().trim(),domainNameName2.getText().trim(),
						domainActive2.getText().trim(),domainToken2.getText().trim(),fieldName2.getText().trim());
				FakeDomain domain3 = parseDomain(domainName3.getText().trim(),domainNamingSuffix3.getText().trim(),domainIdName3.getText().trim(),domainNameName3.getText().trim(),
						domainActive3.getText().trim(),domainToken3.getText().trim(),fieldName3.getText().trim());
			
				FakeDomain domain4 = parseDomain(domainName4.getText().trim(),domainNamingSuffix4.getText().trim(),domainIdName4.getText().trim(),domainNameName4.getText().trim(),
						domainActive4.getText().trim(),domainToken4.getText().trim(),fieldName4.getText().trim());
			
				FakeDomain domain5 = parseDomain(domainName5.getText().trim(),domainNamingSuffix5.getText().trim(),domainIdName5.getText().trim(),domainNameName5.getText().trim(),
						domainActive5.getText().trim(),domainToken5.getText().trim(),fieldName5.getText().trim());
			
				FakeDomain domain6 = parseDomain(domainName6.getText().trim(),domainNamingSuffix6.getText().trim(),domainIdName6.getText().trim(),domainNameName6.getText().trim(),
						domainActive6.getText().trim(),domainToken6.getText().trim(),fieldName6.getText().trim());
			
				List<FakeDomain> validateList = new ArrayList<>();
				validateList.add(domain1);
				validateList.add(domain2);
				validateList.add(domain3);
				validateList.add(domain4);
				validateList.add(domain5);
				validateList.add(domain6);
				Long serialInt = Long.valueOf(serial.getText());
				sourceTxt = domain1.replaceAllTokens(sourceTxt,serialInt,firstTxt,endTxt,isCompleteMethod,isFrontMethod,validateList);
				target.setText(sourceTxt);
				JOptionPane.showMessageDialog(null,  "Congralations!","Ｓuccess", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private Integer countFrpmtSpaceTabs(String s) {
		Integer spaces = 0;
		for (int i=0;i<s.length();i++) {
			if (s.charAt(i)==' ') spaces += 1;
			else if (s.charAt(i)=='\t') spaces += 4;
			else break;
		}
		return (int)Math.ceil(spaces/4);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String [] args) {
		try{	
			java.awt.EventQueue.invokeLater(new Runnable() {

	            @Override
	            public void run() {
	            	try {
	            		new MoreAdvancedTool().setVisible(true);
	            	} catch (Exception e){
	            		e.printStackTrace();
	            	}
	            }
	        });
		} catch (Exception e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "信息", JOptionPane.ERROR_MESSAGE);
		}
	}

	public JTextArea getSource() {
		return source;
	}

	public void setSource(JTextArea source) {
		this.source = source;
	}

	public JTextArea getTarget() {
		return target;
	}

	public void setTarget(JTextArea target) {
		this.target = target;
	}

	public JTextField getFirst() {
		return first;
	}

	public void setFirst(JTextField first) {
		this.first = first;
	}

	public JTextField getSerial() {
		return serial;
	}

	public void setSerial(JTextField serial) {
		this.serial = serial;
	}

	public JTextField getDomainName() {
		return domainName;
	}

	public void setDomainName(JTextField domainName) {
		this.domainName = domainName;
	}

	public JTextField getDomainName2() {
		return domainName2;
	}

	public void setDomainName2(JTextField domainName2) {
		this.domainName2 = domainName2;
	}

	public JTextField getDomainName3() {
		return domainName3;
	}

	public void setDomainName3(JTextField domainName3) {
		this.domainName3 = domainName3;
	}

	public JTextField getDomainName4() {
		return domainName4;
	}

	public void setDomainName4(JTextField domainName4) {
		this.domainName4 = domainName4;
	}

	public JTextField getDomainName5() {
		return domainName5;
	}

	public void setDomainName5(JTextField domainName5) {
		this.domainName5 = domainName5;
	}

	public JTextField getDomainName6() {
		return domainName6;
	}

	public void setDomainName6(JTextField domainName6) {
		this.domainName6 = domainName6;
	}

	public JTextField getFieldName() {
		return fieldName;
	}

	public void setFieldName(JTextField fieldName) {
		this.fieldName = fieldName;
	}

	public JTextField getFieldName2() {
		return fieldName2;
	}

	public void setFieldName2(JTextField fieldName2) {
		this.fieldName2 = fieldName2;
	}

	public JTextField getFieldName3() {
		return fieldName3;
	}

	public void setFieldName3(JTextField fieldName3) {
		this.fieldName3 = fieldName3;
	}

	public JTextField getFieldName4() {
		return fieldName4;
	}

	public void setFieldName4(JTextField fieldName4) {
		this.fieldName4 = fieldName4;
	}

	public JTextField getFieldName5() {
		return fieldName5;
	}

	public void setFieldName5(JTextField fieldName5) {
		this.fieldName5 = fieldName5;
	}

	public JTextField getFieldName6() {
		return fieldName6;
	}

	public void setFieldName6(JTextField fieldName6) {
		this.fieldName6 = fieldName6;
	}

	public JTextField getEnd() {
		return end;
	}

	public void setEnd(JTextField end) {
		this.end = end;
	}

	public JButton getGenerateBtn() {
		return generateBtn;
	}

	public void setGenerateBtn(JButton generateBtn) {
		this.generateBtn = generateBtn;
	}

	public JScrollPane getSourcePane() {
		return sourcePane;
	}

	public void setSourcePane(JScrollPane sourcePane) {
		this.sourcePane = sourcePane;
	}

	public JScrollPane getTargetPane() {
		return targetPane;
	}

	public void setTargetPane(JScrollPane targetPane) {
		this.targetPane = targetPane;
	}

	public JCheckBox getCompleteMethod() {
		return completeMethod;
	}

	public void setCompleteMethod(JCheckBox completeMethod) {
		this.completeMethod = completeMethod;
	}

	public JCheckBox getIsFront() {
		return isFront;
	}

	public void setIsFront(JCheckBox isFront) {
		this.isFront = isFront;
	}

	public JCheckBox getIsAutoPLural() {
		return isAutoPLural;
	}

	public void setIsAutoPLural(JCheckBox isAutoPLural) {
		this.isAutoPLural = isAutoPLural;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JCheckBox getIsAutoUnderLine() {
		return isAutoUnderLine;
	}

	public void setIsAutoUnderLine(JCheckBox isAutoUnderLine) {
		this.isAutoUnderLine = isAutoUnderLine;
	}

	public JTextField getDomainNamingSuffix() {
		return domainNamingSuffix;
	}

	public void setDomainNamingSuffix(JTextField domainNamingSuffix) {
		this.domainNamingSuffix = domainNamingSuffix;
	}

	public JTextField getDomainNamingSuffix2() {
		return domainNamingSuffix2;
	}

	public void setDomainNamingSuffix2(JTextField domainNamingSuffix2) {
		this.domainNamingSuffix2 = domainNamingSuffix2;
	}

	public JTextField getDomainNamingSuffix3() {
		return domainNamingSuffix3;
	}

	public void setDomainNamingSuffix3(JTextField domainNamingSuffix3) {
		this.domainNamingSuffix3 = domainNamingSuffix3;
	}

	public JTextField getDomainNamingSuffix4() {
		return domainNamingSuffix4;
	}

	public void setDomainNamingSuffix4(JTextField domainNamingSuffix4) {
		this.domainNamingSuffix4 = domainNamingSuffix4;
	}

	public JTextField getDomainNamingSuffix5() {
		return domainNamingSuffix5;
	}

	public void setDomainNamingSuffix5(JTextField domainNamingSuffix5) {
		this.domainNamingSuffix5 = domainNamingSuffix5;
	}

	public JTextField getDomainNamingSuffix6() {
		return domainNamingSuffix6;
	}

	public void setDomainNamingSuffix6(JTextField domainNamingSuffix6) {
		this.domainNamingSuffix6 = domainNamingSuffix6;
	}

	public JTextField getDomainToken() {
		return domainToken;
	}

	public void setDomainToken(JTextField domainToken) {
		this.domainToken = domainToken;
	}

	public JTextField getDomainToken2() {
		return domainToken2;
	}

	public void setDomainToken2(JTextField domainToken2) {
		this.domainToken2 = domainToken2;
	}

	public JTextField getDomainToken3() {
		return domainToken3;
	}

	public void setDomainToken3(JTextField domainToken3) {
		this.domainToken3 = domainToken3;
	}

	public JTextField getDomainToken4() {
		return domainToken4;
	}

	public void setDomainToken4(JTextField domainToken4) {
		this.domainToken4 = domainToken4;
	}

	public JTextField getDomainToken5() {
		return domainToken5;
	}

	public void setDomainToken5(JTextField domainToken5) {
		this.domainToken5 = domainToken5;
	}

	public JTextField getDomainToken6() {
		return domainToken6;
	}

	public void setDomainToken6(JTextField domainToken6) {
		this.domainToken6 = domainToken6;
	}

	public JTextField getDomainIdName() {
		return domainIdName;
	}

	public void setDomainIdName(JTextField domainIdName) {
		this.domainIdName = domainIdName;
	}

	public JTextField getDomainIdName2() {
		return domainIdName2;
	}

	public void setDomainIdName2(JTextField domainIdName2) {
		this.domainIdName2 = domainIdName2;
	}

	public JTextField getDomainIdName3() {
		return domainIdName3;
	}

	public void setDomainIdName3(JTextField domainIdName3) {
		this.domainIdName3 = domainIdName3;
	}

	public JTextField getDomainIdName4() {
		return domainIdName4;
	}

	public void setDomainIdName4(JTextField domainIdName4) {
		this.domainIdName4 = domainIdName4;
	}

	public JTextField getDomainIdName5() {
		return domainIdName5;
	}

	public void setDomainIdName5(JTextField domainIdName5) {
		this.domainIdName5 = domainIdName5;
	}

	public JTextField getDomainIdName6() {
		return domainIdName6;
	}

	public void setDomainIdName6(JTextField domainIdName6) {
		this.domainIdName6 = domainIdName6;
	}

	public JTextField getDomainNameName() {
		return domainNameName;
	}

	public void setDomainNameName(JTextField domainNameName) {
		this.domainNameName = domainNameName;
	}

	public JTextField getDomainNameName2() {
		return domainNameName2;
	}

	public void setDomainNameName2(JTextField domainNameName2) {
		this.domainNameName2 = domainNameName2;
	}

	public JTextField getDomainNameName3() {
		return domainNameName3;
	}

	public void setDomainNameName3(JTextField domainNameName3) {
		this.domainNameName3 = domainNameName3;
	}

	public JTextField getDomainNameName4() {
		return domainNameName4;
	}

	public void setDomainNameName4(JTextField domainNameName4) {
		this.domainNameName4 = domainNameName4;
	}

	public JTextField getDomainNameName5() {
		return domainNameName5;
	}

	public void setDomainNameName5(JTextField domainNameName5) {
		this.domainNameName5 = domainNameName5;
	}

	public JTextField getDomainNameName6() {
		return domainNameName6;
	}

	public void setDomainNameName6(JTextField domainNameName6) {
		this.domainNameName6 = domainNameName6;
	}

	public JTextField getDomainActive() {
		return domainActive;
	}

	public void setDomainActive(JTextField domainActive) {
		this.domainActive = domainActive;
	}

	public JTextField getDomainActive2() {
		return domainActive2;
	}

	public void setDomainActive2(JTextField domainActive2) {
		this.domainActive2 = domainActive2;
	}

	public JTextField getDomainActive3() {
		return domainActive3;
	}

	public void setDomainActive3(JTextField domainActive3) {
		this.domainActive3 = domainActive3;
	}

	public JTextField getDomainActive4() {
		return domainActive4;
	}

	public void setDomainActive4(JTextField domainActive4) {
		this.domainActive4 = domainActive4;
	}

	public JTextField getDomainActive5() {
		return domainActive5;
	}

	public void setDomainActive5(JTextField domainActive5) {
		this.domainActive5 = domainActive5;
	}

	public JTextField getDomainActive6() {
		return domainActive6;
	}

	public void setDomainActive6(JTextField domainActive6) {
		this.domainActive6 = domainActive6;
	}

	public Set<FakeDomain> getFakeDomains() {
		return fakeDomains;
	}

	public void setFakeDomains(Set<FakeDomain> fakeDomains) {
		this.fakeDomains = fakeDomains;
	}
	
	public FakeDomain parseDomain(String domainName,String domainNamingSuffix,String domainIdName,
			String domainNameName,String domainActive,String domainToken,String fieldName) {
		FakeDomain fd = new FakeDomain();
		fd.setDomainName(domainName);
		fd.setDomainNamingSuffix(domainNamingSuffix);
		fd.setDomainIdName(domainIdName);
		fd.setDomainNameName(domainNameName);
		fd.setDomainActiveName(domainActive);
		fd.setDomainToken(domainToken);
		Set<String> fields = new TreeSet<>();
		fields.add(fieldName);
		fd.setFields(fields);
		return fd;
	}
}
