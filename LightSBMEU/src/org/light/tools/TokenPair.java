package org.light.tools;

import java.io.Serializable;

public class TokenPair implements Serializable{
	protected String source;
	protected String target;
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public TokenPair(String source,String target) {
		super();
		this.source = source;
		this.target = target;
	}
}
