package org.light.oracle.oracleverb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedStatementGenerator;
import org.light.generator.NamedStatementListGenerator;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.InterVarUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class Update extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("update" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setNoContainer(true);
			List<Writeable> list = new ArrayList<Writeable>();
			list.add(new Statement(100L, 1, "<select id=\"" + StringUtil.lowerFirst(this.getVerbName())
					+ "\" parameterType=\"" + this.domain.getFullName() + "\">"));
			list.add(new Statement(200L, 2, MybatisOracleSqlReflector.generateUpdateSql(domain)));
			list.add(new Statement(300L, 1, "</select>"));
			method.setMethodStatementList(WriteableUtil.merge(list));
			method.setMethodStatementList(WriteableUtil.merge(list));
			return method;
		}
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("update" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("void"));
			method.setThrowException(true);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(
					new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
			return method;
		}
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateDaoImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateDaoMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateDaoImplMethod().generateMethodContentStringWithSerial();
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("update" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("Boolean"));
			method.setThrowException(true);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(
					new Signature(2, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
			return method;
		}
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("update" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("Boolean"));
			method.setThrowException(true);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport("org.springframework.transaction.annotation.Transactional");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDaoSuffix() + "."
					+ this.domain.getStandardName() + "Dao");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getServiceSuffix() + "."
					+ this.domain.getStandardName() + "Service");
			method.addSignature(
					new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
			method.addMetaData("Override");
			method.addMetaData("Transactional");

			// Service method
			Method daomethod = this.generateDaoMethodDefinition();

			List<Writeable> list = new ArrayList<Writeable>();
			list.add(NamedStatementListGenerator.generateServiceImplVoid(1000L, 2, InterVarUtil.DB.dao, daomethod));
			list.add(new Statement(2000L, 2, "return true;"));
			method.setMethodStatementList(WriteableUtil.merge(list));
			return method;
		}
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateServiceImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	public Update(Domain domain)  throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("Update");
		this.setVerbName("Update" + StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("更新");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("Update");
	}

	public Update() {
		super();
		this.setLabel("更新");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("update" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("Map<String,Object>"));
			method.setThrowException(true);
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getServiceSuffix() + "."
					+ this.domain.getStandardName() + "Service");
			method.addSignature(new Signature(1, this.domain.getLowerFirstDomainName(), this.domain.getType(),
					this.domain.getPackageToken(), "RequestBody"));
			method.addMetaData("RequestMapping(value = \"/" + StringUtil.lowerFirst(method.getStandardName())
					+ "\", method = RequestMethod.POST)");

			List<Writeable> wlist = new ArrayList<Writeable>();
			Var service = new Var("service",
					new Type(this.domain.getStandardName() + "Service", this.domain.getPackageToken()));
			Method serviceMethod = this.generateServiceMethodDefinition();
			Var resultMap = new Var("result", new Type("TreeMap<String,Object>", "java.util"));
			wlist.add(NamedStatementGenerator.getJsonResultMap(1000L, 2, resultMap));
			long serial = 2000L;
			boolean imageExists = this.domain.containsImage();
			if (imageExists) 
			{
				wlist.add(new Statement(serial,2,this.domain.getCapFirstDomainNameWithSuffix() +" ori"+this.domain.getCapFirstDomainName()+
						" = service.find"+this.domain.getCapFirstDomainName()+"By"+this.domain.getDomainId().getCapFirstFieldName()+"("+this.domain.getLowerFirstDomainName()+".get"+this.domain.getDomainId().getCapFirstFieldName()+"());"));
				serial += 1000L;
			}
			for (Field f:this.domain.getPlainFields()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					imageExists = true;
					wlist.add(new Statement(serial,2,"if (ori"+this.domain.getCapFirstDomainName()+".get"+f.getCapFirstFieldName()+"()!=null) {"));
					wlist.add(new Statement(serial+1000L,3,this.domain.getLowerFirstDomainName()+".set"+f.getCapFirstFieldName()+"(ori"+this.domain.getCapFirstDomainName()+".get"+f.getCapFirstFieldName()+"());"));
					wlist.add(new Statement(serial+2000L,2,"}"));
					serial += 3000L;
				}				
			}
			for (Field f:this.domain.getPlainFields()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					wlist.add(new Statement(serial,2,"if (temp"+this.domain.getCapFirstDomainName()+"!=null && temp"+this.domain.getCapFirstDomainName()+".get"+f.getCapFirstFieldName()+"()!=null) {"));
					wlist.add(new Statement(serial+1000L,3,this.domain.getLowerFirstDomainName()+".set"+f.getCapFirstFieldName()+"(temp"+this.domain.getCapFirstDomainName()+".get"+f.getCapFirstFieldName()+"());"));
					wlist.add(new Statement(serial+2000L,2,"}"));
					serial += 3000L;
				}				
			}
			if (imageExists) 
			{
				wlist.add(new Statement(serial+1000L,2,"temp"+this.domain.getCapFirstDomainName()+" = new  "+this.domain.getCapFirstDomainNameWithSuffix()+"();"));
				serial += 2000L;
			}
			
			wlist.add(new Statement(serial+2000L, 2, service.getVarName() + "." + serviceMethod.getStandardName() + "("
					+ this.domain.getLowerFirstDomainName() + ");"));
			wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAndNull(serial+3000L, 2, resultMap));
			wlist.add(new Statement(serial+4000L, 2, "return " + resultMap.getVarName() + ";"));
			method.setMethodStatementList(WriteableUtil.merge(wlist));

			return method;
		}
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateControllerMethod();
			return m.generateMethodString();
		}
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateControllerMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied)
			return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("Update" + domain.getCapFirstDomainName());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'Update',"));
			}else {
				sl.add(new Statement(2000, 1, "text:'编辑',"));
			}
			sl.add(new Statement(3000, 1, "iconCls:'icon-edit',"));
			sl.add(new Statement(4000, 1, "handler:function(){ "));
			sl.add(new Statement(5000, 2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
			sl.add(new Statement(6000, 2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(7000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(7000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(8000, 3, "return;"));
			sl.add(new Statement(9000, 2, "}"));
			sl.add(new Statement(10000, 2, "if (rows.length > 1) {"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(11000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(11000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(12000, 3, "return;"));
			sl.add(new Statement(13000, 2, "}"));
			sl.add(new Statement(13500, 2,
					"$(\"#ffedit\").find(\"#" + this.domain.getDomainId().getLowerFirstFieldName()
							+ "\").val(rows[0][\"" + this.domain.getDomainId().getLowerFirstFieldName() + "\"]);"));
			long serial = 14000;
			for (Field f : domain.getFieldsWithoutIdAndActive()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					sl.add(new Statement(serial, 2, "if (rows[0][\""+f.getLowerFirstFieldName()+"\"]!=null)	{"));
					sl.add(new Statement(serial+10, 3, "$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").prop(\"src\",\"data:image/png;base64,\"+rows[0][\""+f.getLowerFirstFieldName()+"\"]);"));
					sl.add(new Statement(serial+20, 2, "}else{"));
					sl.add(new Statement(serial+30, 3, "$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").prop(\"src\",\"../css/images/blank.jpg\");"));
					sl.add(new Statement(serial+40, 2, "}"));
				}else if (f instanceof Dropdown) {
					sl.add(new Statement(serial, 2, "$(\"#ffedit\").find(\"#" + f.getLowerFirstFieldName()
							+ "\").combobox(\"setValue\",rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
				} else {
					if (f.getFieldType().equalsIgnoreCase("boolean")||f.getOriginalType().equalsIgnoreCase("boolean")) {
						sl.add(new Statement(serial, 2,
								"var " + f.getLowerFirstFieldName() + "Checkboxs = $(\"#ffedit\").find(\"input[name='"
										+ f.getLowerFirstFieldName() + "']\");"));
						sl.add(new Statement(serial + 100, 2,
								"for (var i=0;i<" + f.getLowerFirstFieldName() + "Checkboxs.length;i++){"));
						sl.add(new Statement(serial + 200, 3,
								"if (" + f.getLowerFirstFieldName() + "Checkboxs.get(i).value == \"\"+rows[0][\""
										+ f.getLowerFirstFieldName() + "\"]) " + f.getLowerFirstFieldName()
										+ "Checkboxs.get(i).checked=true;"));
						sl.add(new Statement(serial + 500, 2, "}"));
					} else if (f.isTextarea()) {
						sl.add(new Statement(serial, 2, "$(\"#ffedit\").find(\"#" + f.getLowerFirstFieldName()
								+ "\").val(rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
					} else {
						sl.add(new Statement(serial, 2, "$(\"#ffedit\").find(\"#" + f.getLowerFirstFieldName()
								+ "\").textbox(\"setValue\",rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
					}
				}
				serial += 1000;
			}
			sl.add(new Statement(serial, 2, "var checkboxs = $(\"#ffedit\").find(\"input[name='"
					+ domain.getActive().getLowerFirstFieldName() + "']\");"));
			sl.add(new Statement(serial + 100, 2, "for (var i=0;i<checkboxs.length;i++){"));
			sl.add(new Statement(serial + 200, 3, "if (checkboxs.get(i).value == \"\"+rows[0][\""
					+ domain.getActive().getLowerFirstFieldName() + "\"]) checkboxs.get(i).checked=true;"));
			sl.add(new Statement(serial + 500, 2, "}"));
			sl.add(new Statement(serial + 1000, 2,
					"$('#wupdate" + domain.getCapFirstDomainName() + "').window('open');"));
			sl.add(new Statement(serial + 2000, 1, "}"));
			sl.add(new Statement(serial + 3000, 0, "}"));
			block.setMethodStatementList(sl);
			return block;
		}
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSButtonBlock().generateBlockContentString();
		}
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
		}
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Domain domain = this.domain;
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("update" + domain.getCapFirstDomainName());

			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 1, "$.ajax({"));
			sl.add(new Statement(2000, 2, "type: \"post\","));
			sl.add(new Statement(3000, 2, "url: \"../" +this.domain.getControllerPackagePrefix()+ domain.getLowerFirstDomainName()
					+ domain.getControllerNamingSuffix() + "/update" + domain.getCapFirstDomainName() + "\","));
			sl.add(new Statement(4000, 2, "data: JSON.stringify({"));
			long serial = 5000;
			for (Field f : domain.getFields()) {
				if (f instanceof Dropdown) {
					sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":$(\"#ffedit\").find(\"#"
							+ f.getLowerFirstFieldName() + "\").combobox(\"getValue\"),"));
				} else if (f.getFieldType().equalsIgnoreCase("boolean")||f.getOriginalType().equalsIgnoreCase("boolean")
						|| f.getFieldName().equals(domain.getActive().getFieldName())) {
					sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":parseBooleanInt($(\"#ffedit\").find(\"input[name='"
							+ f.getLowerFirstFieldName() + "']:checked\").val()),"));
				} else if (f.isTextarea()) {
					sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":$(\"#ffedit\").find(\"#"
							+ f.getLowerFirstFieldName() + "\").val(),"));
				} else {
					sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":$(\"#ffedit\").find(\"#"
							+ f.getLowerFirstFieldName() + "\").val(),"));
				}
				serial += 1000;
			}
			sl.add(new Statement(serial, 2, "}),"));
			sl.add(new Statement(serial + 1000, 2, "dataType: 'json',"));
			sl.add(new Statement(serial + 2000, 2, "contentType:\"application/json;charset=UTF-8\","));
			sl.add(new Statement(serial + 3000, 2, "success: function(data, textStatus) {"));
			sl.add(new Statement(serial + 4000, 3, "if (data.success){"));
			sl.add(new Statement(serial + 7000, 4,
					"$(\"#wupdate" + domain.getCapFirstDomainName() + "\").window('close');"));
			sl.add(new Statement(serial + 8000, 4, "$(\"#dg\").datagrid(\"load\");"));
			sl.add(new Statement(serial + 9000, 3, "}"));
			sl.add(new Statement(serial + 10000, 2, "},"));
			sl.add(new Statement(serial + 11000, 2, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(serial + 12000, 2, "},"));
			sl.add(new Statement(serial + 13000, 2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(serial + 14000, 3, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(serial + 15000, 3, "alert(errorThrown.toString());"));
			sl.add(new Statement(serial + 16000, 2, "}"));
			sl.add(new Statement(serial + 17000, 1, "}); "));

			method.setMethodStatementList(sl);
			return method;
		}
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSActionMethod().generateMethodContentString();
		}
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSActionMethod().generateMethodContentString();
		}
	}
}
