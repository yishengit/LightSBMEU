package org.light.oracle.core;

import org.light.domain.Domain;
import org.light.domain.ManyToMany;
import org.light.easyui.EasyUIManyToManyTemplate;
import org.light.exception.ValidateException;
import org.light.generator.TwoDomainsDBDefinitionGenerator;
import org.light.layouts.EasyUIMtmPI;
import org.light.oracle.complexverb.Assign;
import org.light.oracle.complexverb.ListMyActive;
import org.light.oracle.complexverb.ListMyAvailableActive;
import org.light.oracle.complexverb.Revoke;
import org.light.utils.StringUtil;

public class OracleManyToMany extends ManyToMany{
	protected Assign assign;
	protected Revoke revoke;
	protected ListMyActive listMyActive;
	protected ListMyAvailableActive listMyAvailableActive;
		
	public OracleManyToMany(String manyToManySalveName){
		super();
		this.manyToManySalveName = manyToManySalveName;
	}
	
	public OracleManyToMany(String manyToManySalveName,String slaveAlias){
		super();
		this.manyToManySalveName = manyToManySalveName;
		this.slaveAlias = slaveAlias;
	}

	public OracleManyToMany(Domain master,Domain slave,String masterValue,String values) throws Exception{
		super();
		this.master = master;
		this.slave = slave;
		this.assign = new Assign(master,slave);
		this.revoke = new Revoke(master,slave);
		this.masterValue = masterValue;
		this.values = values;
		this.listMyActive = new ListMyActive(master,slave);
		this.listMyAvailableActive = new ListMyAvailableActive(master,slave);
		this.euPI = new EasyUIMtmPI(master,slave,this.getSlaveAliasLabelOrText());
	}
	
	public OracleManyToMany(Domain master,Domain slave,String masterValue, String values, String slaveAlias) throws Exception{
		this.master = master;
		this.slave = slave;
		this.slave.setAlias(slaveAlias);
		this.assign = new Assign(master,slave);
		this.revoke = new Revoke(master,slave);
		this.masterValue = masterValue;
		this.values = values;
		this.slaveAlias = slaveAlias;
		this.listMyActive = new ListMyActive(master,slave);
		this.listMyAvailableActive = new ListMyAvailableActive(master,slave);
		this.euPI = new EasyUIMtmPI(master,slave,this.getSlaveAliasLabelOrText());
	}
	
	public void setMaster(Domain master) {
		this.master = master;
	}
	public Domain getSlave() {
		return slave;
	}
	public void setSlave(Domain slave) {
		this.slave = slave;
	}

	public void setListMyAvailableActive(ListMyAvailableActive listMyAvailableActive) {
		this.listMyAvailableActive = listMyAvailableActive;
	}

	public Domain getMaster() {
		return master;
	}
	public String getStandardName(){
		if (this.master !=null && this.slave !=null){
			if (StringUtil.isBlank(this.slave.getAlias())){
				return "Link"+this.master.getStandardName()+this.slave.getStandardName();
			}else {
				return "Link"+this.master.getStandardName()+this.slave.getAlias();
			}
		} else {
			return this.standardName;
		}
	}
	@Override
	public int compareTo(ManyToMany o) {
		return this.getStandardName().compareTo(o.getStandardName());
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getText(){
		if (StringUtil.isBlank(this.master.getLabel())||StringUtil.isEnglishAndDigitalAndEmpty(this.master.getLabel())){
			if (StringUtil.isBlank(this.slave.getAlias())) {
				return "Link "+this.master.getStandardName()+" "+this.slave.getStandardName();
			} else {
				return "Link "+this.master.getStandardName()+" "+this.slave.getAlias();
			}
		}
		else return "链接"+this.master.getText()+this.slave.getText();
	}
	
	public TwoDomainsDBDefinitionGenerator toTwoDBGenerator(){
		TwoDomainsDBDefinitionGenerator mtg = new TwoDomainsDBDefinitionGenerator(this.master,this.slave);
		return mtg;
	}

	public Assign getOracleAssign() {
		return assign;
	}

	public void setOracleAssign(Assign assign) {
		this.assign = assign;
	}

	public Revoke getOracleRevoke() {
		return revoke;
	}

	public void setOracleRevoke(Revoke revoke) {
		this.revoke = revoke;
	}

	public ListMyActive getOracleListMyActive() {
		return listMyActive;
	}

	public void setOracleListMyActive(ListMyActive listMyActive) {
		this.listMyActive = listMyActive;
	}

	public ListMyAvailableActive getOracleListMyAvailableActive() {
		return listMyAvailableActive;
	}
}