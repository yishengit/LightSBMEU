package org.light.oracle.generator;

import org.light.domain.Domain;
import org.light.generator.TwoDomainsDBDefinitionGenerator;
import org.light.utils.StringUtil;

public class OracleTwoDomainsDBDefinitionGenerator extends TwoDomainsDBDefinitionGenerator{
	public OracleTwoDomainsDBDefinitionGenerator(Domain master,Domain slave){
		super(master,slave);
	}

	public String generateDBSql() throws Exception{
		StringBuilder sb = new StringBuilder();

		sb.append(Oracle11gSqlReflector.generateLinkTableDefinition(this.master,this.slave)).append("\n");
		return sb.toString();
	}
	
	@Override
	public String generateDropLinkTableSql() throws Exception{
		if (StringUtil.isBlank(slave.getAlias())){
			String result = "drop table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+StringUtil.capFirst(slave.getStandardName())) +";\n";
			return result;
		}else {
			String result = "drop table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+StringUtil.capFirst(slave.getAlias())) +";\n";
			return result;
		}
	}
	
	public static OracleTwoDomainsDBDefinitionGenerator toOracleTwoDomainsDBDefinitionGenerator(TwoDomainsDBDefinitionGenerator mtg) {
		OracleTwoDomainsDBDefinitionGenerator otg = new OracleTwoDomainsDBDefinitionGenerator(mtg.getMaster(),mtg.getSlave());
		return otg;
	}

}
