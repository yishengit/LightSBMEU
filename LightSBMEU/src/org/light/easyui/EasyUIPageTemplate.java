package org.light.easyui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.complexverb.AddUploadDomainField;
import org.light.complexverb.DomainFieldVerb;
import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.DragonHideStatement;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.MenuItem;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.exception.ValidateException;
import org.light.generator.JsonPagingGridJspTemplate;
import org.light.generator.NamedS2SMJavascriptMethodGenerator;
import org.light.include.JsonUserNav;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.Activate;
import org.light.verb.ActivateAll;
import org.light.verb.Add;
import org.light.verb.Clone;
import org.light.verb.CloneAll;
import org.light.verb.Delete;
import org.light.verb.DeleteAll;
import org.light.verb.Export;
import org.light.verb.ExportPDF;
import org.light.verb.ExportWord;
import org.light.verb.FilterExcel;
import org.light.verb.FilterPDF;
import org.light.verb.FilterWord;
import org.light.verb.FindById;
import org.light.verb.FindByName;
import org.light.verb.ListActive;
import org.light.verb.ListAll;
import org.light.verb.ListAllByPage;
import org.light.verb.SearchByFieldsByPage;
import org.light.verb.SearchByName;
import org.light.verb.SoftDelete;
import org.light.verb.SoftDeleteAll;
import org.light.verb.Toggle;
import org.light.verb.ToggleOne;
import org.light.verb.Update;
import org.light.verb.View;

public class EasyUIPageTemplate extends JsonPagingGridJspTemplate {
	protected JsonUserNav jsonUserNav;
	protected String title = "";
	protected String subTitle = "";
	protected String footer = "";
	protected String resolution = "low";
	protected Set<DomainFieldVerb> domainFieldVerbs = new TreeSet<>();
	
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public JsonUserNav getJsonUserNav() {
		return jsonUserNav;
	}

	public void setJsonUserNav(JsonUserNav jsonUserNav) {
		this.jsonUserNav = jsonUserNav;
	}

	public EasyUIPageTemplate(){
		super();
	}
	public EasyUIPageTemplate(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.standardName = StringUtil.lowerFirst(domain.getPlural());
		this.verbs = new ArrayList<Verb>();
		this.verbs.add(new ListAll(domain));
		this.verbs.add(new ListActive(domain));
		this.verbs.add(new Delete(domain));
		this.verbs.add(new FindById(domain));
		this.verbs.add(new FindByName(domain));
		this.verbs.add(new SearchByName(domain));
		this.verbs.add(new SoftDelete(domain));
		this.verbs.add(new Update(domain));
		this.verbs.add(new Add(domain));
		this.verbs.add(new ListAllByPage(domain));
	}
	
	public Verb findVerb(String verbName){
		for (Verb v : this.verbs){
			if(v.getVerbName().equals(verbName))
				return v;
		}
		return null;
	}
	
	public String generateJspString(){
		return generateStatementList().getContent();
	}
	
	@Override
	public StatementList generateStatementList() {
		try {
			List<Writeable> sList =  new ArrayList<Writeable>();
			sList.add(new Statement(1000L,0,"<!DOCTYPE html>"));
			sList.add(new Statement(2000L,0,"<html>"));
			sList.add(new Statement(3000L,0,"<head>"));
			sList.add(new Statement(4000L,0,"<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />"));
			sList.add(new Statement(5000L,0,"<title>"+this.domain.getText()+"</title>"));
			sList.add(new Statement(6000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/default/easyui.css\">"));
			sList.add(new Statement(7000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/icon.css\">"));
			sList.add(new Statement(8000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/demo/demo.css\">"));
			sList.add(new Statement(9000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.min.js\"></script>"));
			sList.add(new Statement(10000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.easyui.min.js\"></script>"));
			sList.add(new Statement(10100L,0,"<script type=\"text/javascript\" src=\"../uploadjs/vendor/jquery.ui.widget.js\"></script>"));
			sList.add(new Statement(10200L,0,"<script type=\"text/javascript\" src=\"../uploadjs/jquery.iframe-transport.js\"></script>"));
			sList.add(new Statement(10300L,0,"<script type=\"text/javascript\" src=\"../uploadjs/jquery.fileupload.js\"></script>"));
			sList.add(new Statement(10400L,0,"<script type=\"text/javascript\" src=\"../js/sha1.js\"></script>"));
			sList.add(new Statement(10500L,0,"<script type=\"text/javascript\" src=\"../js/auth.js\"></script>"));

			sList.add(new Statement(11000L,0,"</head>"));
			sList.add(new Statement(12000L,0,"<body class=\"easyui-layout\">"));
			if ("high".equalsIgnoreCase(this.getResolution())){
				sList.add(new Statement(13000L,0,"<div data-options=\"region:'north',border:false\" style=\"height:100px;background:#B3DFDA;padding:10px\">"));
			}else {
				sList.add(new Statement(13000L,0,"<div data-options=\"region:'north',border:false\" style=\"height:70px;background:#B3DFDA;padding:10px\">"));
			}
			
			sList.add(new Statement(13100L,1,"<div style=\"float:right;clear:both;margin-top:15px;margin-right:20px\">"));
			if  (this.domain.getLanguage().equalsIgnoreCase("english")){
				sList.add(new Statement(13200L,1,"<a href=\"javascript:logout()\">Logout</a>"));
			}else {
				sList.add(new Statement(13200L,1,"<a href=\"javascript:logout()\">注销</a>"));
			}
			sList.add(new Statement(13300L,1,"</div>"));
			
			if (StringUtil.isBlank(this.title)){
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(13500L,0,"<h2>LightSBMEU generate results.</h2>"));
				}else {
					sList.add(new Statement(13500L,0,"<h2>代码生成器：光 的生成结果</h2>"));
				}
			} else {
				sList.add(new Statement(13500L,0,"<h2>"+this.title+"</h2>"));
			}
			if (!StringUtil.isBlank(this.subTitle)){
				sList.add(new Statement(13700L,0,"<h3>"+this.subTitle+"</h3>"));
			}
			sList.add(new Statement(13900L,0,"</div>"));
			
			if ("high".equalsIgnoreCase(this.getResolution())){
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(14000L,0,"<div data-options=\"region:'west',split:true,title:'Main Menu'\" style=\"width:180px;padding:0px;\">"));
				}else {
					sList.add(new Statement(14000L,0,"<div data-options=\"region:'west',split:true,title:'主菜单'\" style=\"width:180px;padding:0px;\">"));
				}
			}else {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(14000L,0,"<div data-options=\"region:'west',split:true,title:'Main Menu'\" style=\"width:156px;padding:0px;\">"));
				}else {
					sList.add(new Statement(14000L,0,"<div data-options=\"region:'west',split:true,title:'主菜单'\" style=\"width:156px;padding:0px;\">"));
				}
			}
			
			sList.add(new Statement(15000L,0,"<div class=\"easyui-accordion\" data-options=\"fit:true,border:false\">"));
			if  (this.domain.getLanguage().equalsIgnoreCase("english")){
				sList.add(new Statement(16000L,0,"<div title=\"Domain List\" style=\"padding:0px\" data-options=\"selected:true\">"));
			}else {
				sList.add(new Statement(16000L,0,"<div title=\"域对象清单\" style=\"padding:0px\" data-options=\"selected:true\">"));
			}
			
			if ("high".equalsIgnoreCase(this.getResolution())){
				sList.add(new Statement(17000L,0,"<div id=\"mmadmin\" data-options=\"inline:true\" style=\"width: 165px; height: 98%; overflow: scroll; left: 0px; top: 0px; outline: none; display: block;\" class=\"menu-top menu-inline menu easyui-fluid\" tabindex=\"0\"><div class=\"menu-line\" style=\"height: 122px;\"></div>"));
			}else {
				sList.add(new Statement(17000L,0,"<div id=\"mmadmin\" data-options=\"inline:true\" style=\"width: 142px; height: 98%; overflow: scroll; left: 0px; top: 0px; outline: none; display: block;\" class=\"menu-top menu-inline menu easyui-fluid\" tabindex=\"0\"><div class=\"menu-line\" style=\"height: 122px;\"></div>"));
			}
			
			sList.add(new DragonHideStatement(17500L,0,"<div onclick=\"window.location='../pages/index.html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">主页</div><div class=\"menu-icon icon-add\"></div></div>",!StringUtil.isBlank(this.allDomainList)&&this.allDomainList.size()>0&&!StringUtil.isBlank(this.allDomainList.get(0).getLabel())&&!StringUtil.isEnglishAndDigitalAndEmpty(this.allDomainList.get(0).getLabel())));
			sList.add(new DragonHideStatement(17500L,0,"<div onclick=\"window.location='../pages/index.html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">Homepage</div><div class=\"menu-icon icon-add\"></div></div>",!(!StringUtil.isBlank(this.allDomainList)&&this.allDomainList.size()>0&&!StringUtil.isBlank(this.allDomainList.get(0).getLabel())&&!StringUtil.isEnglishAndDigitalAndEmpty(this.allDomainList.get(0).getLabel()))));
			long serial = 18000L;
//			if (this.allDomainList!=null&&this.allDomainList.size()>0) {
//				for (Domain d : this.allDomainList){
//					sList.add(new Statement(serial,0,"<div onclick=\"window.location='../pages/"+d.getPlural().toLowerCase()+".html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">"+d.getText()+"</div><div class=\"menu-icon icon-add\"></div></div>"));
//					serial += 1000L;
//				}
//			}
			for (MenuItem mi : this.menuItems){
				sList.add(new Statement(serial,0,"<div onclick=\"window.location='"+mi.getUrl()+"'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">"+mi.getText()+"</div><div class=\"menu-icon icon-add\"></div></div>"));
				serial += 1000L;
			}
			sList.add(new Statement(serial,0,"</div>"));
			sList.add(new Statement(serial+1000L,0,"</div>"));
			sList.add(new Statement(serial+2000L,0,"</div>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));
			if  (this.domain.getLanguage().equalsIgnoreCase("english")){
				sList.add(new Statement(serial+4000L,0,"<div data-options=\"region:'east',split:true,collapsed:true,title:'Attribute'\" style=\"width:250px;overflow: hidden\">"));
			}else {
				sList.add(new Statement(serial+4000L,0,"<div data-options=\"region:'east',split:true,collapsed:true,title:'属性'\" style=\"width:250px;overflow: hidden\">"));
			}
			sList.add(new Statement(serial+5000L,0,"</div>"));
			sList.add(new Statement(serial+6000L,0,"<div data-options=\"region:'south',border:false\" style=\"height:40px;background:#A9FACD;padding:10px;text-align: center\">"));
			if (StringUtil.isBlank(this.footer)){
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+6500L,0,"Rocketship Software Works. Author email:jerry_shen_sjf@qq.com QQ Group:277689737</div>"));
				}else {
					sList.add(new Statement(serial+6500L,0,"火箭船软件工作室版权所有。作者电邮:jerry_shen_sjf@qq.com QQ群:277689737</div>"));
				}
			}else {
				sList.add(new Statement(serial+6500L,0,this.footer+"</div>"));
			}			
			if  (this.domain.getLanguage().equalsIgnoreCase("english")){
				sList.add(new Statement(serial+7000L,0,"<div data-options=\"region:'center',title:'"+this.domain.getText()+" List'\">"));
			}else {
				sList.add(new Statement(serial+7000L,0,"<div data-options=\"region:'center',title:'"+this.domain.getText()+"清单'\">"));
			}
			
			if ("high".equalsIgnoreCase(this.getResolution())){
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+8000L,0,"<div title=\"Search Panel\" class=\"easyui-panel\" style=\"width:1600px;height:200px\">"));
				}else {
					sList.add(new Statement(serial+8000L,0,"<div title=\"搜索面板\" class=\"easyui-panel\" style=\"width:1600px;height:200px\">"));	
				}
			}else {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+8000L,0,"<div title=\"Search Panel\" class=\"easyui-panel\" style=\"width:1120px;height:200px\">"));
				}else {
					sList.add(new Statement(serial+8000L,0,"<div title=\"搜索面板\" class=\"easyui-panel\" style=\"width:1120px;height:200px\">"));					
				}
			}
			sList.add(new Statement(serial+9000L,0,"<form id=\"ffsearch\" method=\"post\">"));
			sList.add(new Statement(serial+10000L,0,"<table cellpadding=\"5\">"));
			
			serial = serial + 11000L;
			List<Field> fields = new ArrayList<Field>();
			fields.addAll(this.domain.getSearchFields());
			fields.sort(new FieldSerialComparator());
			Field f1,f2,f3;
			for (int i=0;i<fields.size();i=i+3){
				f1 = fields.get(i); 
				sList.add(new Statement(serial,0,"<tr>"));
				sList.add(new DragonHideStatement(serial+1000L,0,"<td>"+f1.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f1.getLowerFirstFieldName()+"' id='"+f1.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f1 instanceof Dropdown)));
				if (f1 instanceof Dropdown)
					sList.add(new DragonHideStatement(serial+1000L,0,"<td>"+f1.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f1.getLowerFirstFieldName()+"' id='"+f1.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f1).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f1).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f1).getTarget().getLowerFirstDomainName()+((Dropdown)f1).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f1).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td>",f1 instanceof Dropdown));
				if (i < fields.size()-1) {
					f2 = fields.get(i+1);
					sList.add(new DragonHideStatement(serial+2000L,0,"<td>"+f2.getText()+":</td><td><input class='easyui-textbox' type='text' name='"+f2.getLowerFirstFieldName()+"' id='"+f2.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f2 instanceof Dropdown)));					
					if (f2 instanceof Dropdown)
						sList.add(new DragonHideStatement(serial+2000L,0,"<td>"+f2.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f2.getLowerFirstFieldName()+"' id='"+f2.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f2).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f2).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f2).getTarget().getLowerFirstDomainName()+((Dropdown)f2).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f2).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td>",f2 instanceof Dropdown));
				}else{
					sList.add(new Statement(serial+2000L,0,"<td>&nbsp;</td>"));
				}
				if (i < fields.size()-2) {
					f3 = fields.get(i+2);
					sList.add(new DragonHideStatement(serial+3000L,0,"<td>"+f3.getText()+":</td><td><input class='easyui-textbox' type='text' name='"+f3.getLowerFirstFieldName()+"' id='"+f3.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f3 instanceof Dropdown)));					
					if (f3 instanceof Dropdown)
						sList.add(new DragonHideStatement(serial+3000L,0,"<td>"+f3.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f3.getLowerFirstFieldName()+"' id='"+f3.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f3).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f3).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f3).getTarget().getLowerFirstDomainName()+((Dropdown)f3).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f3).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td>",f3 instanceof Dropdown));
				}else{
					sList.add(new Statement(serial+3000L,0,"<td>&nbsp;</td>"));
				}
				if (i==0) {
					sList.add(new Statement(serial+3100L,0,"<td>"));
					
					FilterExcel filterExcelDropdown = new FilterExcel(this.domain);
					FilterPDF filterPdfDropdown = new FilterPDF(this.domain);
					FilterWord filterWordDropdown = new FilterWord(this.domain);
					int dropdownindex = 2;
					if (!filterExcelDropdown.isDenied() || !filterPdfDropdown.isDenied()) {
						if  (this.domain.getLanguage().equalsIgnoreCase("english")){
							sList.add(new Statement(serial+3200L,2,"<select id=\"actionSelect\" class='easyui-select' style=\"	width:90px;height:28px\" onchange=\"toggleBtnShow(this.value)\">"));
							sList.add(new Statement(serial+3300L,3,"<option value=\"1\" selected>Search</option>"));
							if (!filterExcelDropdown.isDenied() ) sList.add(new Statement(serial+3400L,3,"<option value=\""+dropdownindex++ +"\">Filter Excel</option>"));
							if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+3500L,3,"<option value=\""+dropdownindex++ +"\">Filter PDF</option>"));
							if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+3550L,3,"<option value=\""+dropdownindex+"\">Filter Word</option>"));
							sList.add(new Statement(serial+3600L,2,"</select>"));
						}else {
							sList.add(new Statement(serial+3200L,2,"<select id=\"actionSelect\" class='easyui-select' style=\"	width:90px;height:28px\" onchange=\"toggleBtnShow(this.value)\">"));
							sList.add(new Statement(serial+3300L,3,"<option value=\"1\" selected>搜索</option>"));
							if (!filterExcelDropdown.isDenied() ) sList.add(new Statement(serial+3400L,3,"<option value=\""+dropdownindex++ +"\">Excel过滤</option>"));
							if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+3500L,3,"<option value=\""+dropdownindex++ +"\">PDF过滤</option>"));
							if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+3550L,3,"<option value=\""+dropdownindex+"\">Word过滤</option>"));
							sList.add(new Statement(serial+3600L,2,"</select>"));						
						}					
					}
					sList.add(new Statement(serial+3700L,2,"</td><td>"));
					sList.add(new Statement(serial+3800L,2,"<div id=\"button-bar\">"));
					if  (this.domain.getLanguage().equalsIgnoreCase("english")){
						sList.add(new Statement(serial+3900L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:block;width:110px;height:28px\" data-options=\"iconCls:'icon-search'\" onclick=\"search"+this.domain.getCapFirstPlural()+"ByFieldsByPage()\">Search</a>"));
						if (!filterExcelDropdown.isDenied()) sList.add(new Statement(serial+4000L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Excel()\">Filter Excel</a>"));
						if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+4100L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"PDF()\"\">Filter PDF</a>"));
						if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+4150L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Word()\"\">Filter Word</a>"));
					}else {
						sList.add(new Statement(serial+3900L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:block;width:110px;height:28px\" data-options=\"iconCls:'icon-search'\" onclick=\"search"+this.domain.getCapFirstPlural()+"ByFieldsByPage()\">搜索</a>"));
						if (!filterExcelDropdown.isDenied()) sList.add(new Statement(serial+4000L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Excel()\">Excel过滤</a>"));
						if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+4100L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"PDF()\"\">PDF过滤</a>"));
						if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+4150L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Word()\"\">Word过滤</a>"));
					}
					sList.add(new Statement(serial+4200L,2,"</div>"));
					sList.add(new Statement(serial+4300L,2,"</td><td>"));
					if  (this.domain.getLanguage().equalsIgnoreCase("english")){
						sList.add(new Statement(serial+4400L,3,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" style=\"width:80px\" data-options=\"iconCls:'icon-clear'\"  onclick=\"clearForm('ffsearch');toggleBtnShow(1);$('#actionSelect').val(1)\">Clear</a>"));
					}else {
						sList.add(new Statement(serial+4400L,3,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" style=\"width:80px\" data-options=\"iconCls:'icon-clear'\"  onclick=\"clearForm('ffsearch');toggleBtnShow(1);$('#actionSelect').val(1)\">清除</a>"));
					}
					sList.add(new Statement(serial+4500L,0,"</td>"));
				}else {
					sList.add(new Statement(serial+4600L,0,"<td></td><td colspan='3'></td>"));
				}
				sList.add(new Statement(serial+5000L,0,"</tr>"));
				serial += 6000L;
			}
			sList.add(new Statement(serial+1000L,0,"</table>"));
			sList.add(new Statement(serial+2000L,0,"</form>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));
			
			if ("high".equalsIgnoreCase(this.getResolution())){
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+4000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+" List\" style=\"width:1600px;height:600px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
				}else{
					sList.add(new Statement(serial+4000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+"清单\" style=\"width:1600px;height:600px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
				}
			}else {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+4000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+" List\" style=\"width:1120px;height:400px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
				}else {
					sList.add(new Statement(serial+4000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+"清单\" style=\"width:1120px;height:400px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
				}
			}			
			
			sList.add(new Statement(serial+5000L,0,"<thead>"));
			sList.add(new Statement(serial+6000L,0,"<tr>"));
			if (this.domain!=null&&this.domain.hasDomainId()) sList.add(new Statement(serial+7000L,0,"<th data-options=\"field:'"+this.domain.getDomainId().getLowerFirstFieldName()+"',checkbox:true\">"+this.domain.getDomainId().getText()+"</th>"));
			serial = serial + 8000L;
			List<Field> fields2 = new ArrayList<Field>();
			fields2.addAll(this.domain.getFieldsWithoutId());
			fields2.sort(new FieldSerialComparator());
			for (Field f: fields2){
				if (f.getFieldType().equalsIgnoreCase("image")) {
					sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:140,formatter:show"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"Image\">"+f.getText()+"</th>"));
				}else {
					sList.add(new DragonHideStatement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>",!(f instanceof Dropdown)));
					if (f instanceof Dropdown)
						sList.add(new DragonHideStatement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80,formatter:translate"+((Dropdown)f).getTarget().getCapFirstDomainName()+"\">"+f.getText()+"</th>",f instanceof Dropdown));
				}
				serial+=1000L;
			}
			sList.add(new Statement(serial,0,"</tr>"));
			sList.add(new Statement(serial+1000L,0,"</thead>"));
			sList.add(new Statement(serial+2000L,0,"</table>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));
			sList.add(new Statement(serial+4000L,0,""));
			Add addDialog = new Add(this.domain);
			if (this.domain.hasDomainId()&&this.domain.hasActiveField()&&!addDialog.isDenied()) {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+5000L,0,"<div class=\"easyui-window\" title=\"Add "+this.domain.getText()+"\" id=\"wadd"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}else {
					sList.add(new Statement(serial+5000L,0,"<div class=\"easyui-window\" title=\"新增"+this.domain.getText()+"\" id=\"wadd"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}
				sList.add(new Statement(serial+6000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
				sList.add(new Statement(serial+7000L,0,"<form id=\"ff\" method=\"post\">"));
				sList.add(new Statement(serial+8000L,0,"<table cellpadding=\"5\">"));
				serial = serial + 9000L;
				List<Field> fields3 = new ArrayList<Field>();
				fields3.addAll(this.domain.getFieldsWithoutIdAndActive());
				fields3.sort(new FieldSerialComparator());
				for (Field f: fields3){
					if (f.getFieldType().equalsIgnoreCase("image")) {
						sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><img name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' width='500px' src='../css/images/blank.jpg'><br>"));
						sList.add(new Statement(serial,0,"<input id=\"add"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"Fileupload\" type=\"file\" name=\"files[]\" data-url=\"../"+this.domain.getControllerPackagePrefix()+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/addUpload"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"\"><br></td></tr>"));
					}else {
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&!f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><textarea  class='easyui-textarea' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' data-options=\"required:false\" cols='20' rows='4'></textarea></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input class='easyui-radio' type='radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='false'/>False</td></tr>",(!(f instanceof Dropdown) && f.getFieldType().equalsIgnoreCase("boolean"))));
						if (f instanceof Dropdown)
							sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f).getTarget().getLowerFirstDomainName()+((Dropdown)f).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td></tr>", f instanceof Dropdown));
					}
					serial+=1000L;
				}
		        sList.add(new Statement(serial,0,"<tr><td>"+this.domain.getActive().getText()+":</td><td><input class='easyui-radio' type='radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='false'/>False</td></tr>"));
				sList.add(new Statement(serial+1000L,0,"</table>"));
				sList.add(new Statement(serial+2000L,0,"</form>"));
				sList.add(new Statement(serial+3000L,0,"<div style=\"text-align:center;padding:5px\">"));
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"add"+this.domain.getCapFirstDomainName()+"()\">Add</a>"));
					sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"clearForm('ff')\">Clear</a>"));
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wadd"+this.domain.getCapFirstDomainName()+"').window('close')\">Cancel</a>"));
				}else {
					sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"add"+this.domain.getCapFirstDomainName()+"()\">新增</a>"));
					sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"clearForm('ff')\">清除</a>"));
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wadd"+this.domain.getCapFirstDomainName()+"').window('close')\">取消</a>"));
				}
				sList.add(new Statement(serial+7000L,0,"</div>"));
				sList.add(new Statement(serial+8000L,0,"</div>"));
			}
			sList.add(new Statement(serial+9000L,0,""));

			Update updateDialog = new Update(this.domain);
			if (!updateDialog.isDenied()&&this.domain.hasDomainId()&&this.domain.hasActiveField()) {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+10000L,0,"<div class=\"easyui-window\" title=\"Edit "+this.domain.getText()+"\" id=\"wupdate"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}else {
					sList.add(new Statement(serial+10000L,0,"<div class=\"easyui-window\" title=\"编辑"+this.domain.getText()+"\" id=\"wupdate"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}
				sList.add(new Statement(serial+11000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
				sList.add(new Statement(serial+12000L,0,"<form id=\"ffedit\" method=\"post\">"));
				sList.add(new Statement(serial+13000L,0,"<input  type='hidden' name='"+this.domain.getDomainId().getLowerFirstFieldName()+"' id='"+this.domain.getDomainId().getLowerFirstFieldName()+"' value=''/>"));
				sList.add(new Statement(serial+14000L,0,"<table cellpadding=\"5\">"));
				serial = serial + 15000L;
				List<Field> fields4 = new ArrayList<Field>();
				fields4.addAll(this.domain.getFieldsWithoutIdAndActive());
				fields4.sort(new FieldSerialComparator());
				for (Field f: fields4){
					if (f.getFieldType().equalsIgnoreCase("image")) {
						sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><img name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' width='500px' src='../css/images/blank.jpg'><br>"));
						sList.add(new Statement(serial,0,"<input id=\""+this.domain.getLowerFirstDomainName()+f.getCapFirstFieldName()+"Fileupload\" type=\"file\" name=\"files[]\" data-url=\"../"+this.domain.getControllerPackagePrefix()+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/addUpload"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"\"><br></td></tr>"));
					}else {
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&!f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><textarea  class='easyui-textarea' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' data-options=\"required:false\" cols='20' rows='4'></textarea></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input class='easyui-radio' type='radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='false'/>False</td></tr>",(!(f instanceof Dropdown) && f.getFieldType().equalsIgnoreCase("boolean"))));
						if (f instanceof Dropdown)
							sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f).getTarget().getLowerFirstDomainName()+((Dropdown)f).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td></tr>", f instanceof Dropdown));
					}
					serial+=1000L;
				}
				if (this.domain!=null&& this.domain.hasActiveField()) sList.add(new Statement(serial,0,"<tr><td>"+this.domain.getActive().getText()+":</td><td><input class='easyui-radio' type='radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='false'/>False</td></tr>"));
				sList.add(new Statement(serial+1000L,0,"</table>"));
				sList.add(new Statement(serial+2000L,0,"</form>"));
				sList.add(new Statement(serial+3000L,0,"<div style=\"text-align:center;padding:5px\">"));
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"update"+this.domain.getCapFirstDomainName()+"()\">Edit</a>"));
					sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#ffedit').form('clear');\">Clear</a>"));
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wupdate"+this.domain.getCapFirstDomainName()+"').window('close')\">Cancel</a>"));
				}else {
					sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"update"+this.domain.getCapFirstDomainName()+"()\">编辑</a>"));
					sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#ffedit').form('clear');\">清除</a>"));
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wupdate"+this.domain.getCapFirstDomainName()+"').window('close')\">取消</a>"));
				}
				sList.add(new Statement(serial+7000L,0,"</div>"));
				sList.add(new Statement(serial+8000L,0,"</div>"));		
			}

			View viewDialog = new View(this.domain);
			if (!viewDialog.isDenied()) {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+10000L,0,"<div class=\"easyui-window\" title=\"View "+this.domain.getText()+"\" id=\"wview"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}else {
					sList.add(new Statement(serial+10000L,0,"<div class=\"easyui-window\" title=\"查看"+this.domain.getText()+"\" id=\"wview"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}
				sList.add(new Statement(serial+11000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
				sList.add(new Statement(serial+12000L,0,"<form id=\"ffview\" method=\"post\">"));
				sList.add(new Statement(serial+13000L,0,"<input  type='hidden' name='"+this.domain.getDomainId().getLowerFirstFieldName()+"' id='"+this.domain.getDomainId().getLowerFirstFieldName()+"' value=''/>"));
				sList.add(new Statement(serial+14000L,0,"<table cellpadding=\"5\">"));
				serial = serial + 15000L;
				List<Field> fields4 = new ArrayList<Field>();
				fields4.addAll(this.domain.getFieldsWithoutIdAndActive());
				fields4.sort(new FieldSerialComparator());
				for (Field f: fields4){
					if (f.getFieldType().equalsIgnoreCase("image")) {
						sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><img name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' width='500px' src='../css/images/blank.jpg'><br>"));
					}else {
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&!f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><textarea  class='easyui-textarea' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' data-options=\"required:false\" cols='20' rows='4'></textarea></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input class='easyui-radio' type='radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='false'/>False</td></tr>",(!(f instanceof Dropdown) && f.getFieldType().equalsIgnoreCase("boolean"))));
						if (f instanceof Dropdown)
							sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f).getTarget().getLowerFirstDomainName()+((Dropdown)f).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td></tr>", f instanceof Dropdown));
					}
					serial+=1000L;
				}
				if (this.domain!=null&& this.domain.hasActiveField()) sList.add(new Statement(serial,0,"<tr><td>"+this.domain.getActive().getText()+":</td><td><input class='easyui-radio' type='radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='false'/>False</td></tr>"));
				sList.add(new Statement(serial+1000L,0,"</table>"));
				sList.add(new Statement(serial+2000L,0,"</form>"));
				sList.add(new Statement(serial+3000L,0,"<div style=\"text-align:center;padding:5px\">"));
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wview"+this.domain.getCapFirstDomainName()+"').window('close')\">Close</a>"));
				}else {
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wview"+this.domain.getCapFirstDomainName()+"').window('close')\">关闭</a>"));
				}
				sList.add(new Statement(serial+7000L,0,"</div>"));
				sList.add(new Statement(serial+8000L,0,"</div>"));		
			}
			
			sList.add(new Statement(serial+9000L,0,"</body>"));
			sList.add(new Statement(serial+9000L,0,"<script type=\"text/javascript\">"));
			sList.add(new Statement(serial+10000L,0,"var params = {};"));			
			sList.add(new Statement(serial+11000L,0,"var pagesize = 10;"));
			sList.add(new Statement(serial+12000L,0,"var pagenum = 1;"));
			serial+=13000L;
			if (this.domainFieldVerbs!=null&&this.domainFieldVerbs.size()>0) {
				sList.add(new Statement(serial,0,"$(function () {"));	
				serial+=1000L;
				for (DomainFieldVerb dfv:this.domainFieldVerbs) {
					JavascriptBlock uploadBlock = dfv.generateEasyUIJSButtonBlock();
					StatementList  slupload = uploadBlock.getMethodStatementList();
					slupload.setSerial(serial);
					sList.add(slupload);
					serial+= 1000L;
				}
				sList.add(new Statement(serial+2000L,0,"});"));
			}
			sList.add(new Statement(serial+13000L,0,"var toolbar = ["));
			JavascriptBlock slViewJb = ((EasyUIPositions)new View(this.domain)).generateEasyUIJSButtonBlock();
			if (slViewJb != null) {
				StatementList  slView = slViewJb.getMethodStatementList();			
				slView.setSerial(serial+13010L);
				sList.add(slView);
				sList.add(new Statement(serial+13050L,0,","));
			}
			JavascriptBlock slAddJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Add(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slAddJb != null) {
				StatementList  slAdd = slAddJb.getMethodStatementList();			
				slAdd.setSerial(serial+13100L);
				sList.add(slAdd);
				sList.add(new Statement(serial+13150L,0,","));
			}
			
			JavascriptBlock slUpdateJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Update(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slUpdateJb != null) {
				StatementList  slUpdate =slUpdateJb.getMethodStatementList();
				slUpdate.setSerial(serial+13200L);
				sList.add(slUpdate);
				sList.add(new Statement(serial+13250L,0,","));
			}
			
			JavascriptBlock slSoftDeleteJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDelete(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slSoftDeleteJb != null) {
				StatementList  slSoftDelete =slSoftDeleteJb.getMethodStatementList();
				slSoftDelete.setSerial(serial+13300L);
				sList.add(slSoftDelete);
				sList.add(new Statement(serial+13320L,0,","));
			}
			
			JavascriptBlock slActivateJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Activate(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slActivateJb != null) {
				StatementList  slActivate =slActivateJb.getMethodStatementList();
				slActivate.setSerial(serial+13340L);
				sList.add(slActivate);
				sList.add(new Statement(serial+13342L,0,","));
			}	
			
			JavascriptBlock slCloneJb = this.domain.hasDomainId()?((EasyUIPositions)new Clone(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slCloneJb != null) {
				StatementList  slClone=slCloneJb.getMethodStatementList();
				slClone.setSerial(serial+13345L);
				sList.add(slClone);			
				sList.add(new Statement(serial+13350L,0,","));
			}
			
			JavascriptBlock slDeleteJb = this.domain.hasDomainId()?((EasyUIPositions)new Delete(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slDeleteJb != null) {
				StatementList  slDelete =slDeleteJb.getMethodStatementList();
				slDelete.setSerial(serial+13400L);
				sList.add(slDelete);
				sList.add(new Statement(serial+13450L,0,","));
			}
			
			JavascriptBlock slToggleJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Toggle(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slToggleJb != null) {
				StatementList  slToggle =slToggleJb.getMethodStatementList();
				slToggle.setSerial(serial+13500L);
				sList.add(slToggle);
				sList.add(new Statement(serial+13550L,0,","));
			}
			
			JavascriptBlock slToggleOneJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ToggleOne(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slToggleOneJb != null) {
				StatementList  slToggleOne =slToggleOneJb.getMethodStatementList();
				slToggleOne.setSerial(serial+13600L);
				sList.add(slToggleOne);
				sList.add(new Statement(serial+13650L,0,",'-',"));
			}
			
			JavascriptBlock slSoftDeleteAllJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDeleteAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slSoftDeleteAllJb != null) {
				StatementList  slSoftDeleteAll =slSoftDeleteAllJb.getMethodStatementList();
				slSoftDeleteAll.setSerial(serial+13700L);
				sList.add(slSoftDeleteAll);
				sList.add(new Statement(serial+13710L,0,","));
			}
			
			JavascriptBlock slActivateAllJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ActivateAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slActivateAllJb != null) {
				StatementList  slActivateAll =slActivateAllJb.getMethodStatementList();
				slActivateAll.setSerial(serial+13720L);
				sList.add(slActivateAll);			
				sList.add(new Statement(serial+13722L,0,","));
			}
			
			JavascriptBlock slCloneAllJb = this.domain.hasDomainId()?((EasyUIPositions)new CloneAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slCloneAllJb != null) {
				StatementList  slCloneAll =slCloneAllJb.getMethodStatementList();
				slCloneAll.setSerial(serial+13725L);
				sList.add(slCloneAll);			
				sList.add(new Statement(serial+13750L,0,","));
			}
			
			JavascriptBlock slDeleteAllJb = this.domain.hasDomainId()?((EasyUIPositions)new DeleteAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slDeleteAllJb != null) {
				StatementList  slDeleteAll =slDeleteAllJb.getMethodStatementList();
				slDeleteAll.setSerial(serial+13800L);
				sList.add(slDeleteAll);				
				sList.add(new Statement(serial+13850L,0,","));
			}
			
			JavascriptBlock slExportJb = ((EasyUIPositions)new Export(this.domain)).generateEasyUIJSButtonBlock();
			if (slExportJb != null) {
				StatementList  slExport =slExportJb.getMethodStatementList();
				slExport.setSerial(serial+13900L);
				sList.add(slExport);			
				sList.add(new Statement(serial+13950L,0,","));
			}
			
			JavascriptBlock slExportPDFJb = ((EasyUIPositions)new ExportPDF(this.domain)).generateEasyUIJSButtonBlock();
			if (slExportPDFJb != null) {
				StatementList  slExportPDF =slExportPDFJb.getMethodStatementList();
				slExportPDF.setSerial(serial+13960L);
				sList.add(slExportPDF);
				sList.add(new Statement(serial+13970L,0,","));
			}
			
			JavascriptBlock slExportWordJb = ((EasyUIPositions)new ExportWord(this.domain)).generateEasyUIJSButtonBlock();
			if (slExportWordJb != null) {
				StatementList  slExportWord =slExportWordJb.getMethodStatementList();
				slExportWord.setSerial(serial+13980L);
				sList.add(slExportWord);
				sList.add(new Statement(serial+13990L,0,","));
			}
			
			// remove last ','
			if (((Statement) sList.get(sList.size()-1)).getContent().contentEquals(",")) {
				sList.remove(sList.size()-1);
			}
			
			sList.add(new Statement(serial+14000L,0,"];"));
			sList.add(new Statement(serial+15000L,0,"$(document).ready(function(){"));
			sList.add(new Statement(serial+16000L,0,"$(\"#dg\").datagrid(\"load\");"));
			sList.add(new Statement(serial+17000L,0,"});"));

			sList.add(new Statement(serial+18000L,0,"function clearForm(formId){"));
			sList.add(new Statement(serial+19000L,0,"$('#'+formId).form('clear');"));
			sList.add(new Statement(serial+20000L,0,"}"));
			
			JavascriptMethod slAddJm = this.domain.hasActiveField()? ((EasyUIPositions)new Add(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slAddJm != null) {
				StatementList slAddAction = slAddJm.generateMethodStatementListIncludingContainer(serial+21000L,0);
				sList.add(slAddAction);
			}
			
			JavascriptMethod slUpdateJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Update(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slUpdateJm != null) {
				StatementList slUpdateAction = slUpdateJm.generateMethodStatementListIncludingContainer(serial+22000L,0);
				sList.add(slUpdateAction);
			}
			
			JavascriptMethod slSoftDeleteJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDelete(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slSoftDeleteJm != null) {
				StatementList slSoftDeleteAction = slSoftDeleteJm.generateMethodStatementListIncludingContainer(serial+23000L,0);
				sList.add(slSoftDeleteAction);
			}
			
			JavascriptMethod slActivateJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Activate(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slActivateJm != null) {
				StatementList slActivateAction = slActivateJm.generateMethodStatementListIncludingContainer(serial+24000L,0);
				sList.add(slActivateAction);
			}
			
			JavascriptMethod slCloneJm =  this.domain.hasDomainId()?((EasyUIPositions)new Clone(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slCloneJm != null) {
				StatementList slCloneAction = slCloneJm.generateMethodStatementListIncludingContainer(serial+25000L,0);
				sList.add(slCloneAction);
			}
			
			JavascriptMethod slDeleteJm = this.domain.hasDomainId()? ((EasyUIPositions)new Delete(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slDeleteJm != null) {
				StatementList slDeleteAction = slDeleteJm.generateMethodStatementListIncludingContainer(serial+28000L,0);
				sList.add(slDeleteAction);
			}
			
			JavascriptMethod slToggleJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Toggle(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slToggleJm != null) {
				StatementList slToggleAction = slToggleJm.generateMethodStatementListIncludingContainer(serial+28200L,0);
				sList.add(slToggleAction);
			}
			
			JavascriptMethod slToggleOneJm = this.domain.hasDomainId()&&this.domain.hasActiveField()? ((EasyUIPositions)new ToggleOne(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slToggleOneJm != null) {
				StatementList slToggleOneAction = slToggleOneJm.generateMethodStatementListIncludingContainer(serial+28400L,0);
				sList.add(slToggleOneAction);
			}
			
			JavascriptMethod slSoftDeleteAllJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDeleteAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slSoftDeleteAllJm != null) {
				StatementList slSoftDeleteAllAction = slSoftDeleteAllJm.generateMethodStatementListIncludingContainer(serial+28600L,0);
				sList.add(slSoftDeleteAllAction);
			}
			
			JavascriptMethod slActivateAllJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ActivateAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slActivateAllJm != null) {
				StatementList slActivateAllAction = slActivateAllJm.generateMethodStatementListIncludingContainer(serial+28800L,0);
				sList.add(slActivateAllAction);
			}
			
			JavascriptMethod slCloneAllJm = this.domain.hasDomainId()? ((EasyUIPositions)new CloneAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slCloneAllJm != null) {
				StatementList slCloneAllAction = slCloneAllJm.generateMethodStatementListIncludingContainer(serial+29000L,0);
				sList.add(slCloneAllAction);
			}
			
			JavascriptMethod slDeleteAllJm =  this.domain.hasDomainId()? ((EasyUIPositions)new DeleteAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slDeleteAllJm != null) {
				StatementList slDeleteAllAction = slDeleteAllJm.generateMethodStatementListIncludingContainer(serial+29200L,0);
				sList.add(slDeleteAllAction);
			}
			
			JavascriptMethod slSearchByFieldsByPageJm =  ((EasyUIPositions)new SearchByFieldsByPage(this.domain)).generateEasyUIJSActionMethod();
			if(	slSearchByFieldsByPageJm != null) {
				StatementList slSearchByFieldsByPageAction = slSearchByFieldsByPageJm.generateMethodStatementListIncludingContainer(serial+29400L,0);
				sList.add(slSearchByFieldsByPageAction);
			}
			
			JavascriptMethod slFilterExcelJm =  ((EasyUIPositions)new FilterExcel(this.domain)).generateEasyUIJSActionMethod();
			if(	slFilterExcelJm != null) {
				StatementList slFilterExcelAction = slFilterExcelJm.generateMethodStatementListIncludingContainer(serial+29600L,0);
				sList.add(slFilterExcelAction);
			}
			
			JavascriptMethod slFilterPDFJm =  ((EasyUIPositions)new FilterPDF(this.domain)).generateEasyUIJSActionMethod();
			if(	slFilterPDFJm != null) {
				StatementList slFilterPDFAction = slFilterPDFJm.generateMethodStatementListIncludingContainer(serial+29800L,0);
				sList.add(slFilterPDFAction);
			}
			
			JavascriptMethod slFilterWordJm =  ((EasyUIPositions)new FilterWord(this.domain)).generateEasyUIJSActionMethod();
			if(	slFilterWordJm != null) {
				StatementList slFilterWordAction = slFilterWordJm.generateMethodStatementListIncludingContainer(serial+29900L,0);
				sList.add(slFilterWordAction);
			}
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseBooleanMethod().generateMethodStatementList(serial+30000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementList(serial+31000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateCheckRadioBoxValueMethod().generateMethodStatementList(serial+32000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateToggleBtnShowMethod().generateMethodStatementList(serial+32500L));
			
			serial = serial+33000L;
			List<Field> fields5 = new ArrayList<Field>();
			fields5.addAll(this.domain.getFieldsWithoutIdAndActive());
			fields5.sort(new FieldSerialComparator());
			List<Domain> translateDomains = new ArrayList<Domain>();
			for (Field f: fields5){
				if (f instanceof Dropdown){
					Dropdown dp = (Dropdown)f;
					Domain target = dp.getTarget();
					if (!target.isLegacy()&&	!DomainUtil.inDomainList(target, translateDomains)){
						sList.add(dp.generateTranslateMethod().generateMethodStatementList(serial));
						serial+=1000L;
						translateDomains.add(target);
					}
				}
			}
			
			for (Field f: fields5){
				if (f.getFieldType().equalsIgnoreCase("image")) {
					AddUploadDomainField audf = new AddUploadDomainField(this.domain,f);
					sList.add(audf.generateEasyUIJSActionMethod().generateMethodStatementList(serial));
					serial+=1000L;
				}
			}
			
			sList.add(new Statement(serial,0,"</script>"));
			sList.add(new Statement(serial+1000L,0,"</html>"));
			StatementList mylist = WriteableUtil.merge(sList);
			return mylist;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public Set<DomainFieldVerb> getDomainFieldVerbs() {
		return domainFieldVerbs;
	}

	public void setDomainFieldVerbs(Set<DomainFieldVerb> domainFieldVerbs) {
		this.domainFieldVerbs = domainFieldVerbs;
	}
}
