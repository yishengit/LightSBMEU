package org.light.shiroauth.easyui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.complexverb.AddUploadDomainField;
import org.light.complexverb.DomainFieldVerb;
import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.DragonHideStatement;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyui.EasyUIPositions;
import org.light.easyuilayouts.chiddgverb.UpdateUploadDomainField;
import org.light.exception.ValidateException;
import org.light.generator.NamedS2SMJavascriptMethodGenerator;
import org.light.include.JsonUserNav;
import org.light.layouts.EasyUIGridPagePI;
import org.light.shiroauth.verb.RegisterUser;
import org.light.utils.FieldUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.Add;
import org.light.verb.Delete;
import org.light.verb.FindById;
import org.light.verb.FindByName;
import org.light.verb.ListActive;
import org.light.verb.ListAll;
import org.light.verb.ListAllByPage;
import org.light.verb.SearchByName;
import org.light.verb.SoftDelete;
import org.light.verb.Update;

public class EasyUIRegisterUserPI extends EasyUIGridPagePI {
	protected String title;
	protected Set<DomainFieldVerb> domainFieldVerbs = new TreeSet<>();
	
	public EasyUIRegisterUserPI(Domain domain) throws Exception{
		super(domain);
		this.standardName = "Register";
		this.verbs = new TreeSet<Verb>();
		this.verbs.add(new ListAll(domain));
		this.verbs.add(new ListActive(domain));
		this.verbs.add(new Delete(domain));
		this.verbs.add(new FindById(domain));
		this.verbs.add(new FindByName(domain));
		this.verbs.add(new SearchByName(domain));
		this.verbs.add(new SoftDelete(domain));
		this.verbs.add(new Update(domain));
		this.verbs.add(new Add(domain));
		this.verbs.add(new ListAllByPage(domain));
		this.setVerbs(verbs);
		for (Field f:this.domain.getPlainFields()) {
			if (f.getFieldType().equalsIgnoreCase("image")) {
				domainFieldVerbs.add(new AddUploadDomainField(this.domain,f));
				domainFieldVerbs.add(new UpdateUploadDomainField(this.domain,f));
			}
		}
	}
	
	public Verb findVerb(String verbName){
		for (Verb v : this.verbs){
			if(v.getVerbName().equals(verbName))
				return v;
		}
		return null;
	}
	
	public String generateJspString(){
		return generateStatementList().getContent();
	}
	
	public StatementList generateStatementList(){
		try {
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,0,"<!DOCTYPE html>"));
			sList.add(new Statement(2000L,0,"<html>"));
			sList.add(new Statement(3000L,0,"<head>"));
			sList.add(new Statement(4000L,2,"<meta charset=\"utf-8\">"));
			sList.add(new Statement(5000L,2,"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"));
			sList.add(new Statement(6000L,2,"<meta http-equiv=\"Expires\" CONTENT=\"0\">"));
			sList.add(new Statement(7000L,2,"<meta http-equiv=\"Cache-Control\" CONTENT=\"no-cache\">"));
			sList.add(new Statement(8000L,2,"<meta http-equiv=\"Pragma\" CONTENT=\"no-cache\">"));
			sList.add(new Statement(9000L,1,"<title>"+this.title+"</title>"));
			sList.add(new Statement(10000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/default/easyui.css\">"));
			sList.add(new Statement(11000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/icon.css\">"));
			sList.add(new Statement(12000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/demo/demo.css\">"));
			sList.add(new Statement(13000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.min.js\"></script>"));
			sList.add(new Statement(14000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.easyui.min.js\"></script>"));
			sList.add(new Statement(15000L,0,"<script type=\"text/javascript\" src=\"../js/sha1.js\"></script>"));
			sList.add(new Statement(16000L,0,"<script type=\"text/javascript\" src=\"../uploadjs/vendor/jquery.ui.widget.js\"></script>"));
			sList.add(new Statement(17000L,0,"<script type=\"text/javascript\" src=\"../uploadjs/jquery.iframe-transport.js\"></script>"));
			sList.add(new Statement(18000L,0,"<script type=\"text/javascript\" src=\"../uploadjs/jquery.fileupload.js\"></script>"));
			sList.add(new Statement(19000L,0,"</head>"));
			sList.add(new Statement(20000L,0,"<body  style=\"background:#B3DFDA;text-align:center\">"));
			sList.add(new Statement(21000L,0,"<center>"));
			sList.add(new Statement(22000L,0,"<div style=\"margin:20px 0\"></div>"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sList.add(new Statement(23000L,0,"<div class=\"easyui-panel\" title=\"Register new user.\" style=\"w"+this.domain.getDomainId().getLowerFirstFieldName()+"th:700px\">"));
			}else {
				sList.add(new Statement(23000L,0,"<div class=\"easyui-panel\" title=\"注册新用户\" style=\"w"+this.domain.getDomainId().getLowerFirstFieldName()+"th:700px\">"));
			}
			sList.add(new Statement(24000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
			sList.add(new Statement(25000L,0,"<form id=\"ff\" method=\"post\">"));
			sList.add(new Statement(26000L,0,"<table cellpadding=\"5\">"));
			sList.add(new Statement(27000L,0,"<tr><td>"+this.domain.findFieldByFixedName("userName").getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+"' id='"+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>"));
			sList.add(new Statement(28000L,0,"<tr><td>"+this.domain.findFieldByFixedName("password").getText()+":</td><td><input  class='easyui-textbox' type='password' name='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' id='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sList.add(new Statement(29000L,0,"<tr><td>Confirm "+this.domain.findFieldByFixedName("password").getText()+":</td><td><input  class='easyui-textbox' type='password' name='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' id='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>"));
			}else {
				sList.add(new Statement(29000L,0,"<tr><td>确认"+this.domain.findFieldByFixedName("password").getText()+":</td><td><input  class='easyui-textbox' type='password' name='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' id='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>"));
			}
			
			long serial = 30000L;
			Set<Field> fields3 = new TreeSet<Field>(new FieldSerialComparator());
			fields3.addAll(this.domain.getFieldsWithoutIdAndActive());
			Set<Field> deniedFields = new TreeSet<Field>(new FieldSerialComparator());
			deniedFields.add(this.domain.findFieldByFixedName("userName"));
			deniedFields.add(this.domain.findFieldByFixedName("password"));
			deniedFields.add(this.domain.findFieldByFixedName("salt"));
			deniedFields.add(this.domain.findFieldByFixedName("loginFailure"));
			fields3 = FieldUtil.filterDeniedFields(fields3,deniedFields);
			for (Field f: fields3){
				if (f.getFieldType().equalsIgnoreCase("image")) {
					sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><img name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' width='500px' src='../css/images/blank.jpg'><br>"));
					sList.add(new Statement(serial,0,"<input id=\"add"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"Fileupload\" type=\"file\" name=\"files[]\" data-url=\"../"+this.domain.getControllerPackagePrefix()+"login"+this.domain.getControllerNamingSuffix()+"/addUpload"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"\"><br></td></tr>"));
				}else {
					sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&!f.isTextarea())));
					sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><textarea  class='easyui-textarea' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' data-options=\"required:false\" cols='20' rows='4'></textarea></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&f.isTextarea())));
					sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input class='easyui-radio' type='radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='false'/>False</td></tr>",(!(f instanceof Dropdown) && f.getFieldType().equalsIgnoreCase("boolean"))));
					if (f instanceof Dropdown)
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f).getTarget().getLowerFirstDomainName()+((Dropdown)f).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td></tr>", f instanceof Dropdown));
				}
				serial+=1000L;
			}
	      			sList.add(new Statement(serial+52000L,0,"</table>"));
			sList.add(new Statement(serial+53000L,0,"</form>"));
			sList.add(new Statement(serial+54000L,0,"<div style=\"text-align:center;padding:5px\">"));
			if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
				sList.add(new Statement(serial+55000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"register"+this.domain.getCapFirstDomainName()+"()\">Register</a>"));
				sList.add(new Statement(serial+56000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"clearForm()\">Clear</a>"));
				sList.add(new Statement(serial+57000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"window.location='index.html'\">Go to login</a>"));
			}else{
				sList.add(new Statement(serial+55000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"register"+this.domain.getCapFirstDomainName()+"()\">注册</a>"));
				sList.add(new Statement(serial+56000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"clearForm()\">清除</a>"));
				sList.add(new Statement(serial+57000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"window.location='index.html'\">转到登录</a>"));
			}
			sList.add(new Statement(serial+58000L,0,"</div>"));
			sList.add(new Statement(serial+59000L,0,"</div>"));
			sList.add(new Statement(serial+60000L,0,"</div>"));
			sList.add(new Statement(serial+61000L,0,"</center>"));
			sList.add(new Statement(serial+62000L,0,"<script>"));
			
			serial += 63000L;
			if (this.domainFieldVerbs!=null&&this.domainFieldVerbs.size()>0) {
				sList.add(new Statement(serial,0,"$(function () {"));	
				serial+=1000L;
				for (DomainFieldVerb dfv:this.domainFieldVerbs) {
					JavascriptBlock uploadBlock = dfv.generateEasyUIJSButtonBlock();
					StatementList  slupload = uploadBlock.getMethodStatementList();
					slupload.setSerial(serial);
					sList.add(slupload);
					serial+= 1000L;
				}
				sList.add(new Statement(serial+2000L,0,"});"));
			}
			
			serial += 3000L;
			JavascriptMethod slRegisterJm = ((EasyUIPositions)new RegisterUser(this.domain)).generateEasyUIJSActionMethod();
			if(	slRegisterJm != null) {
				StatementList slRegisterAction = slRegisterJm.generateMethodStatementListIncludingContainer(serial,0);
				sList.add(slRegisterAction);
			}
			
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseBooleanMethod().generateMethodStatementList(serial+1000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementList(serial+2000L));
			
			serial += 3000L;
			sList.add(new Statement(serial,0,"</script>"));
			sList.add(new Statement(serial+1000L,0,"</body>"));
			sList.add(new Statement(serial+2000L,0,"</html>"));
			return WriteableUtil.merge(sList);
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public String getTitle() {
		return title;
	}
	
	@Override
	public void generatePIFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "WebContent/login/";
		String relativeFolder0 = "src/main/resources/static/login/";
		if ("smeu".equalsIgnoreCase(this.technicalStack)||"msmeu".equalsIgnoreCase(this.technicalStack)) {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+ "register.html", this.generateStatementList().getContent());
		}else {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder0+ "register.html", this.generateStatementList().getContent());
		}
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<DomainFieldVerb> getDomainFieldVerbs() {
		return domainFieldVerbs;
	}

	public void setDomainFieldVerbs(Set<DomainFieldVerb> domainFieldVerbs) {
		this.domainFieldVerbs = domainFieldVerbs;
	}
	
	@Override
	public void setTitles(String title, String subTitle, String footer) {
		this.title = title;
	}
}
