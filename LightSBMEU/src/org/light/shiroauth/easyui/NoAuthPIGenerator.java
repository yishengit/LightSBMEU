package org.light.shiroauth.easyui;

import java.util.ArrayList;
import java.util.List;

import org.light.core.PrismInterface;
import org.light.core.Writeable;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.WriteableUtil;

public class NoAuthPIGenerator extends PrismInterface{
	protected String title = "Java通用代码生成器光输出结果";

	public NoAuthPIGenerator() throws Exception{
		super();	
		this.setStandardName("NoAuth");
	}	

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.getFrame().setTitles(title, subTitle, footer);
	}

	@Override
	public void generatePIFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "WebContent/login/";
		String relativeFolder0 = "src/main/resources/static/login/";
		if ("smeu".equalsIgnoreCase(this.technicalStack)||"msmeu".equalsIgnoreCase(this.technicalStack)) {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+"noauth.html", this.generateStatementList().getContent());
		}else {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder0+"noauth.html", this.generateStatementList().getContent());
		}
	}

	private StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<!DOCTYPE html>"));
		sList.add(new Statement(2000L,0,"<html>"));
		sList.add(new Statement(3000L,0,"<head>"));
		sList.add(new Statement(4000L,0,"<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />"));
		sList.add(new Statement(5000L,1,"<title>"+this.title+"</title>"));
		sList.add(new Statement(6000L,1,"<link rel='stylesheet' type='text/css' href='../easyui/themes/default/easyui.css'>"));
		sList.add(new Statement(7000L,1,"<link rel='stylesheet' type='text/css' href='../easyui/themes/icon.css'>"));
		sList.add(new Statement(8000L,1,"<link rel='stylesheet' type='text/css' href='../easyui/demo/demo.css'>"));
		sList.add(new Statement(9000L,1,"<script type='text/javascript' src='../easyui/jquery.min.js'></script>"));
		sList.add(new Statement(10000L,1,"<script type='text/javascript' src='../easyui/jquery.easyui.min.js'></script>"));
		sList.add(new Statement(11000L,0,"</head>"));
		sList.add(new Statement(12000L,0,"<body  style=\"background:#B3DFDA;\">"));
		sList.add(new Statement(13000L,0,"<script type=\"text/javascript\">"));
		sList.add(new Statement(14000L,0,"$.messager.alert(\"警告\",\"没有相应授权！\",\"warning\",goback);"));
		sList.add(new Statement(15000L,0,"function goback(){"));
		sList.add(new Statement(16000L,1,"history.go(-1);"));
		sList.add(new Statement(17000L,0,"}"));
		sList.add(new Statement(18000L,0,"</script>"));
		sList.add(new Statement(19000L,0,"</body>"));
		return WriteableUtil.merge(sList);
	}

	@Override
	public void generatePIFromDomian() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validateDomain() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayout() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.getFrame().setNav(nav);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
