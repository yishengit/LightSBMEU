package org.light.shiroauth.easyui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.complexverb.DomainFieldVerb;
import org.light.core.PrismInterface;
import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.exception.ValidateException;
import org.light.include.JsonUserNav;
import org.light.utils.WriteableUtil;
import org.light.verb.Add;
import org.light.verb.Delete;
import org.light.verb.FindById;
import org.light.verb.FindByName;
import org.light.verb.ListActive;
import org.light.verb.ListAll;
import org.light.verb.ListAllByPage;
import org.light.verb.SearchByName;
import org.light.verb.SoftDelete;
import org.light.verb.Update;

public class EasyUILoginUserIndexPI extends PrismInterface {
	protected Set<DomainFieldVerb> domainFieldVerbs = new TreeSet<>();
	protected String title = "Java通用代码生成器光生成结果";
	protected String projectName = "Java通用代码生成器光生成结果";
	
	public EasyUILoginUserIndexPI(){
		super();
		this.standardName = "Index";
	}
	public EasyUILoginUserIndexPI(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.standardName = "Index";
		this.verbs = new TreeSet<Verb>();
		this.verbs.add(new ListAll(domain));
		this.verbs.add(new ListActive(domain));
		this.verbs.add(new Delete(domain));
		this.verbs.add(new FindById(domain));
		this.verbs.add(new FindByName(domain));
		this.verbs.add(new SearchByName(domain));
		this.verbs.add(new SoftDelete(domain));
		this.verbs.add(new Update(domain));
		this.verbs.add(new Add(domain));
		this.verbs.add(new ListAllByPage(domain));
	}
	
	public Verb findVerb(String verbName){
		for (Verb v : this.verbs){
			if(v.getVerbName().equals(verbName))
				return v;
		}
		return null;
	}
	
	public String generateJspString(){
		return generateStatementList().getContent();
	}
	
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<!DOCTYPE html>"));
		sList.add(new Statement(2000L,0,"<html>"));
		sList.add(new Statement(3000L,1,"<head>"));
		sList.add(new Statement(4000L,2,"<meta charset=\"utf-8\">"));
		sList.add(new Statement(5000L,2,"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"));
		sList.add(new Statement(6000L,2,"<meta http-equiv=\"Expires\" CONTENT=\"0\">"));
		sList.add(new Statement(7000L,2,"<meta http-equiv=\"Cache-Control\" CONTENT=\"no-cache\">"));
		sList.add(new Statement(8000L,2,"<meta http-equiv=\"Pragma\" CONTENT=\"no-cache\">"));
		sList.add(new Statement(9000L,2,"<title>"+this.projectName+"</title>"));
		sList.add(new Statement(10000L,2,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/default/easyui.css\">"));
		sList.add(new Statement(11000L,2,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/icon.css\">"));
		sList.add(new Statement(12000L,2,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/demo/demo.css\">"));
		sList.add(new Statement(13000L,2,"<!--  link rel=\"stylesheet\" type=\"text/css\" href=\"../qunit/qunit-1.20.0.css\">-->"));
		sList.add(new Statement(14000L,2,"<script type=\"text/javascript\" src=\"../easyui/jquery.min.js\"></script>"));
		sList.add(new Statement(15000L,2,"<script type=\"text/javascript\" src=\"../easyui/jquery.easyui.min.js\"></script>"));
		sList.add(new Statement(16000L,2,"<script type=\"text/javascript\" src=\"../js/sha1.js\"></script>"));
		sList.add(new Statement(17000L,1,"</head>"));
		sList.add(new Statement(18000L,1,"<body style=\"background:#B3DFDA;\">"));
		sList.add(new Statement(19000L,2,"<!--  div id=\"qunit\"></div>"));
		sList.add(new Statement(20000L,2,"<div id=\"qunit-fixture\"></div>-->"));
		sList.add(new Statement(21000L,2,"<div  style=\"height: 100%;width: 100%;padding-top: 200px\">"));
		if (this.domain.getLanguage().equalsIgnoreCase("english")) {
			sList.add(new Statement(22000L,2,"<div id=\"w\" class=\"easyui-window\" title=\"Login System\" data-options=\"iconCls:'icon-save',modal:true,closable:false,draggable:false,resizable:false,shadow:true,minimizable:true,maximizable:false,collapsible:false\" style=\"width:600px;height:400px;background: #B3DFDA;\">"));
		}else {
			sList.add(new Statement(22000L,2,"<div id=\"w\" class=\"easyui-window\" title=\"登录系统\" data-options=\"iconCls:'icon-save',modal:true,closable:false,draggable:false,resizable:false,shadow:true,minimizable:true,maximizable:false,collapsible:false\" style=\"width:600px;height:400px;background: #B3DFDA;\">"));
		}
		sList.add(new Statement(23000L,2,"<center><h2 style=\"margin-top:80px\">"+this.title+"</h2>"));
		sList.add(new Statement(24000L,3,"<table style=\"padding-top: 20px\"><tr><td>"+this.domain.findFieldByFixedName("userName").getText()+"：</td><td><input type=\"text\" class=\"easyui-textbox\" id=\""+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+"\" name=\""+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+"\"></input></td></tr>"));
		sList.add(new Statement(25000L,4,"<tr><td>"+this.domain.findFieldByFixedName("password").getText()+"：</td><td><input type=\"password\" class=\"easyui-textbox\" id=\""+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"\" name=\""+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"\"></input></td></tr>"));
		sList.add(new Statement(26000L,4,"<tr><td colspan=\"2\" style=\"text-align: center\">"));
		if (this.domain.getLanguage().equalsIgnoreCase("english")) {
			sList.add(new Statement(27000L,5,"<input class=\"easyui-button\" type=\"submit\" id=\"submitBtn\" name=\"submitBtn\" value=\"Login\" onclick=\"login()\" />"));
			sList.add(new Statement(28000L,5,"<input class=\"easyui-button\" type=\"reset\" id=\"resetBtn\" name=\"resetBtn\" value=\"Cancel\" />"));
		}else{
			sList.add(new Statement(27000L,5,"<input class=\"easyui-button\" type=\"submit\" id=\"submitBtn\" name=\"submitBtn\" value=\"登录\" onclick=\"login()\" />"));
			sList.add(new Statement(28000L,5,"<input class=\"easyui-button\" type=\"reset\" id=\"resetBtn\" name=\"resetBtn\" value=\"取消\" />"));
		}
		sList.add(new Statement(29000L,4,"</td>"));
		sList.add(new Statement(30000L,4,"</tr>"));
		sList.add(new Statement(31000L,4,"<tr>"));
		if (this.domain.getLanguage().equalsIgnoreCase("english")) {
			sList.add(new Statement(32000L,6,"<td colspan=\"2\" style=\"text-align: left\"><a href=\"register.html\">Register</a></td>"));
		}else {
			sList.add(new Statement(32000L,6,"<td colspan=\"2\" style=\"text-align: left\"><a href=\"register.html\">注册</a></td>"));
		}
		sList.add(new Statement(33000L,4,"</tr>"));
		sList.add(new Statement(34000L,3,"</table>"));
		sList.add(new Statement(35000L,3,"</center>"));
		sList.add(new Statement(36000L,3,"</div>"));
		sList.add(new Statement(37000L,3,"</div>"));
		sList.add(new Statement(38000L,2,"</div>"));
		sList.add(new Statement(39000L,1,""));
		sList.add(new Statement(40000L,1,"</body>"));
		sList.add(new Statement(41000L,0,"<script type=\"text/javascript\">"));
		sList.add(new Statement(42000L,0,"$(function(){"));
		sList.add(new Statement(43000L,1,"$(\"#w\").window();"));
		sList.add(new Statement(44000L,0,"});"));
		sList.add(new Statement(45000L,0,""));
		sList.add(new Statement(46000L,0,"function login(){"));
		sList.add(new Statement(47000L,1,"var "+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+" = $(\"#"+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+"\").val();"));
		sList.add(new Statement(48000L,1,"var "+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+" = hex_sha1($(\"#"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"\").val());"));
		sList.add(new Statement(50000L,1,"$.ajax({"));
		sList.add(new Statement(51000L,2,"type: \"post\","));
		sList.add(new Statement(52000L,2,"url: \"../"+this.domain.getControllerPackagePrefix()+"login"+domain.getControllerNamingSuffix()+"/login"+this.domain.getCapFirstDomainName()+"\","));
		sList.add(new Statement(53000L,2,"data: JSON.stringify( {"));
		sList.add(new Statement(54000L,3,""+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+":"+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+","));
		sList.add(new Statement(55000L,3,""+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+":"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+""));
		sList.add(new Statement(56000L,2,"}),"));
		sList.add(new Statement(57000L,2,"dataType: 'json',"));
		sList.add(new Statement(58000L,2,"contentType:\"application/json;charset=UTF-8\","));
		sList.add(new Statement(59000L,2,"success: function(data, textStatus) {"));
		sList.add(new Statement(60000L,3,"if (data.success) {"));
		sList.add(new Statement(61000L,4,"//var token = data.token;"));
		sList.add(new Statement(62000L,4,"//window.localStorage.setItem(\"mytoken\",token);"));
		sList.add(new Statement(63000L,4,"window.location=\"../pages/index.html\";"));
		sList.add(new Statement(64000L,3,"}"));
		sList.add(new Statement(65000L,2,"},"));
		sList.add(new Statement(66000L,2,"complete : function(XMLHttpRequest, textStatus) {"));
		sList.add(new Statement(67000L,2,"},"));
		sList.add(new Statement(68000L,2,"error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sList.add(new Statement(69000L,3,"alert(\"Error:\"+textStatus);"));
		sList.add(new Statement(70000L,3,"alert(errorThrown.toString());"));
		sList.add(new Statement(71000L,2,"}"));
		sList.add(new Statement(72000L,1,"});"));
		sList.add(new Statement(73000L,0,"}"));
		sList.add(new Statement(74000L,0,""));
		sList.add(new Statement(75000L,0,"</script>"));
		sList.add(new Statement(76000L,0,"</html>"));
		return WriteableUtil.merge(sList);
	}


	public Set<DomainFieldVerb> getDomainFieldVerbs() {
		return domainFieldVerbs;
	}

	public void setDomainFieldVerbs(Set<DomainFieldVerb> domainFieldVerbs) {
		this.domainFieldVerbs = domainFieldVerbs;
	}

	@Override
	public void generatePIFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "WebContent/login/";
		String relativeFolder0 = "src/main/resources/static/login/";
		if ("smeu".equalsIgnoreCase(this.technicalStack)||"msmeu".equalsIgnoreCase(this.technicalStack)) {
			WriteableUtil.writeToFile(targetFolderPath +relativeFolder+ "/index.html", this.generateStatementList().getContent());
		}else {
			WriteableUtil.writeToFile(targetFolderPath +relativeFolder0+ "/index.html", this.generateStatementList().getContent());
		}
	}

	@Override
	public void generatePIFromDomian() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validateDomain() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayout() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setTitles(String title, String subTitle, String footer) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
