package org.light.shiroauth;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.shiro.crypto.hash.Sha1Hash;
import org.light.complexverb.AddUploadDomainField;
import org.light.complexverb.DomainFieldVerb;
import org.light.complexverb.UpdateUploadDomainField;
import org.light.core.Module;
import org.light.core.PrismInterface;
import org.light.core.SpringMVCController;
import org.light.core.Verb;
import org.light.domain.Controller;
import org.light.domain.Dao;
import org.light.domain.DaoImpl;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.domain.Method;
import org.light.domain.Prism;
import org.light.domain.Service;
import org.light.domain.ServiceImpl;
import org.light.domain.Type;
import org.light.domain.Util;
import org.light.domain.ValidateInfo;
import org.light.easyuilayouts.widgets.Nav;
import org.light.exception.ValidateException;
import org.light.generator.MybatisDaoXmlDecorator;
import org.light.generator.NamedUtilMethodGenerator;
import org.light.layouts.EasyUIGridPagePI;
import org.light.layouts.EasyUIMtmPI;
import org.light.limitedverb.CountActiveRecords;
import org.light.limitedverb.CountAllPage;
import org.light.limitedverb.DaoOnlyVerb;
import org.light.limitedverb.NoControllerVerb;
import org.light.oracle.core.OracleDomainDecorator;
import org.light.oracle.core.OraclePrism;
import org.light.oracle.generator.MybatisOracleDaoXmlDecorator;
import org.light.shiroauth.easyui.EasyUIErrorPI;
import org.light.shiroauth.easyui.EasyUILoginUserIndexPI;
import org.light.shiroauth.easyui.EasyUIRegisterUserPI;
import org.light.shiroauth.easyui.EasyUIUserPagePI;
import org.light.shiroauth.easyui.NoAuthPIGenerator;
import org.light.shiroauth.verb.AddUser;
import org.light.shiroauth.verb.ChangePasswordUser;
import org.light.shiroauth.verb.GetPasswordUser;
import org.light.shiroauth.verb.GetSaltUser;
import org.light.shiroauth.verb.LoginUser;
import org.light.shiroauth.verb.LogoutUser;
import org.light.shiroauth.verb.ReadMySession;
import org.light.shiroauth.verb.RegisterUser;
import org.light.shiroauth.verb.UpdateUser;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;
import org.light.verb.Activate;
import org.light.verb.ActivateAll;
import org.light.verb.Add;
import org.light.verb.Clone;
import org.light.verb.CloneAll;
import org.light.verb.CountSearchByFieldsRecordsWithDeniedFields;
import org.light.verb.Delete;
import org.light.verb.DeleteAll;
import org.light.verb.Export;
import org.light.verb.ExportExcelFilterFields;
import org.light.verb.ExportPDF;
import org.light.verb.ExportPDFFilterFields;
import org.light.verb.ExportWord;
import org.light.verb.ExportWordFilterFields;
import org.light.verb.FilterExcel;
import org.light.verb.FilterExcelFilterFields;
import org.light.verb.FilterPDF;
import org.light.verb.FilterPDFFilterFields;
import org.light.verb.FilterWord;
import org.light.verb.FilterWordFilterFields;
import org.light.verb.FindById;
import org.light.verb.FindByName;
import org.light.verb.ListActive;
import org.light.verb.ListAll;
import org.light.verb.ListAllByPage;
import org.light.verb.SearchByFieldsByPageWithDeniedFields;
import org.light.verb.SearchByFieldsWithDeniedFields;
import org.light.verb.SearchByName;
import org.light.verb.SoftDelete;
import org.light.verb.SoftDeleteAll;
import org.light.verb.Toggle;
import org.light.verb.ToggleOne;
import org.light.verb.Update;
public class ShiroAuthModule extends Module{
	protected Domain userDomain;
	protected Domain roleDomain;
	protected Domain privilegeDomain;
	protected Prism loginPrism;
	protected Prism userPrism;
	protected Prism rolePrism;
	protected Prism privilegePrism;
	protected List<Util> shiroUtils = new ArrayList<>();
	protected String generateData = "whennecessary";
	protected ShiroConfigGenerator scg;
	protected UserRealmGenerator urg;
	
	public ShiroAuthModule() {
		super();
		this.standardName = "ShiroAuthModule";
	}
	
	public boolean validateDomains() throws ValidateException{
		ValidateInfo info = new ValidateInfo();
		if (userDomain==null||roleDomain==null||privilegeDomain==null) {
			info.addCompileError("所需域对象缺失！");			
		}
		if (userDomain.findFieldByFixedName("userName")==null||!userDomain.findFieldByFixedName("userName").getFieldType().equalsIgnoreCase("String")) {
			info.addCompileError("用户域对象固定名字userName字段设置有问题！");			
		}
		if (userDomain.findFieldByFixedName("password")==null||!userDomain.findFieldByFixedName("password").getFieldType().equalsIgnoreCase("String")
				||(!StringUtil.isBlank(userDomain.findFieldByFixedName("password").getLengthStr())&&Integer.parseInt(userDomain.findFieldByFixedName("password").getLengthStr())<40)){
			info.addCompileError("用户域对象固定名字password字段设置有问题！");			
		}	
		if (userDomain.findFieldByFixedName("salt")==null||!userDomain.findFieldByFixedName("salt").getFieldType().equalsIgnoreCase("String")
				||(!StringUtil.isBlank(userDomain.findFieldByFixedName("salt").getLengthStr())&&Integer.parseInt(userDomain.findFieldByFixedName("salt").getLengthStr())<8)){
			info.addCompileError("用户域对象固定名字salt字段设置有问题！");			
		}	
		if (userDomain.findFieldByFixedName("loginFailure")==null||!userDomain.findFieldByFixedName("loginFailure").getClassType().getTypeName().equalsIgnoreCase("Integer")){
			info.addCompileError("用户域对象固定名字loginFailure字段设置有问题！");			
		}	
//		if (roleDomain.findFieldByFixedName("roleName")==null||!roleDomain.findFieldByFixedName("roleName").getFieldType().equalsIgnoreCase("String")) {
//			info.addCompileError("角色域对象roleName字段设置有问题！");			
//		}		
		if (info.success(true))	return true;
		else throw new ValidateException(info);
	}
	
	public boolean validateDataDomains() throws ValidateException{
		ValidateInfo info = new ValidateInfo();
		if (info.success(true))	return true;
		else throw new ValidateException(info);
	}

	@Override
	public void generateModuleFiles(String targetFolderPath) throws Exception {
		this.setPrisms(new ArrayList<>());
		this.addPrism(this.loginPrism);
		this.addPrism(this.userPrism);
		this.addPrism(this.rolePrism);
		this.addPrism(this.privilegePrism);
		if ("smeu".equalsIgnoreCase(this.getTechnicalstack())||"msmeu".equalsIgnoreCase(this.getTechnicalstack())) {
			for (Prism p:this.getPrisms()) {
				if (p instanceof OraclePrism) {
					OraclePrism op = (OraclePrism) p;
					setFolderPath(targetFolderPath);
					generateOraclePrismSMEUFiles(op,true,true,true,true,true,true,true);
				}else{
					setFolderPath(targetFolderPath);
					generatePrismSMEUFiles(p,true,true,true,true,true,true,true);
				}
			}
			for (Util u : shiroUtils) {
				String utilPath = targetFolderPath + "src/"
						+ packagetokenToFolder(u.getPackageToken()) + "shiro/";
				writeToFile(utilPath + u.getFileName(), u.generateUtilString());
			}
		}else {
			for (Prism p:this.getPrisms()) {
				if (p instanceof OraclePrism) {
					OraclePrism op = (OraclePrism) p;
					setFolderPath(targetFolderPath);
					generateOraclePrismFiles(op,true,true,true,true,true,true,true);
				}else{
					setFolderPath(targetFolderPath);
					generatePrismFiles(p,true,true,true,true,true,true,true);
				}
			}
			for (Util u : shiroUtils) {
				String utilPath = targetFolderPath + "src/main/java/"
						+ packagetokenToFolder(u.getPackageToken()) + "shiro/";
				writeToFile(utilPath + u.getFileName(), u.generateUtilString());
			}
		}
	}

	@Override
	public void generatePrismsFromDomians() throws Exception {
		if ("MariaDB".equalsIgnoreCase(this.dbType)||"MySQL".equalsIgnoreCase(this.dbType)||"pgsql".equalsIgnoreCase(this.dbType)||"PostgreSQL".equalsIgnoreCase(this.dbType)) {
			Prism loginPrism = new Prism();	
			loginPrism.setDomain(this.userDomain);
			loginPrism.setStandardName("LoginPrism");
			loginPrism.setDbType(this.dbType);
			loginPrism.setPackageToken(this.getPackageToken());
			this.loginPrism = loginPrism;
			this.loginPrism.setTitle(this.title);
			this.loginPrism.setSubTitle(this.subTitle);
			this.loginPrism.setFooter(this.footer);
			this.loginPrism.setTechnicalStack(this.technicalstack);
			generateLoginPrismFromUserDomain(true);
			decorateLoginPrism();
			
			Prism userPrism = new Prism();
			userPrism.setDomain(this.userDomain);
			userPrism.setStandardName("UserPrism");
			userPrism.setDbType(this.dbType);
			userPrism.setPackageToken(this.getPackageToken());
			Set<Domain> projectDomains = new TreeSet<>();
			projectDomains.addAll(this.domains);
			userPrism.setProjectDomains(projectDomains);
			this.userPrism = userPrism;
			
			Set<Field> deniedFields = new TreeSet<>();
			deniedFields.add(this.userDomain.findFieldByFixedName("password"));
			deniedFields.add(this.userDomain.findFieldByFixedName("salt"));
			deniedFields.add(this.userDomain.findFieldByFixedName("loginFailure"));
			
			generateUserPrismFromUserDomainWithDeniedFields(true,deniedFields);

			this.userPrism.setTitle(this.title);
			this.userPrism.setSubTitle(this.subTitle);
			this.userPrism.setFooter(this.footer);
			this.userPrism.setTechnicalStack(this.technicalstack);
			decorateUserPrism();
			
			Prism rolePrism = new Prism();	
			rolePrism.setTitle(this.title);
			rolePrism.setSubTitle(this.subTitle);
			rolePrism.setFooter(this.footer);
			rolePrism.setDomain(this.roleDomain);
			rolePrism.setStandardName("RolePrism");
			rolePrism.setDbType(this.dbType);
			rolePrism.setPackageToken(this.getPackageToken());
			rolePrism.setProjectDomains(projectDomains);
			rolePrism.setTechnicalStack(this.technicalstack);
			rolePrism.generatePrismFromDomain(true);
			Prism privilegePrism = new Prism();
			privilegePrism.setTitle(this.title);
			privilegePrism.setSubTitle(this.subTitle);
			privilegePrism.setFooter(this.footer);
			privilegePrism.setDomain(this.privilegeDomain);
			privilegePrism.setStandardName("PrivilegePrism");
			privilegePrism.setDbType(this.dbType);
			privilegePrism.setPackageToken(this.getPackageToken());
			privilegePrism.setProjectDomains(projectDomains);
			privilegePrism.setTechnicalStack(this.technicalstack);
			privilegePrism.generatePrismFromDomain(true);	
	
			this.rolePrism = rolePrism;
			this.rolePrism.setTitle(this.title);
			this.rolePrism.setSubTitle(this.subTitle);
			this.rolePrism.setFooter(this.footer);
			this.privilegePrism = privilegePrism;
			this.privilegePrism.setTitle(this.title);
			this.privilegePrism.setSubTitle(this.subTitle);
			this.privilegePrism.setFooter(this.footer);		
			decorateVerbDbType(this.loginPrism,this.dbType);
			decorateVerbDbType(this.userPrism,this.dbType);
			decorateVerbDbType(this.rolePrism,this.dbType);
			decorateVerbDbType(this.privilegePrism,this.dbType);
			
			this.scg = new ShiroConfigGenerator(this.getPackageToken());
			scg.setUserDomain(this.userDomain);
			scg.setRoleDomain(this.roleDomain);
			scg.setPrivilegeDomain(this.privilegeDomain);
			scg.setNav(this.nav);
			
			this.urg = new UserRealmGenerator(this.getPackageToken());
			urg.setUserDomain(this.userDomain);
			urg.setRoleDomain(this.roleDomain);
			urg.setPrivilegeDomain(this.privilegeDomain);
			UserRegisteAndLoginGenerator uralg = new UserRegisteAndLoginGenerator(this.getPackageToken());
			uralg.setUserDomain(this.userDomain);
			this.shiroUtils.add(scg);
			this.shiroUtils.add(urg);
			this.shiroUtils.add(uralg);
		}else if ("Oracle".equalsIgnoreCase(this.dbType)) {
			OraclePrism loginPrism = new OraclePrism();	
			OracleDomainDecorator.decorateOracleDomain(this.userDomain);
			loginPrism.setDomain(this.userDomain);
			loginPrism.setStandardName("LoginPrism");
			loginPrism.setPackageToken(this.getPackageToken());
			this.loginPrism = loginPrism;
			this.loginPrism.setTitle(this.title);
			this.loginPrism.setSubTitle(this.subTitle);
			this.loginPrism.setFooter(this.footer);
			this.loginPrism.setTechnicalStack(this.technicalstack);
			generateLoginOraclePrismFromUserDomain(true);
			decorateLoginPrism();
			
			OraclePrism userPrism = new OraclePrism();
			Set<Domain> myDomains = new TreeSet<>();
			myDomains.addAll(this.domains);
			userPrism.setProjectDomains(myDomains);
			userPrism.setDomain(this.userDomain);
			userPrism.setDomain(this.userDomain);
			userPrism.setStandardName("UserPrism");
			userPrism.setPackageToken(this.getPackageToken());
			userPrism.setTechnicalStack(this.technicalstack);
			
			Set<Field> deniedFields = new TreeSet<>();
			deniedFields.add(this.userDomain.findFieldByFixedName("password"));
			deniedFields.add(this.userDomain.findFieldByFixedName("salt"));
			deniedFields.add(this.userDomain.findFieldByFixedName("loginFailure"));
			
			userPrism.generatePrismFromDomainWithDeniedFields(true,deniedFields);
			this.userPrism = userPrism;
			this.userPrism.setTitle(this.title);
			this.userPrism.setSubTitle(this.subTitle);
			this.userPrism.setFooter(this.footer);
			decorateUserPrism();
			
			OraclePrism rolePrism = new OraclePrism();	
			this.roleDomain = OracleDomainDecorator.decorateOracleDomain(this.roleDomain);
			rolePrism.setProjectDomains(myDomains);
			rolePrism.setDomain(this.roleDomain);
			rolePrism.setStandardName("RolePrism");
			rolePrism.setTitle(this.title);
			rolePrism.setSubTitle(this.subTitle);
			rolePrism.setFooter(this.footer);
			rolePrism.setPackageToken(this.getPackageToken());
			rolePrism.setTechnicalStack(this.technicalstack);
			rolePrism.generatePrismFromDomain(true);
			OraclePrism privilegePrism = new OraclePrism();
			this.privilegeDomain = OracleDomainDecorator.decorateOracleDomain(this.privilegeDomain);
			privilegePrism.setTitle(this.title);
			privilegePrism.setSubTitle(this.subTitle);
			privilegePrism.setFooter(this.footer);
			privilegePrism.setDomain(this.privilegeDomain);
			privilegePrism.setStandardName("PrivilegePrism");
			privilegePrism.setPackageToken(this.getPackageToken());
			privilegePrism.setProjectDomains(myDomains);
			privilegePrism.setTechnicalStack(this.technicalstack);
			privilegePrism.generatePrismFromDomain(true);	
	
			this.rolePrism = rolePrism;
			this.privilegePrism = privilegePrism;
			this.privilegePrism.setTitle(this.title);
			this.privilegePrism.setSubTitle(this.subTitle);
			this.privilegePrism.setFooter(this.footer);		
			decorateVerbDbType(this.loginPrism,this.dbType);
			decorateVerbDbType(this.userPrism,this.dbType);
			decorateVerbDbType(this.rolePrism,this.dbType);
			decorateVerbDbType(this.privilegePrism,this.dbType);
			
			this.scg = new ShiroConfigGenerator(this.getPackageToken());			
			scg.setUserDomain(this.userDomain);
			scg.setRoleDomain(this.roleDomain);
			scg.setPrivilegeDomain(this.privilegeDomain);
			scg.setNav(this.nav);
			
			this.urg = new UserRealmGenerator(this.getPackageToken());
			urg.setUserDomain(this.userDomain);
			urg.setRoleDomain(this.roleDomain);
			urg.setPrivilegeDomain(this.privilegeDomain);
			UserRegisteAndLoginGenerator uralg = new UserRegisteAndLoginGenerator(this.getPackageToken());
			uralg.setUserDomain(this.userDomain);
			this.shiroUtils.add(scg);
			this.shiroUtils.add(urg);
			this.shiroUtils.add(uralg);
		}
	}
	
	public void generateLoginPrismFromUserDomain(Boolean ignoreWarning) throws ValidateException, Exception {
		if (this.userDomain != null) {
			Dao dao = new Dao();
			dao.setDomain(this.userDomain);
			dao.setPackageToken(this.userDomain.getPackageToken());
			this.loginPrism.setDao(dao);
			DaoImpl daoimpl = new DaoImpl();
			daoimpl.setPackageToken(this.userDomain.getPackageToken());
			daoimpl.setDomain(this.userDomain);
			this.loginPrism.setDaoimpl(daoimpl);
			Service service = new Service();
			service.setDomain(this.userDomain);
			service.setPackageToken(this.userDomain.getPackageToken());
			this.loginPrism.setService(service);
			ServiceImpl serviceimpl = new ServiceImpl(this.userDomain);
			serviceimpl.setPackageToken(this.userDomain.getPackageToken());
			serviceimpl.getDao().addAnnotation("Autowired");
			Method daoSetter = NamedUtilMethodGenerator.generateSetter("dao",
					new Type(this.userDomain.getCapFirstDomainName() + "Dao"));
			serviceimpl.addMethod(daoSetter);
			serviceimpl.addClassImports("org.springframework.beans.factory.annotation.Autowired");
			serviceimpl.setDomain(this.userDomain);
			serviceimpl.setService(service);
			this.loginPrism.setServiceimpl(serviceimpl);
			
			SpringMVCController controller = new SpringMVCController(this.userDomain);
			controller.decorateCrossOrigin(this.getCrossOrigin());
			controller.setPackageToken(this.packageToken);
			controller.setStandardName("Login"+this.userDomain.getControllerNamingSuffix());
			controller.setDomain(this.userDomain);
			Field sessionField = new Field("session","HttpSession");
			controller.addField(sessionField);
			controller.fixingDecorate(this.userDomain);
			this.loginPrism.setController(controller);
			
			MybatisDaoXmlDecorator mybatisDaoXmlDecorator = new MybatisDaoXmlDecorator();
			mybatisDaoXmlDecorator.setDomain(this.getUserDomain());
			Set<Domain> resultMaps = new TreeSet<Domain>();
			resultMaps.add(this.userDomain);
			mybatisDaoXmlDecorator.setResultMaps(resultMaps);
			this.loginPrism.setMybatisDaoXmlDecorator(mybatisDaoXmlDecorator);
			
			for (Field f:this.userDomain.getPlainFields()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					this.loginPrism.getDomianFieldVerbs().add(new AddUploadDomainField(this.userDomain,f));
					this.loginPrism.getDomianFieldVerbs().add(new UpdateUploadDomainField(this.userDomain,f));
				}
			}
			
			EasyUILoginUserIndexPI eIndex = new EasyUILoginUserIndexPI();
			EasyUIErrorPI eErr = new EasyUIErrorPI();
			EasyUIRegisterUserPI eRegister = new EasyUIRegisterUserPI(this.userDomain);
			NoAuthPIGenerator npg = new NoAuthPIGenerator();
			npg.setTitle(this.title);
			eIndex.setTitles(this.getTitle(),this.getSubTitle(),this.getFooter());
			eIndex.setProjectName(this.getProjectName());
			eErr.setTitles(this.getTitle(),this.getSubTitle(),this.getFooter());
			eRegister.setTitles(this.getTitle(),this.getSubTitle(),this.getFooter());
			eIndex.setDomain(this.userDomain);
			eErr.setDomain(this.userDomain);
			eIndex.setTechnicalStack(this.technicalstack);
			eErr.setTechnicalStack(this.technicalstack);
			eRegister.setTechnicalStack(this.technicalstack);
			npg.setTechnicalStack(this.technicalstack);
			if ("msmeu".equalsIgnoreCase(this.getTechnicalstack())||"smeu".equalsIgnoreCase(this.getTechnicalstack())){
				eErr.setProjectNamePrefix(this.getProjectName());
			}
			eRegister.setTitles(this.getTitle(),this.getSubTitle(),this.getFooter());
			eRegister.setDomain(this.userDomain);
			this.loginPrism.addPage(eIndex);
			this.loginPrism.addPage(eErr);
			this.loginPrism.addPage(eRegister);		
			this.loginPrism.addPage(npg);	
		}
	}
	
	public void generateLoginOraclePrismFromUserDomain(Boolean ignoreWarning) throws ValidateException, Exception {
		if (this.userDomain != null) {
			Dao dao = new Dao();
			dao.setDomain(this.userDomain);
			dao.setPackageToken(this.userDomain.getPackageToken());
			this.loginPrism.setDao(dao);
			DaoImpl daoimpl = new DaoImpl();
			daoimpl.setPackageToken(this.userDomain.getPackageToken());
			daoimpl.setDomain(this.userDomain);
			this.loginPrism.setDaoimpl(daoimpl);
			Service service = new Service();
			service.setDomain(this.userDomain);
			service.setPackageToken(this.userDomain.getPackageToken());
			this.loginPrism.setService(service);
			ServiceImpl serviceimpl = new ServiceImpl(this.userDomain);
			serviceimpl.setPackageToken(this.userDomain.getPackageToken());
			serviceimpl.getDao().addAnnotation("Autowired");
			Method daoSetter = NamedUtilMethodGenerator.generateSetter("dao",
					new Type(this.userDomain.getCapFirstDomainName() + "Dao"));
			serviceimpl.addMethod(daoSetter);
			serviceimpl.addClassImports("org.springframework.beans.factory.annotation.Autowired");
			serviceimpl.setDomain(this.userDomain);
			serviceimpl.setService(service);
			this.loginPrism.setServiceimpl(serviceimpl);
			
			SpringMVCController controller = new SpringMVCController(this.userDomain);
			controller.decorateCrossOrigin(this.getCrossOrigin());
			controller.setPackageToken(this.packageToken);
			controller.setStandardName("Login"+this.userDomain.getControllerNamingSuffix());
			controller.setDomain(this.userDomain);
			Field sessionField = new Field("session","HttpSession");
			controller.addField(sessionField);
			controller.fixingDecorate(this.userDomain);
			this.loginPrism.setController(controller);
			
			MybatisOracleDaoXmlDecorator mybatisOracleDaoXmlDecorator = new MybatisOracleDaoXmlDecorator();
			mybatisOracleDaoXmlDecorator.setDomain(this.getUserDomain());
			Set<Domain> resultMaps = new TreeSet<Domain>();
			resultMaps.add(this.userDomain);
			mybatisOracleDaoXmlDecorator.setResultMaps(resultMaps);
			((OraclePrism)this.loginPrism).setMybatisOracleDaoXmlDecorator(mybatisOracleDaoXmlDecorator);
			
			for (Field f:this.userDomain.getPlainFields()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					this.loginPrism.getDomianFieldVerbs().add(new AddUploadDomainField(this.userDomain,f));
					this.loginPrism.getDomianFieldVerbs().add(new UpdateUploadDomainField(this.userDomain,f));
				}
			}
			
			EasyUILoginUserIndexPI eIndex = new EasyUILoginUserIndexPI();
			EasyUIErrorPI eErr = new EasyUIErrorPI();
			EasyUIRegisterUserPI eRegister = new EasyUIRegisterUserPI(this.userDomain);
			NoAuthPIGenerator npg = new NoAuthPIGenerator();
			npg.setTitle(this.title);
			eIndex.setProjectName(this.getProjectName());
			eIndex.setTitles(this.getTitle(),this.getSubTitle(),this.getFooter());
			eErr.setTitles(this.getTitle(),this.getSubTitle(),this.getFooter());
			eRegister.setTitles(this.getTitle(),this.getSubTitle(),this.getFooter());
			eIndex.setTechnicalStack(this.technicalstack);
			eErr.setTechnicalStack(this.technicalstack);
			eRegister.setTechnicalStack(this.technicalstack);
			npg.setTechnicalStack(this.technicalstack);
			eIndex.setDomain(this.userDomain);
			eErr.setDomain(this.userDomain);
			if ("msmeu".equalsIgnoreCase(this.getTechnicalstack())||"smeu".equalsIgnoreCase(this.getTechnicalstack())){
				eErr.setProjectNamePrefix(this.getProjectName());
			}
			eRegister.setDomain(this.userDomain);
			this.loginPrism.addPage(eIndex);
			this.loginPrism.addPage(eErr);
			this.loginPrism.addPage(eRegister);	
			this.loginPrism.addPage(npg);
		}
	}
	
	public void decorateUserPrism() throws Exception{
		Domain user = this.getUserDomain();
		RegisterUser registerUser = new RegisterUser(user,this.dbType);
		LoginUser loginUser = new LoginUser(user);
		ChangePasswordUser changePassword = new ChangePasswordUser(user,this.dbType);
		GetPasswordUser getPasswordUser = new GetPasswordUser(user);
		GetSaltUser getSalt = new GetSaltUser(user);		
		Set<Verb> verbs = new TreeSet<Verb>();
		verbs.add(registerUser);
		verbs.add(loginUser);
		verbs.add(changePassword);
		verbs.add(getPasswordUser);
		verbs.add(getSalt);
		if (this.userPrism instanceof OraclePrism)decorateVerbsInUserOraclePrism(verbs);
		else decorateVerbsInUserPrism(verbs);
	}
	
	public void decorateLoginPrism() throws Exception{
		Domain user = this.getUserDomain();
		RegisterUser registerUser = new RegisterUser(user,this.dbType);
		LoginUser loginUser = new LoginUser(user);
		LogoutUser logoutUser = new LogoutUser(user);
		//ChangePasswordUser changePassword = new ChangePasswordUser(user);
		GetPasswordUser getPasswordUser = new GetPasswordUser(user);
		ReadMySession readMySession = new ReadMySession(user);
		GetSaltUser getSalt = new GetSaltUser(user);
		Set<Verb> verbs = new TreeSet<Verb>();
		verbs.add(registerUser);
		verbs.add(loginUser);
		verbs.add(logoutUser);
		//verbs.add(changePassword);
		verbs.add(getPasswordUser);
		verbs.add(getSalt);
		verbs.add(readMySession);
		decorateVerbsInLoginPrism(verbs);
	}
	
	public void decorateVerbsInUserPrism(Set<Verb> verbs) throws Exception{
		Set<Verb> removeVerbs = new TreeSet<>();
		
		Update update = new Update(this.userDomain);
		Export ep = new Export(this.userDomain);
		ExportPDF epp = new ExportPDF(this.userDomain);
		ExportWord epw = new ExportWord(this.userDomain);
		FilterExcel fef = new FilterExcel(this.userDomain);
		FilterPDF fpf = new FilterPDF(this.userDomain);
		FilterWord fwf = new FilterWord(this.userDomain);

		removeVerbs.add(update);
		removeVerbs.add(ep);
		removeVerbs.add(epp);
		removeVerbs.add(epw);
		removeVerbs.add(fef);
		removeVerbs.add(fpf);
		removeVerbs.add(fwf);
		
		for (Verb v2:removeVerbs) {
			this.userPrism.removeVerb(v2);				
		}
		
		Domain domain = this.getUserDomain();
		Service service = this.userPrism.getService();
		ServiceImpl serviceimpl = this.userPrism.getServiceImpl();
		Dao dao = this.userPrism.getDao();
		DaoImpl daoimpl = this.userPrism.getDaoImpl();
		MybatisDaoXmlDecorator mdecorater = this.userPrism.getMybatisDaoXmlDecorator();
		Controller controller = this.userPrism.getController();
		for (Verb v : verbs) {
			this.userPrism.addVerb(v);
			v.setDomain(domain);
			service.addMethod(v.generateServiceMethodDefinition());
			serviceimpl.addMethod(v.generateServiceImplMethod());
			dao.addMethod(v.generateDaoMethodDefinition());
			daoimpl.addMethod(v.generateDaoImplMethod());
			mdecorater.addDaoXmlMethod(v.generateDaoImplMethod());
			if (v.getVerbName().equals("ChangePassword"+this.userDomain.getCapFirstDomainName())) {
				controller.addMethod(v.generateControllerMethod());
			}
		}
		
		Set<Field> deniedFields = new TreeSet<>();
		for (Field f:this.userDomain.getFields()) {
			if ("password".equals(f.getFixedName())||"salt".equals(f.getFixedName())||"loginFailure".equals(f.getFixedName())) {
				deniedFields.add(f);
			}
		}
		UpdateUser updateUser = new UpdateUser(this.userDomain);
		updateUser.setDbType(this.dbType);
		updateUser.setDeniedFields(deniedFields);
		Set<Verb> replacedVerbs = new TreeSet<>();
		replacedVerbs.add(updateUser);
		
		ExportExcelFilterFields eeff = new ExportExcelFilterFields(this.userDomain);
		ExportPDFFilterFields epff = new ExportPDFFilterFields(this.userDomain);
		ExportWordFilterFields ewff = new ExportWordFilterFields(this.userDomain);
		
		FilterExcelFilterFields feff = new FilterExcelFilterFields(this.userDomain);
		FilterPDFFilterFields fpff = new FilterPDFFilterFields(this.userDomain);
		FilterWordFilterFields fwff = new FilterWordFilterFields(this.userDomain);
		
		eeff.setDeniedFields(deniedFields);
		epff.setDeniedFields(deniedFields);
		ewff.setDeniedFields(deniedFields);
		feff.setDeniedFields(deniedFields);
		fpff.setDeniedFields(deniedFields);
		fwff.setDeniedFields(deniedFields);
		Set<Verb> exVerbs = new TreeSet<>();
		exVerbs.add(updateUser);
		exVerbs.add(eeff);
		exVerbs.add(epff);
		exVerbs.add(ewff);
		exVerbs.add(feff);
		exVerbs.add(fpff);
		exVerbs.add(fwff);
		for (Verb v : exVerbs) {
			this.userPrism.addVerb(v);
			v.setDomain(domain);
//			service.addMethod(v.generateServiceMethodDefinition());
//			serviceimpl.addMethod(v.generateServiceImplMethod());
//			dao.addMethod(v.generateDaoMethodDefinition());
//			daoimpl.addMethod(v.generateDaoImplMethod());
//			mdecorater.addDaoXmlMethod(v.generateDaoImplMethod());
			controller.addMethod(v.generateControllerMethod());
		}
		
		for (Verb v :replacedVerbs) {
			service.addMethod(v.generateServiceMethodDefinition());
			serviceimpl.addMethod(v.generateServiceImplMethod());
			dao.addMethod(v.generateDaoMethodDefinition());
			daoimpl.addMethod(v.generateDaoImplMethod());
			mdecorater.addDaoXmlMethod(v.generateDaoImplMethod());
			controller.addMethod(v.generateControllerMethod());
		}
		
		this.userPrism.setPages(new TreeSet<PrismInterface>());
		EasyUIUserPagePI eUser = new EasyUIUserPagePI(this.userDomain,this.technicalstack,this.dbType);
		eUser.setTitles(this.title, this.subTitle, this.footer);
		
		List<Domain> allDomainList = new ArrayList<>();
		allDomainList.add(this.userDomain);
		allDomainList.add(this.roleDomain);
		allDomainList.add(this.privilegeDomain);
		this.userPrism.addPage(eUser);
	}	
	
	public void decorateVerbsInUserOraclePrism(Set<Verb> verbs) throws Exception{
		Set<Verb> removeVerbs = new TreeSet<>();
		Update update = new Update(this.userDomain);
		Add add = new Add(this.userDomain);
		
		Export ep = new Export(this.userDomain);
		ExportPDF epp = new ExportPDF(this.userDomain);
		ExportWord epw = new ExportWord(this.userDomain);
		FilterExcel fef = new FilterExcel(this.userDomain);
		FilterPDF fpf = new FilterPDF(this.userDomain);
		FilterWord fwf = new FilterWord(this.userDomain);
		
		removeVerbs.add(add);
		removeVerbs.add(update);
		removeVerbs.add(ep);
		removeVerbs.add(epp);
		removeVerbs.add(epw);
		removeVerbs.add(fef);
		removeVerbs.add(fpf);
		removeVerbs.add(fwf);
		
		for (Verb v2:removeVerbs) {
			this.userPrism.removeVerb(v2);				
		}		
		Domain domain = this.getUserDomain();
		Service service = this.userPrism.getService();
		ServiceImpl serviceimpl = this.userPrism.getServiceImpl();
		Dao dao = this.userPrism.getDao();
		DaoImpl daoimpl = this.userPrism.getDaoImpl();
		MybatisOracleDaoXmlDecorator mdecorater = ((OraclePrism)this.userPrism).getMybatisOracleDaoXmlDecorator();
		Controller controller = this.userPrism.getController();
		for (Verb v : verbs) {
			this.userPrism.addVerb(v);
			v.setDomain(domain);
			service.addMethod(v.generateServiceMethodDefinition());
			serviceimpl.addMethod(v.generateServiceImplMethod());
			dao.addMethod(v.generateDaoMethodDefinition());
			daoimpl.addMethod(v.generateDaoImplMethod());
			mdecorater.addDaoXmlMethod(v.generateDaoImplMethod());
			if (v.getVerbName().equals("ChangePassword"+this.userDomain.getCapFirstDomainName())) {
				controller.addMethod(v.generateControllerMethod());
			}
		}
		
		Set<Field> deniedFields = new TreeSet<>();
		for (Field f:this.userDomain.getFields()) {
			if ("password".equals(f.getFixedName())||"salt".equals(f.getFixedName())||"loginFailure".equals(f.getFixedName())) {
				deniedFields.add(f);
			}
		}
		
		AddUser addUser = new AddUser(this.userDomain);
		addUser.setDbType(this.dbType);
			
		UpdateUser updateUser = new UpdateUser(this.userDomain);
		updateUser.setDbType(this.dbType);
		updateUser.setDeniedFields(deniedFields);
		Set<Verb> replacedVerbs = new TreeSet<>();
		replacedVerbs.add(addUser);
		replacedVerbs.add(updateUser);
		
		ExportExcelFilterFields eeff = new ExportExcelFilterFields(this.userDomain);
		ExportPDFFilterFields epff = new ExportPDFFilterFields(this.userDomain);
		ExportWordFilterFields ewff = new ExportWordFilterFields(this.userDomain);
		
		FilterExcelFilterFields feff = new FilterExcelFilterFields(this.userDomain);
		FilterPDFFilterFields fpff = new FilterPDFFilterFields(this.userDomain);
		FilterWordFilterFields fwff = new FilterWordFilterFields(this.userDomain);
		
		eeff.setDeniedFields(deniedFields);
		epff.setDeniedFields(deniedFields);
		ewff.setDeniedFields(deniedFields);
		feff.setDeniedFields(deniedFields);
		fpff.setDeniedFields(deniedFields);
		fwff.setDeniedFields(deniedFields);
		Set<Verb> exVerbs = new TreeSet<>();
		
		exVerbs.add(eeff);
		exVerbs.add(epff);
		exVerbs.add(ewff);
		exVerbs.add(feff);
		exVerbs.add(fpff);
		exVerbs.add(fwff);
		for (Verb v : exVerbs) {
			this.userPrism.addVerb(v);
			v.setDomain(domain);
//			service.addMethod(v.generateServiceMethodDefinition());
//			serviceimpl.addMethod(v.generateServiceImplMethod());
//			dao.addMethod(v.generateDaoMethodDefinition());
//			daoimpl.addMethod(v.generateDaoImplMethod());
//			mdecorater.addDaoXmlMethod(v.generateDaoImplMethod());
			controller.addMethod(v.generateControllerMethod());
		}
		
		for (Verb v :replacedVerbs) {
			service.addMethod(v.generateServiceMethodDefinition());
			serviceimpl.addMethod(v.generateServiceImplMethod());
			dao.addMethod(v.generateDaoMethodDefinition());
			daoimpl.addMethod(v.generateDaoImplMethod());
			mdecorater.addDaoXmlMethod(v.generateDaoImplMethod());
			controller.addMethod(v.generateControllerMethod());
		}

		this.userPrism.setPages(new TreeSet<PrismInterface>());
		EasyUIUserPagePI eUser = new EasyUIUserPagePI(this.userDomain,this.technicalstack,this.dbType);
		eUser.setTitles(this.title, this.subTitle, this.footer);
		
		List<Domain> allDomainList = new ArrayList<>();
		allDomainList.add(this.userDomain);
		allDomainList.add(this.roleDomain);
		allDomainList.add(this.privilegeDomain);
		this.userPrism.addPage(eUser);
	}	
	
	public void decorateVerbsInLoginPrism(Set<Verb> verbs) throws Exception{
		Domain domain = this.getUserDomain();
//		Service service = this.loginPrism.getService();
//		ServiceImpl serviceimpl = this.loginPrism.getServiceImpl();
//		Dao dao = this.loginPrism.getDao();
//		DaoImpl daoimpl = this.loginPrism.getDaoImpl();
		Controller controller = this.loginPrism.getController();
		for (Verb v : verbs) {
			this.loginPrism.addVerb(v);
			v.setDomain(domain);
			//service.addMethod(v.generateServiceMethodDefinition());
			//serviceimpl.addMethod(v.generateServiceImplMethod());
			//dao.addMethod(v.generateDaoMethodDefinition());
			//daoimpl.addMethod(v.generateDaoImplMethod());
			controller.addMethod(v.generateControllerMethod());
		}
		for (DomainFieldVerb dfv:this.loginPrism.getDomianFieldVerbs()) {
			controller.addMethod(dfv.generateControllerMethod());
		}
	}

	public Domain getUserDomain() {
		return userDomain;
	}

	public void setUserDomain(Domain userDomain) {
		this.userDomain = userDomain;
	}

	public Domain getRoleDomain() {
		return roleDomain;
	}

	public void setRoleDomain(Domain roleDomain) {
		this.roleDomain = roleDomain;
	}

	public Domain getPrivilegeDomain() {
		return privilegeDomain;
	}

	public void setPrivilegeDomain(Domain privilegeDomain) {
		this.privilegeDomain = privilegeDomain;
	}

	public Prism getUserPrism() {
		return userPrism;
	}

	public void setUserPrism(Prism userPrism) {
		this.userPrism = userPrism;
	}

	public Prism getRolePrism() {
		return rolePrism;
	}

	public void setRolePrism(Prism rolePrism) {
		this.rolePrism = rolePrism;
	}

	public Prism getPrivilegePrism() {
		return privilegePrism;
	}

	public void setPrivilegePrism(Prism privilegePrism) {
		this.privilegePrism = privilegePrism;
	}

	public Prism getLoginPrism() {
		return loginPrism;
	}

	public void setLoginPrism(Prism loginPrism) {
		this.loginPrism = loginPrism;
	}
	
	public static String packagetokenToFolder(String packageToken) {
		String folder = packageToken.replace('.', '/');
		folder += "/";
		return folder;
	}
	
	public static String folderToPackageToken(String folder) {
		String packagetoken = folder.replace('/', '.');
		if (packagetoken.charAt(packagetoken.length() - 1) == '.')
			packagetoken = packagetoken.substring(0, packagetoken.length() - 1);
		return packagetoken;
	}
	
	public void writeToFile(String filePath, String content) throws Exception {
		File f = new File(filePath);
		if (!f.getParentFile().exists()) {
			f.getParentFile().mkdirs();
		}
		f.createNewFile();
		try (Writer fw = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(f.getAbsolutePath()), "UTF-8"))) {
			fw.write(content, 0, content.length());
		}
	}
	
	public void generatePrismFiles(Prism prism,Boolean ignoreWarning,Boolean genUi,Boolean genController,Boolean genService,Boolean genServiceImpl,Boolean genDao,Boolean genDaoImpl) throws ValidateException {
		ValidateInfo info = prism.validate(ignoreWarning);
		if (info.success(ignoreWarning) == false) {
			ValidateException e = new ValidateException(info);
			throw e;
		}
		try {
			String srcfolderPath = folderPath;
			if (this.packageToken != null && !"".equals(this.packageToken)){
				srcfolderPath = folderPath + "src/main/java/" + packagetokenToFolder(this.packageToken);

				writeToFile(srcfolderPath +packagetokenToFolder(prism.getDomain().getDomainSuffix())  +prism.getDomain().getCapFirstDomainNameWithSuffix()+ ".java",
						prism.getDomain().generateClassString());
			
				if (genDao && prism.getDao() != null) {
					writeToFile(
							srcfolderPath + packagetokenToFolder(prism.getDomain().getDaoSuffix()) + StringUtil.capFirst(prism.getDomain().getStandardName()) + "Dao.java",
							prism.getDao().generateDaoString());
				}
	
				if (genDaoImpl&&prism.getDaoImpl() != null) {
					writeToFile(
							folderPath + "src/main/resources/mapper/" + StringUtil.capFirst(prism.getDomain().getStandardName()) + "Dao.xml",
							prism.getMybatisDaoXmlDecorator().generateMybatisDaoXmlFileStr());
				}
	
				if (genService&&prism.getService() != null) {
					writeToFile(srcfolderPath +  packagetokenToFolder(prism.getDomain().getServiceSuffix())  + StringUtil.capFirst(prism.getDomain().getStandardName())
							+ "Service.java", prism.getService().generateServiceString());
				}
	
				if (genServiceImpl&&prism.getServiceImpl() != null) {
					writeToFile(srcfolderPath + packagetokenToFolder(prism.getDomain().getServiceimplSuffix()) + StringUtil.capFirst(prism.getDomain().getStandardName())
							+ "ServiceImpl.java", prism.getServiceImpl().generateServiceImplString());
				}
	
				if (genController&&prism.getController() != null) {
					prism.getController().decorateCrossOrigin(this.getCrossOrigin());
					writeToFile(srcfolderPath +  packagetokenToFolder(prism.getDomain().getControllerSuffix()) 
							+ prism.getController().getStandardName()+".java", prism.getController().generateControllerString());
				}
	
				if (genUi) {
					for (PrismInterface page : prism.getPages()) {
						page.generatePIFiles(folderPath);
					}
					
					for (ManyToMany mtm : prism.getManyToManies()) {
						EasyUIMtmPI mpage =  mtm.getEuPI();
						mpage.setTechnicalStack(this.technicalstack);
						mpage.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
						mpage.setNav(this.getNav());
						mpage.generatePIFiles(folderPath);
					}
					
					AuthJsGenerator aug = new AuthJsGenerator(this.userDomain);					
					writeToFile(folderPath + "src/main/resources/static/js/auth.js",aug.generateUtilString());
				
					if ("sbmeu".equalsIgnoreCase(this.getTechnicalstack())) {
						EasyUIErrorPI eErr0 = new EasyUIErrorPI();
						eErr0.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
						eErr0.setDomain(this.userDomain);
					
						writeToFile(folderPath + "src/main/resources/templates/error/4xx.html",
								eErr0.generateStatementList().getContent());
						writeToFile(folderPath + "src/main/resources/templates/error/5xx.html",
								eErr0.generateStatementList().getContent());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	public void generatePrismSMEUFiles(Prism prism,Boolean ignoreWarning,Boolean genUi,Boolean genController,Boolean genService,Boolean genServiceImpl,Boolean genDao,Boolean genDaoImpl) throws ValidateException {
		ValidateInfo info = prism.validate(ignoreWarning);
		if (info.success(ignoreWarning) == false) {
			ValidateException e = new ValidateException(info);
			throw e;
		}
		try {
			String srcfolderPath = folderPath;
			if (this.packageToken != null && !"".equals(this.packageToken)){
				srcfolderPath = folderPath + "src/" + packagetokenToFolder(this.packageToken);

				writeToFile(srcfolderPath +packagetokenToFolder(prism.getDomain().getDomainSuffix())  +prism.getDomain().getCapFirstDomainNameWithSuffix()+ ".java",
						prism.getDomain().generateClassString());
			
				if (genDao && prism.getDao() != null) {
					writeToFile(
							srcfolderPath + packagetokenToFolder(prism.getDomain().getDaoSuffix()) + StringUtil.capFirst(prism.getDomain().getStandardName()) + "Dao.java",
							prism.getDao().generateDaoString());
				}
	
				if (genDaoImpl&&prism.getDaoImpl() != null) {
					writeToFile(
							srcfolderPath + packagetokenToFolder(prism.getDomain().getDaoSuffix())+"/" +StringUtil.capFirst(prism.getDomain().getStandardName()) + "Dao.xml",
							prism.getMybatisDaoXmlDecorator().generateMybatisDaoXmlFileStr());
				}
	
				if (genService&&prism.getService() != null) {
					writeToFile(srcfolderPath +  packagetokenToFolder(prism.getDomain().getServiceSuffix())  + StringUtil.capFirst(prism.getDomain().getStandardName())
							+ "Service.java", prism.getService().generateServiceString());
				}
	
				if (genServiceImpl&&prism.getServiceImpl() != null) {
					writeToFile(srcfolderPath + packagetokenToFolder(prism.getDomain().getServiceimplSuffix()) + StringUtil.capFirst(prism.getDomain().getStandardName())
							+ "ServiceImpl.java", prism.getServiceImpl().generateServiceImplString());
				}
	
				if (genController&&prism.getController() != null) {
					prism.getController().decorateCrossOrigin(this.getCrossOrigin());
					writeToFile(srcfolderPath +  packagetokenToFolder(prism.getDomain().getControllerSuffix()) 
							+ prism.getController().getStandardName()+".java", prism.getController().generateControllerString());
				}
	
				if (genUi) {					
					for (PrismInterface page : prism.getPages()) {
						page.generatePIFiles(folderPath);
					}
					
					for (ManyToMany mtm : prism.getManyToManies()) {
						EasyUIMtmPI mpage =  mtm.getEuPI();
						mpage.setTechnicalStack(this.technicalstack);
						mpage.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
						mpage.setNav(this.getNav());
						mpage.generatePIFiles(folderPath);
					}
					
					AuthJsGenerator aug = new AuthJsGenerator(this.userDomain);					
					writeToFile(folderPath + "WebContent/js/auth.js",aug.generateUtilString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void generateOraclePrismFiles(OraclePrism oprism,Boolean ignoreWarning,Boolean genUi,Boolean genController,Boolean genService,Boolean genServiceImpl,Boolean genDao,Boolean genDaoImpl) throws ValidateException {
		ValidateInfo info = oprism.validate(ignoreWarning);
		if (info.success(ignoreWarning) == false) {
			ValidateException e = new ValidateException(info);
			throw e;
		}
		try {
			String projectFolderPath = folderPath.replace('\\', '/');
			String srcfolderPath = projectFolderPath;
			if (this.packageToken != null && !"".equals(this.packageToken)){
				srcfolderPath = folderPath + "src/main/java/" + packagetokenToFolder(this.packageToken);

				writeToFile(srcfolderPath +packagetokenToFolder(oprism.getDomain().getDomainSuffix())  +oprism.getDomain().getCapFirstDomainNameWithSuffix()+ ".java",
						oprism.getDomain().generateClassString());
			
				if (genDao&&oprism.getDao() != null) {
					writeToFile(
							srcfolderPath + packagetokenToFolder(oprism.getDomain().getDaoSuffix()) + StringUtil.capFirst(oprism.getDomain().getStandardName()) + "Dao.java",
							oprism.getDao().generateDaoString());
				}
	
				if (genDaoImpl&&oprism.getDaoImpl() != null) {
					writeToFile(
							folderPath + "src/main/resources/mapper/" + StringUtil.capFirst(oprism.getDomain().getStandardName()) + "Dao.xml",
							oprism.getMybatisOracleDaoXmlDecorator().generateMybatisDaoXmlFileStr());
				}
	
				if (genService&&oprism.getService() != null) {
					writeToFile(srcfolderPath +  packagetokenToFolder(oprism.getDomain().getServiceSuffix())  + StringUtil.capFirst(oprism.getDomain().getStandardName())
							+ "Service.java", oprism.getService().generateServiceString());
				}
	
				if (genServiceImpl&&oprism.getServiceImpl() != null) {
					writeToFile(srcfolderPath + packagetokenToFolder(oprism.getDomain().getServiceimplSuffix()) + StringUtil.capFirst(oprism.getDomain().getStandardName())
							+ "ServiceImpl.java", oprism.getServiceImpl().generateServiceImplString());
				}
	
				if (genController&&oprism.getController() != null) {
					writeToFile(srcfolderPath +  packagetokenToFolder(oprism.getDomain().getControllerSuffix())  + oprism.getController().getStandardName()+".java",
							oprism.getController().generateControllerString());
				}
	
				if (genUi) {
					for (PrismInterface page : oprism.getPages()) {
						page.generatePIFiles(projectFolderPath);
					}
			
					for (ManyToMany mtm : oprism.getManyToManies()) {
						EasyUIMtmPI mpage =  mtm.getEuPI();
						mpage.setTechnicalStack(this.technicalstack);
						mpage.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
						mpage.setNav(this.getNav());
						mpage.generatePIFiles(projectFolderPath);
					}
				}
				
				AuthJsGenerator aug = new AuthJsGenerator(this.userDomain);					
				writeToFile(folderPath + "src/main/resources/static/js/auth.js",aug.generateUtilString());
			
				if ("sbmeu".equalsIgnoreCase(this.getTechnicalstack())) {
					EasyUIErrorPI eErr0 = new EasyUIErrorPI();
					eErr0.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
					eErr0.setDomain(this.userDomain);
					
					writeToFile(folderPath + "src/main/resources/templates/error/4xx.html",
							eErr0.generateStatementList().getContent());
					writeToFile(folderPath + "src/main/resources/templates/error/5xx.html",
							eErr0.generateStatementList().getContent());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void generateOraclePrismSMEUFiles(OraclePrism oprism,Boolean ignoreWarning,Boolean genUi,Boolean genController,Boolean genService,Boolean genServiceImpl,Boolean genDao,Boolean genDaoImpl) throws ValidateException {
		ValidateInfo info = oprism.validate(ignoreWarning);
		if (info.success(ignoreWarning) == false) {
			ValidateException e = new ValidateException(info);
			throw e;
		}
		try {
			String projectFolderPath = folderPath.replace('\\', '/');
			String srcfolderPath = projectFolderPath;
			if (this.packageToken != null && !"".equals(this.packageToken)){
				srcfolderPath = folderPath + "src/" + packagetokenToFolder(this.packageToken);

				writeToFile(srcfolderPath +packagetokenToFolder(oprism.getDomain().getDomainSuffix())  +oprism.getDomain().getCapFirstDomainNameWithSuffix()+ ".java",
						oprism.getDomain().generateClassString());
			
				if (genDao&&oprism.getDao() != null) {
					writeToFile(
							srcfolderPath + packagetokenToFolder(oprism.getDomain().getDaoSuffix()) + StringUtil.capFirst(oprism.getDomain().getStandardName()) + "Dao.java",
							oprism.getDao().generateDaoString());
				}
	
				if (genDaoImpl&&oprism.getDaoImpl() != null) {
					writeToFile(
							srcfolderPath + packagetokenToFolder(oprism.getDomain().getDaoSuffix())+"/" +StringUtil.capFirst(oprism.getDomain().getStandardName()) + "Dao.xml",
							oprism.getMybatisOracleDaoXmlDecorator().generateMybatisDaoXmlFileStr());
				}
	
				if (genService&&oprism.getService() != null) {
					writeToFile(srcfolderPath +  packagetokenToFolder(oprism.getDomain().getServiceSuffix())  + StringUtil.capFirst(oprism.getDomain().getStandardName())
							+ "Service.java", oprism.getService().generateServiceString());
				}
	
				if (genServiceImpl&&oprism.getServiceImpl() != null) {
					writeToFile(srcfolderPath + packagetokenToFolder(oprism.getDomain().getServiceimplSuffix()) + StringUtil.capFirst(oprism.getDomain().getStandardName())
							+ "ServiceImpl.java", oprism.getServiceImpl().generateServiceImplString());
				}
	
				if (genController&&oprism.getController() != null) {
					writeToFile(srcfolderPath +  packagetokenToFolder(oprism.getDomain().getControllerSuffix())  + oprism.getController().getStandardName()+".java",
							oprism.getController().generateControllerString());
				}
	
				if (genUi) {
					for (PrismInterface page : oprism.getPages()) {
						page.generatePIFiles(projectFolderPath);
					}
			
					for (ManyToMany mtm : oprism.getManyToManies()) {
						EasyUIMtmPI mpage =  mtm.getEuPI();
						mpage.setTechnicalStack(this.technicalstack);
						mpage.setTitles(this.getTitle(), this.getSubTitle(), this.getFooter());
						mpage.setNav(this.getNav());
						mpage.generatePIFiles(projectFolderPath);
					}
				}
				
				AuthJsGenerator aug = new AuthJsGenerator(this.userDomain);					
				writeToFile(folderPath + "WebContent/js/auth.js",aug.generateUtilString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Util> getShiroUtils() {
		return shiroUtils;
	}

	public void setShiroUtils(List<Util> shiroUtils) {
		this.shiroUtils = shiroUtils;
	}

	public String getGenerateData() {
		return generateData;
	}

	public void setGenerateData(String generateData) {
		this.generateData = generateData;
	}
	
	public List<List<Domain>> genData() throws Exception{
		List<List<Domain>> results = new ArrayList<>();
		results.add(genUserData());
		results.add(genRoleData());
		return results;
		
	}
	
	public List<Domain> genRoleData() throws Exception{
		List<Domain> roles = new ArrayList<>();
		Domain role0 = (Domain)this.roleDomain.deepClone();
		role0.getDomainId().setFieldValue("1");
		role0.getDomainName().setFieldValue("admin");
		role0.getActive().setFieldValue(role0.getDomainActiveStr());
		
		Domain role1 = (Domain)this.roleDomain.deepClone();
		role1.getDomainId().setFieldValue("2");
		role1.getDomainName().setFieldValue("user");
		role1.getActive().setFieldValue(role0.getDomainActiveStr());
		
		roles.add(role0);
		roles.add(role1);
		return roles;
	}
	
	public List<Domain> genUserData() throws Exception{
		List<Domain> users = new ArrayList<>();
		Domain user0 = (Domain)this.userDomain.deepClone();
		user0.getDomainId().setFieldValue("1");
		user0.getDomainName().setFieldValue("admin");
		user0.getActive().setFieldValue(user0.getDomainActiveStr());
		user0.findFieldByFixedName("userName").setFieldValue("admin");
		String[] enc0 = UserRegisteAndLogin.encryptPassword("admin");
		user0.findFieldByFixedName("salt").setFieldValue(enc0[0]);
		user0.findFieldByFixedName("password").setFieldValue(enc0[1]);
		user0.findFieldByFixedName("loginFailure").setFieldValue("0");
		
		Domain user1 = (Domain)this.userDomain.deepClone();
		user1.getDomainId().setFieldValue("2");
		user1.getDomainName().setFieldValue("jerry");
		user1.getActive().setFieldValue(user0.getDomainActiveStr());
		user1.findFieldByFixedName("userName").setFieldValue("jerry");
		String[] enc1 = UserRegisteAndLogin.encryptPassword("jerry");
		user1.findFieldByFixedName("salt").setFieldValue(enc1[0]);
		user1.findFieldByFixedName("password").setFieldValue(enc1[1]);
		user1.findFieldByFixedName("loginFailure").setFieldValue("0");
		
		users.add(user0);
		users.add(user1);
		return users;
	}

	public void decorateVerbDbType(Prism prism,String dbType) {
		for (Verb v:prism.getVerbs()) {
			v.setDbType(dbType);
		}
		for (NoControllerVerb nv:prism.getNoControllerVerbs()) {
			nv.setDbType(dbType);
		}
	}
	
	public List<List<Domain>> decorateDataDomains(List<List<Domain>> dataDomains) throws Exception{
		dataDomains = DomainUtil.removeDataDomainsFromLists(dataDomains,this.privilegeDomain.getStandardName());
		long serial = 1L;
		List<Domain> myPrivs = new ArrayList<>();
		Set<Domain> authDomains = new TreeSet<>();
		authDomains.add(this.userDomain);
		authDomains.add(this.roleDomain);
		authDomains.add(this.privilegeDomain);
		
		Set<Domain> privDomains = this.nav.getDomains();
		for (Domain d: privDomains) {
			if (!DomainUtil.inDomainSet(d, authDomains)) {
				Domain priv = (Domain)this.privilegeDomain.deepClone();	
				priv.getDomainId().setFieldValue(""+serial++);
				priv.getDomainName().setFieldValue(d.getStandardName());
				priv.getActive().setFieldValue(this.privilegeDomain.getDomainActiveStr());
				myPrivs.add(priv);
			}
		}
		dataDomains.add(myPrivs);
		
		boolean isAdminRoleExists = false;
		boolean isUserRoleExists = false;
		List<Domain> myRoles = new ArrayList<>();
		for (List<Domain> datas : dataDomains) {
			for (Domain d : datas) {
				if (d.getStandardName().equals(this.roleDomain.getStandardName())) {
					myRoles.add(d);
					if (d.getDomainName().getFieldValue().equals("admin")) {
						isAdminRoleExists = true;
						String privsstr = "";
						for (Domain priv : myPrivs) {
							privsstr = privsstr + priv.getDomainId().getFieldValue() + ",";
						}
						if (privsstr.endsWith(","))
							privsstr = privsstr.substring(0, privsstr.length() - 1);
						for (ManyToMany mtm : d.getManyToManies()) {
							mtm.setMaster(d);
							mtm.setValues(privsstr);
						}
					}
				}
				if (d.getDomainName().getFieldValue().equals("user")) {
					isUserRoleExists = true;
					String privsstr = "";
					for (Domain priv : myPrivs) {
						privsstr = privsstr + priv.getDomainId().getFieldValue() + ",";
					}
					if (privsstr.endsWith(","))
						privsstr = privsstr.substring(0, privsstr.length() - 1);
					for (ManyToMany mtm : d.getManyToManies()) {
						mtm.setMaster(d);
						mtm.setValues(privsstr);
					}
				}
			}
		}
		if (!isAdminRoleExists) {
			Domain adminRole = (Domain)this.roleDomain.deepClone();
			adminRole.getDomainId().setFieldValue("9000");
			adminRole.getDomainName().setFieldValue("admin");
			adminRole.getActive().setFieldValue(adminRole.getDomainActiveStr());
			String privsstr = "";
			for (Domain priv : myPrivs) {
				privsstr = privsstr + priv.getDomainId().getFieldValue() + ",";
			}
			if (privsstr.endsWith(","))
				privsstr = privsstr.substring(0, privsstr.length() - 1);
			for (ManyToMany mtm : adminRole.getManyToManies()) {
				mtm.setMaster(adminRole);
				mtm.setValues(privsstr);
			}
			myRoles.add(adminRole);
		}
		if (!isUserRoleExists) {
			Domain userRole = (Domain)this.roleDomain.deepClone();
			userRole.getDomainId().setFieldValue("9001");
			userRole.getDomainName().setFieldValue("user");
			userRole.getActive().setFieldValue(userRole.getDomainActiveStr());
			
			String privsstr = "";
			for (Domain priv : myPrivs) {
				privsstr = privsstr + priv.getDomainId().getFieldValue() + ",";
			}
			if (privsstr.endsWith(","))
				privsstr = privsstr.substring(0, privsstr.length() - 1);
			for (ManyToMany mtm : userRole.getManyToManies()) {
				mtm.setMaster(userRole);
				mtm.setValues(privsstr);
			}
			
			myRoles.add(userRole);
		}
		dataDomains = DomainUtil.removeDataDomainsFromLists(dataDomains,this.roleDomain.getStandardName());
		dataDomains.add(myRoles);
		
		boolean isAdminExists = false;
		boolean isJerryExists = false;
		List<Domain> myUsers = new ArrayList<>();
		for (List<Domain> datas:dataDomains) {
			for (Domain d : datas)
			{		
				if (d.getStandardName().equals(this.userDomain.getStandardName())) {
					String password = d.findFieldByFixedName("password").getFieldValue();
					password = new Sha1Hash(password).toString();
					String[] saltAndCiphertext = UserRegisteAndLogin.encryptPassword(password);
					password = saltAndCiphertext[1];
					String salt = saltAndCiphertext[0];
					d.findFieldByFixedName("password").setFieldValue(password);
					d.findFieldByFixedName("salt").setFieldValue(salt);
					myUsers.add(d);
					if (d.getDomainName().getFieldValue().equals("admin")) {
						isAdminExists = true;
						for (ManyToMany mtm : d.getManyToManies()) {
							Domain adminRole = DomainUtil.findDataDomainInListByDomainNameValue(myRoles, "admin");
							if (adminRole != null) {
								String fieldValue = mtm.getValues();
								if (!StringUtil.isBlank(fieldValue)) fieldValue = fieldValue + ","+adminRole.getDomainId().getFieldValue();
								else fieldValue = adminRole.getDomainId().getFieldValue();
								mtm.setMaster(d);
								mtm.setValues(fieldValue);
							}
						}
					}
					if (d.getDomainName().getFieldValue().equals("jerry")) {
						isJerryExists = true;
						for (ManyToMany mtm : d.getManyToManies()) {
							Domain adminRole = DomainUtil.findDataDomainInListByDomainNameValue(myRoles, "user");
							if (adminRole != null) {
								String fieldValue = mtm.getValues();
								if (!StringUtil.isBlank(fieldValue)) fieldValue = fieldValue + ","+adminRole.getDomainId().getFieldValue();
								else fieldValue = adminRole.getDomainId().getFieldValue();
								mtm.setMaster(d);
								mtm.setValues(fieldValue);
							}
						}
					}
				}
			}
		}
		if (!isAdminExists) {
			Domain admin = (Domain)this.userDomain.deepClone();
			admin.getDomainId().setFieldValue("9000");
			admin.getDomainName().setFieldValue("admin");
			admin.getActive().setFieldValue(admin.getDomainActiveStr());
			
			String password = "admin";
			password = new Sha1Hash(password).toString();
			String[] saltAndCiphertext = UserRegisteAndLogin.encryptPassword(password);
			password = saltAndCiphertext[1];
			String salt = saltAndCiphertext[0];
			admin.findFieldByFixedName("password").setFieldValue(password);
			admin.findFieldByFixedName("salt").setFieldValue(salt);
			admin.findFieldByFixedName("loginFailure").setFieldValue("0");
			
			for (ManyToMany mtm : admin.getManyToManies()) {
				Domain adminRole = DomainUtil.findDataDomainInListByDomainNameValue(myRoles, "admin");
				if (adminRole != null) {
					String fieldValue = adminRole.getDomainId().getFieldValue();
					mtm.setMaster(admin);
					mtm.setValues(fieldValue);
				}
			}
			myUsers.add(admin);
		}
		if (!isJerryExists) {
			Domain jerry = (Domain)this.userDomain.deepClone();
			jerry.getDomainId().setFieldValue("9001");
			jerry.getDomainName().setFieldValue("jerry");
			jerry.getActive().setFieldValue(jerry.getDomainActiveStr());
			
			String password = "jerry";
			password = new Sha1Hash(password).toString();
			String[] saltAndCiphertext = UserRegisteAndLogin.encryptPassword(password);
			password = saltAndCiphertext[1];
			String salt = saltAndCiphertext[0];
			jerry.findFieldByFixedName("password").setFieldValue(password);
			jerry.findFieldByFixedName("salt").setFieldValue(salt);
			jerry.findFieldByFixedName("loginFailure").setFieldValue("0");
			
			for (ManyToMany mtm : jerry.getManyToManies()) {
				Domain userRole = DomainUtil.findDataDomainInListByDomainNameValue(myRoles, "user");
				if (userRole != null) {
					String fieldValue = userRole.getDomainId().getFieldValue();
					mtm.setMaster(jerry);
					mtm.setValues(fieldValue);
				}
			}
			myUsers.add(jerry);
		}
		dataDomains = DomainUtil.removeDataDomainsFromLists(dataDomains,this.userDomain.getStandardName());
		dataDomains.add(myUsers);		
		return dataDomains;
	}
	
	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.scg.setNav(nav);
		for (PrismInterface page:this.userPrism.getPages()) {
			page.setNav(nav);
		}
		for (PrismInterface page:this.rolePrism.getPages()) {
			page.setNav(nav);
		}
		for (PrismInterface page:this.privilegePrism.getPages()) {
			page.setNav(nav);
		}
	}
	
	@Override
	public Nav getNav() {
		return super.getNav();
	}

	public ShiroConfigGenerator getScg() {
		return scg;
	}

	public void setScg(ShiroConfigGenerator scg) {
		this.scg = scg;
	}

	public UserRealmGenerator getUrg() {
		return urg;
	}

	public void setUrg(UserRealmGenerator urg) {
		this.urg = urg;
	}
	
	public void generateUserPrismFromUserDomainWithDeniedFields(Boolean ignoreWarning,Set<Field> deniedFields) throws ValidateException, Exception {
		if (this.userDomain != null) {
			if (this.getPackageToken() != null) {
				this.userDomain.setPackageToken(packageToken);
			}
			
			this.userDomain.decorateCompareTo();

			Dao dao = new Dao();
			dao.setDomain(this.userDomain);
			dao.setPackageToken(this.userDomain.getPackageToken());
			DaoImpl daoimpl = new DaoImpl();
			daoimpl.setPackageToken(this.userDomain.getPackageToken());
			daoimpl.setDomain(this.userDomain);
			daoimpl.setDao(dao);
			this.userPrism.setDao(dao);
			this.userPrism.setDaoImpl(daoimpl);

			Service service = new Service();
			service.setDomain(this.userDomain);
			service.setPackageToken(this.userDomain.getPackageToken());
			this.userPrism.setService(service);
			
			ServiceImpl serviceimpl = new ServiceImpl(this.userDomain);
			serviceimpl.setPackageToken(this.userDomain.getPackageToken());
			serviceimpl.getDao().addAnnotation("Autowired");
			Method daoSetter = NamedUtilMethodGenerator.generateSetter("dao",
					new Type(this.userDomain.getCapFirstDomainName() + "Dao"));
			serviceimpl.addMethod(daoSetter);
			serviceimpl.addClassImports("org.springframework.beans.factory.annotation.Autowired");
			serviceimpl.setDomain(this.userDomain);
			serviceimpl.setService(service);
			this.userPrism.setServiceImpl(serviceimpl);

			ListAll listAll = new ListAll(this.userDomain);	
			listAll.setDeniedFields(deniedFields);
			listAll.setDbType(this.dbType);
			Verb update = this.userDomain.hasDomainId() ?new Update(this.userDomain):null;
			Verb delete = this.userDomain.hasDomainId() ?new Delete(this.userDomain):null;
			AddUser add = this.userDomain.hasDomainId() ?new AddUser(this.userDomain):null;
			//add.setDeniedFields(deniedFields);
			add.setDbType(this.dbType);
			Verb softdelete = this.userDomain.hasDomainId() && this.userDomain.hasActiveField() ?new SoftDelete(this.userDomain):null;
			FindById findbyid = this.userDomain.hasDomainId() ? new FindById(this.userDomain):null;
			findbyid.setDeniedFields(deniedFields);
			findbyid.setDbType(this.dbType);
			FindByName findbyname = this.userDomain.hasDomainName() ? new FindByName(this.userDomain):null;
			findbyname.setDeniedFields(deniedFields);
			findbyname.setDbType(this.dbType);
			Verb searchbyname = this.userDomain.hasDomainName()?new SearchByName(this.userDomain):null;
			ListActive listactive = this.userDomain.hasActiveField()?new ListActive(this.userDomain):null;
			listactive.setDeniedFields(deniedFields);
			listactive.setDbType(this.dbType);
			Verb listAllByPage = new ListAllByPage(this.userDomain);
			Verb deleteAll = this.userDomain.hasDomainId() ?new DeleteAll(this.userDomain):null;
			Verb softDeleteAll = this.userDomain.hasDomainId() && this.userDomain.hasActiveField() ?new SoftDeleteAll(this.userDomain):null;
			Verb toggle = this.userDomain.hasDomainId() && this.userDomain.hasActiveField() ?new Toggle(this.userDomain):null;
			Verb toggleOne = this.userDomain.hasDomainId() && this.userDomain.hasActiveField() ?new ToggleOne(this.userDomain):null;
			SearchByFieldsByPageWithDeniedFields searchByFieldsByPageWithDeniedFields = new SearchByFieldsByPageWithDeniedFields(this.userDomain);
			searchByFieldsByPageWithDeniedFields.setDeniedFields(deniedFields);
			searchByFieldsByPageWithDeniedFields.setDbType(this.dbType);
			Verb activate = this.userDomain.hasDomainId() && this.userDomain.hasActiveField() ?new Activate(this.userDomain):null;
			Verb activateAll = this.userDomain.hasDomainId() && this.userDomain.hasActiveField() ?new ActivateAll(this.userDomain):null;
			Verb export = new Export(this.userDomain);
			Verb exportPDF = new ExportPDF(this.userDomain);
			Verb exportWord = new ExportWord(this.userDomain);
			SearchByFieldsWithDeniedFields searchByFieldsWithDeniedFields = new SearchByFieldsWithDeniedFields(this.userDomain);
			searchByFieldsWithDeniedFields.setDeniedFields(deniedFields);
			searchByFieldsWithDeniedFields.setDbType(this.dbType);
			Verb filterExcel = new FilterExcel(this.userDomain);
			Verb filterPDF = new FilterPDF(this.userDomain);
			Verb filterWord = new FilterWord(this.userDomain);
			Verb clone	= this.userDomain.hasDomainId() ?new Clone(this.userDomain):null;
			Verb cloneAll = this.userDomain.hasDomainId()  ?new CloneAll(this.userDomain):null;
			
			CountAllPage countAllPage = new CountAllPage(this.userDomain);
			CountSearchByFieldsRecordsWithDeniedFields countSearchByFieldsRecordsWithDeniedFields = new CountSearchByFieldsRecordsWithDeniedFields(this.userDomain);
			countSearchByFieldsRecordsWithDeniedFields.setDeniedFields(deniedFields);
			countSearchByFieldsRecordsWithDeniedFields.setDbType(this.dbType);
			CountActiveRecords countActiveRecords = this.userDomain.hasDomainId() && this.userDomain.hasActiveField() ? new CountActiveRecords(this.userDomain):null;

			for (Field f:this.userDomain.getPlainFields()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					this.userPrism.addDomianFieldVerb(new AddUploadDomainField(this.userDomain,f));
					this.userPrism.addDomianFieldVerb(new UpdateUploadDomainField(this.userDomain,f));
				}
			}
			this.userPrism.addVerb(listAll);
			this.userPrism.addVerb(update);
			this.userPrism.addVerb(delete);
			this.userPrism.addVerb(add);
			this.userPrism.addVerb(softdelete);
			this.userPrism.addVerb(findbyid);
			this.userPrism.addVerb(findbyname);
			this.userPrism.addVerb(searchbyname);
			this.userPrism.addVerb(listactive);
			this.userPrism.addVerb(listAllByPage);
			this.userPrism.addVerb(deleteAll);
			this.userPrism.addVerb(softDeleteAll);
			this.userPrism.addVerb(toggle);
			this.userPrism.addVerb(toggleOne);
			this.userPrism.addVerb(searchByFieldsByPageWithDeniedFields);
			this.userPrism.addVerb(activate);
			this.userPrism.addVerb(activateAll);
			this.userPrism.addVerb(export);
			this.userPrism.addVerb(exportPDF);
			this.userPrism.addVerb(exportWord);
			this.userPrism.addVerb(searchByFieldsWithDeniedFields);
			this.userPrism.addVerb(filterExcel);
			this.userPrism.addVerb(filterPDF);
			this.userPrism.addVerb(filterWord);
			this.userPrism.addVerb(clone);
			this.userPrism.addVerb(cloneAll);

			if (countAllPage !=null) this.userPrism.addNoControllerVerb(countAllPage);
			if (countSearchByFieldsRecordsWithDeniedFields !=null) this.userPrism.addNoControllerVerb(countSearchByFieldsRecordsWithDeniedFields);
			if (countActiveRecords !=null) this.userPrism.addNoControllerVerb(countActiveRecords);
			this.userPrism.setController(new SpringMVCController(this.userPrism.getVerbs(), this.userDomain,ignoreWarning));
			this.userPrism.getController().setPackageToken(this.packageToken);

			for (Verb v : this.userPrism.getVerbs()) {
				v.setDomain(userDomain);
				v.setDbType(this.dbType);
				service.addMethod(v.generateServiceMethodDefinition());
				serviceimpl.addMethod(v.generateServiceImplMethod());
				dao.addMethod(v.generateDaoMethodDefinition());
				daoimpl.addMethod(v.generateDaoImplMethod());
				this.userPrism.getController().addMethod(v.generateControllerMethod());
			}

			for (NoControllerVerb nVerb : this.userPrism.getNoControllerVerbs()) {
				nVerb.setDomain(userDomain);
				service.addMethod(nVerb.generateServiceMethodDefinition());
				serviceimpl.addMethod(nVerb.generateServiceImplMethod());
				dao.addMethod(nVerb.generateDaoMethodDefinition());
				daoimpl.addMethod(nVerb.generateDaoImplMethod());
			}

			for (DaoOnlyVerb oVerb : this.userPrism.getDaoOnlyVerbs()) {
				oVerb.setDomain(userDomain);
				dao.addMethod(oVerb.generateDaoMethodDefinition());
				daoimpl.addMethod(oVerb.generateDaoImplMethod());
			}
			
			for (DomainFieldVerb dfv:this.userPrism.getDomianFieldVerbs()) {
				this.userPrism.getController().addMethod(dfv.generateControllerMethod());
			}

			this.userPrism.setMybatisDaoXmlDecorator(new MybatisDaoXmlDecorator());
			this.userPrism.getMybatisDaoXmlDecorator().setDomain(this.userDomain);
			Set<Domain> resultMaps = new TreeSet<Domain>();
			resultMaps.add(this.userDomain);
			this.userPrism.getMybatisDaoXmlDecorator().setResultMaps(resultMaps);
			Set<Method> daoimplMethods = new TreeSet<Method>();
			for (Verb vb : this.userPrism.getVerbs()) {
				if (vb!=null && vb.generateDaoImplMethod()!=null){
					daoimplMethods.add(vb.generateDaoImplMethod());
				}
			}
			for (DaoOnlyVerb dovb : this.userPrism.getDaoOnlyVerbs()){
				if (dovb!=null && dovb.generateDaoImplMethod()!=null){
					daoimplMethods.add(dovb.generateDaoImplMethod());
				}
			}
			for (NoControllerVerb ncvb : this.userPrism.getNoControllerVerbs()) {
				if (ncvb!=null && ncvb.generateDaoImplMethod()!=null){
					daoimplMethods.add(ncvb.generateDaoImplMethod());
				}
			}
			this.userPrism.getMybatisDaoXmlDecorator().setDaoXmlMethods(daoimplMethods);

			EasyUIGridPagePI easyui = new EasyUIGridPagePI(this.userDomain);
			easyui.setTitles(this.getTitle(),this.getSubTitle(),this.getFooter());
			easyui.setDomain(this.userDomain);
			easyui.setTechnicalStack(this.technicalstack);
			this.userPrism.addPage(easyui);

			if (this.userDomain.getManyToManies() != null && this.userDomain.getManyToManies().size() > 0) {
				for (ManyToMany mtm : this.userDomain.getManyToManies()) {
					String slaveName = mtm.getManyToManySalveName();
					String masterName = this.userDomain.getStandardName();
					if (DomainUtil.setContainsDomain(this.userPrism.getProjectDomains(), masterName)
							&& DomainUtil.setContainsDomain(this.userPrism.getProjectDomains(), slaveName)) {
						Domain tempo = DomainUtil.lookupDoaminInSet(this.userPrism.getProjectDomains(), slaveName);
						Domain myslave = tempo==null?null:(Domain)tempo.deepClone();
						if (myslave == null) continue;
						if (!StringUtil.isBlank(mtm.getSlaveAlias())){
							myslave.setAlias(mtm.getSlaveAlias());
							myslave.setAliasLabel(mtm.getSlaveAliasLabel());
						}
						ManyToMany mymtm = new ManyToMany(DomainUtil.lookupDoaminInSet(this.userPrism.getProjectDomains(), masterName),
								myslave,mtm.getMasterValue(),mtm.getValues());	
						mymtm.setSlaveAlias(myslave.getAlias());
						mymtm.setSlaveAliasLabel(mtm.getSlaveAliasLabel());
						mymtm.setValues(mtm.getValues());
						this.userPrism.getManyToManies().add(mymtm);
					} else {
						ValidateInfo validateInfo = new ValidateInfo();
						validateInfo.addCompileError("棱柱" + this.userPrism.getText() + "多对多设置有误。");
						ValidateException em = new ValidateException(validateInfo);
						throw em;
					}
				}
			}
			for (ManyToMany mtm : this.userPrism.getManyToManies()) {
				mtm.setTitle(this.title);
				mtm.setSubTitle(this.subTitle);
				mtm.setFooter(this.footer);
				mtm.setCrossOrigin(this.crossOrigin);
				this.userPrism.getService().addMethod(mtm.getAssign().generateServiceMethodDefinition());
				this.userPrism.getServiceimpl().addMethod(mtm.getAssign().generateServiceImplMethod());
				this.userPrism.getDao().addMethod(mtm.getAssign().generateDaoMethodDefinition());
				this.userPrism.getDaoimpl().addMethod(mtm.getAssign().generateDaoImplMethod());
				this.userPrism.getMybatisDaoXmlDecorator().addDaoXmlMethod(mtm.getAssign().generateDaoImplMethod());
				this.userPrism.getController().addMethod(mtm.getAssign().generateControllerMethod());

				this.userPrism.getService().addMethod(mtm.getRevoke().generateServiceMethodDefinition());
				this.userPrism.getServiceimpl().addMethod(mtm.getRevoke().generateServiceImplMethod());
				this.userPrism.getDao().addMethod(mtm.getRevoke().generateDaoMethodDefinition());
				this.userPrism.getDaoimpl().addMethod(mtm.getRevoke().generateDaoImplMethod());
				this.userPrism.getMybatisDaoXmlDecorator().addDaoXmlMethod(mtm.getRevoke().generateDaoImplMethod());
				this.userPrism.getController().addMethod(mtm.getRevoke().generateControllerMethod());

				this.userPrism.getService().addMethod(mtm.getListMyActive().generateServiceMethodDefinition());
				this.userPrism.getServiceimpl().addMethod(mtm.getListMyActive().generateServiceImplMethod());
				this.userPrism.getDao().addMethod(mtm.getListMyActive().generateDaoMethodDefinition());
				this.userPrism.getDaoimpl().addMethod(mtm.getListMyActive().generateDaoImplMethod());
				this.userPrism.getMybatisDaoXmlDecorator().addResultMap(mtm.getSlave());
				this.userPrism.getMybatisDaoXmlDecorator().addDaoXmlMethod(mtm.getListMyActive().generateDaoImplMethod());
				this.userPrism.getController().addMethod(mtm.getListMyActive().generateControllerMethod());

				this.userPrism.getService().addMethod(mtm.getListMyAvailableActive().generateServiceMethodDefinition());
				this.userPrism.getServiceimpl().addMethod(mtm.getListMyAvailableActive().generateServiceImplMethod());
				this.userPrism.getDao().addMethod(mtm.getListMyAvailableActive().generateDaoMethodDefinition());
				this.userPrism.getDaoimpl().addMethod(mtm.getListMyAvailableActive().generateDaoImplMethod());
				this.userPrism.getMybatisDaoXmlDecorator().addDaoXmlMethod(mtm.getListMyAvailableActive().generateDaoImplMethod());
				this.userPrism.getController().addMethod(mtm.getListMyAvailableActive().generateControllerMethod());
				//mtm.slave.decorateCompareTo();
				
				Service slaveService = new Service();
				slaveService.setDomain(mtm.getSlave());
				slaveService.addAnnotation("Autowired");
				slaveService.setStandardName(mtm.getSlave().getCapFirstDomainName() + "Service");
				
				Method slaveServiceSetter = NamedUtilMethodGenerator.generateSetter(mtm.getSlave().getLowerFirstDomainName()+"Service",
						new Type(mtm.getSlave().getCapFirstDomainName() + "Service",mtm.getSlave().getPackageToken()+"."+mtm.getSlave().getServiceSuffix()+"."+mtm.getSlave().getCapFirstDomainName() + "Service"));
				this.userPrism.getServiceimpl().addMethod(slaveServiceSetter);
				this.userPrism.getServiceimpl().addOtherService(slaveService);
			}
		}
	}
}
