package org.light.shiroauth.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedStatementGenerator;
import org.light.utils.InterVarUtil;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class LoginUser extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}	

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception  {
		return null;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception  {
		return null;
	}
	
	public LoginUser(){
		super();
		this.setLabel("登录");
	}
	
	public LoginUser(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("LoginUser");
		this.setVerbName("Login"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("登录");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("LoginUser");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("login"+StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("Map<String,Object>"));
			method.setThrowException(true);
			method.addAdditionalImport("javax.servlet.http.HttpSession");
			method.addAdditionalImport(this.domain.getPackageToken()+".shiro.UserRegisteAndLogin");
			method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken()+"."+this.domain.getServiceSuffix()+"."+this.domain.getStandardName()+"Service");
			method.addSignature(new Signature(1,this.domain.getLowerFirstDomainName(),this.domain.getType(),this.domain.getPackageToken(),"RequestBody"));
			//method.addSignature(new Signature(2,"httpSession","HttpSession"));
			method.addMetaData("RequestMapping(value = \"/login"+this.domain.getCapFirstDomainName()+"\", method = RequestMethod.POST)");

			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,2,"Map<String,Object> result = new TreeMap<String,Object>();"));
			sList.add(new Statement(2000L,2,"String salt = service.getSalt"+this.domain.getCapFirstDomainName()+"("+this.domain.getLowerFirstDomainName()+".get"+this.domain.getDomainName().getCapFirstFieldName()+"());"));
			sList.add(new Statement(3000L,2,""+this.domain.getLowerFirstDomainName()+"."+this.domain.findFieldByFixedName("password").getSetterCallName()+"(UserRegisteAndLogin.getInputPasswordCiph("+this.domain.getLowerFirstDomainName()+"."+this.domain.findFieldByFieldName(""+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"").getGetterCallName()+"(),salt));"));
			sList.add(new Statement(5000L,2,"String loginSuccess = UserRegisteAndLogin.userLogin("+this.domain.getLowerFirstDomainName()+");"));
			sList.add(new Statement(6000L,2,"result.put(\"success\",loginSuccess.equals(\"index\"));"));
			sList.add(new Statement(7000L,2,"result.put(\"data\",null);"));
			sList.add(new Statement(8000L,2,"return result;"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}	

	@Override
	public String generateControllerMethodString()throws Exception {
		if (this.denied) return null;
		else {
			Method m = this.generateControllerMethod();
			return m.generateMethodString();
		}
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		if (this.denied) return null;
		else {
			Method m = this.generateControllerMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}
	
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;		
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return null;
	}



	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return null;
	}
	
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return null;
	}
}
