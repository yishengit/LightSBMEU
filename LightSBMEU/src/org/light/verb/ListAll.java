package org.light.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedStatementGenerator;
import org.light.generator.NamedStatementListGenerator;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.InterVarUtil;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ListAll extends Verb implements EasyUIPositions {
	protected Set<Field> deniedFields = new TreeSet<>();
	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			if ("pgsql".equalsIgnoreCase(this.dbType)||"postgresql".equalsIgnoreCase(this.dbType)) {
				Method method = new Method();
				method.setStandardName("listAll" + StringUtil.capFirst(this.domain.getPlural()));
				method.setNoContainer(true);
				List<Writeable> list = new ArrayList<Writeable>();
				list.add(new Statement(100L, 1, "<select id=\"" + StringUtil.lowerFirst(this.getVerbName())
						+ "\" resultMap=\"" + this.domain.getLowerFirstDomainName() + "\">"));
				list.add(new Statement(200L, 2, PgsqlReflector.generateSelectAllStatementWithDeniedFields(domain,deniedFields)));
				list.add(new Statement(300L, 1, "</select>"));
				method.setMethodStatementList(WriteableUtil.merge(list));
				return method;
			}else if ("oracle".equalsIgnoreCase(this.dbType)){
				Method method = new Method();
				method.setStandardName("listAll" + StringUtil.capFirst(this.domain.getPlural()));
				method.setNoContainer(true);
				List<Writeable> list = new ArrayList<Writeable>();
				list.add(new Statement(100L, 1, "<select id=\"" + StringUtil.lowerFirst(this.getVerbName())
						+ "\" resultMap=\"" + this.domain.getLowerFirstDomainName() + "\">"));
				list.add(new Statement(200L, 2, MybatisOracleSqlReflector.generateSelectAllStatement(domain)));
				list.add(new Statement(300L, 1, "</select>"));
				method.setMethodStatementList(WriteableUtil.merge(list));
				return method;
			}else {
				Method method = new Method();
				method.setStandardName("listAll" + StringUtil.capFirst(this.domain.getPlural()));
				method.setNoContainer(true);
				List<Writeable> list = new ArrayList<Writeable>();
				list.add(new Statement(100L, 1, "<select id=\"" + StringUtil.lowerFirst(this.getVerbName())
						+ "\" resultMap=\"" + this.domain.getLowerFirstDomainName() + "\">"));
				list.add(new Statement(200L, 2, MybatisSqlReflector.generateSelectAllStatement(domain)));
				list.add(new Statement(300L, 1, "</select>"));
				method.setMethodStatementList(WriteableUtil.merge(list));
				return method;
			}
		}
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			String s = m.generateMethodString();
			return s;
		}
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("listAll" + StringUtil.capFirst(this.domain.getPlural()));
			method.setReturnType(new Type("List", this.domain, this.domain.getPackageToken()));
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.setThrowException(true);
			return method;
		}
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateDaoMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("listAll" + StringUtil.capFirst(this.domain.getPlural()));
			method.setReturnType(new Type("List", this.domain, this.domain.getPackageToken()));
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.setThrowException(true);

			return method;
		}
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("listAll" + StringUtil.capFirst(this.domain.getPlural()));
			method.setReturnType(new Type("List", this.domain, this.domain.getPackageToken()));
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDaoSuffix() + "."
					+ this.domain.getStandardName() + "Dao");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getServiceSuffix() + "."
					+ this.domain.getStandardName() + "Service");
			method.setThrowException(true);
			method.addMetaData("Override");

			Method daomethod = this.generateDaoMethodDefinition();

			List<Writeable> list = new ArrayList<Writeable>();
			list.add(NamedStatementListGenerator.generateServiceImplReturnList(1000L, 2, this.domain,
					InterVarUtil.DB.dao, daomethod));
			method.setMethodStatementList(WriteableUtil.merge(list));

			return method;
		}
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateServiceImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	public ListAll() throws Exception {
		super();
		this.dbType = "MariaDB";
		if (this.domain != null)
			this.setVerbName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
		else
			this.setVerbName("ListAll");
		this.setLabel("列出全部");
	}
	
	public ListAll(String dbType) throws Exception {
		super();
		this.dbType = dbType;
		if (this.domain != null)
			this.setVerbName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
		else
			this.setVerbName("ListAll");
		this.setLabel("列出全部");
	}

	public ListAll(Domain domain)  throws ValidateException{
		super();
		this.domain = domain;
		this.dbType = "MariaDB";
		this.denied = domain.isVerbDenied("ListAll");
		this.setVerbName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
		this.setLabel("列出全部");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("ListAll");
	}
	
	public ListAll(Domain domain,String dbType)  throws ValidateException{
		super();
		this.domain = domain;
		this.dbType = dbType;
		this.denied = domain.isVerbDenied("ListAll");
		this.setVerbName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
		this.setLabel("列出全部");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("ListAll");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
			method.setReturnType(new Type("Map<String,Object>"));
			method.setThrowException(true);
			method.setIsprotected(false);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getServiceSuffix() + "."
					+ this.domain.getStandardName() + "Service");
			method.addMetaData("RequestMapping(value = \"/" + StringUtil.lowerFirst(method.getStandardName())
					+ "\", method = RequestMethod.POST)");

			List<Writeable> wlist = new ArrayList<Writeable>();
			Var service = new Var("service",
					new Type(this.domain.getStandardName() + "Service", this.domain.getPackageToken()));
			Var vlist = new Var("list", new Type("List", this.domain, this.domain.getPackageToken()));
			Method serviceMethod = this.generateServiceMethodDefinition();
			Var resultMap = new Var("result", new Type("TreeMap<String,Object>", "java.util"));
			wlist.add(NamedStatementGenerator.getJsonResultMap(1000L, 2, resultMap));
			wlist.add(new Statement(2000, 2, vlist.generateTypeVarString() + " = " + service.getVarName() + "."
					+ serviceMethod.getStandardName() + "();"));
			wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAndDomainList(3000L, 2, resultMap,
					vlist));
			wlist.add(new Statement(4000L, 2, "return " + resultMap.getVarName() + ";"));

			method.setMethodStatementList(WriteableUtil.merge(wlist));
			return method;
		}
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateControllerMethod();
			return m.generateMethodString();
		}
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateControllerMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied)
			return null;
		else {
			return null;
		}
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSButtonBlock().generateBlockContentString();
		}
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
		}
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			return null;
		}
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSActionMethod().generateMethodContentString();
		}
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateEasyUIJSActionMethod().generateMethodContentString();
		}
	}

	public Set<Field> getDeniedFields() {
		return deniedFields;
	}

	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}
}
