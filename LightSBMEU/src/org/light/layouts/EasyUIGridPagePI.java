package org.light.layouts;

import org.light.core.PrismInterface;
import org.light.domain.Domain;
import org.light.easyuilayouts.EasyUIGridPageLayout;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.WriteableUtil;

public class EasyUIGridPagePI extends PrismInterface{
	protected EasyUIGridPageLayout eLayout;

	public EasyUIGridPagePI(Domain domain) throws Exception{
		super();
		this.setStandardName(domain.getCapFirstPlural()+"GridPage");
		this.setLabel(domain.getText());
		this.domain = domain;
		this.eLayout = new EasyUIGridPageLayout(domain);
		this.eLayout.parse();
		this.frame.setStandardName(domain.getText());
		this.getFrame().setMainContent(this.eLayout);
		this.getFrame().setLanguage(domain.getLanguage());
	}	

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.getFrame().setTitles(title, subTitle, footer);
	}

	@Override
	public void generatePIFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "WebContent/pages/";
		String relativeFolder0 = "src/main/resources/static/pages/";
		if ("smeu".equalsIgnoreCase(this.technicalStack)||"msmeu".equalsIgnoreCase(this.technicalStack)) {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+this.domain.getPlural().toLowerCase()+".html", this.getFrame().generateFrameSetStatementList().getContent());
		}else {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder0+this.domain.getPlural().toLowerCase()+".html", this.getFrame().generateFrameSetStatementList().getContent());
		}
	}

	@Override
	public void generatePIFromDomian() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validateDomain() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayout() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void setNav(Nav nav) {
		this.getFrame().setNav(nav);
	}

	public EasyUIGridPageLayout geteLayout() {
		return eLayout;
	}

	public void seteLayout(EasyUIGridPageLayout eLayout) {
		this.eLayout = eLayout;
	}

}
