package org.light.layouts;

import org.light.core.LayoutComb;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ValidateInfo;
import org.light.easyuilayouts.EasyUIFrameSet;
import org.light.easyuilayouts.EasyUITreeParentChild;
import org.light.easyuilayouts.OracleEasyUITreeParentChild;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;

public class TreeParentChildLayout extends LayoutComb{
	protected EasyUIFrameSet frame = new EasyUIFrameSet();
	protected EasyUITreeParentChild eLayout = new EasyUITreeParentChild();
	protected Domain treeDomain;
	protected Domain parentDomain;
	protected Domain childDomain;
	protected String innerTreeParentId = "parentId";
	protected String treeParentId = "parentId";
	protected String parentId = "parentId";
	
	public TreeParentChildLayout(Domain treeDomain,Domain parentDomain,Domain childDomain,String innerTreeParentId,String treeParentId,String parentId,String dbType) {
		super();
		this.standardName = "TreeParentChild"+treeDomain.getCapFirstDomainName()+parentDomain.getCapFirstDomainName()+childDomain.getCapFirstDomainName();
		this.label =treeDomain.getText()+ parentDomain.getText()+ childDomain.getText() +"树父子表";
		if ("english".equalsIgnoreCase(treeDomain.getLanguage())) {
			this.label ="Tree parent child grid for "+treeDomain.getText()+ ", " +parentDomain.getText()+" and "+  childDomain.getText();
		}
		this.treeDomain = treeDomain;
		this.parentDomain = parentDomain;
		this.childDomain = childDomain;
		this.treeParentId = treeParentId;
		this.parentId = parentId;
		this.innerTreeParentId = innerTreeParentId;
		
		this.domains.add(this.treeDomain);
		this.domains.add(this.parentDomain);
		this.domains.add(this.childDomain);		
		
		if ("oracle".equalsIgnoreCase(dbType)) eLayout = new OracleEasyUITreeParentChild();
		eLayout.setTreeDomain(treeDomain);
		eLayout.setParentDomain(parentDomain);
		eLayout.setChildDomain(childDomain);
		eLayout.setInnerTreeParentId(this.treeDomain.findFieldByFieldName(this.innerTreeParentId));
		eLayout.setTreeParentId(this.parentDomain.findFieldByFieldName(this.treeParentId));
		eLayout.setParentId(this.childDomain.findFieldByFieldName(this.parentId));
		eLayout.parse();
		frame.setMainContent(eLayout);
		frame.setStandardName(this.label);
		frame.setLanguage(this.treeDomain.getLanguage());
	}

	public Domain getParentDomain() {
		return parentDomain;
	}

	public void setParentDomain(Domain parentDomain) {
		this.parentDomain = parentDomain;
	}

	public Domain getChildDomain() {
		return childDomain;
	}

	public void setChildDomain(Domain childDomain) {
		this.childDomain = childDomain;
	}


	@Override
	public void generateCombFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "WebContent/pages/";
		String relativeFolder0 = "src/main/resources/static/pages/";
		if ("smeu".equalsIgnoreCase(this.technicalStack)||"msmeu".equalsIgnoreCase(this.technicalStack)) {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+this.standardName.toLowerCase()+".html", frame.generateFrameSetStatementList().getContent());
		}else {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder0+this.standardName.toLowerCase()+".html", frame.generateFrameSetStatementList().getContent());
		}
	}

	@Override
	public void generateCombFromDomians() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ValidateInfo validateDomains() throws Exception {
		ValidateInfo info = new ValidateInfo();
		Field tf = DomainUtil.findDomainFieldByFieldName(this.parentDomain, this.treeParentId);
		Field f = DomainUtil.findDomainFieldByFieldName(this.childDomain, this.parentId);
		if (tf==null) info.addCompileError(this.getText()+"的treeParentId字段找不到！");
		if (f==null) info.addCompileError(this.getText()+"的parentId字段找不到！");
		if (tf!=null && tf instanceof Dropdown) {
			Dropdown tdp = (Dropdown) tf;
			if (!tdp.getTargetName().equals(this.treeDomain.getStandardName())) {
				info.addCompileError(this.getText()+"的treeParentId原字段类型不匹配！");
			}
			if (f!=null && f instanceof Dropdown) {
				Dropdown dp = (Dropdown) f;
				if (!dp.getTargetName().equals(this.parentDomain.getStandardName())) {
					info.addCompileError(this.getText()+"的parentId原字段类型不匹配！");
				}
			}
		}
		return info;
	}

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayouts() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	public EasyUIFrameSet getFrame() {
		return frame;
	}

	public void setFrame(EasyUIFrameSet frame) {
		this.frame = frame;
	}

	public Domain getTreeDomain() {
		return treeDomain;
	}

	public void setTreeDomain(Domain treeDomain) {
		this.treeDomain = treeDomain;
	}

	public EasyUITreeParentChild geteLayout() {
		return eLayout;
	}

	public void seteLayout(EasyUITreeParentChild eLayout) {
		this.eLayout = eLayout;
	}
	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.frame.setNav(nav);
	}
	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.frame.setTitles(title, subTitle, footer);
	}
	
	@Override
	public String getDomainNamesStr() {
		return this.treeDomain.getStandardName()+","+this.parentDomain.getStandardName()+","+this.childDomain.getStandardName();
	}

	public String getTreeParentId() {
		return treeParentId;
	}

	public void setTreeParentId(String treeParentId) {
		this.treeParentId = treeParentId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getInnerTreeParentId() {
		return innerTreeParentId;
	}

	public void setInnerTreeParentId(String innerTreeParentId) {
		this.innerTreeParentId = innerTreeParentId;
	}
}
