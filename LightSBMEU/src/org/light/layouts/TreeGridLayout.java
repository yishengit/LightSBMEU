package org.light.layouts;

import org.light.core.LayoutComb;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ValidateInfo;
import org.light.easyuilayouts.EasyUIFrameSet;
import org.light.easyuilayouts.EasyUITreeGrid;
import org.light.easyuilayouts.OracleEasyUITreeGrid;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;

public class TreeGridLayout extends LayoutComb{
	protected EasyUIFrameSet frame = new EasyUIFrameSet();
	protected EasyUITreeGrid eLayout = new EasyUITreeGrid();
	protected Domain parentTreeDomain;
	protected Domain childDomain;
	protected String parentId = "parentId";
	protected String innerTreeParentId = "parentId";
	
	public TreeGridLayout(Domain parentTreeDomain,Domain childDomain,String innerTreeParentId,String parentId,String dbType) {
		super();
		this.standardName = "TreeGrid"+parentTreeDomain.getCapFirstDomainName()+childDomain.getCapFirstDomainName();
		this.label = parentTreeDomain.getText()+ childDomain.getText() +"树表";
		if ("english".equalsIgnoreCase(parentTreeDomain.getLanguage())) {
			this.label ="Tree grid for "+parentTreeDomain.getText()+ " and " + childDomain.getText();
		}
		this.parentTreeDomain = parentTreeDomain;
		this.childDomain = childDomain;	
		this.innerTreeParentId = innerTreeParentId;
		this.parentId = parentId;
		
		this.domains.add(this.parentTreeDomain);
		this.domains.add(this.childDomain);
		
		if ("oracle".equalsIgnoreCase(dbType)) {
			eLayout = new OracleEasyUITreeGrid();
		}
		eLayout.setParentTreeDomain(parentTreeDomain);
		eLayout.setChildDomain(childDomain);
		eLayout.setInnerTreeParentId(parentTreeDomain.findFieldByFieldName(this.innerTreeParentId));
		eLayout.setTreeParentId(childDomain.findFieldByFieldName(this.parentId));
		eLayout.parse();
		frame.setMainContent(eLayout);	
		frame.setStandardName(this.label);
		frame.setLanguage(this.parentTreeDomain.getLanguage());
	}

	public Domain getChildDomain() {
		return childDomain;
	}

	public void setChildDomain(Domain childDomain) {
		this.childDomain = childDomain;
	}

	public EasyUITreeGrid geteLayout() {
		return eLayout;
	}

	public void seteLayout(EasyUITreeGrid eLayout) {
		this.eLayout = eLayout;
	}

	public Domain getParentTreeDomain() {
		return parentTreeDomain;
	}

	public void setParentTreeDomain(Domain parentTreeDomain) {
		this.parentTreeDomain = parentTreeDomain;
	}

	@Override
	public void generateCombFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "WebContent/pages/";
		String relativeFolder0 = "src/main/resources/static/pages/";
		if ("smeu".equalsIgnoreCase(this.technicalStack)||"msmeu".equalsIgnoreCase(this.technicalStack)) {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+this.standardName.toLowerCase()+".html", frame.generateFrameSetStatementList().getContent());
		}else {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder0+this.standardName.toLowerCase()+".html", frame.generateFrameSetStatementList().getContent());
		}
	}

	@Override
	public void generateCombFromDomians() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ValidateInfo validateDomains() throws Exception {
		ValidateInfo info = new ValidateInfo();
		Field f = DomainUtil.findDomainFieldByFieldName(this.childDomain, this.parentId);
		if (f==null) info.addCompileError(this.getText()+"的parentId字段找不到！");
		if (f!=null && f instanceof Dropdown) {
			Dropdown dp = (Dropdown) f;
			if (!dp.getTargetName().equals(this.parentTreeDomain.getStandardName())) {
				info.addCompileError(this.getText()+"的parentId原字段类型不匹配！");
			}
		}
		return info;
	}

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayouts() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	public EasyUIFrameSet getFrame() {
		return frame;
	}

	public void setFrame(EasyUIFrameSet frame) {
		this.frame = frame;
	}

	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.frame.setNav(nav);
	}
	
	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.frame.setTitles(title, subTitle, footer);
	}
	
	@Override
	public String getDomainNamesStr() {
		return this.parentTreeDomain.getStandardName()+","+this.childDomain.getStandardName();
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getInnerTreeParentId() {
		return innerTreeParentId;
	}

	public void setInnerTreeParentId(String innerTreeParentId) {
		this.innerTreeParentId = innerTreeParentId;
	}
}
