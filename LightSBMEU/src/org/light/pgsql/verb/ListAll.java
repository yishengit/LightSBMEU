package org.light.pgsql.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.exception.ValidateException;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ListAll extends org.light.verb.ListAll{
	protected Set<Field> deniedFields = new TreeSet<>();
	public ListAll() throws Exception {
		super();
	}
	
	public ListAll(Domain domain)  throws ValidateException{
		super(domain);
	}

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("listAll" + StringUtil.capFirst(this.domain.getPlural()));
			method.setNoContainer(true);
			List<Writeable> list = new ArrayList<Writeable>();
			list.add(new Statement(100L, 1, "<select id=\"" + StringUtil.lowerFirst(this.getVerbName())
					+ "\" resultMap=\"" + this.domain.getLowerFirstDomainName() + "\">"));
			list.add(new Statement(200L, 2, PgsqlReflector.generateSelectAllStatementWithDeniedFields(domain,deniedFields)));
			list.add(new Statement(300L, 1, "</select>"));
			method.setMethodStatementList(WriteableUtil.merge(list));
			return method;
		}
	}

	public Set<Field> getDeniedFields() {
		return deniedFields;
	}

	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}
}
