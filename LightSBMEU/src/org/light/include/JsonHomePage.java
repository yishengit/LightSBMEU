package org.light.include;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Include;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.WriteableUtil;

public class JsonHomePage extends Include{
	protected String headTitile = "Home Page";
	protected String pageTitile = "Welcome to the Json UI.";
	protected JsonUserNav jsonUserNav;
	
	public JsonUserNav getJsonUserNav() {
		return jsonUserNav;
	}

	public void setJsonUserNav(JsonUserNav jsonUserNav) {
		this.jsonUserNav = jsonUserNav;
	}

	public JsonHomePage(){
		super();
		this.fileName = "index.html";
		this.packageToken = "";
	}
	
	public JsonHomePage(String headTitile, String pageTitile){
		super();
		this.fileName = "index.html";
		this.packageToken = "";
		this.headTitile = headTitile;
		this.pageTitile = pageTitile;
	}

	@Override
	public String generateIncludeString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<%@page contentType=\"text/html\" pageEncoding=\"UTF-8\"%>\n");
		sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
		sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
		sb.append("<head>\n");
		sb.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n");
        sb.append("<title>"+this.headTitile+"</title>\n");
        sb.append("<link href=\"../css/default.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
        sb.append("</head>\n");
        sb.append("<body>\n");
        sb.append("<div id=\"wrapper\">\n");
        sb.append("<jsp:include page=\"../include/header.jsp\" />\n");
        sb.append("<!-- end div#header -->\n");
        sb.append("<div id=\"page_wide\">\n");
        sb.append("<div id=\"content\">\n");
        sb.append("<div id=\"welcome\">\n");
        sb.append("&nbsp;&nbsp;"+this.pageTitile+".\n");
        sb.append("</div>\n");
        sb.append("<!-- end div#welcome -->\n");
    	sb.append("</div>\n");
        sb.append("<!-- end div#content -->\n");
        sb.append("<div id=\"sidebar\">\n");
        sb.append("<ul>\n");
        sb.append("<%if (request.getSession().getAttribute(\"isadmin\")!=null && (Boolean)request.getSession().getAttribute(\"isadmin\")){%>\n");
        sb.append("<jsp:include page=\"../include/adminnav.jsp\"/>\n");
        sb.append("<%} else { %>\n");
        sb.append("<jsp:include page=\"../include/jsonusernav.jsp\"/>\n");
        sb.append("<%} %>\n");
        sb.append("<!-- end navigation -->\n");
        sb.append("<jsp:include page=\"../include/updates.jsp\"/>\n");
        sb.append("</ul>\n");
        sb.append("</div>\n");
        sb.append("<!-- end div#sidebar -->\n");
        sb.append("<div style=\"clear: both; height: 1px\"></div>\n");
        sb.append("</div>\n");
        sb.append("<jsp:include page=\"../include/footer.jsp\" />\n");
        sb.append("</div>\n");
        sb.append("<!-- end div#wrapper -->\n");
        sb.append("</body>\n");
        sb.append("</html>\n");

		return sb.toString();
	}

	public String getHeadTitile() {
		return headTitile;
	}

	public void setHeadTitile(String headTitile) {
		this.headTitile = headTitile;
	}

	public String getPageTitile() {
		return pageTitile;
	}

	public void setPageTitile(String pageTitile) {
		this.pageTitile = pageTitile;
	}

	@Override
	public StatementList getStatementList(long serial, int indent) {
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,indent,"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"));
		list.add(new Statement(2000L,indent,"<html xmlns=\"http://www.w3.org/1999/xhtml\">"));
		list.add(new Statement(3000L,indent,"<head>"));
		list.add(new Statement(4000L,indent,"<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"));
	    list.add(new Statement(5000L,indent,"<title>"+this.headTitile+"</title>"));
	    list.add(new Statement(6000L,indent,"<link href=\"css/default.css\" rel=\"stylesheet\" type=\"text/css\" />"));
	    list.add(new Statement(7000L,indent,"</head>"));
	    list.add(new Statement(8000L,indent,"<body>"));
	    list.add(new Statement(9000L,indent,"<div id=\"wrapper\">"));
	    list.add(new Header().getStatementList(9500L, indent));
	    list.add(new Statement(10000L,indent,"<!-- end div#header -->"));
	    list.add(new Statement(11000L,indent,"<div id=\"page_wide\">"));
	    list.add(new Statement(12000L,indent,"<div id=\"content\">"));
	    list.add(new Statement(13000L,indent,"<div id=\"welcome\">"));
	    list.add(new Statement(14000L,indent,"&nbsp;&nbsp;"+this.pageTitile+"."));
	    list.add(new Statement(15000L,indent,"</div>"));
	    list.add(new Statement(16000L,indent,"<!-- end div#welcome -->"));
		list.add(new Statement(17000L,indent,"</div>"));
	    list.add(new Statement(18000L,indent,"<!-- end div#content -->"));
	    list.add(new Statement(19000L,indent,"<div id=\"sidebar\">"));
	    list.add(new Statement(20000L,indent,"<ul>"));
	    //list.add(new Statement(1000L,indent,"<%if (request.getSession().getAttribute(\"isadmin\")!=null && (Boolean)request.getSession().getAttribute(\"isadmin\")){%>"));
	    //list.add(new Statement(1000L,indent,"<jsp:include page=\"include/adminnav.jsp\"/>"));
	    //list.add(new Statement(1000L,indent,"<%} else { %>"));
	    list.add(this.getJsonUserNav().getStatementList(20500, indent));
	    //list.add(new Statement(1000L,indent,"<%} %>"));
	    list.add(new Statement(21000L,indent,"<!-- end navigation -->"));
	    //list.add(new Statement(1000L,indent,"<jsp:include page=\"include/updates.jsp\"/>"));
	    list.add(new Statement(22000L,indent,"</ul>"));
	    list.add(new Statement(23000L,indent,"</div>"));
	    list.add(new Statement(24000L,indent,"<!-- end div#sidebar -->"));
	    list.add(new Statement(25000L,indent,"<div style=\"clear: both; height: 1px\"></div>"));
	    list.add(new Statement(26000L,indent,"</div>"));
	    list.add(new Footer().getStatementList(27000, indent));
	    list.add(new Statement(27000L,indent,"</div>"));
	    list.add(new Statement(28000L,indent,"<!-- end div#wrapper -->"));
	    list.add(new Statement(29000L,indent,"</body>"));
	    list.add(new Statement(30000L,indent,"</html>"));
//		list.add(new Statement(31000L,indent+1,"</ul>\n"));
//		list.add(new Statement(32000L,indent,"</li>\n"));		
		StatementList myList = WriteableUtil.merge(list);
		myList.setSerial(serial);
		return myList;
	}
}
