package org.light.generator;

import org.light.domain.Domain;
import org.light.exception.ValidateException;

public class JspTemplateFactory {
	public static JspTemplate getInstance(String type,Domain domain) throws ValidateException{
		switch (type) {
			case "grid" : return new GridJspTemplate(domain);
			case "pagingGrid" : return new PagingGridJspTemplate(domain);
			case "onlyloginindex":
						  return new OnlyLoginIndexJspTemplate();
			case "jsonPagingGrid": return new JsonPagingGridJspTemplate(domain);
			default		: return null;
		}
	}
}
