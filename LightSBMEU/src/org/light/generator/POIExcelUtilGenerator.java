package org.light.generator;

import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Util;

public class POIExcelUtilGenerator extends Util{
	public POIExcelUtilGenerator(){
		super();
		super.fileName = "POIExcelUtil.java";
	}
	
	public POIExcelUtilGenerator(String packageToken){
		super();
		this.setPackageToken(packageToken);
		super.fileName = "POIExcelUtil.java";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		StatementList sList = new StatementList();
		sList.add(new Statement(500L,0, "package "+this.packageToken+".utils;"));
		sList.add(new Statement(2000L,0,""));
		sList.add(new Statement(3000L,0,"import java.awt.Color;"));
		sList.add(new Statement(4000L,0,"import java.awt.image.BufferedImage;"));
		sList.add(new Statement(5000L,0,"import java.io.ByteArrayInputStream;"));
		sList.add(new Statement(6000L,0,"import java.io.ByteArrayOutputStream;"));
		sList.add(new Statement(7000L,0,"import java.io.IOException;"));
		sList.add(new Statement(8000L,0,"import java.io.InputStream;"));
		sList.add(new Statement(9000L,0,"import java.io.OutputStream;"));
		sList.add(new Statement(10000L,0,"import java.util.List;"));
		sList.add(new Statement(11000L,0,""));
		sList.add(new Statement(12000L,0,"import javax.imageio.ImageIO;"));
		sList.add(new Statement(13000L,0,""));
		sList.add(new Statement(14000L,0,"import org.apache.poi.hssf.usermodel.HSSFCell;"));
		sList.add(new Statement(15000L,0,"import org.apache.poi.hssf.usermodel.HSSFCellStyle;"));
		sList.add(new Statement(16000L,0,"import org.apache.poi.hssf.usermodel.HSSFClientAnchor;"));
		sList.add(new Statement(17000L,0,"import org.apache.poi.hssf.usermodel.HSSFPalette;"));
		sList.add(new Statement(18000L,0,"import org.apache.poi.hssf.usermodel.HSSFPatriarch;"));
		sList.add(new Statement(19000L,0,"import org.apache.poi.hssf.usermodel.HSSFRow;"));
		sList.add(new Statement(20000L,0,"import org.apache.poi.hssf.usermodel.HSSFSheet;"));
		sList.add(new Statement(21000L,0,"import org.apache.poi.hssf.usermodel.HSSFWorkbook;"));
		sList.add(new Statement(22000L,0,"import org.apache.poi.ss.usermodel.BorderStyle;"));
		sList.add(new Statement(23000L,0,"import org.apache.poi.ss.usermodel.Cell;"));
		sList.add(new Statement(24000L,0,"import org.apache.poi.ss.usermodel.ClientAnchor;"));
		sList.add(new Statement(25000L,0,"import org.apache.poi.ss.usermodel.FillPatternType;"));
		sList.add(new Statement(26000L,0,"import org.apache.poi.ss.usermodel.HorizontalAlignment;"));
		sList.add(new Statement(27000L,0,"import org.apache.poi.ss.usermodel.VerticalAlignment;"));
		sList.add(new Statement(28000L,0,""));
		sList.add(new Statement(29000L,0,"public final class POIExcelUtil {"));
		sList.add(new Statement(30000L,1,"public static void exportExcelWorkbookWithImage(OutputStream out, String sheetName, List<String> headers,"));
		sList.add(new Statement(31000L,3,"List<List<Object>> contents, List<Boolean> isImages) throws Exception {"));
		sList.add(new Statement(32000L,2,"HSSFWorkbook wb = new HSSFWorkbook();"));
		sList.add(new Statement(33000L,2,"HSSFSheet sheet = wb.createSheet(sheetName);"));
		sList.add(new Statement(34000L,2,"HSSFRow row;"));
		sList.add(new Statement(35000L,2,"HSSFCell cell;"));
		sList.add(new Statement(36000L,0,""));
		sList.add(new Statement(37000L,2,"short colorIndex = 10;"));
		sList.add(new Statement(38000L,2,"HSSFPalette palette = wb.getCustomPalette();"));
		sList.add(new Statement(39000L,2,"Color rgb = Color.YELLOW;"));
		sList.add(new Statement(40000L,2,"short bgIndex = colorIndex++;"));
		sList.add(new Statement(41000L,2,"palette.setColorAtIndex(bgIndex, (byte) rgb.getRed(), (byte) rgb.getGreen(), (byte) rgb.getBlue());"));
		sList.add(new Statement(42000L,2,"short bdIndex = colorIndex++;"));
		sList.add(new Statement(43000L,2,"rgb = Color.BLACK;"));
		sList.add(new Statement(44000L,2,"palette.setColorAtIndex(bdIndex, (byte) rgb.getRed(), (byte) rgb.getGreen(), (byte) rgb.getBlue());"));
		sList.add(new Statement(45000L,0,""));
		sList.add(new Statement(46000L,2,"HSSFCellStyle cellStyle = wb.createCellStyle();"));
		sList.add(new Statement(47000L,2,"cellStyle.setBorderBottom(BorderStyle.THIN);"));
		sList.add(new Statement(48000L,2,"cellStyle.setBorderLeft(BorderStyle.THIN);"));
		sList.add(new Statement(49000L,2,"cellStyle.setBorderTop(BorderStyle.THIN);"));
		sList.add(new Statement(50000L,2,"cellStyle.setBorderRight(BorderStyle.THIN);"));
		sList.add(new Statement(51000L,2,"// bdIndex 边框颜色下标值"));
		sList.add(new Statement(52000L,2,"cellStyle.setBottomBorderColor(bdIndex);"));
		sList.add(new Statement(53000L,2,"cellStyle.setLeftBorderColor(bdIndex);"));
		sList.add(new Statement(54000L,2,"cellStyle.setRightBorderColor(bdIndex);"));
		sList.add(new Statement(55000L,2,"cellStyle.setTopBorderColor(bdIndex);"));
		sList.add(new Statement(56000L,0,""));
		sList.add(new Statement(57000L,2,"cellStyle.setAlignment(HorizontalAlignment.CENTER);"));
		sList.add(new Statement(58000L,2,"cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);"));
		sList.add(new Statement(59000L,0,""));
		sList.add(new Statement(60000L,2,"HSSFCellStyle cellHeaderStyle = wb.createCellStyle();"));
		sList.add(new Statement(61000L,2,"cellHeaderStyle.cloneStyleFrom(cellStyle);"));
		sList.add(new Statement(62000L,0,""));
		sList.add(new Statement(63000L,2,"cellHeaderStyle.setFillForegroundColor(bgIndex); // bgIndex 背景颜色下标值"));
		sList.add(new Statement(64000L,2,"cellHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);"));
		sList.add(new Statement(65000L,0,""));
		sList.add(new Statement(66000L,2,"writeRow(wb, sheet, bgIndex, bdIndex, 1, cellHeaderStyle, headers);"));
		sList.add(new Statement(67000L,0,""));
		sList.add(new Statement(68000L,2,"for (int i = 0; i < contents.size(); i++) {"));
		sList.add(new Statement(69000L,3,"writeRowWithImage(wb, sheet, bgIndex, bdIndex, i + 2, cellStyle, contents.get(i));"));
		sList.add(new Statement(70000L,2,"}"));
		sList.add(new Statement(71000L,0,""));
		sList.add(new Statement(72000L,2,"boolean containsImage = containsImage(isImages);"));
		sList.add(new Statement(73000L,2,"int rowHeight = 30;"));
		sList.add(new Statement(74000L,2,"if (containsImage)"));
		sList.add(new Statement(75000L,3,"rowHeight = 50;"));
		sList.add(new Statement(76000L,0,""));
		sList.add(new Statement(77000L,2,"// 创建表格之后设置行高与列宽"));
		sList.add(new Statement(78000L,2,"for (int i = 1; i < contents.size() + 2; i++) {"));
		sList.add(new Statement(79000L,3,"row = sheet.getRow(i);"));
		sList.add(new Statement(80000L,3,"row.setHeightInPoints(rowHeight);"));
		sList.add(new Statement(81000L,2,"}"));
		sList.add(new Statement(82000L,2,"for (int j = 1; j < headers.size() + 1; j++) {"));
		sList.add(new Statement(83000L,3,"sheet.setColumnWidth(j, MSExcelUtil.pixel2WidthUnits(120));"));
		sList.add(new Statement(84000L,2,"}"));
		sList.add(new Statement(85000L,2,"wb.write(out);"));
		sList.add(new Statement(86000L,1,"}"));
		sList.add(new Statement(87000L,0,""));
		sList.add(new Statement(88000L,1,"public static BufferedImage getBufferedImage(byte[] image) {"));
		sList.add(new Statement(89000L,2,"BufferedImage bi = null;"));
		sList.add(new Statement(90000L,2,"try {"));
		sList.add(new Statement(91000L,3,"InputStream buffin = new ByteArrayInputStream(image);"));
		sList.add(new Statement(92000L,3,"bi = ImageIO.read(buffin);"));
		sList.add(new Statement(93000L,2,"} catch (IOException e) {"));
		sList.add(new Statement(94000L,3,"e.printStackTrace();"));
		sList.add(new Statement(95000L,2,"}"));
		sList.add(new Statement(96000L,2,"return bi;"));
		sList.add(new Statement(97000L,1,"}"));
		sList.add(new Statement(98000L,0,""));
		sList.add(new Statement(99000L,1,"private static void setPicture(HSSFWorkbook workbook, HSSFSheet sheet, int rowNum, int colNum, byte[] image)"));
		sList.add(new Statement(100000L,3,"throws Exception {"));
		sList.add(new Statement(101000L,2,"HSSFPatriarch patriarch = sheet.createDrawingPatriarch();"));
		sList.add(new Statement(102000L,2,"ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();"));
		sList.add(new Statement(103000L,2,"BufferedImage bi = getBufferedImage(image);"));
		sList.add(new Statement(104000L,2,"if (bi != null && bi.getHeight() > 0) {"));
		sList.add(new Statement(105000L,3,"ImageIO.write(bi, \"png\", byteArrayOut);"));
		sList.add(new Statement(106000L,0,""));
		sList.add(new Statement(107000L,3,"int pdx1 = 120 - (int) ((50.0 / bi.getHeight()) * bi.getWidth());"));
		sList.add(new Statement(108000L,3,"int dx1 = (int) (Double.valueOf(pdx1) / 120.0 * 1023.0 / 2);"));
		sList.add(new Statement(109000L,3,"if (dx1 <= 0)"));
		sList.add(new Statement(110000L,4,"dx1 = 0;"));
		sList.add(new Statement(111000L,3,"int dx2 = 1023 - dx1;"));
		sList.add(new Statement(112000L,3,"System.out.println(\"JerryDebug:\" + pdx1 + \":dx1:\" + dx1 + \":bi.width:\" + bi.getWidth() + \":bi.height:\""));
		sList.add(new Statement(113000L,5,"+ bi.getHeight());"));
		sList.add(new Statement(114000L,3,"HSSFClientAnchor anchor = new HSSFClientAnchor(dx1, 0, dx2, 0, (short) colNum, rowNum, (short) colNum,"));
		sList.add(new Statement(115000L,5,"rowNum + 1);"));
		sList.add(new Statement(116000L,3,"anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);"));
		sList.add(new Statement(117000L,3,"patriarch.createPicture(anchor,"));
		sList.add(new Statement(118000L,5,"workbook.addPicture(byteArrayOut.toByteArray(), HSSFWorkbook.PICTURE_TYPE_PNG));"));
		sList.add(new Statement(119000L,2,"}"));
		sList.add(new Statement(120000L,1,"}"));
		sList.add(new Statement(121000L,0,""));
		sList.add(new Statement(122000L,1,"protected static void writeRow(HSSFWorkbook wb, HSSFSheet sheet, short bgIndex, short bdIndex, int rowIndex,"));
		sList.add(new Statement(123000L,3,"HSSFCellStyle cellStyle, List<String> data) {"));
		sList.add(new Statement(124000L,2,"HSSFRow row = sheet.createRow(rowIndex);// 创建表格行"));
		sList.add(new Statement(125000L,2,"for (int j = 0; j < data.size(); j++) {"));
		sList.add(new Statement(126000L,3,"Cell cell = row.createCell(j + 1);// 根据表格行创建单元格"));
		sList.add(new Statement(127000L,3,"cell.setCellStyle(cellStyle);"));
		sList.add(new Statement(128000L,3,"cell.setCellValue(String.valueOf(StringUtil.nullTrim(data.get(j))));"));
		sList.add(new Statement(129000L,2,"}"));
		sList.add(new Statement(130000L,1,"}"));
		sList.add(new Statement(131000L,0,""));
		sList.add(new Statement(132000L,1,"protected static void writeRowWithImage(HSSFWorkbook wb, HSSFSheet sheet, short bgIndex, short bdIndex,"));
		sList.add(new Statement(133000L,3,"int rowIndex, HSSFCellStyle cellStyle, List<Object> data) throws Exception {"));
		sList.add(new Statement(134000L,2,"HSSFRow row = sheet.createRow(rowIndex);// 创建表格行"));
		sList.add(new Statement(135000L,2,"for (int j = 0; j < data.size(); j++) {"));
		sList.add(new Statement(136000L,3,"Cell cell = row.createCell(j + 1);// 根据表格行创建单元格"));
		sList.add(new Statement(137000L,3,"cell.setCellStyle(cellStyle);"));
		sList.add(new Statement(138000L,3,"if (data.get(j) instanceof String)"));
		sList.add(new Statement(139000L,4,"cell.setCellValue(String.valueOf(StringUtil.nullTrim((String) data.get(j))));"));
		sList.add(new Statement(140000L,3,"else if (data.get(j) instanceof byte[])"));
		sList.add(new Statement(141000L,4,"setPicture(wb, sheet, rowIndex, j + 1, (byte[]) data.get(j));"));
		sList.add(new Statement(142000L,2,"}"));
		sList.add(new Statement(143000L,1,"}"));
		sList.add(new Statement(144000L,0,""));
		sList.add(new Statement(145000L,1,"private static boolean containsImage(List<Boolean> isImages) {"));
		sList.add(new Statement(146000L,2,"for (Boolean isImage : isImages) {"));
		sList.add(new Statement(147000L,3,"if (isImage)"));
		sList.add(new Statement(148000L,4,"return true;"));
		sList.add(new Statement(149000L,2,"}"));
		sList.add(new Statement(150000L,2,"return false;"));
		sList.add(new Statement(151000L,1,"}"));
		sList.add(new Statement(152000L,0,"}"));

		return sList.getContent();
	}

}
