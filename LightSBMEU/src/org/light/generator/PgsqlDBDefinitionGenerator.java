package org.light.generator;

import org.light.domain.Domain;
import org.light.utils.PgsqlReflector;
import org.light.utils.SqlReflector;

public class PgsqlDBDefinitionGenerator extends DBDefinitionGenerator{

	public String generateDBSql(boolean createDB) throws Exception{
		// set up the database;
		StringBuilder sb = new StringBuilder();
		if (createDB == true){
			sb.append("drop database if exists ").append(this.getDbName()).append(";\n");
			sb.append("create database ").append(this.getDbName()).append(";\n");
			sb.append("\\c ").append(this.getDbName()).append("\n\n");
		}
			
		for (int i=0; i < this.getDomains().size();i++){
			Domain domain = this.getDomains().get(i);
			sb.append(PgsqlReflector.generateTableDefinition(domain)).append("\n");
		}
		return sb.toString();
	}
	
	public String generateDBSql() throws Exception{
		return generateDBSql(false);
	}

	@Override
	public String generateDropTableSqls(boolean createNew) throws Exception {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i < this.getDomains().size();i++){
			Domain domain = this.getDomains().get(i);
			sb.append(PgsqlReflector.generatePgsqlDropTableStatement(domain)).append("\n");
		}
		sb.append("\n");
		return sb.toString();
	}
	
}
