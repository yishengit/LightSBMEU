package org.light.core;

import java.util.Set;
import java.util.TreeSet;

import org.light.domain.Field;
import org.light.domain.ManyToMany;

public class CustomManyToMany extends ManyToMany{
	protected Set<Field> fields = new TreeSet<>();

	public Set<Field> getFields() {
		return fields;
	}

	public void setFields(Set<Field> fields) {
		this.fields = fields;
	}	

}
