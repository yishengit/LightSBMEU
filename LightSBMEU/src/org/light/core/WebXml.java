package org.light.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.domain.ConfigFile;
import org.light.domain.Statement;
import org.light.utils.WriteableUtil;

public class WebXml extends ConfigFile{
	protected List<Action> actions = new ArrayList<Action>();
	protected String xmlDefinition = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	protected String webApp = "<web-app xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://java.sun.com/xml/ns/javaee\" xmlns:web=\"http://java.sun.com/xml/ns/javaee\" xmlns:jsp=\"http://java.sun.com/xml/ns/javaee/jsp\" xsi:schemaLocation=\"http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd\" version=\"2.5\">\n";
	protected String projectName;
	protected List<String> welcomeFileList = new ArrayList<String>();
	protected List<ErrPage> errPages = new ArrayList<ErrPage>();
	protected String packageToken;
	protected String controllerPackage = "";
	protected Set<String> moduleNames = new TreeSet<>();
	protected String springConfigContent(Set<String> configfiles) {
		String start = "<!-- needed for ContextLoaderListener -->\n" +
	
		 "<context-param>\n" +
		 "\t<param-name>contextConfigLocation</param-name>\n" +
		 "\t<param-value>classpath:applicationContext*.xml";
		
		for (String str:configfiles) {
			start = start+",classpath:"+str;
		}
		 return start + "</param-value>\n" +
		 "</context-param>\n\n" +
		 "<!-- Bootstraps the root web application context before servlet initialization -->\n" +
		 "<listener>\n" +
		 "\t<listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>\n" +
		 "</listener>\n\n";
	}
	
	protected String getSpringMVCConfigContent() {
		return "<servlet>\n"+
		    "<servlet-name>spring</servlet-name>\n"+
		    "<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>\n"+
		    "<init-param>\n"+
		      "\t<param-name>contextConfigLocation</param-name>\n"+
		      "\t<param-value>/WEB-INF/spring-mvc.xml</param-value>\n"+
		    "</init-param>\n"+
		    "<load-on-startup>1</load-on-startup>\n"+
		  "</servlet>\n"+
		  "<servlet-mapping>\n"+
		    "\t<servlet-name>spring</servlet-name>\n"+
		    "\t<url-pattern>/"+this.getControllerPackage()+"/*</url-pattern>\n"+
		  "</servlet-mapping>\n\n";
	}
	
	protected String getShiroFilter() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,1,"<filter>"));
		sList.add(new Statement(2000L,2,"<filter-name>shiroFilter</filter-name>"));
		sList.add(new Statement(3000L,2,"<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>"));
		sList.add(new Statement(4000L,2,"<init-param>"));
		sList.add(new Statement(5000L,3,"<param-name>targetFilterLifecycle</param-name>"));
		sList.add(new Statement(6000L,3,"<param-value>true</param-value>"));
		sList.add(new Statement(7000L,2,"</init-param>"));
		sList.add(new Statement(8000L,1,"</filter>"));
		sList.add(new Statement(9000L,1,"<filter-mapping>"));
		sList.add(new Statement(10000L,2,"<filter-name>shiroFilter</filter-name>"));
		sList.add(new Statement(11000L,2,"<url-pattern>/*</url-pattern>"));
		sList.add(new Statement(12000L,1,"</filter-mapping>"));
		sList.add(new Statement(13000L,1,""));
		return WriteableUtil.merge(sList).getContent();
	}
	
	public WebXml(){
		super();
		this.standardName = "web.xml";
	}
	
	@Override 
	public void setStandardName(String standardName){
	}
	
	@Override
	public String generateConfigFileString() {
		StringBuilder sb = new StringBuilder();
		sb.append(xmlDefinition).append(webApp);//.append("<display-name>").append(this.getProjectName()).append("</display-name>\n");
		if (welcomeFileList.size() > 0){
			sb.append("<welcome-file-list>\n");
			for (String welcomeFile : welcomeFileList){
				sb.append("<welcome-file>").append(welcomeFile).append("</welcome-file>\n");
			}
			sb.append("</welcome-file-list>\n\n");
		}
		
		if (this.moduleNames.contains("ShiroAuthModule")) {
			this.errPages.add(new ErrPage("/login/error.html","403"));
			this.errPages.add(new ErrPage("/login/error.html","404"));
			this.errPages.add(new ErrPage("/login/error.html","405"));
			this.errPages.add(new ErrPage("/login/error.html","500"));
		}
		for (ErrPage ep : this.errPages){
			sb.append("<error-page>\n").append("<error-code>").append(ep.getErrCode()).append("</error-code>\n")
					.append("<location>").append(ep.getLocation()).append("</location>\n").append("</error-page>\n\n");
		}
		
		Set<String> configfiles = new TreeSet<>();
		if (this.moduleNames.contains("ShiroAuthModule")) configfiles.add("spring-shiro.xml");
		sb.append(springConfigContent(configfiles));
		if (this.moduleNames.contains("ShiroAuthModule")) sb.append(getShiroFilter());
		sb.append(getSpringMVCConfigContent());
		sb.append("</web-app>\n");
		return sb.toString();
	}


	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}
	
	public void addAction(Action action) {
		this.actions.add(action);
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public List<String> getWelcomeFileList() {
		return welcomeFileList;
	}

	public void setWelcomeFileList(List<String> welcomeFileList) {
		this.welcomeFileList = welcomeFileList;
	}

	public List<ErrPage> getErrPages() {
		return errPages;
	}

	public void setErrPages(List<ErrPage> errPages) {
		this.errPages = errPages;
	}
	
	public void addErrPage(ErrPage errPage){
		this.errPages.add(errPage);
	}
	
	public void addErrPages(List<ErrPage> errPageList) {
		this.errPages.addAll(errPageList);
	}
	
	public void addWelcome(String welcomePage){
		this.welcomeFileList.add(welcomePage);
	}
	
	public void addWelcomes(List<String> welcomePages){
		this.welcomeFileList.addAll(welcomePages);
	}

	public String getPackageToken() {
		return packageToken;
	}

	public String getControllerPackage() {
		return controllerPackage;
	}

	public void setControllerPackage(String controllerPackage) {
		this.controllerPackage = controllerPackage;
	}

	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}

	public Set<String> getModuleNames() {
		return moduleNames;
	}

	public void setModuleNames(Set<String> moduleNames) {
		this.moduleNames = moduleNames;
	}

}

class ErrPage {
	protected String location;
	protected String errCode;
	public ErrPage(String location,String errCode) {
		super();
		this.location = location;
		this.errCode = errCode;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
}