package org.light.core;

import org.light.domain.Dropdown;
import org.light.domain.Field;

public class ParentIdDomainNameSet extends FieldSet{

	@Override
	public int compareTo(FieldSet o) {
		return this.getStandardName().compareTo(o.getStandardName());
	}

	@Override
	public boolean validate() {
		Field f0 = new Field("parentId","Long");
		if (this.refDomainContainsField(this.refDomain,f0)) return true;
		Field f1 = this.refDomain.findFieldByFieldName("parentId");
		if (f1!=null && f1 instanceof Dropdown) {
			Field f2 = this.refDomain.findFieldByFieldName("domainName");
			if (f2 !=null &&"String".equalsIgnoreCase(f2.getClassType().getTypeName())) return true;
			else return false;
		}
		else if (f1!=null && "Long".equals(f1.getClassType().getTypeName())) {
			Field f2 = this.refDomain.findFieldByFieldName("domainName");
			if (f2 !=null &&"String".equalsIgnoreCase(f2.getClassType().getTypeName())) return true;
			else return false;
		}
		else return false;
	}

}
