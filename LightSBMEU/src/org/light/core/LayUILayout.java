package org.light.core;

import java.util.Collections;

import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.StringUtil;

public class LayUILayout implements Comparable<LayUILayout>{
	protected int serial = 1;
	protected String standardName;
	protected String content = "";
	protected String blockComment;
	protected StatementList blockStatementList = new StatementList();
	protected HtmlBlock nav;
	protected HtmlBlock header;
	protected HtmlBlock pageFooter;
	protected HtmlBlock mainContent;
	protected HtmlBlock loginWidget;

	public String getBlockComment() {
		return blockComment;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setBlockComment(String blockComment) {
		this.blockComment = blockComment;
	}

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getCapFirstMethodName() {
		return StringUtil.capFirst(standardName);
	}
	
	public String getLowerFirstMethodName() {
		return StringUtil.lowerFirst(standardName);
	}

	public String getThrowException() {
		return "Exception";
	}

	public StatementList getMethodStatementList() {
		return blockStatementList;
	}
	
	public StatementList getMethodStatementList(long serial) {
		blockStatementList.setSerial(serial);
		return blockStatementList;
	}

	public void setMethodStatementList(StatementList blockStatementList) {
		this.blockStatementList = blockStatementList;
	}
	
	public String generateBlockContentString() {
		if (this.blockStatementList != null){
			StringBuilder sb = new StringBuilder();
			Collections.sort(this.blockStatementList);
			for (Statement s : this.blockStatementList){
				for (int i=0;i < s.getIndent();i++) sb.append("\t");
				sb.append(s.getContent()).append("\n");
			}
			return sb.toString();
		}
		else return this.content;
	}

	
	@Override
	public int compareTo(LayUILayout o) {
		if (this.getSerial() > o.getSerial()) return 1;
		else if (this.getSerial() == o.getSerial()) return 0;
		else return -1;
	}

	public StatementList getBlockStatementList() {
		return blockStatementList;
	}

	public void setBlockStatementList(StatementList blockStatementList) {
		this.blockStatementList = blockStatementList;
	}

	public HtmlBlock getNav() {
		return nav;
	}

	public void setNav(HtmlBlock nav) {
		this.nav = nav;
	}

	public HtmlBlock getHeader() {
		return header;
	}

	public void setHeader(HtmlBlock header) {
		this.header = header;
	}

	public HtmlBlock getPageFooter() {
		return pageFooter;
	}

	public void setPageFooter(HtmlBlock pageFooter) {
		this.pageFooter = pageFooter;
	}

	public HtmlBlock getMainContent() {
		return mainContent;
	}

	public void setMainContent(HtmlBlock mainContent) {
		this.mainContent = mainContent;
	}

	public HtmlBlock getLoginWidget() {
		return loginWidget;
	}

	public void setLoginWidget(HtmlBlock loginWidget) {
		this.loginWidget = loginWidget;
	}
}
