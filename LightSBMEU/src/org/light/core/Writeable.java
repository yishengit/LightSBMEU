package org.light.core;

import java.io.Serializable;

public interface Writeable extends Comparable<Writeable>,Serializable{
	public long getSerial();
	public String getContent();
	public String getContentWithSerial();
	public String getStatement(long pos);
	int compareTo(Writeable o);
}
