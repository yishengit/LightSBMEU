package org.light.core;

import java.util.Set;

import org.light.domain.Domain;
import org.light.domain.Field;

public abstract class FieldSet implements Comparable<FieldSet>{
	protected long serial = 1L;
	protected String standardName;
	protected Set<Field> fields;
	protected Domain refDomain;
	public abstract boolean validate();
	public boolean refDomainContainsField(Domain refDomain,Field field) {
		Set<Field> fields = refDomain.getFields();
		for (Field f:fields) {
			if (f.getFieldName().equals(field.getFieldName())) {
				if (f.getFieldType().equals(field.getFieldType())){
					if ("string".equalsIgnoreCase(f.getFieldType())) {
						if(Double.valueOf(f.getLengthStr())>=Double.valueOf(field.getLengthStr())) {
							return true;
						}else {
							return false;
						}
					}
					return true;
				}
			}
		}
		return false;
	}
	public long getSerial() {
		return serial;
	}
	public void setSerial(long serial) {
		this.serial = serial;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public Set<Field> getFields() {
		return fields;
	}
	public void setFields(Set<Field> fields) {
		this.fields = fields;
	}
	public Domain getRefDomain() {
		return refDomain;
	}
	public void setRefDomain(Domain refDomain) {
		this.refDomain = refDomain;
	}
}
