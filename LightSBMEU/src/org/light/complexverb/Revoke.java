package org.light.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.DragonHideStatement;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class Revoke extends TwoDomainVerb implements EasyUIPositions{

	@Override
	public Method generateDaoImplMethod() throws Exception {
		Method method = new Method();
		if (StringUtil.isBlank(this.slave.getAlias())){
			method.setStandardName("revoke"+this.slave.getCapFirstDomainName()+"From"+this.master.getCapFirstDomainName());		
		} else {
			method.setStandardName("revoke"+StringUtil.capFirst(this.slave.getAlias())+"From"+this.master.getCapFirstDomainName());	
		}
		method.setNoContainer(true);
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(100L,1,"<delete id=\""+method.getStandardName()+"\">"));
		list.add(new Statement(200L,2, MybatisSqlReflector.generateDeleteLinkTwoSql(master,slave)));
		list.add(new Statement(300L,1,"</delete>"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		Method m = this.generateDaoImplMethod();
		String s = m.generateMethodString();
		return s;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		Method method = new Method();
		if (StringUtil.isBlank(this.slave.getAlias())){
			method.setStandardName("revoke"+this.slave.getCapFirstDomainName()+"From"+this.master.getCapFirstDomainName());
		} else {
			method.setStandardName("revoke"+StringUtil.capFirst(this.slave.getAlias())+"From"+this.master.getCapFirstDomainName());
		}
		method.setReturnType(new Type("Integer"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("org.apache.ibatis.annotations.Param");
		
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType(),this.master.getPackageToken(),"Param(value=\"" + this.master.getLowerFirstDomainName() + "Id\")"));
		if (StringUtil.isBlank(this.slave.getAlias())){
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Id", this.slave.getDomainId().getClassType(),this.slave.getPackageToken(),"Param(value=\"" + this.slave.getLowerFirstDomainName() + "Id\")"));
		} else {
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getAlias())+"Id", this.slave.getDomainId().getClassType(),this.slave.getPackageToken(),"Param(value=\"" + StringUtil.lowerFirst(this.slave.getAlias()) + "Id\")"));
		}
		return method;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(getVerbName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType()));
		if (StringUtil.isBlank(this.slave.getAlias())){
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Ids", new Type("String")));
		} else {
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getAlias())+"Ids", new Type("String")));
		}
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(getVerbName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("org.springframework.transaction.annotation.Transactional");
		method.addAdditionalImport(this.master.getPackageToken()+"."+this.master.getDaoSuffix()+"."+this.master.getStandardName()+"Dao");
		method.addAdditionalImport(this.slave.getPackageToken()+"."+this.slave.getServiceSuffix()+"."+this.slave.getStandardName()+"Service");
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType()));
		if (StringUtil.isBlank(this.slave.getAlias())){
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Ids", new Type("String")));
		} else {
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getAlias())+"Ids", new Type("String")));
		}
		method.addMetaData("Override");	
		method.addMetaData("Transactional");	
		
		List<Writeable> list = new ArrayList<Writeable>();
		if (StringUtil.isBlank(this.slave.getAlias())){
			list.add(new Statement(1000L,2,"String [] "+this.slave.getLowerFirstDomainName()+"IdsArr = "+this.slave.getLowerFirstDomainName()+"Ids.split(\",\");"));
			list.add(new Statement(2000L,2,"for (int i = 0; i < "+this.slave.getLowerFirstDomainName()+"IdsArr.length; i++ ){"));
			list.add(new DragonHideStatement(3000L,2,"String "+this.slave.getLowerFirstDomainName()+"Id = "+this.slave.getLowerFirstDomainName()+"IdsArr[i];",this.slave.getDomainId().getClassType().toString().equals("String")));
			list.add(new DragonHideStatement(3000L,2,"Long "+this.slave.getLowerFirstDomainName()+"Id = Long.parseLong("+this.slave.getLowerFirstDomainName()+"IdsArr[i]);",this.slave.getDomainId().getClassType().toString().equals("Long")));
			list.add(new DragonHideStatement(3000L,2,"Integer "+this.slave.getLowerFirstDomainName()+"Id = Integer.parseInt("+this.slave.getLowerFirstDomainName()+"IdsArr[i]);",this.slave.getDomainId().getClassType().toString().equals("Integer")));
			list.add(new Statement(4000L,2,"Integer success = dao."+generateDaoMethodDefinition().generateStandardCallString()+";"));
			list.add(new Statement(5000L,2,"if (success < 0) return false;"));
			list.add(new Statement(6000L,2,"}"));
			list.add(new Statement(7000L,2,"return true;"));
		} else {
			list.add(new Statement(1000L,2,"String [] "+StringUtil.lowerFirst(this.slave.getAlias())+"IdsArr = "+StringUtil.lowerFirst(this.slave.getAlias())+"Ids.split(\",\");"));
			list.add(new Statement(2000L,2,"for (int i = 0; i < "+StringUtil.lowerFirst(this.slave.getAlias())+"IdsArr.length; i++ ){"));
			list.add(new DragonHideStatement(3000L,2,"String "+StringUtil.lowerFirst(this.slave.getAlias())+"Id = "+StringUtil.lowerFirst(this.slave.getAlias())+"IdsArr[i];",this.slave.getDomainId().getClassType().toString().equals("String")));
			list.add(new DragonHideStatement(3000L,2,"Long "+StringUtil.lowerFirst(this.slave.getAlias())+"Id = Long.parseLong("+StringUtil.lowerFirst(this.slave.getAlias())+"IdsArr[i]);",this.slave.getDomainId().getClassType().toString().equals("Long")));
			list.add(new DragonHideStatement(3000L,2,"Integer "+StringUtil.lowerFirst(this.slave.getAlias())+"Id = Integer.parseInt("+StringUtil.lowerFirst(this.slave.getAlias())+"IdsArr[i]);",this.slave.getDomainId().getClassType().toString().equals("Integer")));
			list.add(new Statement(4000L,2,"Integer success = dao."+generateDaoMethodDefinition().generateStandardCallString()+";"));
			list.add(new Statement(5000L,2,"if (success < 0) return false;"));
			list.add(new Statement(6000L,2,"}"));
			list.add(new Statement(7000L,2,"return true;"));
		}
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	@Override
	public Method generateControllerMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(getVerbName()));
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addAdditionalImport(this.master.getPackageToken()+"."+this.master.getServiceSuffix()+"."+this.master.getStandardName()+"Service");
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType(),"RequestParam(value = \""+this.master.getLowerFirstDomainName()+"Id\", required = true)"));
		if (StringUtil.isBlank(this.slave.getAlias())){
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Ids", new Type("String"),"RequestParam(value = \""+this.slave.getLowerFirstDomainName()+"Ids\", required = true)"));
		} else {
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getAlias())+"Ids", new Type("String"),"RequestParam(value = \""+StringUtil.lowerFirst(this.slave.getAlias())+"Ids\", required = true)"));	
		}
		method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(getVerbName())+"\", method = RequestMethod.POST)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		wlist.add(new Statement(1000L, 2, "Map<String,Object> result = new TreeMap<String,Object>();"));
		wlist.add(new Statement(2000L, 2, "boolean success = service."+generateServiceMethodDefinition().generateStandardCallString()+";"));
		wlist.add(new Statement(3000L, 2, "result.put(\"success\",success);"));
		wlist.add(new Statement(4000L, 2, "result.put(\"rows\",null);"));
		wlist.add(new Statement(5000L, 2, "return result;"));
		method.setMethodStatementList(WriteableUtil.merge(wlist));		
		return method;
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		Method m = this.generateControllerMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		Method m = this.generateControllerMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public Revoke(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
		if (StringUtil.isBlank(this.slave.getAlias())){
			this.setVerbName("Revoke"+this.slave.getCapFirstPlural()+"From"+this.master.getCapFirstDomainName());
		} else {
			this.setVerbName("Revoke"+StringUtil.capFirst(this.slave.getAliasPlural())+"From"+this.master.getCapFirstDomainName());
		}
		this.setLabel("撤销");
	}
	

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		
		StatementList sl = new StatementList();
		if (StringUtil.isBlank(this.slave.getAlias())){
			sl.add(new Statement(1000,1, "var "+this.slave.getLowerFirstDomainName()+"Ids = new Array();"));
			sl.add(new Statement(2000,1, "var rows = $(\"#my"+this.slave.getCapFirstPlural()+"\").datalist(\"getSelections\");"));
			sl.add(new Statement(3000,1, "if (isBlank(rows)) {"));
			if ("english".equalsIgnoreCase(this.master.getLanguage())) {
				sl.add(new Statement(4000,1, "$.messager.alert(\"Warning\", \"Please select current "+this.slave.getText()+"！\");"));
			}else {
				sl.add(new Statement(4000,1, "$.messager.alert(\"操作提示\", \"请选择现有"+this.slave.getText()+"！\");"));
			}
			sl.add(new Statement(5000,1, "return;"));
			sl.add(new Statement(6000,1, "}"));
			sl.add(new Statement(7000,1, "for (var i = 0;i<rows.length;i++){"));
			sl.add(new Statement(8000,1, this.slave.getLowerFirstDomainName()+"Ids[i] = rows[i][\""+this.slave.getDomainId().getLowerFirstFieldName()+"\"]; "));
			sl.add(new Statement(9000,1, "}"));
			sl.add(new Statement(10000,1, "var "+this.master.getLowerFirstDomainName()+"Id = $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"getSelected\")."+this.master.getDomainId().getLowerFirstFieldName()+";"));
			sl.add(new Statement(11000,1, "$.ajax({"));
			sl.add(new Statement(12000,1, "type: \"post\","));
			sl.add(new Statement(13000,1, "url: \"../"+this.master.getControllerPackagePrefix()+this.master.getLowerFirstDomainName()+this.master.getControllerNamingSuffix()+"/"+StringUtil.lowerFirst(this.getVerbName())+"\","));
			sl.add(new Statement(14000,1, "data: {"));
			sl.add(new Statement(15000,1, this.slave.getLowerFirstDomainName()+"Ids:"+this.slave.getLowerFirstDomainName()+"Ids.join(\",\"),"));
			sl.add(new Statement(16000,1, this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id,"));
			sl.add(new Statement(17000,1, "},"));
			sl.add(new Statement(18000,1, "dataType: 'json',"));
			sl.add(new Statement(19000,1, "success: function(data, textStatus) {"));
			sl.add(new Statement(20000,1, "if (data.success) $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"load\");"));
			sl.add(new Statement(21000,1, "},"));
			sl.add(new Statement(22000,1, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(23000,1, "},"));
			sl.add(new Statement(24000,1, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(25000,1, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(26000,1, "alert(errorThrown.toString());"));
			sl.add(new Statement(27000,1, "}"));
			sl.add(new Statement(28000,1, "});"));
		} else {
			sl.add(new Statement(1000,1, "var "+StringUtil.lowerFirst(this.slave.getAlias())+"Ids = new Array();"));
			sl.add(new Statement(2000,1, "var rows = $(\"#my"+StringUtil.capFirst(this.slave.getAliasPlural())+"\").datalist(\"getSelections\");"));
			sl.add(new Statement(3000,1, "if (isBlank(rows)) {"));
			if ("english".equalsIgnoreCase(this.master.getLanguage())) {
				sl.add(new Statement(4000,1, "$.messager.alert(\"Warning\", \"Please select current "+this.slave.getAliasText()+"！\");"));
			}else {
				sl.add(new Statement(4000,1, "$.messager.alert(\"操作提示\", \"请选择现有"+this.slave.getAliasText()+"！\");"));
			}
			sl.add(new Statement(5000,1, "return;"));
			sl.add(new Statement(6000,1, "}"));
			sl.add(new Statement(7000,1, "for (var i = 0;i<rows.length;i++){"));
			sl.add(new Statement(8000,1, StringUtil.lowerFirst(this.slave.getAlias())+"Ids[i] = rows[i][\""+this.slave.getDomainId().getLowerFirstFieldName()+"\"]; "));
			sl.add(new Statement(9000,1, "}"));
			sl.add(new Statement(10000,1, "var "+this.master.getLowerFirstDomainName()+"Id = $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"getSelected\")."+this.master.getDomainId().getLowerFirstFieldName()+";"));
			sl.add(new Statement(11000,1, "$.ajax({"));
			sl.add(new Statement(12000,1, "type: \"post\","));
			sl.add(new Statement(13000,1, "url: \"../"+this.master.getControllerPackagePrefix()+this.master.getLowerFirstDomainName()+this.master.getControllerNamingSuffix()+"/"+StringUtil.lowerFirst(this.getVerbName())+"\","));
			sl.add(new Statement(14000,1, "data: {"));
			sl.add(new Statement(15000,1, StringUtil.lowerFirst(this.slave.getAlias())+"Ids:"+StringUtil.lowerFirst(this.slave.getAlias())+"Ids.join(\",\"),"));
			sl.add(new Statement(16000,1, this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id,"));
			sl.add(new Statement(17000,1, "},"));
			sl.add(new Statement(18000,1, "dataType: 'json',"));
			sl.add(new Statement(19000,1, "success: function(data, textStatus) {"));
			sl.add(new Statement(20000,1, "if (data.success) $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"load\");"));
			sl.add(new Statement(21000,1, "},"));
			sl.add(new Statement(22000,1, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(23000,1, "},"));
			sl.add(new Statement(24000,1, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(25000,1, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(26000,1, "alert(errorThrown.toString());"));
			sl.add(new Statement(27000,1, "}"));
			sl.add(new Statement(28000,1, "});"));
		}
		
		method.setMethodStatementList(sl);
		return method;	
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodStringWithSerial();
	}

}
