package org.light.complexverb;

import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.Method;
import org.light.easyui.EasyUIPositions;

public abstract class DomainFieldVerb implements Comparable<DomainFieldVerb>,EasyUIPositions{
	protected Domain domain;
	protected Field field;
	protected String label;
	protected String verbName;
	protected boolean denied = false;
	
	public abstract Method generateDaoImplMethod() throws Exception;
	public abstract String generateDaoImplMethodString() throws Exception;
	public abstract String generateDaoImplMethodStringWithSerial() throws Exception;
	public abstract Method generateDaoMethodDefinition() throws Exception;
	public abstract String generateDaoMethodDefinitionString() throws Exception;
	public abstract Method generateServiceMethodDefinition() throws Exception;
	public abstract String generateServiceMethodDefinitionString() throws Exception;
	public abstract Method generateServiceImplMethod() throws Exception;
	public abstract String generateServiceImplMethodString() throws Exception;
	public abstract String generateServiceImplMethodStringWithSerial() throws Exception;
	public abstract Method generateControllerMethod() throws Exception;
	public abstract String generateControllerMethodString() throws Exception;
	public abstract String generateControllerMethodStringWithSerial() throws Exception;

	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.verbName;
	}
	public String getVerbName() {
		return verbName;
	}
	public void setVerbName(String verbName) {
		this.verbName = verbName;
	}
	
	@Override
	public int compareTo(DomainFieldVerb o) {
		return this.getVerbName().compareTo(o.getVerbName());
	}
	
	public boolean isDenied() {
		return denied;
	}
	public void setDenied(boolean denied) {
		this.denied = denied;
	}
}
