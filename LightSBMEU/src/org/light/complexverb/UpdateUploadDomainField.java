package org.light.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.DragonHideStatement;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.generator.NamedStatementGenerator;
import org.light.generator.NamedStatementListGenerator;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class UpdateUploadDomainField extends DomainFieldVerb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		return null;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(getVerbName()));
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addSignature(new Signature(1, "request", new Type("MultipartHttpServletRequest","")));
		method.addAdditionalImport("org.springframework.web.multipart.MultipartFile");
		method.addAdditionalImport("org.springframework.web.multipart.MultipartHttpServletRequest");
		method.addAdditionalImport("org.springframework.web.bind.annotation.ResponseBody");
		method.addAdditionalImport("java.util.Iterator");
		method.addMetaData("RequestMapping(value = \"/" + StringUtil.lowerFirst(method.getStandardName())
		+ "\", method = RequestMethod.POST)");
		method.addMetaData("ResponseBody");
		
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,2,"Map<String, Object> result = new TreeMap<String, Object>();"));
		sList.add(new Statement(2000L,2,"Iterator iter = request.getFileNames();"));
		sList.add(new Statement(3000L,2,"byte[] imageBytes = null;"));
		sList.add(new Statement(4000L,0,""));
		sList.add(new Statement(5000L,2,"while (iter.hasNext()) {"));
		sList.add(new Statement(6000L,3,"MultipartFile file = request.getFile(iter.next().toString());"));
		sList.add(new Statement(7000L,0,""));
		sList.add(new Statement(8000L,3,"if (file != null) {"));
		sList.add(new Statement(9000L,4,"imageBytes = file.getBytes();"));
		sList.add(new Statement(10000L,4,"if (temp"+this.domain.getCapFirstDomainName()+" == null) temp"+this.domain.getCapFirstDomainName()+" = new "+this.domain.getStandardNameWithSuffix()+"();"));
		sList.add(new Statement(11000L,4,"temp"+this.domain.getCapFirstDomainName()+".set"+this.field.getCapFirstFieldName()+"(imageBytes);"));
		sList.add(new Statement(13000L,4,"break;"));
		sList.add(new Statement(14000L,3,"}"));
		sList.add(new Statement(15000L,2,"}"));
		sList.add(new Statement(16000L,2,"if (imageBytes != null) {"));
		sList.add(new Statement(17000L,3,"result.put(\"success\", true);"));
		sList.add(new Statement(18000L,3,"result.put(\"data\", imageBytes);"));
		sList.add(new Statement(19000L,2,"} else {"));
		sList.add(new Statement(20000L,3,"result.put(\"success\", false);"));
		sList.add(new Statement(21000L,3,"result.put(\"data\", null);"));
		sList.add(new Statement(22000L,2,"}"));
		sList.add(new Statement(23000L,2,"return result;"));

		method.setMethodStatementList(WriteableUtil.merge(sList));
		
		return method;
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		Method m = this.generateControllerMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		Method m = this.generateControllerMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
		
	public UpdateUploadDomainField(Domain domain,Field field){
		super();
		this.domain = domain;
		this.field = field;
		this.verbName = "UpdateUpload"+this.domain.getStandardName()+this.field.getCapFirstFieldName();
		this.setLabel("更新上传");
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		JavascriptBlock block = new JavascriptBlock();
		block.setStandardName("upload"+StringUtil.capFirst(this.domain.getStandardName())+this.field.getCapFirstFieldName());
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"$('#"+this.domain.getLowerFirstDomainName()+this.field.getCapFirstFieldName()+"Fileupload').fileupload({"));
		sList.add(new Statement(2000L,2,"autoUpload: true,"));
		sList.add(new Statement(3000L,2,"dataType: 'json',"));
		sList.add(new Statement(4000L,2,"async: false,"));
		sList.add(new Statement(5000L,2,"formData : function() {"));
		sList.add(new Statement(6000L,3,"return [{name:\""+this.domain.getDomainId().getLowerFirstFieldName()+"\",value:$(\"#ffedit\").find(\"#"+this.domain.getDomainId().getLowerFirstFieldName()+"\").val()}"));
		sList.add(new Statement(7000L,4,"];"));
		sList.add(new Statement(8000L,2,"},"));
		sList.add(new Statement(9000L,2,"success: function(data, textStatus) {"));
		sList.add(new Statement(10000L,2,"if (data.success == true){"));
		sList.add(new Statement(11000L,3,"$(\"#ffedit\").find(\"#"+this.field.getLowerFirstFieldName()+"\").prop(\"src\",\"data:image/png;base64,\"+data.data)"));
		sList.add(new Statement(12000L,2,"}"));
		sList.add(new Statement(13000L,2,"},"));
		sList.add(new Statement(14000L,1,"progressall: function (e, data) {"));
		sList.add(new Statement(15000L,1,"var progress = parseInt(data.loaded / data.total * 100, 10);"));
		sList.add(new Statement(16000L,1,"},"));
		sList.add(new Statement(17000L,0,"});"));
		block.setMethodStatementList(WriteableUtil.merge(sList));
		return block;

	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("show"+this.domain.getCapFirstDomainName()+this.field.getCapFirstFieldName()+"Image");
		
		Signature s1 = new Signature(1,"value","var");
		Signature s2 = new Signature(2,"row","var");
		Signature s3 = new Signature(2,"index","var");
		method.addSignature(s1);
		method.addSignature(s2);
		method.addSignature(s3);
		
		StatementList sList = new StatementList();
		sList.add(new Statement(1000L,1,"if(row."+this.field.getLowerFirstFieldName()+"){"));
		sList.add(new Statement(2000L,2,"return \"<img style='height:50px;' border='1' src='data:image/png;base64,\"+row."+this.field.getLowerFirstFieldName()+"+\"'/>\";"));
		sList.add(new Statement(3000L,1,"} else {"));
		sList.add(new Statement(4000L,2,"return \"<img style='height:50px;' border='1' src='../css/images/blank.jpg'/>\";"));
		sList.add(new Statement(5000L,1,"}"));
		
		method.setMethodStatementList(sList);
		return method;		
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodStringWithSerial();
	}

}
