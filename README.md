# 第三代动词算子式代码生成器：光SBMEU版

### 版本与简介
本代码生成器最新版本是　Java通用代码生成器光2.3.0　文明版本尝鲜版。

此版本新增ShiroAuth弹性登录模块，使用Apache Shiro权限框架。新增三种复杂版面。包括父子表，树表和树父子表。新增三种报表。使用Echarts报表框架。包括报表，带数据网格的报表和计划与执行对比报表，带双数据网格。显著增强编译错与编译警告功能，增强更准确的错误信息和域对象簿记检查功能。请在本站附件处下载二进制发行版。

其中ShiroAuth模块。使用Apache Shiro权限框架。本弹性登录模块具有强大的变形能力。您可以指定User,Role,Privilege的具体对象。系统会严格校验，并生成相应的Shiro登录模块。完全无需人工编程。注意，Privilege对象的数据由系统生成，您无需配置。Role会自动增加admin和user两个Role。admin和user都自动关联所有权限。但是admin可以访问User,Role,Privilege三个对象，而user不行。系统会在User表中新增admin和jerry两个用户。其中amdin的角色是admin。jerry的角色是user。用户的密码您可以以明文设置。系统自动把密码转化为密文。若您未设置。amdin的密码为admin。而jerry的密码为jerry。

新功能截图：
登录
![Image description](https://images.gitee.com/uploads/images/2021/0901/214658_b2ff9e14_1203742.png "civ_login.png")

错误
![Image description](https://images.gitee.com/uploads/images/2021/0901/214716_99dcf14e_1203742.png "civ_err.png")

登录后
![Image description](https://images.gitee.com/uploads/images/2021/0901/214745_da0f3efd_1203742.png "civ_logged.png")

新功能Excel模板页签
![Image description](https://images.gitee.com/uploads/images/2021/0901/214801_d0c18787_1203742.png "civ_sheet.png")

新功能，复杂版面，树表

![Image description](https://images.gitee.com/uploads/images/2021/0901/215201_8176eaba_1203742.png "civ_treegrid.png")

新功能，报表

![Image description](https://images.gitee.com/uploads/images/2021/0901/215244_a7559fbc_1203742.png "civ_report.png")

新功能B站介绍视频：
[https://www.bilibili.com/video/BV1xf4y1J7FX/](https://www.bilibili.com/video/BV1xf4y1J7FX/)

介绍视频二：

[https://www.bilibili.com/video/BV1VM4y1G7Qk/](https://www.bilibili.com/video/BV1VM4y1G7Qk/)

更多视频：
[https://www.bilibili.com/video/BV1BQ4y1C7YR/](https://www.bilibili.com/video/BV1BQ4y1C7YR/)

[https://www.bilibili.com/video/BV1Uq4y1N7hm/](https://www.bilibili.com/video/BV1Uq4y1N7hm/)

#### 架构变化
从光2.3.0 文明尝鲜版２开始，光使用Maven管理jar依赖，方便您从源码构建代码生成器。同时开始支持Tomcat9。

#### 智慧之光的版本变化

Java通用代码生成器光2.2.0 智慧版本Beta7版，改进编译检查，模板向导功能自动匹配前端设置。前端支持图片功能，前端增加Excel,PDF,Word数据导出格式，前端增强基地址配置特性。使用时空之门前端代码生成器4.6.0 Beta2的生成引擎。

目前智慧之光Beta7支持sbmeu,smeu和msmeu三种技术栈。支持MariaDB,MySQL,Oracle,PostgreSQL四种数据库。一键支持图片类型，EasyUI升级至最新，支持导出Excel,PDF,Ｗord三种格式数据。智慧之光Beta7版更新了部分理论文档。

Java通用代码生成器光2.2.0 智慧版本Beta6版，前端支持图片功能，前端增加Excel,PDF,Word数据导出格式，前端增强基地址配置特性。使用时空之门前端代码生成器4.6.0 Beta2的生成引擎。

光2.2.0 智慧Beta５版，修复更多缺陷，改进编译错与编译警告，更多语法检查，更多测试，改进模板向导。Beta5版增加了对元字段和字段类型的语法检查。

光2.2.0 智慧Beta５版的模板向导界面，是现在光的主力开发界面，有机融合了Excel模板和界面操作的功能，可以有多种使用方法，并且可以将操作结果导出成Excel模板，欢迎使用，老用户可以发现它相对于原有的Excel生产界面的巨大改进。

光2.2.0 智慧Beta５版，支持动词否定向导。可以通过界面操作，生成域对象的动词否定字符串。以对域对象的功能进行裁减。动词否定功能群是光的三大变形功能群之一，可以生成灵活，动态，可裁减的代码生产物。您值得一试。

光2.2.0 智慧Beta５版，的模板向导经过了改进。可以更多使用下拉列表选择字段类型，方便了使用。

光2.2.0 智慧Beta５版，支持sbmeu,smeu,msmeu三种技术栈。支持图片类型，支持四种数据库。即MariaDB,MySQL,Oracle和PostgreSQL。EasyUI升级至最新，新支持导出Ｗord格式数据。一共支持Excel,PDF,Word三种数据格式。支持Nomal,DBTools两种模式。此款代码生成器一共有15种输出结果。欢迎使用。代码生成器war包请部署在Tomcat8.5 webapps目录下。


### 智慧之光介绍视频地址

智慧之光Beta8版介绍视频：
[https://www.bilibili.com/video/BV1jy4y1M7Xw/](https://www.bilibili.com/video/BV1jy4y1M7Xw/)

智慧之光Beta7版介绍视频：[https://www.bilibili.com/video/BV1H64y1k7ks/](https://www.bilibili.com/video/BV1H64y1k7ks/)

智慧之光Beta5版已发布，是一个错误修复版本。
[https://www.bilibili.com/video/BV1z64y117cN/](https://www.bilibili.com/video/BV1z64y117cN/)


项目图片：

![Image description](https://images.gitee.com/uploads/images/2021/0510/230007_79a01c9c_1203742.jpeg "light.jpg")
 

支持PostgreSQL数据库。从此，光正式支持MariaDB,Oracle,PostgreSQL三种数据库。 同时，尝鲜版六改进了字段排序，和您输入的顺序一致，并有其他一些错误修正。

持图片类型，您只需把字段定义成image类型，即可一键支持图片类型。

已完成的图片支持：

![Image description](https://images.gitee.com/uploads/images/2021/0510/230505_14bbfc72_1203742.png "apic2.png")

前端图片功能截图：
![Image description](https://images.gitee.com/uploads/images/2021/0620/190230_6f2b2e8d_1203742.png "frontimage0.png")

![Image description](https://images.gitee.com/uploads/images/2021/0620/190254_261d3c9c_1203742.png "frontimage1.png")

前端PDF导出截图：
![Image description](https://images.gitee.com/uploads/images/2021/0620/190423_637d9240_1203742.png "export_pdf.png")

前端Word导出截图：
![Image description](https://images.gitee.com/uploads/images/2021/0620/190358_26566167_1203742.png "exportPDF.png")


支持Word格式数据导出。现在，智慧之光支持Excel,PDF,Word三种数据导出。

更新EasyUI至最新版1.9.12，更加精致，美观。

增加了图片功能相关示例。

恢复了对smeu技术栈的支持，兼容和平之翼代码生成器smeu版4.1.0宝船，并含有所有光的先进特性，支持图片类型，支持三种数据库。请和平之翼代码生成器的用户尽快迁徙到光。

支持msmeu技术栈，即有Maven支持的smeu技术栈，便携易用，压缩包文件小。

请去[https://gitee.com/jerryshensjf/LightSBMEU/attach_files](https://gitee.com/jerryshensjf/LightSBMEU/attach_files)　下载

### 使用前端图片功能的注意事项
由于图片文件比较大，原来前端使用cnpm instll安装类型，npm run dev运行有所改动，改为cnpm install类库， node --max-http-header-size=1000000 ./node_modules/.bin/webpack-dev-server --inline --progress --config build/webpack.dev.conf.js　
运行系统。



### 贡献者和他们的贡献

QQ好友100100101贡献了一个MySQL8的补丁包。

QQ好友天空之城贡献了模板向导界面的需求。

QQ好友Rain贡献了高级定制功能的需求。

QQ好友珈飞喵报告了Oracle数据库后端Long型数据类型的一个缺陷。

QQ好友soo乐报告了前端代码生成物基地址的一个缺陷。


### 交流QQ群
无垠式代码生成器群 277689737

### 作者电子邮件：jerry_shen_sjf@qq.com

### 官方配乐：邓紫棋《光年之外》